/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     PERCENT = 1,
     AMPERSAND = 2,
     ASTER = 3,
     CLUSTER = 4,
     COLON = 5,
     COMMA = 6,
     DASTER = 7,
     DEFINED_OPERATOR = 8,
     DOT = 9,
     DQUOTE = 10,
     GLOBAL_A = 11,
     LEFTAB = 12,
     LEFTPAR = 13,
     MINUS = 14,
     PLUS = 15,
     POINT_TO = 16,
     QUOTE = 17,
     RIGHTAB = 18,
     RIGHTPAR = 19,
     AND = 20,
     DSLASH = 21,
     EQV = 22,
     EQ = 23,
     EQUAL = 24,
     FFALSE = 25,
     GE = 26,
     GT = 27,
     LE = 28,
     LT = 29,
     NE = 30,
     NEQV = 31,
     NOT = 32,
     OR = 33,
     TTRUE = 34,
     SLASH = 35,
     XOR = 36,
     REFERENCE = 37,
     AT = 38,
     ACROSS = 39,
     ALIGN_WITH = 40,
     ALIGN = 41,
     ALLOCATABLE = 42,
     ALLOCATE = 43,
     ARITHIF = 44,
     ASSIGNMENT = 45,
     ASSIGN = 46,
     ASSIGNGOTO = 47,
     ASYNCHRONOUS = 48,
     ASYNCID = 49,
     ASYNCWAIT = 50,
     BACKSPACE = 51,
     BAD_CCONST = 52,
     BAD_SYMBOL = 53,
     BARRIER = 54,
     BLOCKDATA = 55,
     BLOCK = 56,
     BOZ_CONSTANT = 57,
     BYTE = 58,
     CALL = 59,
     CASE = 60,
     CHARACTER = 61,
     CHAR_CONSTANT = 62,
     CHECK = 63,
     CLOSE = 64,
     COMMON = 65,
     COMPLEX = 66,
     COMPGOTO = 67,
     CONSISTENT_GROUP = 68,
     CONSISTENT_SPEC = 69,
     CONSISTENT_START = 70,
     CONSISTENT_WAIT = 71,
     CONSISTENT = 72,
     CONSTRUCT_ID = 73,
     CONTAINS = 74,
     CONTINUE = 75,
     CORNER = 76,
     CYCLE = 77,
     DATA = 78,
     DEALLOCATE = 79,
     HPF_TEMPLATE = 80,
     DEBUG = 81,
     DEFAULT_CASE = 82,
     DEFINE = 83,
     DERIVED = 84,
     DIMENSION = 85,
     DISTRIBUTE = 86,
     DOWHILE = 87,
     DOUBLEPRECISION = 88,
     DOUBLECOMPLEX = 89,
     DP_CONSTANT = 90,
     DVM_POINTER = 91,
     DYNAMIC = 92,
     ELEMENTAL = 93,
     ELSE = 94,
     ELSEIF = 95,
     ELSEWHERE = 96,
     ENDASYNCHRONOUS = 97,
     ENDDEBUG = 98,
     ENDINTERVAL = 99,
     ENDUNIT = 100,
     ENDDO = 101,
     ENDFILE = 102,
     ENDFORALL = 103,
     ENDIF = 104,
     ENDINTERFACE = 105,
     ENDMODULE = 106,
     ENDON = 107,
     ENDSELECT = 108,
     ENDTASK_REGION = 109,
     ENDTYPE = 110,
     ENDWHERE = 111,
     ENTRY = 112,
     EXIT = 113,
     EOLN = 114,
     EQUIVALENCE = 115,
     ERROR = 116,
     EXTERNAL = 117,
     F90 = 118,
     FIND = 119,
     FORALL = 120,
     FORMAT = 121,
     FUNCTION = 122,
     GATE = 123,
     GEN_BLOCK = 124,
     HEAP = 125,
     HIGH = 126,
     IDENTIFIER = 127,
     IMPLICIT = 128,
     IMPLICITNONE = 129,
     INCLUDE_TO = 130,
     INCLUDE = 131,
     INDEPENDENT = 132,
     INDIRECT_ACCESS = 133,
     INDIRECT_GROUP = 134,
     INDIRECT = 135,
     INHERIT = 136,
     INQUIRE = 137,
     INTERFACEASSIGNMENT = 138,
     INTERFACEOPERATOR = 139,
     INTERFACE = 140,
     INTRINSIC = 141,
     INTEGER = 142,
     INTENT = 143,
     INTERVAL = 144,
     INOUT = 145,
     IN = 146,
     INT_CONSTANT = 147,
     LABEL = 148,
     LABEL_DECLARE = 149,
     LET = 150,
     LOCALIZE = 151,
     LOGICAL = 152,
     LOGICALIF = 153,
     LOOP = 154,
     LOW = 155,
     MAXLOC = 156,
     MAX = 157,
     MAP = 158,
     MINLOC = 159,
     MIN = 160,
     MODULE_PROCEDURE = 161,
     MODULE = 162,
     MULT_BLOCK = 163,
     NAMEEQ = 164,
     NAMELIST = 165,
     NEW_VALUE = 166,
     NEW = 167,
     NULLIFY = 168,
     OCTAL_CONSTANT = 169,
     ONLY = 170,
     ON = 171,
     ON_DIR = 172,
     ONTO = 173,
     OPEN = 174,
     OPERATOR = 175,
     OPTIONAL = 176,
     OTHERWISE = 177,
     OUT = 178,
     OWN = 179,
     PARALLEL = 180,
     PARAMETER = 181,
     PAUSE = 182,
     PLAINDO = 183,
     PLAINGOTO = 184,
     POINTER = 185,
     POINTERLET = 186,
     PREFETCH = 187,
     PRINT = 188,
     PRIVATE = 189,
     PRODUCT = 190,
     PROGRAM = 191,
     PUBLIC = 192,
     PURE = 193,
     RANGE = 194,
     READ = 195,
     REALIGN_WITH = 196,
     REALIGN = 197,
     REAL = 198,
     REAL_CONSTANT = 199,
     RECURSIVE = 200,
     REDISTRIBUTE_NEW = 201,
     REDISTRIBUTE = 202,
     REDUCTION_GROUP = 203,
     REDUCTION_START = 204,
     REDUCTION_WAIT = 205,
     REDUCTION = 206,
     REMOTE_ACCESS_SPEC = 207,
     REMOTE_ACCESS = 208,
     REMOTE_GROUP = 209,
     RESET = 210,
     RESULT = 211,
     RETURN = 212,
     REWIND = 213,
     SAVE = 214,
     SECTION = 215,
     SELECT = 216,
     SEQUENCE = 217,
     SHADOW_ADD = 218,
     SHADOW_COMPUTE = 219,
     SHADOW_GROUP = 220,
     SHADOW_RENEW = 221,
     SHADOW_START_SPEC = 222,
     SHADOW_START = 223,
     SHADOW_WAIT_SPEC = 224,
     SHADOW_WAIT = 225,
     SHADOW = 226,
     STAGE = 227,
     STATIC = 228,
     STAT = 229,
     STOP = 230,
     SUBROUTINE = 231,
     SUM = 232,
     SYNC = 233,
     TARGET = 234,
     TASK = 235,
     TASK_REGION = 236,
     THEN = 237,
     TO = 238,
     TRACEON = 239,
     TRACEOFF = 240,
     TRUNC = 241,
     TYPE = 242,
     TYPE_DECL = 243,
     UNDER = 244,
     UNKNOWN = 245,
     USE = 246,
     VIRTUAL = 247,
     VARIABLE = 248,
     WAIT = 249,
     WHERE = 250,
     WHERE_ASSIGN = 251,
     WHILE = 252,
     WITH = 253,
     WRITE = 254,
     COMMENT = 255,
     WGT_BLOCK = 256,
     HPF_PROCESSORS = 257,
     IOSTAT = 258,
     ERR = 259,
     END = 260,
     OMPDVM_ATOMIC = 261,
     OMPDVM_BARRIER = 262,
     OMPDVM_COPYIN = 263,
     OMPDVM_COPYPRIVATE = 264,
     OMPDVM_CRITICAL = 265,
     OMPDVM_ONETHREAD = 266,
     OMPDVM_DO = 267,
     OMPDVM_DYNAMIC = 268,
     OMPDVM_ENDCRITICAL = 269,
     OMPDVM_ENDDO = 270,
     OMPDVM_ENDMASTER = 271,
     OMPDVM_ENDORDERED = 272,
     OMPDVM_ENDPARALLEL = 273,
     OMPDVM_ENDPARALLELDO = 274,
     OMPDVM_ENDPARALLELSECTIONS = 275,
     OMPDVM_ENDPARALLELWORKSHARE = 276,
     OMPDVM_ENDSECTIONS = 277,
     OMPDVM_ENDSINGLE = 278,
     OMPDVM_ENDWORKSHARE = 279,
     OMPDVM_FIRSTPRIVATE = 280,
     OMPDVM_FLUSH = 281,
     OMPDVM_GUIDED = 282,
     OMPDVM_LASTPRIVATE = 283,
     OMPDVM_MASTER = 284,
     OMPDVM_NOWAIT = 285,
     OMPDVM_NONE = 286,
     OMPDVM_NUM_THREADS = 287,
     OMPDVM_ORDERED = 288,
     OMPDVM_PARALLEL = 289,
     OMPDVM_PARALLELDO = 290,
     OMPDVM_PARALLELSECTIONS = 291,
     OMPDVM_PARALLELWORKSHARE = 292,
     OMPDVM_RUNTIME = 293,
     OMPDVM_SECTION = 294,
     OMPDVM_SECTIONS = 295,
     OMPDVM_SCHEDULE = 296,
     OMPDVM_SHARED = 297,
     OMPDVM_SINGLE = 298,
     OMPDVM_THREADPRIVATE = 299,
     OMPDVM_WORKSHARE = 300,
     OMPDVM_NODES = 301,
     OMPDVM_IF = 302,
     IAND = 303,
     IEOR = 304,
     IOR = 305,
     ACC_REGION = 306,
     ACC_END_REGION = 307,
     ACC_CHECKSECTION = 308,
     ACC_END_CHECKSECTION = 309,
     ACC_GET_ACTUAL = 310,
     ACC_ACTUAL = 311,
     ACC_TARGETS = 312,
     ACC_ASYNC = 313,
     ACC_HOST = 314,
     ACC_CUDA = 315,
     ACC_LOCAL = 316,
     ACC_INLOCAL = 317,
     ACC_CUDA_BLOCK = 318,
     ACC_ROUTINE = 319,
     ACC_TIE = 320,
     BY = 321,
     IO_MODE = 322,
     CP_CREATE = 323,
     CP_LOAD = 324,
     CP_SAVE = 325,
     CP_WAIT = 326,
     FILES = 327,
     VARLIST = 328,
     STATUS = 329,
     EXITINTERVAL = 330,
     TEMPLATE_CREATE = 331,
     TEMPLATE_DELETE = 332,
     SPF_ANALYSIS = 333,
     SPF_PARALLEL = 334,
     SPF_TRANSFORM = 335,
     SPF_NOINLINE = 336,
     SPF_PARALLEL_REG = 337,
     SPF_END_PARALLEL_REG = 338,
     SPF_EXPAND = 339,
     SPF_FISSION = 340,
     SPF_SHRINK = 341,
     SPF_CHECKPOINT = 342,
     SPF_EXCEPT = 343,
     SPF_FILES_COUNT = 344,
     SPF_INTERVAL = 345,
     SPF_TIME = 346,
     SPF_ITER = 347,
     SPF_FLEXIBLE = 348,
     SPF_APPLY_REGION = 349,
     SPF_APPLY_FRAGMENT = 350,
     SPF_CODE_COVERAGE = 351,
     BINARY_OP = 354,
     UNARY_OP = 355
   };
#endif
/* Tokens.  */
#define PERCENT 1
#define AMPERSAND 2
#define ASTER 3
#define CLUSTER 4
#define COLON 5
#define COMMA 6
#define DASTER 7
#define DEFINED_OPERATOR 8
#define DOT 9
#define DQUOTE 10
#define GLOBAL_A 11
#define LEFTAB 12
#define LEFTPAR 13
#define MINUS 14
#define PLUS 15
#define POINT_TO 16
#define QUOTE 17
#define RIGHTAB 18
#define RIGHTPAR 19
#define AND 20
#define DSLASH 21
#define EQV 22
#define EQ 23
#define EQUAL 24
#define FFALSE 25
#define GE 26
#define GT 27
#define LE 28
#define LT 29
#define NE 30
#define NEQV 31
#define NOT 32
#define OR 33
#define TTRUE 34
#define SLASH 35
#define XOR 36
#define REFERENCE 37
#define AT 38
#define ACROSS 39
#define ALIGN_WITH 40
#define ALIGN 41
#define ALLOCATABLE 42
#define ALLOCATE 43
#define ARITHIF 44
#define ASSIGNMENT 45
#define ASSIGN 46
#define ASSIGNGOTO 47
#define ASYNCHRONOUS 48
#define ASYNCID 49
#define ASYNCWAIT 50
#define BACKSPACE 51
#define BAD_CCONST 52
#define BAD_SYMBOL 53
#define BARRIER 54
#define BLOCKDATA 55
#define BLOCK 56
#define BOZ_CONSTANT 57
#define BYTE 58
#define CALL 59
#define CASE 60
#define CHARACTER 61
#define CHAR_CONSTANT 62
#define CHECK 63
#define CLOSE 64
#define COMMON 65
#define COMPLEX 66
#define COMPGOTO 67
#define CONSISTENT_GROUP 68
#define CONSISTENT_SPEC 69
#define CONSISTENT_START 70
#define CONSISTENT_WAIT 71
#define CONSISTENT 72
#define CONSTRUCT_ID 73
#define CONTAINS 74
#define CONTINUE 75
#define CORNER 76
#define CYCLE 77
#define DATA 78
#define DEALLOCATE 79
#define HPF_TEMPLATE 80
#define DEBUG 81
#define DEFAULT_CASE 82
#define DEFINE 83
#define DERIVED 84
#define DIMENSION 85
#define DISTRIBUTE 86
#define DOWHILE 87
#define DOUBLEPRECISION 88
#define DOUBLECOMPLEX 89
#define DP_CONSTANT 90
#define DVM_POINTER 91
#define DYNAMIC 92
#define ELEMENTAL 93
#define ELSE 94
#define ELSEIF 95
#define ELSEWHERE 96
#define ENDASYNCHRONOUS 97
#define ENDDEBUG 98
#define ENDINTERVAL 99
#define ENDUNIT 100
#define ENDDO 101
#define ENDFILE 102
#define ENDFORALL 103
#define ENDIF 104
#define ENDINTERFACE 105
#define ENDMODULE 106
#define ENDON 107
#define ENDSELECT 108
#define ENDTASK_REGION 109
#define ENDTYPE 110
#define ENDWHERE 111
#define ENTRY 112
#define EXIT 113
#define EOLN 114
#define EQUIVALENCE 115
#define ERROR 116
#define EXTERNAL 117
#define F90 118
#define FIND 119
#define FORALL 120
#define FORMAT 121
#define FUNCTION 122
#define GATE 123
#define GEN_BLOCK 124
#define HEAP 125
#define HIGH 126
#define IDENTIFIER 127
#define IMPLICIT 128
#define IMPLICITNONE 129
#define INCLUDE_TO 130
#define INCLUDE 131
#define INDEPENDENT 132
#define INDIRECT_ACCESS 133
#define INDIRECT_GROUP 134
#define INDIRECT 135
#define INHERIT 136
#define INQUIRE 137
#define INTERFACEASSIGNMENT 138
#define INTERFACEOPERATOR 139
#define INTERFACE 140
#define INTRINSIC 141
#define INTEGER 142
#define INTENT 143
#define INTERVAL 144
#define INOUT 145
#define IN 146
#define INT_CONSTANT 147
#define LABEL 148
#define LABEL_DECLARE 149
#define LET 150
#define LOCALIZE 151
#define LOGICAL 152
#define LOGICALIF 153
#define LOOP 154
#define LOW 155
#define MAXLOC 156
#define MAX 157
#define MAP 158
#define MINLOC 159
#define MIN 160
#define MODULE_PROCEDURE 161
#define MODULE 162
#define MULT_BLOCK 163
#define NAMEEQ 164
#define NAMELIST 165
#define NEW_VALUE 166
#define NEW 167
#define NULLIFY 168
#define OCTAL_CONSTANT 169
#define ONLY 170
#define ON 171
#define ON_DIR 172
#define ONTO 173
#define OPEN 174
#define OPERATOR 175
#define OPTIONAL 176
#define OTHERWISE 177
#define OUT 178
#define OWN 179
#define PARALLEL 180
#define PARAMETER 181
#define PAUSE 182
#define PLAINDO 183
#define PLAINGOTO 184
#define POINTER 185
#define POINTERLET 186
#define PREFETCH 187
#define PRINT 188
#define PRIVATE 189
#define PRODUCT 190
#define PROGRAM 191
#define PUBLIC 192
#define PURE 193
#define RANGE 194
#define READ 195
#define REALIGN_WITH 196
#define REALIGN 197
#define REAL 198
#define REAL_CONSTANT 199
#define RECURSIVE 200
#define REDISTRIBUTE_NEW 201
#define REDISTRIBUTE 202
#define REDUCTION_GROUP 203
#define REDUCTION_START 204
#define REDUCTION_WAIT 205
#define REDUCTION 206
#define REMOTE_ACCESS_SPEC 207
#define REMOTE_ACCESS 208
#define REMOTE_GROUP 209
#define RESET 210
#define RESULT 211
#define RETURN 212
#define REWIND 213
#define SAVE 214
#define SECTION 215
#define SELECT 216
#define SEQUENCE 217
#define SHADOW_ADD 218
#define SHADOW_COMPUTE 219
#define SHADOW_GROUP 220
#define SHADOW_RENEW 221
#define SHADOW_START_SPEC 222
#define SHADOW_START 223
#define SHADOW_WAIT_SPEC 224
#define SHADOW_WAIT 225
#define SHADOW 226
#define STAGE 227
#define STATIC 228
#define STAT 229
#define STOP 230
#define SUBROUTINE 231
#define SUM 232
#define SYNC 233
#define TARGET 234
#define TASK 235
#define TASK_REGION 236
#define THEN 237
#define TO 238
#define TRACEON 239
#define TRACEOFF 240
#define TRUNC 241
#define TYPE 242
#define TYPE_DECL 243
#define UNDER 244
#define UNKNOWN 245
#define USE 246
#define VIRTUAL 247
#define VARIABLE 248
#define WAIT 249
#define WHERE 250
#define WHERE_ASSIGN 251
#define WHILE 252
#define WITH 253
#define WRITE 254
#define COMMENT 255
#define WGT_BLOCK 256
#define HPF_PROCESSORS 257
#define IOSTAT 258
#define ERR 259
#define END 260
#define OMPDVM_ATOMIC 261
#define OMPDVM_BARRIER 262
#define OMPDVM_COPYIN 263
#define OMPDVM_COPYPRIVATE 264
#define OMPDVM_CRITICAL 265
#define OMPDVM_ONETHREAD 266
#define OMPDVM_DO 267
#define OMPDVM_DYNAMIC 268
#define OMPDVM_ENDCRITICAL 269
#define OMPDVM_ENDDO 270
#define OMPDVM_ENDMASTER 271
#define OMPDVM_ENDORDERED 272
#define OMPDVM_ENDPARALLEL 273
#define OMPDVM_ENDPARALLELDO 274
#define OMPDVM_ENDPARALLELSECTIONS 275
#define OMPDVM_ENDPARALLELWORKSHARE 276
#define OMPDVM_ENDSECTIONS 277
#define OMPDVM_ENDSINGLE 278
#define OMPDVM_ENDWORKSHARE 279
#define OMPDVM_FIRSTPRIVATE 280
#define OMPDVM_FLUSH 281
#define OMPDVM_GUIDED 282
#define OMPDVM_LASTPRIVATE 283
#define OMPDVM_MASTER 284
#define OMPDVM_NOWAIT 285
#define OMPDVM_NONE 286
#define OMPDVM_NUM_THREADS 287
#define OMPDVM_ORDERED 288
#define OMPDVM_PARALLEL 289
#define OMPDVM_PARALLELDO 290
#define OMPDVM_PARALLELSECTIONS 291
#define OMPDVM_PARALLELWORKSHARE 292
#define OMPDVM_RUNTIME 293
#define OMPDVM_SECTION 294
#define OMPDVM_SECTIONS 295
#define OMPDVM_SCHEDULE 296
#define OMPDVM_SHARED 297
#define OMPDVM_SINGLE 298
#define OMPDVM_THREADPRIVATE 299
#define OMPDVM_WORKSHARE 300
#define OMPDVM_NODES 301
#define OMPDVM_IF 302
#define IAND 303
#define IEOR 304
#define IOR 305
#define ACC_REGION 306
#define ACC_END_REGION 307
#define ACC_CHECKSECTION 308
#define ACC_END_CHECKSECTION 309
#define ACC_GET_ACTUAL 310
#define ACC_ACTUAL 311
#define ACC_TARGETS 312
#define ACC_ASYNC 313
#define ACC_HOST 314
#define ACC_CUDA 315
#define ACC_LOCAL 316
#define ACC_INLOCAL 317
#define ACC_CUDA_BLOCK 318
#define ACC_ROUTINE 319
#define ACC_TIE 320
#define BY 321
#define IO_MODE 322
#define CP_CREATE 323
#define CP_LOAD 324
#define CP_SAVE 325
#define CP_WAIT 326
#define FILES 327
#define VARLIST 328
#define STATUS 329
#define EXITINTERVAL 330
#define TEMPLATE_CREATE 331
#define TEMPLATE_DELETE 332
#define SPF_ANALYSIS 333
#define SPF_PARALLEL 334
#define SPF_TRANSFORM 335
#define SPF_NOINLINE 336
#define SPF_PARALLEL_REG 337
#define SPF_END_PARALLEL_REG 338
#define SPF_EXPAND 339
#define SPF_FISSION 340
#define SPF_SHRINK 341
#define SPF_CHECKPOINT 342
#define SPF_EXCEPT 343
#define SPF_FILES_COUNT 344
#define SPF_INTERVAL 345
#define SPF_TIME 346
#define SPF_ITER 347
#define SPF_FLEXIBLE 348
#define SPF_APPLY_REGION 349
#define SPF_APPLY_FRAGMENT 350
#define SPF_CODE_COVERAGE 351
#define BINARY_OP 354
#define UNARY_OP 355




/* Copy the first part of user declarations.  */
#line 353 "gram1.y"

#include <string.h>
#include "inc.h"
#include "extern.h"
#include "defines.h"
#include "fdvm.h"
#include "fm.h"

/* We may use builtin alloca */
#include "compatible.h"
#ifdef _NEEDALLOCAH_
#  include <alloca.h>
#endif

#define EXTEND_NODE 2  /* move the definition to h/ files. */

extern PTR_BFND global_bfnd, pred_bfnd;
extern PTR_SYMB star_symb;
extern PTR_SYMB global_list;
extern PTR_TYPE global_bool;
extern PTR_TYPE global_int;
extern PTR_TYPE global_float;
extern PTR_TYPE global_double;
extern PTR_TYPE global_char;
extern PTR_TYPE global_string;
extern PTR_TYPE global_string_2;
extern PTR_TYPE global_complex;
extern PTR_TYPE global_dcomplex;
extern PTR_TYPE global_gate;
extern PTR_TYPE global_event;
extern PTR_TYPE global_sequence;
extern PTR_TYPE global_default;
extern PTR_LABEL thislabel;
extern PTR_CMNT comments, cur_comment;
extern PTR_BFND last_bfnd;
extern PTR_TYPE impltype[];
extern int nioctl;
extern int maxdim;
extern long yystno;	/* statement label */
extern char stmtbuf[];	/* input buffer */
extern char *commentbuf;	/* comments buffer from scanner */
extern PTR_BLOB head_blob;
extern PTR_BLOB cur_blob;
extern PTR_TYPE vartype; /* variable type */
extern int end_group;
extern char saveall;
extern int privateall;
extern int needkwd;
extern int implkwd;
extern int opt_kwd_hedr;
/* added for FORTRAN 90 */
extern PTR_LLND first_unresolved_call;
extern PTR_LLND last_unresolved_call;
extern int data_stat;
extern char yyquote;

extern int warn_all;
extern int statement_kind; /* kind of statement: 1 - HPF-DVM-directive, 0 - Fortran statement*/ 
int extend_flag = 0;

static int do_name_err;
static int ndim;	/* number of dimension */
/*!!! hpf */
static int explicit_shape; /*  1 if shape specification is explicit */
/* static int varleng;*/	/* variable size */
static int lastwasbranch = NO;	/* set if last stmt was a branch stmt */
static int thiswasbranch = NO;	/* set if this stmt is a branch stmt */
static PTR_SYMB type_var = SMNULL;
static PTR_LLND stat_alloc = LLNULL; /* set if ALLOCATE/DEALLOCATE stmt has STAT-clause*/
/* static int subscripts_status = 0; */
static int type_options,type_opt;   /* The various options used to declare a name -
                                      RECURSIVE, POINTER, OPTIONAL etc.         */
static PTR_BFND module_scope;
static int position = IN_OUTSIDE;            
static int attr_ndim;           /* number of dimensions in DIMENSION (array_spec)
                                   attribute declaration */
static PTR_LLND attr_dims;     /* low level representation of array_spec in
                                   DIMENSION (array_spec) attribute declarartion. */
static int in_vec = NO;	      /* set if processing array constructor */


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 434 "gram1.y"
{
    int token;
    char charv;
    char *charp;
    PTR_BFND bf_node;
    PTR_LLND ll_node;
    PTR_SYMB symbol;
    PTR_TYPE data_type;
    PTR_HASH hash_entry;
    PTR_LABEL label;
}
/* Line 187 of yacc.c.  */
#line 899 "gram1.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */
#line 641 "gram1.y"

void add_scope_level();
void delete_beyond_scope_level();
PTR_HASH look_up_sym();
PTR_HASH just_look_up_sym();
PTR_HASH just_look_up_sym_in_scope();
PTR_HASH look_up_op();
PTR_SYMB make_constant();
PTR_SYMB make_scalar();
PTR_SYMB make_array();
PTR_SYMB make_pointer();
PTR_SYMB make_function();
PTR_SYMB make_external();
PTR_SYMB make_intrinsic();
PTR_SYMB make_procedure();
PTR_SYMB make_process();
PTR_SYMB make_program();
PTR_SYMB make_module();
PTR_SYMB make_common();
PTR_SYMB make_parallel_region();
PTR_SYMB make_derived_type();
PTR_SYMB make_local_entity();
PTR_SYMB make_global_entity();
PTR_TYPE make_type_node();
PTR_TYPE lookup_type(), make_type();
void     process_type();
void     process_interface();
void     bind();
void     late_bind_if_needed();
PTR_SYMB component();
PTR_SYMB lookup_type_symbol();
PTR_SYMB resolve_overloading();
PTR_BFND cur_scope();
PTR_BFND subroutine_call();
PTR_BFND process_call();
PTR_LLND deal_with_options();
PTR_LLND intrinsic_op_node();
PTR_LLND defined_op_node();
int is_substring_ref();
int is_array_section_ref();
PTR_LLND dim_expr(); 
PTR_BFND exit_stat();
PTR_BFND make_do();
PTR_BFND make_pardo();
PTR_BFND make_enddoall();
PTR_TYPE install_array(); 
PTR_SYMB install_entry(); 
void install_param_list();
PTR_LLND construct_entry_list();
void copy_sym_data();
PTR_LLND check_and_install();
PTR_HASH look_up();
PTR_BFND get_bfnd(); 
PTR_BLOB make_blob();
PTR_LABEL make_label();
PTR_LABEL make_label_node();
int is_interface_stat();
PTR_LLND make_llnd (); 
PTR_LLND make_llnd_label (); 
PTR_TYPE make_sa_type(); 
PTR_SYMB procedure_call();
PTR_BFND proc_list();
PTR_SYMB set_id_list();
PTR_LLND set_ll_list();
PTR_LLND add_to_lowLevelList(), add_to_lowList();
PTR_BFND set_stat_list() ;
PTR_BLOB follow_blob();
PTR_SYMB proc_decl_init();
PTR_CMNT make_comment();
PTR_HASH correct_symtab();
char *copyn();
char *convic();
char *StringConcatenation();
int atoi();
PTR_BFND make_logif();
PTR_BFND make_if();
PTR_BFND make_forall();
void startproc();
void match_parameters();
void make_else();
void make_elseif();
void make_endif();
void make_elsewhere();
void make_elsewhere_mask();
void make_endwhere();
void make_endforall();
void make_endselect();
void make_extend();
void make_endextend();
void make_section();
void make_section_extend();
void doinclude();
void endproc();
void err();
void execerr();
void flline();
void warn();
void warn1();
void newprog();
void set_type();
void dclerr();
void enddcl();
void install_const();
void setimpl();
void copy_module_scope();
void delete_symbol();
void replace_symbol_in_expr();
long convci();
void set_expr_type();
void errstr();
void yyerror();
void set_blobs();
void make_loop();
void startioctl();
void endioctl();
void redefine_func_arg_type();
int isResultVar();

/* used by FORTRAN M */
PTR_BFND make_processdo();
PTR_BFND make_processes();
PTR_BFND make_endprocesses();

PTR_BFND make_endparallel();/*OMP*/
PTR_BFND make_parallel();/*OMP*/
PTR_BFND make_endsingle();/*OMP*/
PTR_BFND make_single();/*OMP*/
PTR_BFND make_endmaster();/*OMP*/
PTR_BFND make_master();/*OMP*/
PTR_BFND make_endordered();/*OMP*/
PTR_BFND make_ordered();/*OMP*/
PTR_BFND make_endcritical();/*OMP*/
PTR_BFND make_critical();/*OMP*/
PTR_BFND make_endsections();/*OMP*/
PTR_BFND make_sections();/*OMP*/
PTR_BFND make_ompsection();/*OMP*/
PTR_BFND make_endparallelsections();/*OMP*/
PTR_BFND make_parallelsections();/*OMP*/
PTR_BFND make_endworkshare();/*OMP*/
PTR_BFND make_workshare();/*OMP*/
PTR_BFND make_endparallelworkshare();/*OMP*/
PTR_BFND make_parallelworkshare();/*OMP*/



/* Line 216 of yacc.c.  */
#line 1056 "gram1.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   5667

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  356
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  538
/* YYNRULES -- Number of rules.  */
#define YYNRULES  1294
/* YYNRULES -- Number of states.  */
#define YYNSTATES  2576

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   355

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint16 yytranslate[] =
{
       0,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,   207,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,   233,   234,   235,   236,   237,   238,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,   254,   255,   256,   257,   258,   259,   260,   261,
     262,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,   295,   296,   297,   298,   299,   300,   301,
     302,   303,   304,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,   316,   317,   318,   319,   320,   321,
     322,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,   338,   339,   340,   341,
     342,   343,   344,   345,   346,   347,   348,   349,   350,   351,
     352,   353,     1,     2,   354,   355
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     8,    12,    16,    20,    24,    27,
      29,    31,    33,    37,    41,    46,    52,    58,    62,    67,
      71,    72,    75,    78,    81,    83,    85,    90,    96,   101,
     107,   110,   116,   118,   119,   121,   122,   124,   125,   128,
     132,   134,   138,   140,   142,   144,   145,   146,   147,   149,
     151,   153,   155,   157,   159,   161,   163,   165,   167,   169,
     171,   173,   176,   181,   184,   190,   192,   194,   196,   198,
     200,   202,   204,   206,   208,   210,   212,   214,   217,   221,
     227,   233,   236,   238,   240,   242,   244,   246,   248,   250,
     252,   254,   256,   258,   260,   262,   264,   266,   268,   270,
     272,   274,   276,   278,   283,   291,   294,   298,   306,   313,
     314,   317,   323,   325,   330,   332,   334,   336,   339,   341,
     346,   348,   350,   352,   354,   356,   358,   361,   364,   367,
     369,   371,   379,   383,   388,   392,   397,   401,   404,   410,
     411,   414,   417,   423,   424,   429,   435,   436,   439,   443,
     445,   447,   449,   451,   453,   455,   457,   459,   461,   463,
     464,   466,   472,   479,   486,   487,   489,   495,   505,   507,
     509,   512,   515,   516,   517,   520,   523,   529,   534,   539,
     543,   548,   552,   557,   561,   565,   570,   576,   580,   585,
     591,   595,   599,   601,   605,   608,   613,   617,   622,   626,
     630,   634,   638,   642,   646,   648,   653,   655,   657,   662,
     666,   667,   668,   673,   675,   679,   681,   685,   688,   692,
     696,   701,   704,   705,   707,   709,   713,   719,   721,   725,
     726,   728,   736,   738,   742,   745,   748,   752,   754,   756,
     761,   765,   768,   770,   772,   774,   776,   780,   782,   786,
     788,   790,   797,   799,   801,   804,   807,   809,   813,   815,
     818,   821,   823,   827,   829,   833,   839,   841,   843,   845,
     848,   851,   855,   859,   861,   865,   869,   871,   875,   877,
     879,   883,   885,   889,   891,   893,   897,   903,   904,   905,
     907,   912,   917,   919,   923,   927,   930,   932,   936,   940,
     947,   954,   962,   964,   966,   970,   972,   974,   976,   980,
     984,   985,   989,   990,   993,   997,   999,  1001,  1004,  1008,
    1010,  1012,  1014,  1018,  1020,  1024,  1026,  1028,  1032,  1037,
    1038,  1041,  1044,  1046,  1048,  1052,  1054,  1058,  1060,  1061,
    1062,  1063,  1066,  1067,  1069,  1071,  1073,  1076,  1079,  1084,
    1086,  1090,  1092,  1096,  1098,  1100,  1102,  1104,  1108,  1112,
    1116,  1120,  1124,  1127,  1130,  1133,  1137,  1141,  1145,  1149,
    1153,  1157,  1161,  1165,  1169,  1173,  1177,  1180,  1184,  1188,
    1190,  1192,  1194,  1196,  1198,  1200,  1206,  1213,  1218,  1224,
    1228,  1230,  1232,  1238,  1243,  1246,  1247,  1249,  1255,  1256,
    1258,  1260,  1264,  1266,  1270,  1273,  1275,  1277,  1279,  1281,
    1283,  1285,  1289,  1293,  1299,  1301,  1303,  1307,  1310,  1316,
    1321,  1326,  1330,  1333,  1335,  1336,  1337,  1344,  1346,  1348,
    1350,  1355,  1361,  1363,  1368,  1374,  1375,  1377,  1381,  1383,
    1385,  1387,  1390,  1394,  1398,  1401,  1403,  1406,  1409,  1412,
    1416,  1424,  1428,  1432,  1434,  1437,  1440,  1442,  1445,  1449,
    1451,  1453,  1455,  1461,  1469,  1470,  1477,  1482,  1494,  1508,
    1513,  1517,  1521,  1529,  1538,  1542,  1544,  1547,  1550,  1554,
    1556,  1560,  1561,  1563,  1564,  1566,  1568,  1571,  1577,  1584,
    1586,  1590,  1594,  1595,  1598,  1600,  1606,  1614,  1615,  1617,
    1621,  1625,  1632,  1638,  1645,  1650,  1656,  1662,  1665,  1667,
    1669,  1680,  1682,  1686,  1691,  1695,  1699,  1703,  1707,  1714,
    1721,  1727,  1736,  1739,  1743,  1747,  1755,  1763,  1764,  1766,
    1771,  1774,  1779,  1781,  1784,  1787,  1789,  1791,  1792,  1793,
    1794,  1797,  1800,  1803,  1806,  1809,  1811,  1814,  1817,  1821,
    1826,  1829,  1833,  1835,  1839,  1843,  1845,  1847,  1849,  1853,
    1855,  1857,  1862,  1868,  1870,  1872,  1876,  1880,  1882,  1887,
    1889,  1891,  1893,  1896,  1899,  1902,  1904,  1908,  1912,  1917,
    1922,  1924,  1928,  1930,  1936,  1938,  1940,  1942,  1946,  1950,
    1954,  1958,  1962,  1966,  1968,  1972,  1978,  1984,  1990,  1991,
    1992,  1994,  1998,  2000,  2002,  2006,  2010,  2014,  2018,  2021,
    2025,  2029,  2030,  2032,  2034,  2036,  2038,  2040,  2042,  2044,
    2046,  2048,  2050,  2052,  2054,  2056,  2058,  2060,  2062,  2064,
    2066,  2068,  2070,  2072,  2074,  2076,  2078,  2080,  2082,  2084,
    2086,  2088,  2090,  2092,  2094,  2096,  2098,  2100,  2102,  2104,
    2106,  2108,  2110,  2112,  2114,  2116,  2118,  2120,  2122,  2124,
    2126,  2128,  2130,  2132,  2134,  2136,  2138,  2140,  2142,  2144,
    2146,  2148,  2150,  2152,  2154,  2156,  2158,  2162,  2166,  2169,
    2173,  2175,  2179,  2181,  2185,  2187,  2191,  2193,  2198,  2202,
    2204,  2208,  2210,  2214,  2219,  2221,  2226,  2231,  2236,  2240,
    2244,  2246,  2250,  2254,  2256,  2260,  2264,  2266,  2270,  2274,
    2276,  2280,  2281,  2287,  2294,  2303,  2305,  2309,  2311,  2313,
    2315,  2320,  2322,  2323,  2326,  2330,  2333,  2338,  2339,  2341,
    2347,  2352,  2359,  2364,  2366,  2371,  2376,  2378,  2385,  2387,
    2391,  2393,  2397,  2399,  2404,  2406,  2408,  2412,  2414,  2416,
    2420,  2422,  2423,  2425,  2428,  2432,  2434,  2437,  2443,  2448,
    2453,  2460,  2462,  2466,  2468,  2470,  2477,  2482,  2484,  2488,
    2490,  2492,  2494,  2496,  2498,  2502,  2504,  2506,  2508,  2515,
    2520,  2522,  2527,  2529,  2531,  2533,  2535,  2540,  2543,  2551,
    2553,  2558,  2560,  2562,  2574,  2575,  2578,  2582,  2584,  2588,
    2590,  2594,  2596,  2600,  2602,  2606,  2608,  2612,  2614,  2618,
    2627,  2629,  2633,  2636,  2639,  2647,  2649,  2653,  2657,  2659,
    2664,  2666,  2670,  2672,  2674,  2675,  2677,  2679,  2682,  2684,
    2686,  2688,  2690,  2692,  2694,  2696,  2698,  2700,  2702,  2704,
    2713,  2720,  2729,  2736,  2738,  2745,  2752,  2759,  2766,  2768,
    2772,  2778,  2780,  2784,  2791,  2793,  2797,  2806,  2813,  2820,
    2825,  2831,  2837,  2838,  2841,  2844,  2845,  2847,  2851,  2853,
    2858,  2866,  2868,  2872,  2876,  2878,  2882,  2888,  2892,  2896,
    2898,  2902,  2904,  2906,  2910,  2914,  2918,  2922,  2933,  2942,
    2953,  2954,  2955,  2957,  2960,  2965,  2970,  2977,  2979,  2981,
    2983,  2985,  2987,  2989,  2991,  2993,  2995,  2997,  2999,  3006,
    3011,  3016,  3020,  3030,  3032,  3034,  3038,  3040,  3046,  3052,
    3062,  3063,  3065,  3067,  3071,  3075,  3079,  3083,  3087,  3094,
    3098,  3102,  3106,  3110,  3118,  3124,  3126,  3128,  3132,  3137,
    3139,  3141,  3145,  3147,  3149,  3153,  3157,  3160,  3164,  3169,
    3174,  3180,  3186,  3188,  3191,  3196,  3201,  3206,  3207,  3209,
    3212,  3220,  3227,  3231,  3235,  3243,  3249,  3251,  3255,  3257,
    3262,  3265,  3269,  3273,  3278,  3285,  3289,  3292,  3296,  3298,
    3300,  3305,  3311,  3315,  3322,  3325,  3330,  3333,  3335,  3339,
    3343,  3344,  3346,  3350,  3353,  3356,  3359,  3362,  3372,  3378,
    3380,  3384,  3387,  3390,  3393,  3403,  3408,  3410,  3414,  3416,
    3418,  3421,  3422,  3430,  3432,  3437,  3439,  3443,  3445,  3447,
    3449,  3466,  3467,  3471,  3475,  3479,  3483,  3490,  3500,  3506,
    3508,  3512,  3518,  3520,  3522,  3524,  3526,  3528,  3530,  3532,
    3534,  3536,  3538,  3540,  3542,  3544,  3546,  3548,  3550,  3552,
    3554,  3556,  3558,  3560,  3562,  3564,  3566,  3568,  3570,  3572,
    3575,  3578,  3583,  3587,  3592,  3598,  3600,  3602,  3604,  3606,
    3608,  3610,  3612,  3614,  3616,  3622,  3625,  3628,  3631,  3634,
    3637,  3643,  3645,  3647,  3649,  3654,  3659,  3664,  3669,  3671,
    3673,  3675,  3677,  3679,  3681,  3683,  3685,  3687,  3689,  3691,
    3693,  3695,  3697,  3699,  3704,  3708,  3713,  3719,  3721,  3723,
    3725,  3727,  3732,  3736,  3739,  3744,  3748,  3753,  3757,  3762,
    3768,  3770,  3772,  3774,  3776,  3778,  3780,  3782,  3790,  3796,
    3798,  3800,  3802,  3804,  3809,  3813,  3818,  3824,  3826,  3828,
    3833,  3837,  3842,  3848,  3850,  3852,  3855,  3857,  3860,  3865,
    3869,  3874,  3878,  3883,  3889,  3891,  3893,  3895,  3897,  3899,
    3901,  3903,  3905,  3907,  3909,  3911,  3914,  3919,  3923,  3926,
    3931,  3935,  3938,  3942,  3945,  3948,  3951,  3954,  3957,  3960,
    3964,  3967,  3973,  3976,  3982,  3985,  3991,  3993,  3995,  3999,
    4003,  4004,  4005,  4007,  4009,  4011,  4013,  4015,  4017,  4019,
    4023,  4026,  4032,  4037,  4040,  4046,  4051,  4054,  4057,  4059,
    4061,  4065,  4068,  4071,  4074,  4079,  4084,  4089,  4094,  4099,
    4104,  4106,  4108,  4110,  4114,  4117,  4120,  4122,  4124,  4128,
    4131,  4134,  4136,  4138,  4140,  4142,  4144,  4146,  4152,  4158,
    4164,  4168,  4179,  4190,  4192,  4196,  4199,  4200,  4207,  4208,
    4215,  4218,  4220,  4224,  4226,  4228,  4230,  4236,  4242,  4248,
    4250,  4254,  4258,  4260,  4264,  4266,  4268,  4270,  4276,  4282,
    4288,  4290,  4294,  4297,  4303,  4306,  4312,  4318,  4320,  4322,
    4326,  4332,  4334,  4338,  4344,  4350,  4356,  4362,  4370,  4372,
    4376,  4379,  4382,  4385,  4388
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     357,     0,    -1,    -1,   357,   358,   116,    -1,   359,   360,
     577,    -1,   359,   377,   577,    -1,   359,   522,   577,    -1,
     359,   133,   373,    -1,   359,   247,    -1,   257,    -1,     1,
      -1,   150,    -1,   193,   361,   368,    -1,    57,   361,   369,
      -1,   233,   361,   363,   370,    -1,   362,   233,   361,   363,
     370,    -1,   124,   361,   364,   370,   366,    -1,   365,   370,
     366,    -1,   114,   367,   370,   366,    -1,   164,   361,   367,
      -1,    -1,   202,   374,    -1,   195,   374,    -1,    95,   374,
      -1,   367,    -1,   367,    -1,   398,   124,   361,   367,    -1,
     398,   362,   124,   361,   367,    -1,   362,   124,   361,   367,
      -1,   362,   398,   124,   361,   367,    -1,   374,   375,    -1,
     374,   213,    15,   367,    21,    -1,   129,    -1,    -1,   367,
      -1,    -1,   367,    -1,    -1,    15,    21,    -1,    15,   371,
      21,    -1,   372,    -1,   371,     8,   372,    -1,   367,    -1,
       5,    -1,    64,    -1,    -1,    -1,    -1,   382,    -1,   383,
      -1,   384,    -1,   414,    -1,   410,    -1,   578,    -1,   419,
      -1,   420,    -1,   421,    -1,   479,    -1,   400,    -1,   415,
      -1,   425,    -1,   216,   489,    -1,   216,   489,   490,   456,
      -1,   123,   488,    -1,   183,   489,    15,   462,    21,    -1,
     390,    -1,   391,    -1,   396,    -1,   393,    -1,   395,    -1,
     411,    -1,   412,    -1,   413,    -1,   378,    -1,   466,    -1,
     464,    -1,   392,    -1,   142,   489,    -1,   142,   489,   367,
      -1,   141,   489,    15,   380,    21,    -1,   140,   489,    15,
      26,    21,    -1,   107,   531,    -1,    10,    -1,   379,    -1,
     381,    -1,    17,    -1,    16,    -1,     5,    -1,     9,    -1,
      37,    -1,    23,    -1,    22,    -1,    35,    -1,    38,    -1,
      34,    -1,    25,    -1,    32,    -1,    29,    -1,    28,    -1,
      31,    -1,    30,    -1,    33,    -1,    24,    -1,   245,   489,
     490,   367,    -1,   245,     8,   489,   374,   389,   490,   367,
      -1,   112,   489,    -1,   112,   489,   367,    -1,   398,   385,
     367,   489,   472,   404,   409,    -1,   384,     8,   367,   472,
     404,   409,    -1,    -1,     7,     7,    -1,     8,   374,   386,
       7,     7,    -1,   387,    -1,   386,     8,   374,   387,    -1,
     183,    -1,   389,    -1,    44,    -1,    87,   472,    -1,   119,
      -1,   145,    15,   388,    21,    -1,   143,    -1,   178,    -1,
     187,    -1,   216,    -1,   230,    -1,   236,    -1,   374,   148,
      -1,   374,   180,    -1,   374,   147,    -1,   194,    -1,   191,
      -1,   145,   489,    15,   388,    21,   490,   367,    -1,   390,
       8,   367,    -1,   178,   489,   490,   367,    -1,   391,     8,
     367,    -1,   230,   489,   490,   418,    -1,   392,     8,   418,
      -1,   191,   489,    -1,   191,   489,   490,   394,   458,    -1,
      -1,   219,   489,    -1,   194,   489,    -1,   194,   489,   490,
     397,   458,    -1,    -1,   402,   399,   406,   399,    -1,   244,
      15,   367,    21,   399,    -1,    -1,   401,   367,    -1,   400,
       8,   367,    -1,    13,    -1,     6,    -1,   403,    -1,   144,
      -1,   200,    -1,    68,    -1,    90,    -1,    91,    -1,   154,
      -1,    63,    -1,    -1,   405,    -1,     5,   552,   509,   553,
     399,    -1,     5,   552,    15,   553,     5,    21,    -1,     5,
     552,    15,   553,   495,    21,    -1,    -1,   405,    -1,    15,
     573,   407,   408,    21,    -1,    15,   573,   407,   408,     8,
     573,   407,   408,    21,    -1,   495,    -1,     5,    -1,   564,
     495,    -1,   564,     5,    -1,    -1,    -1,    26,   495,    -1,
      18,   495,    -1,    87,   490,   489,   367,   472,    -1,   410,
       8,   367,   472,    -1,    44,   489,   490,   418,    -1,   411,
       8,   418,    -1,   187,   489,   490,   418,    -1,   412,     8,
     418,    -1,   236,   489,   490,   418,    -1,   413,     8,   418,
      -1,    67,   489,   418,    -1,    67,   489,   417,   418,    -1,
     414,   546,   417,   546,   418,    -1,   414,     8,   418,    -1,
     167,   489,   416,   498,    -1,   415,   546,   416,   546,   498,
      -1,   415,     8,   498,    -1,    37,   367,    37,    -1,    23,
      -1,    37,   367,    37,    -1,   367,   472,    -1,   119,   489,
     490,   367,    -1,   419,     8,   367,    -1,   143,   489,   490,
     367,    -1,   420,     8,   367,    -1,   117,   489,   422,    -1,
     421,     8,   422,    -1,    15,   423,    21,    -1,   424,     8,
     424,    -1,   423,     8,   424,    -1,   367,    -1,   367,    15,
     494,    21,    -1,   503,    -1,   426,    -1,    80,   488,   427,
     429,    -1,   426,   546,   429,    -1,    -1,    -1,   430,    37,
     431,    37,    -1,   432,    -1,   430,     8,   432,    -1,   443,
      -1,   431,     8,   443,    -1,   433,   435,    -1,   433,   435,
     436,    -1,   433,   435,   437,    -1,   433,   435,   436,   437,
      -1,   433,   440,    -1,    -1,   367,    -1,   367,    -1,    15,
     438,    21,    -1,    15,   439,     7,   439,    21,    -1,   452,
      -1,   438,     8,   452,    -1,    -1,   452,    -1,    15,   441,
       8,   434,    26,   438,    21,    -1,   442,    -1,   441,     8,
     442,    -1,   435,   436,    -1,   435,   437,    -1,   435,   436,
     437,    -1,   440,    -1,   444,    -1,   433,   434,     5,   444,
      -1,   447,     5,   444,    -1,   433,   434,    -1,   446,    -1,
     448,    -1,   450,    -1,    36,    -1,    36,   246,   512,    -1,
      27,    -1,    27,   246,   512,    -1,    64,    -1,   445,    -1,
     433,   498,    15,   573,   491,    21,    -1,    59,    -1,   447,
      -1,    17,   447,    -1,    16,   447,    -1,   149,    -1,   149,
     246,   512,    -1,   449,    -1,    17,   449,    -1,    16,   449,
      -1,   201,    -1,   201,   246,   512,    -1,    92,    -1,    92,
     246,   512,    -1,    15,   451,     8,   451,    21,    -1,   448,
      -1,   446,    -1,   453,    -1,    17,   453,    -1,    16,   453,
      -1,   452,    17,   453,    -1,   452,    16,   453,    -1,   454,
      -1,   453,     5,   454,    -1,   453,    37,   454,    -1,   455,
      -1,   455,     9,   454,    -1,   149,    -1,   434,    -1,    15,
     452,    21,    -1,   457,    -1,   456,     8,   457,    -1,   418,
      -1,   417,    -1,   459,   461,   460,    -1,   458,     8,   459,
     461,   460,    -1,    -1,    -1,   367,    -1,   177,    15,   380,
      21,    -1,    47,    15,    26,    21,    -1,   463,    -1,   462,
       8,   463,    -1,   367,    26,   495,    -1,   163,   465,    -1,
     367,    -1,   465,     8,   367,    -1,   248,   489,   467,    -1,
     248,   489,   467,     8,   376,   470,    -1,   248,   489,   467,
       8,   376,   172,    -1,   248,   489,   467,     8,   376,   172,
     468,    -1,   367,    -1,   469,    -1,   468,     8,   469,    -1,
     471,    -1,   367,    -1,   471,    -1,   470,     8,   471,    -1,
     367,    18,   367,    -1,    -1,    15,   473,    21,    -1,    -1,
     474,   475,    -1,   473,     8,   475,    -1,   476,    -1,     7,
      -1,   495,     7,    -1,   495,     7,   476,    -1,     5,    -1,
     495,    -1,   478,    -1,   477,     8,   478,    -1,   149,    -1,
     130,   489,   480,    -1,   131,    -1,   481,    -1,   480,     8,
     481,    -1,   482,    15,   485,    21,    -1,    -1,   483,   484,
      -1,   231,   403,    -1,   398,    -1,   486,    -1,   485,     8,
     486,    -1,   487,    -1,   487,    16,   487,    -1,   129,    -1,
      -1,    -1,    -1,     7,     7,    -1,    -1,   493,    -1,   495,
      -1,   513,    -1,   564,   495,    -1,   573,   492,    -1,   493,
       8,   573,   492,    -1,   495,    -1,   494,     8,   495,    -1,
     496,    -1,    15,   495,    21,    -1,   511,    -1,   499,    -1,
     507,    -1,   514,    -1,   495,    17,   495,    -1,   495,    16,
     495,    -1,   495,     5,   495,    -1,   495,    37,   495,    -1,
     495,     9,   495,    -1,   379,   495,    -1,    17,   495,    -1,
      16,   495,    -1,   495,    25,   495,    -1,   495,    29,   495,
      -1,   495,    31,   495,    -1,   495,    28,   495,    -1,   495,
      30,   495,    -1,   495,    32,   495,    -1,   495,    24,   495,
      -1,   495,    33,   495,    -1,   495,    38,   495,    -1,   495,
      35,   495,    -1,   495,    22,   495,    -1,    34,   495,    -1,
     495,    23,   495,    -1,   495,   379,   495,    -1,    17,    -1,
      16,    -1,   367,    -1,   498,    -1,   501,    -1,   500,    -1,
     498,    15,   573,   491,    21,    -1,   498,    15,   573,   491,
      21,   505,    -1,   501,    15,   491,    21,    -1,   501,    15,
     491,    21,   505,    -1,   499,     3,   129,    -1,   498,    -1,
     501,    -1,   498,    15,   573,   491,    21,    -1,   501,    15,
     491,    21,    -1,   498,   505,    -1,    -1,   505,    -1,    15,
     506,     7,   506,    21,    -1,    -1,   495,    -1,   508,    -1,
     508,   246,   512,    -1,   509,    -1,   509,   246,   512,    -1,
     510,   504,    -1,    36,    -1,    27,    -1,   201,    -1,    92,
      -1,   149,    -1,    64,    -1,   498,   246,    64,    -1,   509,
     246,    64,    -1,    15,   496,     8,   496,    21,    -1,   498,
      -1,   509,    -1,   495,     7,   495,    -1,   495,     7,    -1,
     495,     7,   495,     7,   495,    -1,   495,     7,     7,   495,
      -1,     7,   495,     7,   495,    -1,     7,     7,   495,    -1,
       7,   495,    -1,     7,    -1,    -1,    -1,    14,   408,   515,
     570,   516,    20,    -1,   498,    -1,   501,    -1,   502,    -1,
     518,     8,   573,   502,    -1,   518,     8,   573,   564,   498,
      -1,   517,    -1,   519,     8,   573,   517,    -1,   519,     8,
     573,   564,   498,    -1,    -1,   498,    -1,   521,     8,   498,
      -1,   543,    -1,   542,    -1,   525,    -1,   533,   525,    -1,
     102,   551,   531,    -1,   103,   551,   530,    -1,   108,   531,
      -1,   523,    -1,   533,   523,    -1,   534,   543,    -1,   534,
     239,    -1,   533,   534,   239,    -1,    97,   551,    15,   495,
      21,   239,   530,    -1,    96,   551,   530,    -1,   106,   551,
     530,    -1,   526,    -1,    76,   551,    -1,   535,   543,    -1,
     535,    -1,   533,   535,    -1,   105,   551,   530,    -1,   579,
      -1,   843,    -1,   861,    -1,    89,   551,    15,   495,    21,
      -1,    89,   551,   552,   541,   553,   613,   524,    -1,    -1,
       8,   374,   254,    15,   495,    21,    -1,   254,    15,   495,
      21,    -1,   185,   551,   552,   541,   553,   546,   539,    26,
     495,     8,   495,    -1,   185,   551,   552,   541,   553,   546,
     539,    26,   495,     8,   495,     8,   495,    -1,    62,   551,
     527,   530,    -1,    84,   551,   530,    -1,   110,   551,   530,
      -1,   218,   551,   374,    62,    15,   495,    21,    -1,   533,
     218,   551,   374,    62,    15,   495,    21,    -1,    15,   529,
      21,    -1,   495,    -1,   495,     7,    -1,     7,   495,    -1,
     495,     7,   495,    -1,   528,    -1,   529,     8,   528,    -1,
      -1,   367,    -1,    -1,   367,    -1,    75,    -1,   532,     7,
      -1,   155,   551,    15,   495,    21,    -1,   122,   551,    15,
     536,   538,    21,    -1,   537,    -1,   536,     8,   537,    -1,
     539,    26,   513,    -1,    -1,     8,   495,    -1,   367,    -1,
     539,    26,   495,     8,   495,    -1,   539,    26,   495,     8,
     495,     8,   495,    -1,    -1,   149,    -1,   113,   551,   530,
      -1,    98,   551,   530,    -1,    98,   551,    15,   495,    21,
     530,    -1,   252,   551,    15,   495,    21,    -1,   533,   252,
     551,    15,   495,    21,    -1,   544,   495,    26,   495,    -1,
     188,   551,   499,    18,   495,    -1,    48,   551,   478,   240,
     367,    -1,    77,   551,    -1,   545,    -1,   554,    -1,    46,
     551,    15,   495,    21,   478,     8,   478,     8,   478,    -1,
     547,    -1,   547,    15,    21,    -1,   547,    15,   548,    21,
      -1,   214,   551,   506,    -1,   550,   551,   506,    -1,    79,
     551,   530,    -1,   115,   551,   530,    -1,    45,   551,    15,
     520,   518,    21,    -1,    81,   551,    15,   520,   519,    21,
      -1,   170,   551,    15,   521,    21,    -1,   253,   551,    15,
     495,    21,   499,    26,   495,    -1,   152,   428,    -1,   186,
     551,   478,    -1,    49,   551,   367,    -1,    49,   551,   367,
     546,    15,   477,    21,    -1,    69,   551,    15,   477,    21,
     546,   495,    -1,    -1,     8,    -1,    61,   551,   367,   573,
      -1,   573,   549,    -1,   548,     8,   573,   549,    -1,   495,
      -1,   564,   495,    -1,     5,   478,    -1,   184,    -1,   232,
      -1,    -1,    -1,    -1,   555,   561,    -1,   555,   576,    -1,
     555,     5,    -1,   555,     9,    -1,   557,   561,    -1,   559,
      -1,   565,   561,    -1,   565,   560,    -1,   565,   561,   568,
      -1,   565,   560,     8,   568,    -1,   566,   561,    -1,   566,
     561,   570,    -1,   567,    -1,   567,     8,   570,    -1,   556,
     551,   574,    -1,    53,    -1,   215,    -1,   104,    -1,   558,
     551,   574,    -1,   176,    -1,    66,    -1,   139,   551,   574,
     561,    -1,   139,   551,   574,   561,   570,    -1,   576,    -1,
       5,    -1,    15,   575,    21,    -1,    15,   562,    21,    -1,
     563,    -1,   562,     8,   573,   563,    -1,   575,    -1,     5,
      -1,     9,    -1,   564,   495,    -1,   564,     5,    -1,   564,
       9,    -1,   166,    -1,   197,   551,   574,    -1,   256,   551,
     574,    -1,   190,   551,   575,   574,    -1,   190,   551,     5,
     574,    -1,   569,    -1,   568,     8,   569,    -1,   499,    -1,
      15,   568,     8,   540,    21,    -1,   496,    -1,   572,    -1,
     571,    -1,   496,     8,   496,    -1,   496,     8,   572,    -1,
     572,     8,   496,    -1,   572,     8,   572,    -1,   571,     8,
     496,    -1,   571,     8,   572,    -1,   511,    -1,    15,   495,
      21,    -1,    15,   496,     8,   540,    21,    -1,    15,   572,
       8,   540,    21,    -1,    15,   571,     8,   540,    21,    -1,
      -1,    -1,   576,    -1,    15,   575,    21,    -1,   499,    -1,
     507,    -1,   575,   497,   575,    -1,   575,     5,   575,    -1,
     575,    37,   575,    -1,   575,     9,   575,    -1,   497,   575,
      -1,   575,    23,   575,    -1,   129,    26,   495,    -1,    -1,
     257,    -1,   580,    -1,   628,    -1,   603,    -1,   582,    -1,
     593,    -1,   588,    -1,   640,    -1,   643,    -1,   721,    -1,
     585,    -1,   594,    -1,   596,    -1,   598,    -1,   600,    -1,
     648,    -1,   654,    -1,   651,    -1,   780,    -1,   778,    -1,
     604,    -1,   629,    -1,   658,    -1,   710,    -1,   708,    -1,
     709,    -1,   711,    -1,   712,    -1,   713,    -1,   714,    -1,
     715,    -1,   723,    -1,   725,    -1,   730,    -1,   727,    -1,
     729,    -1,   733,    -1,   731,    -1,   732,    -1,   744,    -1,
     748,    -1,   749,    -1,   752,    -1,   751,    -1,   753,    -1,
     754,    -1,   755,    -1,   756,    -1,   657,    -1,   738,    -1,
     739,    -1,   740,    -1,   743,    -1,   757,    -1,   760,    -1,
     765,    -1,   770,    -1,   772,    -1,   773,    -1,   774,    -1,
     775,    -1,   777,    -1,   736,    -1,   779,    -1,    82,   489,
     581,    -1,   580,     8,   581,    -1,   367,   472,    -1,    94,
     489,   583,    -1,   584,    -1,   583,     8,   584,    -1,   367,
      -1,   138,   489,   586,    -1,   587,    -1,   586,     8,   587,
      -1,   367,    -1,   228,   489,   592,   589,    -1,    15,   590,
      21,    -1,   591,    -1,   590,     8,   591,    -1,   495,    -1,
     495,     7,   495,    -1,     7,    15,   494,    21,    -1,   367,
      -1,   259,   489,   367,   472,    -1,   303,   489,   367,   472,
      -1,   593,     8,   367,   472,    -1,   136,   489,   595,    -1,
     594,     8,   595,    -1,   367,    -1,   211,   489,   597,    -1,
     596,     8,   597,    -1,   367,    -1,   205,   489,   599,    -1,
     598,     8,   599,    -1,   367,    -1,    70,   489,   601,    -1,
     600,     8,   601,    -1,   367,    -1,   175,   367,   472,    -1,
      -1,    88,   489,   607,   610,   602,    -1,   204,   551,   607,
     611,   613,   602,    -1,   204,   551,   611,   613,   602,     7,
       7,   605,    -1,   606,    -1,   605,     8,   606,    -1,   607,
      -1,   608,    -1,   367,    -1,   367,    15,   494,    21,    -1,
     367,    -1,    -1,   611,   613,    -1,    15,   612,    21,    -1,
     613,   614,    -1,   612,     8,   613,   614,    -1,    -1,    58,
      -1,    58,    15,   573,   627,    21,    -1,   126,    15,   615,
      21,    -1,   258,    15,   615,     8,   495,    21,    -1,   165,
      15,   495,    21,    -1,     5,    -1,   137,    15,   615,    21,
      -1,    86,    15,   616,    21,    -1,   367,    -1,    15,   617,
      21,   374,   255,   619,    -1,   618,    -1,   617,     8,   618,
      -1,   495,    -1,   495,     7,   495,    -1,   620,    -1,   620,
      15,   621,    21,    -1,   367,    -1,   622,    -1,   621,     8,
     622,    -1,   495,    -1,   769,    -1,    40,   623,   624,    -1,
     367,    -1,    -1,   625,    -1,    17,   626,    -1,   624,    17,
     626,    -1,   495,    -1,   564,   495,    -1,   564,   495,     8,
     564,   495,    -1,    43,   489,   631,   633,    -1,   199,   551,
     632,   633,    -1,   199,   551,   633,     7,     7,   630,    -1,
     632,    -1,   630,     8,   632,    -1,   367,    -1,   498,    -1,
      15,   638,    21,   374,   255,   634,    -1,   637,    15,   635,
      21,    -1,   636,    -1,   635,     8,   636,    -1,   495,    -1,
       5,    -1,   513,    -1,   367,    -1,   639,    -1,   638,     8,
     639,    -1,   367,    -1,     5,    -1,     7,    -1,   641,     7,
       7,   489,   367,   472,    -1,   640,     8,   367,   472,    -1,
     642,    -1,   641,     8,   374,   642,    -1,    82,    -1,   259,
      -1,   303,    -1,    94,    -1,    87,    15,   473,    21,    -1,
     228,   589,    -1,    43,    15,   638,    21,   374,   255,   634,
      -1,    43,    -1,    88,   611,   613,   602,    -1,    88,    -1,
      67,    -1,   398,     8,   374,    93,   489,    15,   644,    21,
       7,     7,   646,    -1,    -1,   645,     7,    -1,   644,     8,
       7,    -1,   647,    -1,   646,     8,   647,    -1,   367,    -1,
     127,   489,   649,    -1,   650,    -1,   649,     8,   650,    -1,
     367,    -1,    74,   489,   652,    -1,   653,    -1,   652,     8,
     653,    -1,   367,    -1,    51,   489,   655,    -1,    51,   489,
       8,   374,    67,     7,     7,   655,    -1,   656,    -1,   655,
       8,   656,    -1,   367,   472,    -1,   168,   551,    -1,   182,
     551,    15,   659,    21,   660,   664,    -1,   498,    -1,   659,
       8,   498,    -1,   613,   173,   661,    -1,   613,    -1,   498,
      15,   662,    21,    -1,   663,    -1,   662,     8,   663,    -1,
     495,    -1,     5,    -1,    -1,   665,    -1,   666,    -1,   665,
     666,    -1,   670,    -1,   693,    -1,   701,    -1,   667,    -1,
     677,    -1,   679,    -1,   678,    -1,   668,    -1,   671,    -1,
     672,    -1,   675,    -1,     8,   374,   209,    15,   716,     7,
     717,    21,    -1,     8,   374,   209,    15,   717,    21,    -1,
       8,   374,    71,    15,   669,     7,   717,    21,    -1,     8,
     374,    71,    15,   717,    21,    -1,   367,    -1,     8,   374,
     169,    15,   674,    21,    -1,     8,   374,   282,    15,   674,
      21,    -1,     8,   374,   191,    15,   674,    21,    -1,     8,
     374,   320,    15,   673,    21,    -1,   495,    -1,   495,     8,
     495,    -1,   495,     8,   495,     8,   495,    -1,   499,    -1,
     674,     8,   499,    -1,     8,   374,   322,    15,   676,    21,
      -1,   661,    -1,   676,     8,   661,    -1,     8,   374,   135,
      15,   716,     7,   734,    21,    -1,     8,   374,   135,    15,
     734,    21,    -1,     8,   374,   229,    15,   495,    21,    -1,
       8,   374,    41,   680,    -1,     8,   374,    41,   680,   680,
      -1,    15,   681,   682,   683,    21,    -1,    -1,   148,     7,
      -1,   180,     7,    -1,    -1,   684,    -1,   683,     8,   684,
      -1,   706,    -1,   706,    15,   685,    21,    -1,   706,    15,
     685,    21,    15,   687,    21,    -1,   686,    -1,   685,     8,
     686,    -1,   495,     7,   495,    -1,   688,    -1,   687,     8,
     688,    -1,   689,     7,   690,     7,   691,    -1,   689,     7,
     690,    -1,   689,     7,   691,    -1,   689,    -1,   690,     7,
     691,    -1,   690,    -1,   691,    -1,   374,   217,   692,    -1,
     374,   157,   692,    -1,   374,   128,   692,    -1,    15,   493,
      21,    -1,     8,   374,   208,    15,   694,   698,   695,     8,
     697,    21,    -1,     8,   374,   208,    15,   694,   698,   695,
      21,    -1,     8,   374,   208,    15,   694,   696,   695,     7,
     697,    21,    -1,    -1,    -1,   367,    -1,   374,   698,    -1,
     697,     8,   374,   698,    -1,   699,    15,   499,    21,    -1,
     700,    15,   674,     8,   495,    21,    -1,   234,    -1,   192,
      -1,   162,    -1,   159,    -1,    35,    -1,    22,    -1,    24,
      -1,    33,    -1,   247,    -1,   158,    -1,   161,    -1,     8,
     374,   223,    15,   703,    21,    -1,     8,   374,   224,   702,
      -1,     8,   374,   226,   702,    -1,     8,   374,   221,    -1,
       8,   374,   221,    15,   706,    15,   590,    21,    21,    -1,
     367,    -1,   704,    -1,   703,     8,   704,    -1,   706,    -1,
     706,    15,   705,    78,    21,    -1,   706,    15,   705,   590,
      21,    -1,   706,    15,   705,   590,    21,    15,   374,    78,
      21,    -1,    -1,   498,    -1,   706,    -1,   707,     8,   706,
      -1,   225,   551,   702,    -1,   224,   551,   702,    -1,   227,
     551,   702,    -1,   226,   551,   702,    -1,   222,   551,   702,
      15,   703,    21,    -1,   206,   551,   696,    -1,   207,   551,
     696,    -1,    72,   551,   669,    -1,    73,   551,   669,    -1,
     210,   551,    15,   716,     7,   717,    21,    -1,   210,   551,
      15,   717,    21,    -1,   367,    -1,   718,    -1,   717,     8,
     718,    -1,   706,    15,   719,    21,    -1,   706,    -1,   720,
      -1,   719,     8,   720,    -1,   495,    -1,     7,    -1,   237,
     489,   722,    -1,   721,     8,   722,    -1,   367,   472,    -1,
     238,   551,   724,    -1,   238,   551,   724,   693,    -1,   238,
     551,   724,   668,    -1,   238,   551,   724,   693,   668,    -1,
     238,   551,   724,   668,   693,    -1,   367,    -1,   111,   551,
      -1,   724,    15,   495,    21,    -1,   724,    15,   513,    21,
      -1,   174,   551,   500,   728,    -1,    -1,   670,    -1,   109,
     551,    -1,   160,   551,   726,   374,   175,   609,   472,    -1,
     160,   551,   726,   374,   323,   499,    -1,   189,   551,   716,
      -1,   212,   551,   716,    -1,   135,   551,    15,   716,     7,
     734,    21,    -1,   135,   551,    15,   734,    21,    -1,   735,
      -1,   734,     8,   735,    -1,   706,    -1,   706,    15,   494,
      21,    -1,   134,   551,    -1,   134,   551,   670,    -1,   134,
     551,   737,    -1,   134,   551,   670,   737,    -1,     8,   374,
     208,    15,   674,    21,    -1,    50,   551,   742,    -1,    99,
     551,    -1,    52,   551,   742,    -1,   367,    -1,   741,    -1,
     741,    15,   494,    21,    -1,   120,   551,   499,    26,   499,
      -1,    83,   551,   747,    -1,    83,   551,   747,    15,   745,
      21,    -1,   573,   746,    -1,   745,     8,   573,   746,    -1,
     564,   495,    -1,   149,    -1,   100,   551,   747,    -1,   146,
     551,   750,    -1,    -1,   495,    -1,   332,   551,   494,    -1,
     101,   551,    -1,   241,   551,    -1,   242,   551,    -1,    56,
     551,    -1,    65,   551,   573,    15,   548,    21,   408,   490,
     674,    -1,   324,   551,    15,   758,    21,    -1,   759,    -1,
     758,     8,   759,    -1,   374,   315,    -1,   374,   318,    -1,
     374,   182,    -1,   220,   551,    15,   761,    26,   626,    21,
     613,   764,    -1,   498,    15,   762,    21,    -1,   763,    -1,
     762,     8,   763,    -1,   616,    -1,   769,    -1,   132,   707,
      -1,    -1,   153,   551,    15,   498,    18,   766,    21,    -1,
     498,    -1,   498,    15,   767,    21,    -1,   768,    -1,   767,
       8,   768,    -1,   769,    -1,     7,    -1,     5,    -1,   325,
     551,   495,     8,   374,   330,    15,   674,    21,     8,   374,
     329,    15,   494,    21,   771,    -1,    -1,     8,   374,   182,
      -1,     8,   374,   318,    -1,   326,   551,   495,    -1,   327,
     551,   495,    -1,   327,   551,   495,     8,   374,   315,    -1,
     328,   551,   495,     8,   374,   331,    15,   498,    21,    -1,
     333,   551,    15,   776,    21,    -1,   502,    -1,   776,     8,
     502,    -1,   334,   551,    15,   659,    21,    -1,   829,    -1,
     782,    -1,   781,    -1,   799,    -1,   802,    -1,   803,    -1,
     804,    -1,   805,    -1,   811,    -1,   814,    -1,   819,    -1,
     820,    -1,   821,    -1,   824,    -1,   825,    -1,   826,    -1,
     827,    -1,   828,    -1,   830,    -1,   831,    -1,   832,    -1,
     833,    -1,   834,    -1,   835,    -1,   836,    -1,   837,    -1,
     838,    -1,   268,   551,    -1,   275,   551,    -1,   291,   551,
     613,   783,    -1,   291,   551,   613,    -1,   546,   613,   784,
     613,    -1,   783,   546,   613,   784,   613,    -1,   786,    -1,
     795,    -1,   790,    -1,   791,    -1,   787,    -1,   788,    -1,
     789,    -1,   793,    -1,   794,    -1,   841,    15,   842,   840,
      21,    -1,   191,   785,    -1,   282,   785,    -1,   285,   785,
      -1,   265,   785,    -1,   299,   785,    -1,    84,    15,   374,
     792,    21,    -1,   191,    -1,   299,    -1,   288,    -1,   304,
      15,   495,    21,    -1,   289,    15,   495,    21,    -1,   208,
      15,   796,    21,    -1,   613,   798,     7,   797,    -1,   674,
      -1,    17,    -1,    16,    -1,     5,    -1,    37,    -1,   162,
      -1,   159,    -1,    35,    -1,    22,    -1,    24,    -1,    33,
      -1,   305,    -1,   306,    -1,   307,    -1,   247,    -1,   297,
     551,   613,   800,    -1,   297,   551,   613,    -1,   546,   613,
     801,   613,    -1,   800,   546,   613,   801,   613,    -1,   786,
      -1,   795,    -1,   787,    -1,   788,    -1,   279,   551,   613,
     818,    -1,   279,   551,   613,    -1,   296,   551,    -1,   269,
     551,   613,   806,    -1,   269,   551,   613,    -1,   272,   551,
     613,   818,    -1,   272,   551,   613,    -1,   546,   613,   807,
     613,    -1,   806,   546,   613,   807,   613,    -1,   786,    -1,
     795,    -1,   787,    -1,   788,    -1,   809,    -1,   808,    -1,
     290,    -1,   298,    15,   374,   810,     8,   495,    21,    -1,
     298,    15,   374,   810,    21,    -1,   230,    -1,    94,    -1,
     284,    -1,   295,    -1,   300,   551,   613,   812,    -1,   300,
     551,   613,    -1,   546,   613,   813,   613,    -1,   812,   546,
     613,   813,   613,    -1,   786,    -1,   787,    -1,   280,   551,
     613,   815,    -1,   280,   551,   613,    -1,   546,   613,   816,
     613,    -1,   815,   546,   613,   816,   613,    -1,   818,    -1,
     817,    -1,   266,   785,    -1,   287,    -1,   302,   551,    -1,
     281,   551,   613,   818,    -1,   281,   551,   613,    -1,   292,
     551,   613,   822,    -1,   292,   551,   613,    -1,   546,   613,
     823,   613,    -1,   822,   546,   613,   823,   613,    -1,   786,
      -1,   795,    -1,   790,    -1,   791,    -1,   787,    -1,   788,
      -1,   789,    -1,   793,    -1,   794,    -1,   809,    -1,   808,
      -1,   276,   551,    -1,   293,   551,   613,   783,    -1,   293,
     551,   613,    -1,   277,   551,    -1,   294,   551,   613,   783,
      -1,   294,   551,   613,    -1,   278,   551,    -1,   301,   489,
     785,    -1,   286,   551,    -1,   273,   551,    -1,   290,   551,
      -1,   274,   551,    -1,   264,   551,    -1,   263,   551,    -1,
     283,   551,   785,    -1,   283,   551,    -1,   267,   551,    15,
     498,    21,    -1,   267,   551,    -1,   271,   551,    15,   498,
      21,    -1,   271,   551,    -1,    37,   367,   841,    37,   842,
      -1,   839,    -1,   498,    -1,   840,     8,   839,    -1,   840,
       8,   498,    -1,    -1,    -1,   844,    -1,   857,    -1,   845,
      -1,   858,    -1,   846,    -1,   847,    -1,   859,    -1,   308,
     551,   848,    -1,   310,   551,    -1,   312,   551,    15,   854,
      21,    -1,   312,   551,    15,    21,    -1,   312,   551,    -1,
     313,   551,    15,   854,    21,    -1,   313,   551,    15,    21,
      -1,   313,   551,    -1,   374,   375,    -1,   849,    -1,   850,
      -1,   849,     8,   850,    -1,   374,   851,    -1,   374,   853,
      -1,   374,   852,    -1,   147,    15,   854,    21,    -1,   148,
      15,   854,    21,    -1,   180,    15,   854,    21,    -1,   318,
      15,   854,    21,    -1,   319,    15,   854,    21,    -1,   314,
      15,   855,    21,    -1,   315,    -1,   674,    -1,   856,    -1,
     855,     8,   856,    -1,   374,   316,    -1,   374,   317,    -1,
     309,    -1,   311,    -1,   321,   489,   860,    -1,   374,   375,
      -1,   374,   852,    -1,   862,    -1,   863,    -1,   864,    -1,
     865,    -1,   870,    -1,   887,    -1,   335,   893,    15,   871,
      21,    -1,   336,   893,    15,   878,    21,    -1,   337,   893,
      15,   883,    21,    -1,   339,   893,   885,    -1,   339,   893,
     885,     8,   374,   351,    15,   866,    21,   868,    -1,   339,
     893,   885,     8,   374,   352,    15,   866,    21,   869,    -1,
     867,    -1,   866,     8,   867,    -1,   374,   353,    -1,    -1,
       8,   374,   352,    15,   866,    21,    -1,    -1,     8,   374,
     351,    15,   866,    21,    -1,   340,   893,    -1,   872,    -1,
     871,     8,   872,    -1,   873,    -1,   874,    -1,   875,    -1,
     374,   208,    15,   697,    21,    -1,   374,   191,    15,   674,
      21,    -1,   374,   183,    15,   876,    21,    -1,   877,    -1,
     876,     8,   877,    -1,   502,    26,   495,    -1,   879,    -1,
     878,     8,   879,    -1,   880,    -1,   881,    -1,   882,    -1,
     374,   228,    15,   703,    21,    -1,   374,    41,    15,   703,
      21,    -1,   374,   209,    15,   717,    21,    -1,   884,    -1,
     883,     8,   884,    -1,   374,   338,    -1,   374,   342,    15,
     659,    21,    -1,   374,   341,    -1,   374,   341,    15,   659,
      21,    -1,   374,   343,    15,   886,    21,    -1,   367,    -1,
     502,    -1,   886,     8,   502,    -1,   344,   893,    15,   888,
      21,    -1,   889,    -1,   888,     8,   889,    -1,   374,   244,
      15,   890,    21,    -1,   374,   330,    15,   659,    21,    -1,
     374,   345,    15,   659,    21,    -1,   374,   346,    15,   495,
      21,    -1,   374,   347,    15,   892,     8,   495,    21,    -1,
     891,    -1,   890,     8,   891,    -1,   374,   315,    -1,   374,
     350,    -1,   374,   348,    -1,   374,   349,    -1,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   787,   787,   788,   792,   794,   808,   839,   848,   854,
     874,   883,   899,   911,   921,   928,   934,   939,   944,   968,
     995,  1009,  1011,  1013,  1017,  1034,  1048,  1072,  1088,  1102,
    1120,  1122,  1129,  1133,  1134,  1141,  1142,  1150,  1151,  1153,
    1157,  1158,  1162,  1166,  1172,  1182,  1186,  1191,  1198,  1199,
    1200,  1201,  1202,  1203,  1204,  1205,  1206,  1207,  1208,  1209,
    1210,  1211,  1216,  1221,  1228,  1230,  1231,  1232,  1233,  1234,
    1235,  1236,  1237,  1238,  1239,  1240,  1241,  1244,  1248,  1256,
    1264,  1273,  1281,  1285,  1287,  1291,  1293,  1295,  1297,  1299,
    1301,  1303,  1305,  1307,  1309,  1311,  1313,  1315,  1317,  1319,
    1321,  1323,  1325,  1330,  1339,  1349,  1357,  1367,  1388,  1408,
    1409,  1411,  1415,  1417,  1421,  1425,  1427,  1431,  1437,  1441,
    1443,  1447,  1451,  1455,  1459,  1463,  1469,  1473,  1477,  1483,
    1488,  1495,  1506,  1519,  1530,  1543,  1553,  1566,  1571,  1578,
    1581,  1586,  1591,  1598,  1601,  1611,  1625,  1628,  1647,  1674,
    1676,  1688,  1696,  1697,  1698,  1699,  1700,  1701,  1702,  1707,
    1708,  1712,  1714,  1721,  1726,  1727,  1729,  1731,  1744,  1750,
    1756,  1765,  1774,  1787,  1788,  1791,  1795,  1810,  1825,  1843,
    1864,  1884,  1906,  1923,  1941,  1948,  1955,  1962,  1975,  1982,
    1989,  2000,  2004,  2006,  2011,  2029,  2040,  2052,  2064,  2078,
    2084,  2091,  2097,  2103,  2111,  2118,  2134,  2137,  2146,  2148,
    2152,  2156,  2176,  2180,  2182,  2186,  2187,  2190,  2192,  2194,
    2196,  2198,  2201,  2204,  2208,  2214,  2218,  2222,  2224,  2229,
    2230,  2234,  2238,  2240,  2244,  2246,  2248,  2253,  2257,  2259,
    2261,  2264,  2266,  2267,  2268,  2269,  2270,  2271,  2272,  2273,
    2276,  2277,  2283,  2286,  2287,  2289,  2293,  2294,  2297,  2298,
    2300,  2304,  2305,  2306,  2307,  2309,  2312,  2313,  2322,  2324,
    2331,  2338,  2345,  2354,  2356,  2358,  2362,  2364,  2368,  2377,
    2384,  2391,  2393,  2397,  2401,  2407,  2409,  2414,  2418,  2422,
    2429,  2436,  2446,  2448,  2452,  2464,  2467,  2476,  2489,  2495,
    2501,  2507,  2515,  2525,  2527,  2531,  2533,  2566,  2568,  2572,
    2611,  2612,  2616,  2616,  2621,  2625,  2633,  2642,  2651,  2661,
    2667,  2670,  2672,  2676,  2684,  2699,  2706,  2708,  2712,  2728,
    2728,  2732,  2734,  2746,  2748,  2752,  2758,  2770,  2782,  2799,
    2828,  2829,  2837,  2838,  2842,  2844,  2846,  2857,  2861,  2867,
    2869,  2873,  2875,  2877,  2881,  2883,  2887,  2889,  2891,  2893,
    2895,  2897,  2899,  2901,  2903,  2905,  2907,  2909,  2911,  2913,
    2915,  2917,  2919,  2921,  2923,  2925,  2927,  2929,  2931,  2935,
    2936,  2947,  3021,  3033,  3035,  3039,  3170,  3220,  3264,  3306,
    3364,  3366,  3368,  3407,  3450,  3461,  3462,  3466,  3471,  3472,
    3476,  3478,  3484,  3486,  3492,  3505,  3511,  3518,  3524,  3532,
    3540,  3556,  3566,  3579,  3586,  3588,  3611,  3613,  3615,  3617,
    3619,  3621,  3623,  3625,  3629,  3629,  3629,  3643,  3645,  3668,
    3670,  3672,  3688,  3690,  3692,  3706,  3709,  3711,  3719,  3721,
    3723,  3725,  3779,  3799,  3814,  3823,  3826,  3876,  3882,  3887,
    3905,  3907,  3909,  3911,  3913,  3916,  3922,  3924,  3926,  3929,
    3931,  3933,  3960,  3969,  3978,  3979,  3981,  3986,  3993,  4001,
    4003,  4007,  4010,  4012,  4016,  4022,  4024,  4026,  4028,  4032,
    4034,  4043,  4044,  4051,  4052,  4056,  4060,  4081,  4084,  4088,
    4090,  4097,  4102,  4103,  4114,  4126,  4149,  4174,  4175,  4182,
    4184,  4186,  4188,  4190,  4194,  4271,  4283,  4290,  4292,  4293,
    4295,  4304,  4311,  4318,  4326,  4331,  4336,  4339,  4342,  4345,
    4348,  4351,  4355,  4373,  4378,  4397,  4416,  4420,  4421,  4424,
    4428,  4433,  4440,  4442,  4444,  4448,  4449,  4460,  4475,  4479,
    4486,  4489,  4499,  4512,  4525,  4528,  4530,  4533,  4536,  4540,
    4549,  4552,  4556,  4558,  4564,  4568,  4570,  4572,  4579,  4583,
    4585,  4589,  4591,  4595,  4614,  4630,  4639,  4648,  4650,  4654,
    4680,  4695,  4710,  4727,  4735,  4744,  4752,  4757,  4762,  4784,
    4800,  4802,  4806,  4808,  4815,  4817,  4819,  4823,  4825,  4827,
    4829,  4831,  4833,  4837,  4840,  4843,  4849,  4855,  4864,  4868,
    4875,  4877,  4881,  4883,  4885,  4890,  4895,  4900,  4905,  4914,
    4919,  4925,  4926,  4941,  4942,  4943,  4944,  4945,  4946,  4947,
    4948,  4949,  4950,  4951,  4952,  4953,  4954,  4955,  4956,  4957,
    4958,  4959,  4962,  4963,  4964,  4965,  4966,  4967,  4968,  4969,
    4970,  4971,  4972,  4973,  4974,  4975,  4976,  4977,  4978,  4979,
    4980,  4981,  4982,  4983,  4984,  4985,  4986,  4987,  4988,  4989,
    4990,  4991,  4992,  4993,  4994,  4995,  4996,  4997,  4998,  4999,
    5000,  5001,  5002,  5003,  5004,  5005,  5009,  5011,  5022,  5043,
    5047,  5049,  5053,  5066,  5070,  5072,  5076,  5087,  5098,  5102,
    5104,  5108,  5110,  5112,  5127,  5139,  5159,  5179,  5201,  5207,
    5216,  5224,  5230,  5238,  5245,  5251,  5260,  5264,  5270,  5278,
    5292,  5306,  5311,  5327,  5342,  5370,  5372,  5376,  5378,  5382,
    5411,  5434,  5455,  5456,  5460,  5481,  5483,  5487,  5495,  5499,
    5504,  5506,  5508,  5510,  5516,  5518,  5522,  5532,  5536,  5538,
    5543,  5545,  5549,  5553,  5559,  5569,  5571,  5575,  5577,  5579,
    5586,  5604,  5605,  5609,  5611,  5615,  5622,  5632,  5661,  5676,
    5683,  5701,  5703,  5707,  5721,  5747,  5760,  5776,  5778,  5781,
    5783,  5789,  5793,  5821,  5823,  5827,  5835,  5841,  5844,  5902,
    5966,  5968,  5971,  5975,  5979,  5983,  6000,  6012,  6016,  6020,
    6030,  6035,  6040,  6047,  6056,  6056,  6067,  6078,  6080,  6084,
    6095,  6099,  6101,  6105,  6116,  6120,  6122,  6126,  6138,  6140,
    6147,  6149,  6153,  6169,  6177,  6188,  6190,  6194,  6197,  6202,
    6212,  6214,  6218,  6220,  6229,  6230,  6234,  6236,  6241,  6242,
    6243,  6244,  6245,  6246,  6247,  6248,  6249,  6250,  6251,  6254,
    6259,  6263,  6267,  6271,  6284,  6288,  6292,  6296,  6299,  6301,
    6303,  6307,  6309,  6313,  6317,  6319,  6323,  6328,  6332,  6336,
    6338,  6342,  6351,  6354,  6360,  6367,  6370,  6372,  6376,  6378,
    6382,  6394,  6396,  6400,  6404,  6406,  6410,  6412,  6414,  6416,
    6418,  6420,  6422,  6426,  6430,  6434,  6438,  6442,  6449,  6455,
    6460,  6463,  6466,  6479,  6481,  6485,  6487,  6492,  6498,  6504,
    6510,  6516,  6522,  6528,  6534,  6540,  6549,  6555,  6572,  6574,
    6582,  6590,  6592,  6596,  6600,  6602,  6606,  6608,  6616,  6620,
    6632,  6635,  6653,  6655,  6659,  6661,  6665,  6667,  6671,  6675,
    6679,  6688,  6692,  6696,  6701,  6705,  6717,  6719,  6723,  6728,
    6732,  6734,  6738,  6740,  6744,  6749,  6756,  6779,  6781,  6783,
    6785,  6787,  6791,  6802,  6806,  6821,  6828,  6835,  6836,  6840,
    6844,  6852,  6856,  6860,  6868,  6873,  6887,  6889,  6893,  6895,
    6904,  6906,  6908,  6910,  6946,  6950,  6954,  6958,  6962,  6974,
    6976,  6980,  6983,  6985,  6989,  6994,  7001,  7004,  7012,  7016,
    7021,  7023,  7030,  7035,  7039,  7043,  7047,  7051,  7055,  7058,
    7060,  7064,  7066,  7068,  7072,  7076,  7088,  7090,  7094,  7096,
    7100,  7103,  7106,  7110,  7116,  7128,  7130,  7134,  7136,  7140,
    7148,  7160,  7161,  7163,  7167,  7171,  7173,  7181,  7185,  7188,
    7190,  7194,  7198,  7200,  7201,  7202,  7203,  7204,  7205,  7206,
    7207,  7208,  7209,  7210,  7211,  7212,  7213,  7214,  7215,  7216,
    7217,  7218,  7219,  7220,  7221,  7222,  7223,  7224,  7225,  7228,
    7234,  7240,  7246,  7252,  7256,  7262,  7263,  7264,  7265,  7266,
    7267,  7268,  7269,  7270,  7273,  7278,  7283,  7289,  7295,  7301,
    7306,  7312,  7318,  7324,  7331,  7337,  7343,  7350,  7354,  7356,
    7362,  7369,  7375,  7381,  7387,  7393,  7399,  7405,  7411,  7417,
    7423,  7429,  7435,  7445,  7450,  7456,  7460,  7466,  7467,  7468,
    7469,  7472,  7480,  7486,  7492,  7497,  7503,  7510,  7516,  7520,
    7526,  7527,  7528,  7529,  7530,  7531,  7534,  7543,  7547,  7553,
    7560,  7567,  7574,  7583,  7589,  7595,  7599,  7605,  7606,  7609,
    7615,  7621,  7625,  7632,  7633,  7636,  7642,  7648,  7653,  7661,
    7667,  7672,  7679,  7683,  7689,  7690,  7691,  7692,  7693,  7694,
    7695,  7696,  7697,  7698,  7699,  7703,  7708,  7713,  7720,  7725,
    7731,  7737,  7742,  7747,  7752,  7756,  7761,  7766,  7770,  7775,
    7779,  7785,  7790,  7796,  7801,  7807,  7817,  7821,  7825,  7829,
    7835,  7838,  7842,  7843,  7844,  7845,  7846,  7847,  7848,  7851,
    7855,  7859,  7861,  7863,  7867,  7869,  7871,  7875,  7877,  7881,
    7883,  7887,  7890,  7893,  7898,  7900,  7902,  7904,  7906,  7910,
    7914,  7919,  7923,  7925,  7929,  7931,  7935,  7939,  7943,  7947,
    7949,  7953,  7954,  7955,  7956,  7957,  7958,  7961,  7965,  7969,
    7973,  7975,  7977,  7981,  7983,  7987,  7992,  7993,  7998,  7999,
    8003,  8007,  8009,  8013,  8014,  8015,  8018,  8022,  8026,  8029,
    8031,  8035,  8039,  8041,  8045,  8046,  8047,  8050,  8054,  8058,
    8062,  8064,  8068,  8070,  8072,  8074,  8077,  8081,  8085,  8087,
    8091,  8095,  8097,  8101,  8103,  8105,  8107,  8109,  8113,  8115,
    8119,  8121,  8125,  8127,  8132
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "PERCENT", "AMPERSAND", "ASTER",
  "CLUSTER", "COLON", "COMMA", "DASTER", "DEFINED_OPERATOR", "DOT",
  "DQUOTE", "GLOBAL_A", "LEFTAB", "LEFTPAR", "MINUS", "PLUS", "POINT_TO",
  "QUOTE", "RIGHTAB", "RIGHTPAR", "AND", "DSLASH", "EQV", "EQ", "EQUAL",
  "FFALSE", "GE", "GT", "LE", "LT", "NE", "NEQV", "NOT", "OR", "TTRUE",
  "SLASH", "XOR", "REFERENCE", "AT", "ACROSS", "ALIGN_WITH", "ALIGN",
  "ALLOCATABLE", "ALLOCATE", "ARITHIF", "ASSIGNMENT", "ASSIGN",
  "ASSIGNGOTO", "ASYNCHRONOUS", "ASYNCID", "ASYNCWAIT", "BACKSPACE",
  "BAD_CCONST", "BAD_SYMBOL", "BARRIER", "BLOCKDATA", "BLOCK",
  "BOZ_CONSTANT", "BYTE", "CALL", "CASE", "CHARACTER", "CHAR_CONSTANT",
  "CHECK", "CLOSE", "COMMON", "COMPLEX", "COMPGOTO", "CONSISTENT_GROUP",
  "CONSISTENT_SPEC", "CONSISTENT_START", "CONSISTENT_WAIT", "CONSISTENT",
  "CONSTRUCT_ID", "CONTAINS", "CONTINUE", "CORNER", "CYCLE", "DATA",
  "DEALLOCATE", "HPF_TEMPLATE", "DEBUG", "DEFAULT_CASE", "DEFINE",
  "DERIVED", "DIMENSION", "DISTRIBUTE", "DOWHILE", "DOUBLEPRECISION",
  "DOUBLECOMPLEX", "DP_CONSTANT", "DVM_POINTER", "DYNAMIC", "ELEMENTAL",
  "ELSE", "ELSEIF", "ELSEWHERE", "ENDASYNCHRONOUS", "ENDDEBUG",
  "ENDINTERVAL", "ENDUNIT", "ENDDO", "ENDFILE", "ENDFORALL", "ENDIF",
  "ENDINTERFACE", "ENDMODULE", "ENDON", "ENDSELECT", "ENDTASK_REGION",
  "ENDTYPE", "ENDWHERE", "ENTRY", "EXIT", "EOLN", "EQUIVALENCE", "ERROR",
  "EXTERNAL", "F90", "FIND", "FORALL", "FORMAT", "FUNCTION", "GATE",
  "GEN_BLOCK", "HEAP", "HIGH", "IDENTIFIER", "IMPLICIT", "IMPLICITNONE",
  "INCLUDE_TO", "INCLUDE", "INDEPENDENT", "INDIRECT_ACCESS",
  "INDIRECT_GROUP", "INDIRECT", "INHERIT", "INQUIRE",
  "INTERFACEASSIGNMENT", "INTERFACEOPERATOR", "INTERFACE", "INTRINSIC",
  "INTEGER", "INTENT", "INTERVAL", "INOUT", "IN", "INT_CONSTANT", "LABEL",
  "LABEL_DECLARE", "LET", "LOCALIZE", "LOGICAL", "LOGICALIF", "LOOP",
  "LOW", "MAXLOC", "MAX", "MAP", "MINLOC", "MIN", "MODULE_PROCEDURE",
  "MODULE", "MULT_BLOCK", "NAMEEQ", "NAMELIST", "NEW_VALUE", "NEW",
  "NULLIFY", "OCTAL_CONSTANT", "ONLY", "ON", "ON_DIR", "ONTO", "OPEN",
  "OPERATOR", "OPTIONAL", "OTHERWISE", "OUT", "OWN", "PARALLEL",
  "PARAMETER", "PAUSE", "PLAINDO", "PLAINGOTO", "POINTER", "POINTERLET",
  "PREFETCH", "PRINT", "PRIVATE", "PRODUCT", "PROGRAM", "PUBLIC", "PURE",
  "RANGE", "READ", "REALIGN_WITH", "REALIGN", "REAL", "REAL_CONSTANT",
  "RECURSIVE", "REDISTRIBUTE_NEW", "REDISTRIBUTE", "REDUCTION_GROUP",
  "REDUCTION_START", "REDUCTION_WAIT", "REDUCTION", "REMOTE_ACCESS_SPEC",
  "REMOTE_ACCESS", "REMOTE_GROUP", "RESET", "RESULT", "RETURN", "REWIND",
  "SAVE", "SECTION", "SELECT", "SEQUENCE", "SHADOW_ADD", "SHADOW_COMPUTE",
  "SHADOW_GROUP", "SHADOW_RENEW", "SHADOW_START_SPEC", "SHADOW_START",
  "SHADOW_WAIT_SPEC", "SHADOW_WAIT", "SHADOW", "STAGE", "STATIC", "STAT",
  "STOP", "SUBROUTINE", "SUM", "SYNC", "TARGET", "TASK", "TASK_REGION",
  "THEN", "TO", "TRACEON", "TRACEOFF", "TRUNC", "TYPE", "TYPE_DECL",
  "UNDER", "UNKNOWN", "USE", "VIRTUAL", "VARIABLE", "WAIT", "WHERE",
  "WHERE_ASSIGN", "WHILE", "WITH", "WRITE", "COMMENT", "WGT_BLOCK",
  "HPF_PROCESSORS", "IOSTAT", "ERR", "END", "OMPDVM_ATOMIC",
  "OMPDVM_BARRIER", "OMPDVM_COPYIN", "OMPDVM_COPYPRIVATE",
  "OMPDVM_CRITICAL", "OMPDVM_ONETHREAD", "OMPDVM_DO", "OMPDVM_DYNAMIC",
  "OMPDVM_ENDCRITICAL", "OMPDVM_ENDDO", "OMPDVM_ENDMASTER",
  "OMPDVM_ENDORDERED", "OMPDVM_ENDPARALLEL", "OMPDVM_ENDPARALLELDO",
  "OMPDVM_ENDPARALLELSECTIONS", "OMPDVM_ENDPARALLELWORKSHARE",
  "OMPDVM_ENDSECTIONS", "OMPDVM_ENDSINGLE", "OMPDVM_ENDWORKSHARE",
  "OMPDVM_FIRSTPRIVATE", "OMPDVM_FLUSH", "OMPDVM_GUIDED",
  "OMPDVM_LASTPRIVATE", "OMPDVM_MASTER", "OMPDVM_NOWAIT", "OMPDVM_NONE",
  "OMPDVM_NUM_THREADS", "OMPDVM_ORDERED", "OMPDVM_PARALLEL",
  "OMPDVM_PARALLELDO", "OMPDVM_PARALLELSECTIONS",
  "OMPDVM_PARALLELWORKSHARE", "OMPDVM_RUNTIME", "OMPDVM_SECTION",
  "OMPDVM_SECTIONS", "OMPDVM_SCHEDULE", "OMPDVM_SHARED", "OMPDVM_SINGLE",
  "OMPDVM_THREADPRIVATE", "OMPDVM_WORKSHARE", "OMPDVM_NODES", "OMPDVM_IF",
  "IAND", "IEOR", "IOR", "ACC_REGION", "ACC_END_REGION",
  "ACC_CHECKSECTION", "ACC_END_CHECKSECTION", "ACC_GET_ACTUAL",
  "ACC_ACTUAL", "ACC_TARGETS", "ACC_ASYNC", "ACC_HOST", "ACC_CUDA",
  "ACC_LOCAL", "ACC_INLOCAL", "ACC_CUDA_BLOCK", "ACC_ROUTINE", "ACC_TIE",
  "BY", "IO_MODE", "CP_CREATE", "CP_LOAD", "CP_SAVE", "CP_WAIT", "FILES",
  "VARLIST", "STATUS", "EXITINTERVAL", "TEMPLATE_CREATE",
  "TEMPLATE_DELETE", "SPF_ANALYSIS", "SPF_PARALLEL", "SPF_TRANSFORM",
  "SPF_NOINLINE", "SPF_PARALLEL_REG", "SPF_END_PARALLEL_REG", "SPF_EXPAND",
  "SPF_FISSION", "SPF_SHRINK", "SPF_CHECKPOINT", "SPF_EXCEPT",
  "SPF_FILES_COUNT", "SPF_INTERVAL", "SPF_TIME", "SPF_ITER",
  "SPF_FLEXIBLE", "SPF_APPLY_REGION", "SPF_APPLY_FRAGMENT",
  "SPF_CODE_COVERAGE", "BINARY_OP", "UNARY_OP", "$accept", "program",
  "stat", "thislabel", "entry", "new_prog", "proc_attr", "procname",
  "funcname", "typedfunc", "opt_result_clause", "name", "progname",
  "blokname", "arglist", "args", "arg", "filename", "needkeyword",
  "keywordoff", "keyword_if_colon_follow", "spec", "interface",
  "defined_op", "operator", "intrinsic_op", "type_dcl", "end_type", "dcl",
  "options", "attr_spec_list", "attr_spec", "intent_spec", "access_spec",
  "intent", "optional", "static", "private", "private_attr", "sequence",
  "public", "public_attr", "type", "opt_key_hedr", "attrib", "att_type",
  "typespec", "typename", "lengspec", "proper_lengspec", "selector",
  "clause", "end_ioctl", "initial_value", "dimension", "allocatable",
  "pointer", "target", "common", "namelist", "namelist_group", "comblock",
  "var", "external", "intrinsic", "equivalence", "equivset", "equivlist",
  "equi_object", "data", "data1", "data_in", "in_data", "datapair",
  "datalvals", "datarvals", "datalval", "data_null", "d_name", "dataname",
  "datasubs", "datarange", "iconexprlist", "opticonexpr", "dataimplieddo",
  "dlist", "dataelt", "datarval", "datavalue", "BOZ_const", "int_const",
  "unsignedint", "real_const", "unsignedreal", "complex_const_data",
  "complex_part", "iconexpr", "iconterm", "iconfactor", "iconprimary",
  "savelist", "saveitem", "use_name_list", "use_key_word",
  "no_use_key_word", "use_name", "paramlist", "paramitem",
  "module_proc_stmt", "proc_name_list", "use_stat", "module_name",
  "only_list", "only_name", "rename_list", "rename_name", "dims",
  "dimlist", "@1", "dim", "ubound", "labellist", "label", "implicit",
  "implist", "impitem", "imptype", "@2", "type_implicit", "letgroups",
  "letgroup", "letter", "inside", "in_dcl", "opt_double_colon",
  "funarglist", "funarg", "funargs", "subscript_list", "expr", "uexpr",
  "addop", "ident", "lhs", "array_ele_substring_func_ref",
  "structure_component", "array_element", "asubstring", "opt_substring",
  "substring", "opt_expr", "simple_const", "numeric_bool_const",
  "integer_constant", "string_constant", "complex_const", "kind",
  "triplet", "vec", "@3", "@4", "allocate_object", "allocation_list",
  "allocate_object_list", "stat_spec", "pointer_name_list", "exec",
  "do_while", "opt_while", "plain_do", "case", "case_selector",
  "case_value_range", "case_value_range_list", "opt_construct_name",
  "opt_unit_name", "construct_name", "construct_name_colon", "logif",
  "forall", "forall_list", "forall_expr", "opt_forall_cond", "do_var",
  "dospec", "dotarget", "whereable", "iffable", "let", "goto", "opt_comma",
  "call", "callarglist", "callarg", "stop", "end_spec", "intonlyon",
  "intonlyoff", "io", "iofmove", "fmkwd", "iofctl", "ctlkwd", "inquire",
  "infmt", "ioctl", "ctllist", "ioclause", "nameeq", "read", "write",
  "print", "inlist", "inelt", "outlist", "out2", "other", "in_ioctl",
  "start_ioctl", "fexpr", "unpar_fexpr", "cmnt", "dvm_specification",
  "dvm_exec", "dvm_template", "template_obj", "dvm_dynamic",
  "dyn_array_name_list", "dyn_array_name", "dvm_inherit",
  "dummy_array_name_list", "dummy_array_name", "dvm_shadow",
  "shadow_attr_stuff", "sh_width_list", "sh_width", "sh_array_name",
  "dvm_processors", "dvm_indirect_group", "indirect_group_name",
  "dvm_remote_group", "remote_group_name", "dvm_reduction_group",
  "reduction_group_name", "dvm_consistent_group", "consistent_group_name",
  "opt_onto", "dvm_distribute", "dvm_redistribute", "dist_name_list",
  "distributee", "dist_name", "pointer_ar_elem", "processors_name",
  "opt_dist_format_clause", "dist_format_clause", "dist_format_list",
  "opt_key_word", "dist_format", "array_name", "derived_spec",
  "derived_elem_list", "derived_elem", "target_spec", "derived_target",
  "derived_subscript_list", "derived_subscript", "dummy_ident",
  "opt_plus_shadow", "plus_shadow", "shadow_id", "shadow_width",
  "dvm_align", "dvm_realign", "realignee_list", "alignee", "realignee",
  "align_directive_stuff", "align_base", "align_subscript_list",
  "align_subscript", "align_base_name", "dim_ident_list", "dim_ident",
  "dvm_combined_dir", "dvm_attribute_list", "dvm_attribute", "dvm_pointer",
  "dimension_list", "@5", "pointer_var_list", "pointer_var", "dvm_heap",
  "heap_array_name_list", "heap_array_name", "dvm_consistent",
  "consistent_array_name_list", "consistent_array_name", "dvm_asyncid",
  "async_id_list", "async_id", "dvm_new_value", "dvm_parallel_on",
  "ident_list", "opt_on", "distribute_cycles", "par_subscript_list",
  "par_subscript", "opt_spec", "spec_list", "par_spec",
  "remote_access_spec", "consistent_spec", "consistent_group", "new_spec",
  "private_spec", "cuda_block_spec", "sizelist", "variable_list",
  "tie_spec", "tied_array_list", "indirect_access_spec", "stage_spec",
  "across_spec", "in_out_across", "opt_keyword_in_out", "opt_in_out",
  "dependent_array_list", "dependent_array", "dependence_list",
  "dependence", "section_spec_list", "section_spec", "ar_section",
  "low_section", "high_section", "section", "reduction_spec",
  "opt_key_word_r", "no_opt_key_word_r", "reduction_group",
  "reduction_list", "reduction", "reduction_op", "loc_op", "shadow_spec",
  "shadow_group_name", "shadow_list", "shadow", "opt_corner",
  "array_ident", "array_ident_list", "dvm_shadow_start", "dvm_shadow_wait",
  "dvm_shadow_group", "dvm_reduction_start", "dvm_reduction_wait",
  "dvm_consistent_start", "dvm_consistent_wait", "dvm_remote_access",
  "group_name", "remote_data_list", "remote_data", "remote_index_list",
  "remote_index", "dvm_task", "task_array", "dvm_task_region", "task_name",
  "dvm_end_task_region", "task", "dvm_on", "opt_new_spec", "dvm_end_on",
  "dvm_map", "dvm_prefetch", "dvm_reset", "dvm_indirect_access",
  "indirect_list", "indirect_reference", "hpf_independent",
  "hpf_reduction_spec", "dvm_asynchronous", "dvm_endasynchronous",
  "dvm_asyncwait", "async_ident", "async", "dvm_f90", "dvm_debug_dir",
  "debparamlist", "debparam", "fragment_number", "dvm_enddebug_dir",
  "dvm_interval_dir", "interval_number", "dvm_exit_interval_dir",
  "dvm_endinterval_dir", "dvm_traceon_dir", "dvm_traceoff_dir",
  "dvm_barrier_dir", "dvm_check", "dvm_io_mode_dir", "mode_list",
  "mode_spec", "dvm_shadow_add", "template_ref", "shadow_axis_list",
  "shadow_axis", "opt_include_to", "dvm_localize", "localize_target",
  "target_subscript_list", "target_subscript", "aster_expr",
  "dvm_cp_create", "opt_mode", "dvm_cp_load", "dvm_cp_save", "dvm_cp_wait",
  "dvm_template_create", "template_list", "dvm_template_delete",
  "omp_specification_directive", "omp_execution_directive",
  "ompdvm_onethread", "omp_parallel_end_directive",
  "omp_parallel_begin_directive", "parallel_clause_list",
  "parallel_clause", "omp_variable_list_in_par", "ompprivate_clause",
  "ompfirstprivate_clause", "omplastprivate_clause", "ompcopyin_clause",
  "ompshared_clause", "ompdefault_clause", "def_expr", "ompif_clause",
  "ompnumthreads_clause", "ompreduction_clause", "ompreduction",
  "ompreduction_vars", "ompreduction_op", "omp_sections_begin_directive",
  "sections_clause_list", "sections_clause", "omp_sections_end_directive",
  "omp_section_directive", "omp_do_begin_directive",
  "omp_do_end_directive", "do_clause_list", "do_clause",
  "ompordered_clause", "ompschedule_clause", "ompschedule_op",
  "omp_single_begin_directive", "single_clause_list", "single_clause",
  "omp_single_end_directive", "end_single_clause_list",
  "end_single_clause", "ompcopyprivate_clause", "ompnowait_clause",
  "omp_workshare_begin_directive", "omp_workshare_end_directive",
  "omp_parallel_do_begin_directive", "paralleldo_clause_list",
  "paralleldo_clause", "omp_parallel_do_end_directive",
  "omp_parallel_sections_begin_directive",
  "omp_parallel_sections_end_directive",
  "omp_parallel_workshare_begin_directive",
  "omp_parallel_workshare_end_directive", "omp_threadprivate_directive",
  "omp_master_begin_directive", "omp_master_end_directive",
  "omp_ordered_begin_directive", "omp_ordered_end_directive",
  "omp_barrier_directive", "omp_atomic_directive", "omp_flush_directive",
  "omp_critical_begin_directive", "omp_critical_end_directive",
  "omp_common_var", "omp_variable_list", "op_slash_1", "op_slash_0",
  "acc_directive", "acc_region", "acc_checksection", "acc_get_actual",
  "acc_actual", "opt_clause", "acc_clause_list", "acc_clause",
  "data_clause", "targets_clause", "async_clause", "acc_var_list",
  "computer_list", "computer", "acc_end_region", "acc_end_checksection",
  "acc_routine", "opt_targets_clause", "spf_directive", "spf_analysis",
  "spf_parallel", "spf_transform", "spf_parallel_reg",
  "characteristic_list", "characteristic", "opt_clause_apply_fragment",
  "opt_clause_apply_region", "spf_end_parallel_reg", "analysis_spec_list",
  "analysis_spec", "analysis_reduction_spec", "analysis_private_spec",
  "analysis_parameter_spec", "spf_parameter_list", "spf_parameter",
  "parallel_spec_list", "parallel_spec", "parallel_shadow_spec",
  "parallel_across_spec", "parallel_remote_access_spec",
  "transform_spec_list", "transform_spec", "region_name",
  "array_element_list", "spf_checkpoint", "checkpoint_spec_list",
  "checkpoint_spec", "spf_type_list", "spf_type", "interval_spec",
  "in_unit", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   352,   353,     1,     2,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   293,   294,   295,   296,   297,
     298,   299,   300,   301,   302,   303,   304,   305,   306,   307,
     308,   309,   310,   311,   312,   313,   314,   315,   316,   317,
     318,   319,   320,   321,   322,   323,   324,   325,   326,   327,
     328,   329,   330,   331,   332,   333,   334,   335,   336,   337,
     338,   339,   340,   341,   342,   343,   344,   345,   346,   347,
     348,   349,   350,   351,   354,   355
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   356,   357,   357,   358,   358,   358,   358,   358,   358,
     358,   359,   360,   360,   360,   360,   360,   360,   360,   360,
     361,   362,   362,   362,   363,   364,   365,   365,   365,   365,
     366,   366,   367,   368,   368,   369,   369,   370,   370,   370,
     371,   371,   372,   372,   373,   374,   375,   376,   377,   377,
     377,   377,   377,   377,   377,   377,   377,   377,   377,   377,
     377,   377,   377,   377,   377,   377,   377,   377,   377,   377,
     377,   377,   377,   377,   377,   377,   377,   378,   378,   378,
     378,   378,   379,   380,   380,   381,   381,   381,   381,   381,
     381,   381,   381,   381,   381,   381,   381,   381,   381,   381,
     381,   381,   381,   382,   382,   383,   383,   384,   384,   385,
     385,   385,   386,   386,   387,   387,   387,   387,   387,   387,
     387,   387,   387,   387,   387,   387,   388,   388,   388,   389,
     389,   390,   390,   391,   391,   392,   392,   393,   393,   394,
     395,   396,   396,   397,   398,   398,   399,   400,   400,   401,
     401,   402,   403,   403,   403,   403,   403,   403,   403,   404,
     404,   405,   405,   405,   406,   406,   406,   406,   407,   407,
     407,   407,   408,   409,   409,   409,   410,   410,   411,   411,
     412,   412,   413,   413,   414,   414,   414,   414,   415,   415,
     415,   416,   417,   417,   418,   419,   419,   420,   420,   421,
     421,   422,   423,   423,   424,   424,   424,   425,   426,   426,
     427,   428,   429,   430,   430,   431,   431,   432,   432,   432,
     432,   432,   433,   434,   435,   436,   437,   438,   438,   439,
     439,   440,   441,   441,   442,   442,   442,   442,   443,   443,
     443,   444,   444,   444,   444,   444,   444,   444,   444,   444,
     444,   444,   445,   446,   446,   446,   447,   447,   448,   448,
     448,   449,   449,   449,   449,   450,   451,   451,   452,   452,
     452,   452,   452,   453,   453,   453,   454,   454,   455,   455,
     455,   456,   456,   457,   457,   458,   458,   459,   460,   461,
     461,   461,   462,   462,   463,   464,   465,   465,   466,   466,
     466,   466,   467,   468,   468,   469,   469,   470,   470,   471,
     472,   472,   474,   473,   473,   475,   475,   475,   475,   476,
     476,   477,   477,   478,   479,   479,   480,   480,   481,   483,
     482,   484,   484,   485,   485,   486,   486,   487,   488,   489,
     490,   490,   491,   491,   492,   492,   492,   493,   493,   494,
     494,   495,   495,   495,   496,   496,   496,   496,   496,   496,
     496,   496,   496,   496,   496,   496,   496,   496,   496,   496,
     496,   496,   496,   496,   496,   496,   496,   496,   496,   497,
     497,   498,   499,   499,   499,   500,   500,   500,   500,   501,
     502,   502,   502,   502,   503,   504,   504,   505,   506,   506,
     507,   507,   507,   507,   507,   508,   508,   508,   508,   509,
     510,   510,   510,   511,   512,   512,   513,   513,   513,   513,
     513,   513,   513,   513,   515,   516,   514,   517,   517,   518,
     518,   518,   519,   519,   519,   520,   521,   521,   522,   522,
     522,   522,   522,   522,   522,   522,   522,   522,   522,   522,
     522,   522,   522,   522,   522,   522,   522,   522,   522,   522,
     522,   522,   523,   523,   524,   524,   524,   525,   525,   526,
     526,   526,   526,   526,   527,   528,   528,   528,   528,   529,
     529,   530,   530,   531,   531,   532,   533,   534,   535,   536,
     536,   537,   538,   538,   539,   540,   540,   541,   541,   542,
     542,   542,   542,   542,   543,   543,   543,   543,   543,   543,
     543,   543,   543,   543,   543,   543,   543,   543,   543,   543,
     543,   543,   544,   545,   545,   545,   545,   546,   546,   547,
     548,   548,   549,   549,   549,   550,   550,   551,   552,   553,
     554,   554,   554,   554,   554,   554,   554,   554,   554,   554,
     554,   554,   554,   554,   555,   556,   556,   556,   557,   558,
     558,   559,   559,   560,   560,   561,   561,   562,   562,   563,
     563,   563,   563,   563,   563,   564,   565,   566,   567,   567,
     568,   568,   569,   569,   570,   570,   570,   571,   571,   571,
     571,   571,   571,   572,   572,   572,   572,   572,   573,   574,
     575,   575,   576,   576,   576,   576,   576,   576,   576,   576,
     576,   577,   577,   578,   578,   578,   578,   578,   578,   578,
     578,   578,   578,   578,   578,   578,   578,   578,   578,   578,
     578,   578,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   580,   580,   581,   582,
     583,   583,   584,   585,   586,   586,   587,   588,   589,   590,
     590,   591,   591,   591,   592,   593,   593,   593,   594,   594,
     595,   596,   596,   597,   598,   598,   599,   600,   600,   601,
     602,   602,   603,   604,   604,   605,   605,   606,   606,   607,
     608,   609,   610,   610,   611,   612,   612,   613,   614,   614,
     614,   614,   614,   614,   614,   614,   615,   616,   617,   617,
     618,   618,   619,   619,   620,   621,   621,   622,   622,   622,
     623,   624,   624,   625,   625,   626,   627,   627,   628,   629,
     629,   630,   630,   631,   632,   633,   634,   635,   635,   636,
     636,   636,   637,   638,   638,   639,   639,   639,   640,   640,
     641,   641,   642,   642,   642,   642,   642,   642,   642,   642,
     642,   642,   642,   643,   645,   644,   644,   646,   646,   647,
     648,   649,   649,   650,   651,   652,   652,   653,   654,   654,
     655,   655,   656,   657,   658,   659,   659,   660,   660,   661,
     662,   662,   663,   663,   664,   664,   665,   665,   666,   666,
     666,   666,   666,   666,   666,   666,   666,   666,   666,   667,
     667,   668,   668,   669,   670,   670,   671,   672,   673,   673,
     673,   674,   674,   675,   676,   676,   677,   677,   678,   679,
     679,   680,   681,   682,   682,   682,   683,   683,   684,   684,
     684,   685,   685,   686,   687,   687,   688,   688,   688,   688,
     688,   688,   688,   689,   690,   691,   692,   693,   693,   693,
     694,   695,   696,   697,   697,   698,   698,   699,   699,   699,
     699,   699,   699,   699,   699,   699,   700,   700,   701,   701,
     701,   701,   701,   702,   703,   703,   704,   704,   704,   704,
     705,   706,   707,   707,   708,   708,   709,   709,   710,   711,
     712,   713,   714,   715,   715,   716,   717,   717,   718,   718,
     719,   719,   720,   720,   721,   721,   722,   723,   723,   723,
     723,   723,   724,   725,   726,   726,   727,   728,   728,   729,
     730,   730,   731,   732,   733,   733,   734,   734,   735,   735,
     736,   736,   736,   736,   737,   738,   739,   740,   741,   742,
     742,   743,   744,   744,   745,   745,   746,   747,   748,   749,
     750,   750,   751,   752,   753,   754,   755,   756,   757,   758,
     758,   759,   759,   759,   760,   761,   762,   762,   763,   763,
     764,   764,   765,   766,   766,   767,   767,   768,   768,   769,
     770,   771,   771,   771,   772,   773,   773,   774,   775,   776,
     776,   777,   778,   779,   779,   779,   779,   779,   779,   779,
     779,   779,   779,   779,   779,   779,   779,   779,   779,   779,
     779,   779,   779,   779,   779,   779,   779,   779,   779,   780,
     781,   782,   782,   783,   783,   784,   784,   784,   784,   784,
     784,   784,   784,   784,   785,   786,   787,   788,   789,   790,
     791,   792,   792,   792,   793,   794,   795,   796,   797,   798,
     798,   798,   798,   798,   798,   798,   798,   798,   798,   798,
     798,   798,   798,   799,   799,   800,   800,   801,   801,   801,
     801,   802,   802,   803,   804,   804,   805,   805,   806,   806,
     807,   807,   807,   807,   807,   807,   808,   809,   809,   810,
     810,   810,   810,   811,   811,   812,   812,   813,   813,   814,
     814,   815,   815,   816,   816,   817,   818,   819,   820,   820,
     821,   821,   822,   822,   823,   823,   823,   823,   823,   823,
     823,   823,   823,   823,   823,   824,   825,   825,   826,   827,
     827,   828,   829,   830,   831,   832,   833,   834,   835,   836,
     836,   837,   837,   838,   838,   839,   840,   840,   840,   840,
     841,   842,   843,   843,   843,   843,   843,   843,   843,   844,
     845,   846,   846,   846,   847,   847,   847,   848,   848,   849,
     849,   850,   850,   850,   851,   851,   851,   851,   851,   852,
     853,   854,   855,   855,   856,   856,   857,   858,   859,   860,
     860,   861,   861,   861,   861,   861,   861,   862,   863,   864,
     865,   865,   865,   866,   866,   867,   868,   868,   869,   869,
     870,   871,   871,   872,   872,   872,   873,   874,   875,   876,
     876,   877,   878,   878,   879,   879,   879,   880,   881,   882,
     883,   883,   884,   884,   884,   884,   884,   885,   886,   886,
     887,   888,   888,   889,   889,   889,   889,   889,   890,   890,
     891,   891,   892,   892,   893
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     3,     3,     3,     3,     3,     2,     1,
       1,     1,     3,     3,     4,     5,     5,     3,     4,     3,
       0,     2,     2,     2,     1,     1,     4,     5,     4,     5,
       2,     5,     1,     0,     1,     0,     1,     0,     2,     3,
       1,     3,     1,     1,     1,     0,     0,     0,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     4,     2,     5,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     2,     3,     5,
       5,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     4,     7,     2,     3,     7,     6,     0,
       2,     5,     1,     4,     1,     1,     1,     2,     1,     4,
       1,     1,     1,     1,     1,     1,     2,     2,     2,     1,
       1,     7,     3,     4,     3,     4,     3,     2,     5,     0,
       2,     2,     5,     0,     4,     5,     0,     2,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       1,     5,     6,     6,     0,     1,     5,     9,     1,     1,
       2,     2,     0,     0,     2,     2,     5,     4,     4,     3,
       4,     3,     4,     3,     3,     4,     5,     3,     4,     5,
       3,     3,     1,     3,     2,     4,     3,     4,     3,     3,
       3,     3,     3,     3,     1,     4,     1,     1,     4,     3,
       0,     0,     4,     1,     3,     1,     3,     2,     3,     3,
       4,     2,     0,     1,     1,     3,     5,     1,     3,     0,
       1,     7,     1,     3,     2,     2,     3,     1,     1,     4,
       3,     2,     1,     1,     1,     1,     3,     1,     3,     1,
       1,     6,     1,     1,     2,     2,     1,     3,     1,     2,
       2,     1,     3,     1,     3,     5,     1,     1,     1,     2,
       2,     3,     3,     1,     3,     3,     1,     3,     1,     1,
       3,     1,     3,     1,     1,     3,     5,     0,     0,     1,
       4,     4,     1,     3,     3,     2,     1,     3,     3,     6,
       6,     7,     1,     1,     3,     1,     1,     1,     3,     3,
       0,     3,     0,     2,     3,     1,     1,     2,     3,     1,
       1,     1,     3,     1,     3,     1,     1,     3,     4,     0,
       2,     2,     1,     1,     3,     1,     3,     1,     0,     0,
       0,     2,     0,     1,     1,     1,     2,     2,     4,     1,
       3,     1,     3,     1,     1,     1,     1,     3,     3,     3,
       3,     3,     2,     2,     2,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     2,     3,     3,     1,
       1,     1,     1,     1,     1,     5,     6,     4,     5,     3,
       1,     1,     5,     4,     2,     0,     1,     5,     0,     1,
       1,     3,     1,     3,     2,     1,     1,     1,     1,     1,
       1,     3,     3,     5,     1,     1,     3,     2,     5,     4,
       4,     3,     2,     1,     0,     0,     6,     1,     1,     1,
       4,     5,     1,     4,     5,     0,     1,     3,     1,     1,
       1,     2,     3,     3,     2,     1,     2,     2,     2,     3,
       7,     3,     3,     1,     2,     2,     1,     2,     3,     1,
       1,     1,     5,     7,     0,     6,     4,    11,    13,     4,
       3,     3,     7,     8,     3,     1,     2,     2,     3,     1,
       3,     0,     1,     0,     1,     1,     2,     5,     6,     1,
       3,     3,     0,     2,     1,     5,     7,     0,     1,     3,
       3,     6,     5,     6,     4,     5,     5,     2,     1,     1,
      10,     1,     3,     4,     3,     3,     3,     3,     6,     6,
       5,     8,     2,     3,     3,     7,     7,     0,     1,     4,
       2,     4,     1,     2,     2,     1,     1,     0,     0,     0,
       2,     2,     2,     2,     2,     1,     2,     2,     3,     4,
       2,     3,     1,     3,     3,     1,     1,     1,     3,     1,
       1,     4,     5,     1,     1,     3,     3,     1,     4,     1,
       1,     1,     2,     2,     2,     1,     3,     3,     4,     4,
       1,     3,     1,     5,     1,     1,     1,     3,     3,     3,
       3,     3,     3,     1,     3,     5,     5,     5,     0,     0,
       1,     3,     1,     1,     3,     3,     3,     3,     2,     3,
       3,     0,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     3,     3,     2,     3,
       1,     3,     1,     3,     1,     3,     1,     4,     3,     1,
       3,     1,     3,     4,     1,     4,     4,     4,     3,     3,
       1,     3,     3,     1,     3,     3,     1,     3,     3,     1,
       3,     0,     5,     6,     8,     1,     3,     1,     1,     1,
       4,     1,     0,     2,     3,     2,     4,     0,     1,     5,
       4,     6,     4,     1,     4,     4,     1,     6,     1,     3,
       1,     3,     1,     4,     1,     1,     3,     1,     1,     3,
       1,     0,     1,     2,     3,     1,     2,     5,     4,     4,
       6,     1,     3,     1,     1,     6,     4,     1,     3,     1,
       1,     1,     1,     1,     3,     1,     1,     1,     6,     4,
       1,     4,     1,     1,     1,     1,     4,     2,     7,     1,
       4,     1,     1,    11,     0,     2,     3,     1,     3,     1,
       3,     1,     3,     1,     3,     1,     3,     1,     3,     8,
       1,     3,     2,     2,     7,     1,     3,     3,     1,     4,
       1,     3,     1,     1,     0,     1,     1,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     8,
       6,     8,     6,     1,     6,     6,     6,     6,     1,     3,
       5,     1,     3,     6,     1,     3,     8,     6,     6,     4,
       5,     5,     0,     2,     2,     0,     1,     3,     1,     4,
       7,     1,     3,     3,     1,     3,     5,     3,     3,     1,
       3,     1,     1,     3,     3,     3,     3,    10,     8,    10,
       0,     0,     1,     2,     4,     4,     6,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     6,     4,
       4,     3,     9,     1,     1,     3,     1,     5,     5,     9,
       0,     1,     1,     3,     3,     3,     3,     3,     6,     3,
       3,     3,     3,     7,     5,     1,     1,     3,     4,     1,
       1,     3,     1,     1,     3,     3,     2,     3,     4,     4,
       5,     5,     1,     2,     4,     4,     4,     0,     1,     2,
       7,     6,     3,     3,     7,     5,     1,     3,     1,     4,
       2,     3,     3,     4,     6,     3,     2,     3,     1,     1,
       4,     5,     3,     6,     2,     4,     2,     1,     3,     3,
       0,     1,     3,     2,     2,     2,     2,     9,     5,     1,
       3,     2,     2,     2,     9,     4,     1,     3,     1,     1,
       2,     0,     7,     1,     4,     1,     3,     1,     1,     1,
      16,     0,     3,     3,     3,     3,     6,     9,     5,     1,
       3,     5,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     2,
       2,     4,     3,     4,     5,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     5,     2,     2,     2,     2,     2,
       5,     1,     1,     1,     4,     4,     4,     4,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     4,     3,     4,     5,     1,     1,     1,
       1,     4,     3,     2,     4,     3,     4,     3,     4,     5,
       1,     1,     1,     1,     1,     1,     1,     7,     5,     1,
       1,     1,     1,     4,     3,     4,     5,     1,     1,     4,
       3,     4,     5,     1,     1,     2,     1,     2,     4,     3,
       4,     3,     4,     5,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     2,     4,     3,     2,     4,
       3,     2,     3,     2,     2,     2,     2,     2,     2,     3,
       2,     5,     2,     5,     2,     5,     1,     1,     3,     3,
       0,     0,     1,     1,     1,     1,     1,     1,     1,     3,
       2,     5,     4,     2,     5,     4,     2,     2,     1,     1,
       3,     2,     2,     2,     4,     4,     4,     4,     4,     4,
       1,     1,     1,     3,     2,     2,     1,     1,     3,     2,
       2,     1,     1,     1,     1,     1,     1,     5,     5,     5,
       3,    10,    10,     1,     3,     2,     0,     6,     0,     6,
       2,     1,     3,     1,     1,     1,     5,     5,     5,     1,
       3,     3,     1,     3,     1,     1,     1,     5,     5,     5,
       1,     3,     2,     5,     2,     5,     5,     1,     1,     3,
       5,     1,     3,     5,     5,     5,     5,     7,     1,     3,
       2,     2,     2,     2,     0
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       2,     0,     1,    10,    11,     9,     0,     0,     3,   150,
     149,   789,   339,   537,   537,   537,   537,   537,   339,   537,
     555,   537,    20,   537,   537,   158,   537,   560,   339,   154,
     537,   339,   537,   537,   339,   485,   537,   537,   537,   338,
     537,   782,   537,   537,   340,   791,   537,   155,   156,   785,
      45,   537,   537,   537,   537,   537,   537,   537,   537,   557,
     537,   537,   483,   483,   537,   537,   537,   339,   537,     0,
     537,   339,   339,   537,   537,   338,    20,   339,   339,   325,
       0,   537,   537,   339,   339,   537,   339,   339,   339,   339,
     152,   339,   537,   211,   537,   157,   537,   537,     0,    20,
     339,   537,   537,   537,   559,   339,   537,   339,   535,   537,
     537,   339,   537,   537,   537,   339,    20,   339,    45,   537,
     537,   153,    45,   537,   339,   537,   537,   537,   339,   537,
     537,   556,   339,   537,   339,   537,   537,   537,   537,   537,
     537,   339,   339,   536,    20,   339,   339,   537,   537,   537,
       0,   339,     8,   339,   537,   537,   537,   783,   537,   537,
     537,   537,   537,   537,   537,   537,   537,   537,   537,   537,
     537,   537,   537,   537,   537,   537,   537,   537,   537,   537,
     537,   537,   537,   537,   339,   537,   784,   537,  1226,   537,
    1227,   537,   537,   339,   537,   537,   537,   537,   537,   537,
     537,   537,  1294,  1294,  1294,  1294,  1294,  1294,   611,     0,
      37,   611,    73,    48,    49,    50,    65,    66,    76,    68,
      69,    67,   109,    58,     0,   146,   151,    52,    70,    71,
      72,    51,    59,    54,    55,    56,    60,   207,    75,    74,
      57,   611,   445,   440,   453,     0,     0,     0,   456,   439,
     438,     0,   508,   511,   537,   509,     0,   537,     0,   537,
     545,     0,     0,   552,    53,   459,   613,   616,   622,   618,
     617,   623,   624,   625,   626,   615,   632,   614,   633,   619,
       0,   780,   620,   627,   629,   628,   660,   634,   636,   637,
     635,   638,   639,   640,   641,   642,   621,   643,   644,   646,
     647,   645,   649,   650,   648,   674,   661,   662,   663,   664,
     651,   652,   653,   655,   654,   656,   657,   658,   659,   665,
     666,   667,   668,   669,   670,   671,   672,   673,   631,   675,
     630,  1034,  1033,  1035,  1036,  1037,  1038,  1039,  1040,  1041,
    1042,  1043,  1044,  1045,  1046,  1047,  1048,  1049,  1032,  1050,
    1051,  1052,  1053,  1054,  1055,  1056,  1057,  1058,   460,  1192,
    1194,  1196,  1197,  1193,  1195,  1198,   461,  1231,  1232,  1233,
    1234,  1235,  1236,     0,     0,   340,     0,     0,     0,     0,
       0,     0,     0,   996,    35,     0,     0,   598,     0,     0,
       0,     0,     0,     0,   454,   507,   481,   210,     0,     0,
       0,   481,     0,   312,   339,   727,     0,   727,   538,     0,
      23,   481,     0,   481,   976,     0,   993,   483,   481,   481,
     481,    32,   484,    81,   444,   959,   481,   953,   105,   481,
      37,   481,     0,   340,     0,     0,    63,     0,     0,   329,
      44,     7,   970,     0,     0,     0,   599,     0,     0,    77,
     340,     0,   990,   522,     0,     0,     0,   296,   295,     0,
       0,   813,     0,     0,   340,     0,     0,   538,     0,   340,
       0,     0,     0,   340,    33,   340,    22,   599,     0,    21,
       0,     0,     0,     0,     0,     0,     0,   398,   340,    45,
     140,     0,     0,     0,     0,     0,     0,     0,     0,   787,
     340,     0,   340,     0,     0,   994,   995,     0,   339,   340,
       0,     0,     0,   599,     0,  1178,  1177,  1182,  1059,   727,
    1184,   727,  1174,  1176,  1060,  1165,  1168,  1171,   727,   727,
     727,  1180,  1173,  1175,   727,   727,   727,   727,  1113,   727,
     727,  1190,  1147,     0,    45,  1200,  1203,  1206,    45,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1250,     0,   612,     4,    20,    20,     0,     0,    45,
       5,     0,     0,     0,     0,     0,    45,    20,     0,     0,
       0,   147,   164,     0,     0,     0,     0,   528,     0,   528,
       0,     0,     0,     0,   528,   222,     6,   486,   537,   537,
     446,   441,     0,   457,   448,   447,   455,    82,   172,     0,
       0,     0,   406,     0,   405,   410,   408,   409,   407,   381,
       0,     0,   351,   382,   354,   384,   383,   355,   400,   402,
     395,   353,   356,   598,   398,   542,   543,     0,   380,   379,
      32,     0,   602,   603,   540,     0,   600,   599,     0,   544,
     599,   564,   547,   546,   600,   550,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    45,     0,   776,   777,   775,
       0,   773,   763,     0,     0,   435,     0,   323,     0,   524,
     978,   979,   975,    45,   310,   808,   810,   977,    36,    13,
     598,     0,   481,     0,   192,     0,   310,     0,   184,     0,
     709,   707,   843,   931,   932,   807,   804,   805,   482,   516,
     222,   435,   310,   676,   987,   982,   470,   341,     0,     0,
       0,     0,     0,   719,   722,   711,     0,   497,   682,   679,
     680,   451,     0,     0,   500,   988,   442,   443,   458,   452,
     471,   106,   499,    45,   517,     0,   199,     0,   382,     0,
       0,    37,    25,   803,   800,   801,   324,   326,     0,     0,
      45,   971,   972,     0,   700,   698,   686,   683,   684,     0,
       0,     0,    78,     0,    45,   991,   989,     0,     0,   952,
       0,    45,     0,    19,     0,     0,     0,     0,   957,     0,
       0,     0,   497,   523,     0,     0,   935,   962,   599,     0,
     599,   600,   139,    34,    12,   143,   576,     0,   764,     0,
       0,     0,   727,   706,   704,   892,   929,   930,     0,   703,
     701,   963,   399,   514,     0,     0,     0,   913,     0,   925,
     924,   927,   926,     0,   691,     0,   689,   694,     0,     0,
      37,    24,     0,   310,   944,   947,     0,    45,     0,   302,
     298,     0,     0,   577,   310,     0,   527,     0,  1117,  1112,
     527,  1149,  1179,     0,   527,   527,   527,   527,   527,   527,
    1172,   310,    46,  1199,  1208,  1209,     0,     0,    46,  1228,
      45,     0,  1024,  1025,     0,   992,   349,     0,     0,    45,
      45,    45,  1277,  1240,    45,     0,     0,    20,    43,    38,
      42,     0,    40,    17,    46,   310,   132,   134,   136,   110,
       0,     0,    20,   339,   148,   538,   598,   165,   146,   310,
     179,   181,   183,   187,   527,   190,   527,   196,   198,   200,
     209,     0,   213,     0,    45,     0,   449,   424,     0,   351,
     364,   363,   376,   362,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   598,     0,     0,   598,     0,     0,   398,
     404,   396,   512,     0,     0,   515,   570,   571,   575,     0,
     567,     0,   569,     0,   608,     0,     0,     0,     0,     0,
     554,   569,   558,     0,     0,   582,   548,   580,     0,     0,
     351,   353,   551,   586,   585,   553,   677,   310,   699,   702,
     705,   708,   310,   339,     0,   945,     0,    45,   758,   178,
       0,     0,     0,     0,     0,     0,   312,   812,     0,   529,
       0,   475,   479,     0,   469,   598,     0,   194,   185,     0,
     321,     0,   208,     0,   678,   598,     0,   786,   319,   316,
     313,   315,   320,   310,   727,   724,   733,   728,     0,     0,
       0,     0,     0,   725,   711,   727,     0,   790,     0,   498,
     539,     0,     0,     0,    18,   204,     0,     0,     0,   206,
     195,     0,   494,   492,   489,     0,    45,     0,   329,     0,
       0,   332,   330,     0,    45,   973,   381,   921,   968,     0,
       0,   966,     0,   561,     0,    87,    88,    86,    85,    91,
      90,   102,    95,    98,    97,   100,    99,    96,   101,    94,
      92,    89,    93,    83,     0,    84,   197,     0,     0,     0,
       0,     0,     0,   297,     0,   188,   436,     0,    45,   958,
     956,   133,   815,     0,     0,     0,   292,   539,   180,     0,
     579,     0,   578,   287,   287,     0,   759,     0,   727,   711,
     939,     0,     0,   936,   284,   283,    62,   281,     0,     0,
       0,     0,     0,     0,     0,   688,   687,   135,    14,   182,
     946,    45,   949,   948,   146,     0,   103,    47,     0,     0,
     695,     0,   727,   527,     0,  1146,  1116,  1111,   727,   527,
    1148,  1191,   727,   527,   727,   527,   527,   527,   727,   527,
     727,   527,   696,     0,     0,     0,     0,  1220,     0,     0,
    1207,  1211,  1213,  1212,    45,  1202,   851,  1221,     0,  1205,
       0,  1229,  1230,     0,     0,   999,    45,    45,    45,     0,
     390,   391,  1029,     0,     0,     0,     0,  1251,  1253,  1254,
    1255,     0,     0,  1262,  1264,  1265,  1266,     0,     0,  1270,
      45,     0,     0,  1281,    28,    37,     0,     0,    39,     0,
      30,   159,   116,   310,   339,   118,   120,     0,   121,   114,
     122,   130,   129,   123,   124,   125,     0,   112,   115,    26,
       0,   310,     0,     0,   144,   177,     0,     0,   222,   222,
       0,   224,   217,   221,     0,     0,     0,   352,     0,   359,
     361,   358,   357,   375,   377,   371,   365,   504,   368,   366,
     369,   367,   370,   372,   374,   360,   373,   378,   598,   411,
     389,     0,   343,     0,   414,   415,   401,   412,   403,     0,
     598,   513,     0,   532,   530,     0,   598,   566,   573,   574,
     572,   601,   610,   605,   607,   609,   606,   604,   565,   549,
       0,     0,     0,   351,     0,     0,     0,     0,     0,   697,
     779,     0,   789,   792,   782,     0,   791,   785,     0,   783,
     784,   781,   774,     0,   429,     0,     0,   506,     0,     0,
       0,     0,   811,   477,   476,     0,   474,     0,   193,     0,
     527,   806,   427,   428,   432,     0,     0,     0,   314,   317,
     176,     0,   598,     0,     0,     0,     0,     0,   712,   723,
     310,   462,   727,   681,     0,   481,     0,     0,   201,     0,
     394,   981,     0,     0,     0,    16,   802,   327,   337,     0,
     333,   335,   331,     0,     0,     0,     0,     0,     0,     0,
     965,   685,   562,    80,    79,   128,   126,   127,   340,     0,
     487,   423,     0,     0,     0,     0,   191,     0,   520,     0,
       0,   727,     0,     0,    64,   527,   505,   601,   138,     0,
     142,    45,     0,   711,     0,     0,     0,     0,   934,     0,
       0,     0,     0,     0,   914,   916,     0,   692,   690,     0,
      45,   951,    45,   950,   145,   340,     0,   502,     0,  1181,
       0,   727,  1183,     0,   727,     0,     0,   727,     0,   727,
       0,   727,     0,   727,     0,     0,     0,    45,     0,     0,
       0,  1210,     0,  1201,  1204,  1003,  1001,  1002,    45,   998,
       0,     0,     0,   350,   598,   598,     0,  1028,  1031,     0,
       0,     0,    45,  1237,     0,     0,     0,    45,  1238,  1272,
    1274,     0,     0,    45,  1239,     0,     0,     0,     0,     0,
       0,    45,  1280,    15,    29,    41,     0,   173,   160,   117,
       0,    45,     0,    45,    27,   159,   539,   539,   169,   172,
     168,     0,   186,   189,   214,     0,     0,     0,   247,   245,
     252,   249,   263,   256,   261,     0,     0,   215,   238,   250,
     242,   253,   243,   258,   244,     0,   237,     0,   232,   229,
     218,   219,     0,     0,   425,   351,     0,   387,   598,   347,
     344,   345,     0,   398,     0,   534,   533,     0,     0,   581,
     352,     0,     0,     0,   351,   588,   351,   592,   351,   590,
     310,     0,   598,   518,     0,     0,   980,     0,   311,   478,
     480,   172,   322,     0,   598,   519,     0,   984,   598,   983,
     318,   320,   726,     0,     0,     0,   736,     0,     0,     0,
       0,   710,   464,   481,   501,     0,   203,   202,   381,   493,
     490,   488,     0,   491,     0,   328,     0,     0,     0,     0,
       0,     0,   967,     0,  1013,     0,     0,   422,   417,   954,
     955,   721,   310,   961,   437,   816,   818,   824,   294,   293,
       0,   287,     0,     0,   289,   288,     0,   760,   761,   713,
       0,   943,   942,     0,   940,     0,   937,   282,     0,  1019,
    1008,     0,  1006,  1009,   755,     0,     0,   928,   920,   693,
       0,     0,     0,     0,     0,   300,     0,   299,   307,     0,
    1190,     0,  1190,  1190,  1126,     0,  1120,  1122,  1123,  1121,
     727,  1125,  1124,     0,  1190,   727,  1144,  1143,     0,     0,
    1187,  1186,     0,     0,  1190,     0,  1190,     0,   727,  1065,
    1069,  1070,  1071,  1067,  1068,  1072,  1073,  1066,     0,  1154,
    1158,  1159,  1160,  1156,  1157,  1161,  1162,  1155,  1164,  1163,
     727,     0,  1107,  1109,  1110,  1108,   727,     0,  1137,  1138,
     727,     0,     0,     0,     0,     0,     0,  1222,     0,     0,
     852,  1000,     0,  1026,     0,   598,     0,  1030,     0,     0,
      45,  1252,     0,     0,     0,  1263,     0,     0,     0,  1271,
       0,     0,    45,     0,     0,     0,    45,  1282,     0,     0,
       0,   108,   794,     0,   111,     0,   173,     0,   146,     0,
     171,   170,   267,   253,   266,     0,   255,   260,   254,   259,
       0,     0,     0,     0,     0,   222,   212,   223,   241,     0,
     222,   234,   235,     0,     0,     0,     0,   278,   223,   279,
       0,     0,   227,   268,   273,   276,   229,   220,     0,   503,
       0,   413,   385,   388,     0,   346,     0,   531,   568,   569,
       0,     0,   351,     0,     0,     0,   778,   772,   788,     0,
       0,     0,   525,     0,   340,   526,     0,   986,     0,     0,
       0,   740,     0,   738,   735,   730,   734,   732,     0,    45,
       0,   463,   450,   205,   334,   336,     0,     0,     0,   969,
     964,   131,     0,  1012,   421,     0,     0,   416,   960,     0,
      45,   814,   825,   826,   831,   835,   828,   836,   837,   838,
     832,   834,   833,   829,   830,     0,     0,     0,     0,   285,
       0,     0,     0,     0,   938,   933,   472,     0,  1005,   727,
     915,     0,     0,   890,   104,   306,   301,   303,   305,     0,
       0,     0,  1075,   727,  1076,  1077,    45,  1118,   727,  1145,
    1141,   727,  1190,     0,  1074,    45,  1078,     0,  1079,     0,
    1063,   727,  1152,   727,  1105,   727,  1135,   727,  1214,  1215,
    1216,  1224,  1225,    45,  1219,  1217,  1218,     0,     0,     0,
     393,     0,     0,  1259,     0,     0,     0,     0,     0,     0,
       0,     0,  1278,     0,    45,    45,     0,     0,  1288,     0,
       0,     0,     0,     0,    31,   175,   174,     0,     0,   119,
     113,   107,     0,     0,   161,   598,   166,     0,   248,   246,
     264,   257,   262,   216,   222,   598,     0,   240,   236,   223,
       0,   233,     0,   270,   269,     0,   225,   229,     0,     0,
       0,     0,     0,   230,     0,   426,   386,   348,   397,     0,
     583,   595,   597,   596,     0,   430,     0,     0,   809,     0,
     433,     0,   985,   756,   729,     0,     0,    45,     0,     0,
       0,   844,   974,   845,  1018,     0,  1015,  1017,   420,   419,
       0,     0,   817,     0,   827,     0,   288,     0,     0,   765,
     762,   719,   714,   715,   717,   718,   941,  1007,  1011,     0,
       0,   381,     0,     0,     0,     0,   309,   308,   521,     0,
       0,     0,  1119,  1142,     0,  1189,  1188,     0,     0,     0,
    1064,  1153,  1106,  1136,  1223,     0,     0,   392,     0,     0,
    1258,  1257,   902,   903,   904,   901,   906,   900,   907,   899,
     898,   897,   905,   893,     0,     0,    45,  1256,  1268,  1269,
    1267,  1275,  1273,     0,  1276,     0,     0,  1243,     0,  1290,
    1291,    45,  1283,  1284,  1285,  1286,  1292,  1293,     0,     0,
       0,   795,   162,   163,     0,     0,   239,   598,   241,     0,
     280,   228,     0,   272,   271,   274,   275,   277,   473,     0,
     770,   769,   771,     0,   767,   431,     0,   997,   434,     0,
     741,   739,     0,   731,     0,     0,     0,  1014,   418,     0,
       0,     0,     0,     0,   911,     0,     0,     0,     0,     0,
       0,     0,   286,   291,   290,     0,     0,     0,  1004,   917,
     918,     0,   842,   891,   891,   304,  1091,  1090,  1089,  1096,
    1097,  1098,  1095,  1092,  1094,  1093,  1102,  1099,  1100,  1101,
       0,  1086,  1130,  1129,  1131,  1132,     0,  1191,  1081,  1083,
    1082,     0,  1085,  1084,     0,  1027,  1261,  1260,     0,     0,
       0,  1279,  1245,    45,  1246,  1248,  1289,     0,   796,     0,
     172,   265,     0,     0,   227,   226,     0,     0,   766,   510,
       0,     0,     0,   466,  1016,   823,   822,     0,   820,   862,
     859,     0,     0,     0,     0,     0,   909,   910,     0,     0,
       0,     0,     0,   716,   922,  1010,    45,     0,     0,     0,
       0,     0,  1128,  1185,  1080,    45,     0,     0,   894,  1244,
      45,  1241,    45,  1242,  1287,     0,     0,   251,   231,   495,
     768,   757,   744,   737,   742,     0,     0,   819,   865,   860,
       0,     0,     0,     0,     0,     0,     0,     0,   848,     0,
     854,     0,   467,   720,     0,     0,   841,    45,    45,   888,
    1088,  1087,     0,     0,   895,     0,     0,     0,   799,   793,
     797,   167,     0,     0,   465,   821,     0,     0,     0,     0,
     857,   846,     0,   840,     0,   908,   858,     0,   847,     0,
     853,     0,   923,     0,     0,     0,  1127,     0,     0,   354,
       0,     0,     0,   496,     0,   747,     0,   745,   748,   863,
     864,     0,   866,   868,     0,     0,     0,   849,   855,   468,
     919,   889,   887,     0,   896,    45,    45,   798,   750,   751,
       0,   743,     0,   861,     0,   856,   839,     0,     0,     0,
       0,     0,     0,   749,   752,   746,   867,     0,     0,   871,
     912,   850,  1021,  1247,  1249,   753,     0,     0,     0,   869,
      45,  1020,   754,   873,   872,    45,     0,     0,     0,   874,
     879,   881,   882,  1022,  1023,     0,     0,     0,    45,   870,
      45,    45,   598,   885,   884,   883,   875,     0,   877,   878,
       0,   880,     0,    45,   886,   876
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     6,     7,   208,   384,   209,   840,   751,   210,
     903,   619,   804,   689,   569,   901,   902,   441,  2225,  1220,
    1506,   211,   212,   620,  1124,  1125,   213,   214,   215,   579,
    1286,  1287,  1128,  1288,   216,   217,   218,   219,  1153,   220,
     221,  1154,   222,   582,   223,   224,   225,   226,  1577,  1578,
     918,  1589,   937,  1861,   227,   228,   229,   230,   231,   232,
     785,  1164,  1165,   233,   234,   235,   746,  1076,  1077,   236,
     237,   710,   453,   930,   931,  1605,   932,   933,  1899,  1615,
    1620,  1621,  1900,  1901,  1616,  1617,  1618,  1607,  1608,  1609,
    1610,  1873,  1612,  1613,  1614,  1875,  2113,  1903,  1904,  1905,
    1166,  1167,  1478,  1479,  1989,  1725,  1145,  1146,   238,   458,
     239,   850,  2006,  2007,  1757,  2008,  1027,   718,   719,  1050,
    1051,  1039,  1040,   240,   756,   757,   758,   759,  1092,  1439,
    1440,  1441,   397,   374,   404,  1331,  1629,  1332,   885,   999,
     622,   641,   623,   624,   625,   626,  2051,  1079,   970,  1913,
     823,   627,   628,   629,   630,   631,  1336,  1631,   632,  1306,
    1910,  1404,  1385,  1405,  1020,  1137,   241,   242,  1951,   243,
     244,   692,  1032,  1033,   709,   423,   245,   246,   247,   248,
    1083,  1084,  1433,  1920,  1921,  1070,   249,   250,   251,   252,
    1202,   253,   973,  1344,   254,   376,   727,  1422,   255,   256,
     257,   258,   259,   260,   652,   644,   979,   980,   981,   261,
     262,   263,   996,   997,  1002,  1003,  1004,  1333,   769,   645,
     801,   564,   264,   265,   266,   713,   267,   729,   730,   268,
     767,   768,   269,   499,   835,   836,   838,   270,   271,   765,
     272,   820,   273,   814,   274,   701,  1067,   275,   276,  2162,
    2163,  2164,  2165,  1712,  1064,   407,   721,   722,  1063,  1677,
    1740,  1942,  1943,  2413,  2414,  2486,  2487,  2509,  2523,  2524,
    1745,  1940,   277,   278,  1727,   673,   809,   810,  1928,  2263,
    2264,  1929,   670,   671,   279,   280,   281,   282,  2077,  2078,
    2449,  2450,   283,   754,   755,   284,   706,   707,   285,   685,
     686,   286,   287,  1143,  1717,  2152,  2367,  2368,  1971,  1972,
    1973,  1974,  1975,   703,  1976,  1977,  1978,  2429,  1227,  1979,
    2431,  1980,  1981,  1982,  2370,  2418,  2458,  2491,  2492,  2528,
    2529,  2548,  2549,  2550,  2551,  2552,  2563,  1983,  2174,  2388,
     816,  2056,  2213,  2214,  2215,  1984,   828,  1493,  1494,  2001,
    1160,  2385,   288,   289,   290,   291,   292,   293,   294,   295,
     797,  1162,  1163,  1733,  1734,   296,   844,   297,   780,   298,
     781,   299,  1140,   300,   301,   302,   303,   304,  1100,  1101,
     305,   762,   306,   307,   308,   681,   682,   309,   310,  1407,
    1667,   715,   311,   312,   776,   313,   314,   315,   316,   317,
     318,   319,  1234,  1235,   320,  1170,  1741,  1742,  2298,   321,
    1705,  2145,  2146,  1743,   322,  2541,   323,   324,   325,   326,
    1243,   327,   328,   329,   330,   331,   332,  1203,  1788,   862,
    1766,  1767,  1768,  1792,  1793,  1794,  2331,  1795,  1796,  1769,
    2180,  2441,  2320,   333,  1209,  1816,   334,   335,   336,   337,
    1193,  1770,  1771,  1772,  2326,   338,  1211,  1820,   339,  1199,
    1775,  1776,  1777,   340,   341,   342,  1205,  1810,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,  1781,  1782,   863,  1515,   358,   359,   360,
     361,   362,   873,   874,   875,  1221,  1222,  1223,  1228,  1826,
    1827,   363,   364,   365,   879,   366,   367,   368,   369,   370,
    2226,  2227,  2401,  2403,   371,  1246,  1247,  1248,  1249,  1250,
    2052,  2053,  1252,  1253,  1254,  1255,  1256,  1258,  1259,   893,
    2063,   372,  1262,  1263,  2067,  2068,  2073,   557
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -2158
static const yytype_int16 yypact[] =
{
   -2158,   102, -2158, -2158, -2158, -2158,   206,  4642, -2158, -2158,
   -2158,   118, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,   846, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158,   111, -2158, -2158,   889,   122, -2158, -2158, -2158,   111,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158,   353,   353, -2158, -2158, -2158, -2158, -2158,   353,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
     142, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,   353, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158,   261, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
     426,   492, -2158, -2158, -2158, -2158, -2158,   111, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158,   111, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,   247,  1339,
     516,   247, -2158, -2158, -2158,   573,   648,   668,   710, -2158,
   -2158, -2158,   747,   754,   353, -2158, -2158,   806,   866,   895,
     898,   451,   312,   904,   907,   912, -2158,   168, -2158, -2158,
   -2158,   247, -2158, -2158, -2158,   645,   704,  2127,  2480, -2158,
   -2158,  3245, -2158,   658, -2158, -2158,  2768, -2158,   724, -2158,
   -2158,  1329,   724,   915, -2158, -2158,   932, -2158, -2158, -2158,
     954,   958,   961,   963,   965, -2158, -2158, -2158, -2158,   973,
     984, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158,   977, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158,   167,   353,   982,   945,   980,   858,   353,
     353,   124,   353, -2158,   353,   353,  1025, -2158,   205,  1047,
     353,   353,   353,   353, -2158, -2158,   353, -2158,  1056,   353,
     931,   353,  1075, -2158, -2158, -2158,   353, -2158,  1070,   353,
   -2158,   353,  1072,   160, -2158,   931, -2158,   353,   353,   353,
     353, -2158, -2158, -2158, -2158, -2158,   353, -2158,   353,   353,
     516,   353,  1098,   982,   353,  1100, -2158,   353,   353, -2158,
   -2158, -2158,  1095,  1104,   353,   353, -2158,  1118,  1133,   353,
     982,  1162,  3245, -2158,  1168,  1182,   353, -2158,  1181,   353,
    1172, -2158,  1211,   353,   982,  1221,  1227, -2158,   858,   982,
     353,   353,  1767,   127,   353,   139, -2158, -2158,   162, -2158,
     166,   353,   353,   353,  1238,   353,   353,  3245,   147, -2158,
   -2158,  1241,   353,   353,   353,   353,   353,  1733,   353, -2158,
     982,   353,   982,   353,   353, -2158, -2158,   353, -2158,   982,
     353,  1262,  1269, -2158,   353, -2158, -2158,  1288, -2158, -2158,
    1309, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158,  1320, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158,   353, -2158, -2158,  1328,  1358, -2158,  1368,
    3245,  3245,  3245,  3245,  3245,  1380,  1383,  1405,  1412,  1427,
     353, -2158,  1428, -2158, -2158, -2158, -2158,  1124,   188, -2158,
   -2158,   353,   353,   353,   353,  1268, -2158, -2158,  1282,   353,
     353, -2158,   569,   353,   353,   353,   353,   353,   472,   353,
    1172,   353,   353,  1098, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158,  1199, -2158, -2158, -2158, -2158, -2158, -2158,  3245,
    3245,  3245, -2158,  3245, -2158, -2158, -2158, -2158, -2158, -2158,
    3245,  3353, -2158,    73,  1442, -2158,  1433, -2158,  1203,  1218,
    1450, -2158, -2158,  1454,  3245, -2158, -2158,  1369, -2158, -2158,
    1464,  1333,  1442, -2158, -2158,  1067,    -9, -2158,  1369, -2158,
   -2158, -2158,  1468,   169,   224,  3281,  3281,   353,   353,   353,
     353,   353,   353,   353,  1488, -2158,   353, -2158, -2158, -2158,
     504, -2158, -2158,  1465,   353, -2158,  3245, -2158,  1280,   696,
   -2158,  1485, -2158, -2158,  1526,  1514, -2158, -2158, -2158, -2158,
   -2158,  2929,   353,  1527, -2158,   353,  1526,   353, -2158,   858,
   -2158, -2158, -2158, -2158, -2158, -2158,  1535, -2158, -2158, -2158,
   -2158, -2158,  1526, -2158, -2158,  1529, -2158, -2158,   695,  1659,
     353,   711,   112, -2158,  1531,  1372,  3245,  1418, -2158,  1563,
   -2158, -2158,  3245,  3245, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158,   353, -2158,   353,  1560,   334,
     353,   516, -2158, -2158,  1568, -2158,  1571, -2158,  1565,   967,
   -2158,  1574, -2158,   353, -2158, -2158, -2158,  1576, -2158,   724,
    1567,  3404, -2158,   353, -2158,  5598, -2158,   353,  3245, -2158,
    1579, -2158,   353, -2158,   353,   353,   353,  1442,   238,   353,
     353,   353,  1418, -2158,   353,   483, -2158, -2158, -2158,  1333,
    1067, -2158, -2158, -2158, -2158, -2158, -2158,   167, -2158,  1465,
    1588,  1531, -2158, -2158, -2158, -2158, -2158, -2158,   353, -2158,
   -2158, -2158,  5598, -2158,   205,  1534,   353, -2158,  1582, -2158,
   -2158, -2158, -2158,  1583,  3497,   763, -2158, -2158,   261,   353,
     516, -2158,   353,  1526, -2158,  1591,  1580, -2158,   353, -2158,
    1598,  3245,  3245, -2158,  1526,   353,   330,   353,  1321,  1321,
     456,  1321, -2158,  1592,   513,   524,   542,   550,   578,   589,
   -2158,  1526,   952, -2158,  1601, -2158,   129,   196,  1296, -2158,
   -2158,  3542,  5598,  3573,  3608,  1603,  5598,   353,   353, -2158,
   -2158, -2158, -2158,  1604, -2158,   353,   353, -2158, -2158, -2158,
   -2158,   768, -2158, -2158,  1401,  1526, -2158, -2158, -2158, -2158,
    1796,   353, -2158, -2158, -2158, -2158, -2158, -2158, -2158,  1526,
   -2158, -2158, -2158, -2158,  1607, -2158,  1607, -2158, -2158, -2158,
   -2158,   479, -2158,   185, -2158,  1605, -2158, -2158,  3646,  1608,
    1609,  1609,  1480, -2158,  3245,  3245,  3245,  3245,  3245,  3245,
    3245,  3245,  3245,  3245,  3245,  3245,  3245,  3245,  3245,  3245,
    3245,  3245,  3245, -2158,  1555,  1492,  1602,   286,   591,  3245,
   -2158, -2158, -2158,   774,  1836, -2158, -2158, -2158, -2158,   794,
   -2158,  1991,   922,  3245,  1613,  1333,  1333,  1333,  1333,  1333,
   -2158,  1006, -2158,   169,   169,  1442,  1616, -2158,  3281,  5598,
     140,   145, -2158,  1617,  1618, -2158, -2158,  1526, -2158, -2158,
   -2158, -2158,  1526, -2158,   655, -2158,   167, -2158, -2158, -2158,
     353,  3683,   353,  1612,  3245,  1562, -2158, -2158,   353, -2158,
    3245,  3740, -2158,   801, -2158, -2158,  1594, -2158, -2158,   810,
   -2158,   353, -2158,   353, -2158, -2158,  1659, -2158, -2158, -2158,
   -2158, -2158,  3870,  1526, -2158, -2158, -2158,  1620,  1622,  1623,
    1624,  1632,  1634, -2158,  1372, -2158,   353, -2158,  3901, -2158,
   -2158,   353,  3932,  3963, -2158,  1637,   811,  1625,  1450, -2158,
   -2158,   353, -2158,  1645, -2158,  1606, -2158,   353, -2158,  1528,
    1049, -2158, -2158,    46, -2158, -2158,  1649, -2158,  1643,  1652,
     817, -2158,   353,  3281,  1639, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158,  1641, -2158, -2158,   305,  1642,  1650,
    4209,  2982,   -63, -2158,  1630, -2158, -2158,   829, -2158, -2158,
   -2158, -2158, -2158,   835,  1644,   839, -2158, -2158, -2158,  3245,
   -2158,  1273, -2158, -2158, -2158,   840, -2158,  1664, -2158,  1372,
    1662,  1671,   855, -2158, -2158, -2158,  1672, -2158,  1666,  1668,
    1653,   353,  3245,  3245,  1733, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158,  1677,  1679, -2158,   444, -2158, -2158,  4252,  4283,
   -2158,  1667, -2158,   605,  1669, -2158, -2158, -2158, -2158,   634,
   -2158, -2158, -2158,   636, -2158,   652,   656,   667, -2158,   669,
   -2158,   671, -2158,  1674,  1676,  1681,  1683, -2158,  1684,  1688,
   -2158, -2158, -2158, -2158, -2158, -2158,  1442,  1686,  1685, -2158,
    1689, -2158, -2158,   -47,   877, -2158, -2158, -2158, -2158,  3245,
     446,   577, -2158,   900,   936,   470,   947, -2158, -2158, -2158,
   -2158,    77,   953, -2158, -2158, -2158, -2158,  1131,   991, -2158,
   -2158,   572,   998, -2158, -2158,   516,   353,   174, -2158,  1692,
   -2158,  1687, -2158,  1526, -2158, -2158, -2158,  1694, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158,   993, -2158, -2158, -2158,
     353,  1526,   130,  2143, -2158, -2158,   353,   353, -2158,  1191,
     185, -2158,  1696, -2158,  1651,  3245,  3281, -2158,  3245,  1609,
    1609,   466,   466,  1480,  1399,  3138,  2468,  5598,  2468,  2468,
    2468,  2468,  2468,  3138,  1901,  1609,  1901,  5629,  1602, -2158,
   -2158,  1691,  1707,  2898, -2158, -2158, -2158, -2158, -2158,  1711,
   -2158, -2158,   858,  5598, -2158,  3245, -2158, -2158, -2158, -2158,
    5598,   177,  5598,  1613,  1613,  1423,  1613,   597, -2158,  1616,
    1712,   169,  4323,  1716,  1717,  1718,  3281,  3281,  3281, -2158,
   -2158,   353,  1704, -2158, -2158,  1713,  1531, -2158,   261, -2158,
   -2158, -2158, -2158,  1472, -2158,  1010,   858, -2158,   858,  1013,
    1722,  1017, -2158,  5598,  3245,  2929, -2158,  1024, -2158,   858,
    1607, -2158,   615,   712, -2158,  1034,  1564,  1038, -2158,  2638,
   -2158,   112, -2158,  1719,   353,   353,  3245,   353, -2158, -2158,
    1526, -2158, -2158, -2158,  1494,   353,  3245,   353, -2158,   353,
   -2158,  1442,  3245,  1715,  2982, -2158, -2158, -2158, -2158,  1039,
   -2158,  1721, -2158,  1727,  1729,  1730,  1538,  3245,   353,   353,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,   982,   353,
   -2158,  2996,  2252,  1731,   353,   353, -2158,   353, -2158,    42,
     353, -2158,  3245,   353, -2158,  1607,  5598, -2158,  1724,   152,
    1724, -2158,   353,  1372,  1747,  3070,   353,   353, -2158,   205,
    3245,   863,  3245,  1053, -2158,  1740,  1060,  5598, -2158,    49,
   -2158, -2158, -2158, -2158, -2158,   982,    90, -2158,   353, -2158,
     788, -2158, -2158,    52, -2158,   187,   612, -2158,   886, -2158,
     526, -2158,   -48, -2158,   353,   353,   353, -2158,   353,   353,
     952, -2158,   353, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
    1429,  1441,  1430,  5598, -2158,  1602,   353, -2158, -2158,  1742,
    1743,  1748, -2158, -2158,  1749,  1751,  1753, -2158, -2158, -2158,
    1755,  1756,  1758, -2158, -2158,   665,  1759,  1760,  1764,  1765,
    1766, -2158, -2158, -2158, -2158, -2158,   353,   623, -2158, -2158,
    1770, -2158,  1779, -2158, -2158,  1687, -2158, -2158, -2158, -2158,
    5598,  2670, -2158, -2158, -2158,   110,   358,   358,  1516,  1541,
   -2158, -2158,  1543,  1545,  1547,   567,   353, -2158, -2158, -2158,
   -2158,  1795, -2158, -2158, -2158,  1696, -2158,  1793, -2158,   758,
    1789, -2158,  1792,  4354, -2158,  1791,  1797,  1450, -2158, -2158,
    4410, -2158,  3245,  3245,  1836, -2158,  5598,  1369,   169, -2158,
     469,  3281,  3281,  3281,   531, -2158,   549, -2158,   592, -2158,
    1526,   353, -2158, -2158,  1802,  1071, -2158,  1809, -2158,  5598,
   -2158, -2158, -2158,  3245, -2158, -2158,  3245, -2158, -2158, -2158,
   -2158,  5598, -2158,  1564,  3245,  1798, -2158,  1799,  1800,  4441,
    1814, -2158,   154,   353, -2158,  1081, -2158, -2158,  1803,  5598,
   -2158, -2158,  4410, -2158,  1528, -2158,  1528,   353,   353,   353,
    1088,  1097, -2158,   353,  1808,  1805,  3245,  4485,  3222, -2158,
   -2158, -2158,  1526,  1442, -2158, -2158,  1655,  1816,  5598, -2158,
     353, -2158,  1815,  1817, -2158, -2158,  1578,  1827, -2158, -2158,
    1829, -2158,  5598,  1099, -2158,  1102, -2158, -2158,  4516, -2158,
   -2158,  1106, -2158, -2158,  5598,  1818,   353, -2158, -2158, -2158,
    1823,  1830,  1636,  1776,   353,   353,  1831,  1840, -2158,   465,
   -2158,  1839, -2158, -2158, -2158,  1846, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158,   788, -2158, -2158, -2158, -2158,    52,   353,
   -2158, -2158,  1117,  1849, -2158,  1850, -2158,  1851, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,   612, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158,   886, -2158, -2158, -2158, -2158, -2158,   526, -2158, -2158,
   -2158,   -48,  1848,  1855,  1856,   613,  1145, -2158,  1857,  1858,
    1442, -2158,  1852, -2158,  1865,  1602,  1860, -2158,   353,   353,
   -2158, -2158,   353,   353,   353, -2158,   353,   353,   353, -2158,
    1869,  1870, -2158,   353,   353,  3245, -2158, -2158,  1866,  3245,
    3245, -2158, -2158,  1871, -2158,  3037,   623,  2736, -2158,  1151,
   -2158,  5598, -2158, -2158, -2158,  1863, -2158, -2158, -2158, -2158,
     286,   286,   286,   286,   286,  1191, -2158,  1875,  1886,  1878,
    1191,  1789, -2158,   185,   758,   121,   121, -2158, -2158, -2158,
    1152,  1887,   731,   208, -2158,  1889,   758, -2158,  3245, -2158,
    1881, -2158,  1450, -2158,  2898,  5598,  1882, -2158, -2158,  1067,
    1876,  1884,  1170,  1890,  1892,  1893, -2158, -2158, -2158,  1904,
      12,   858, -2158,   353,   982,  5598,    12,  5598,  1564,  3245,
    1900,  4577,  1171, -2158, -2158, -2158, -2158, -2158,  3245, -2158,
    1907, -2158, -2158, -2158, -2158, -2158,  1173,  1174,  1204, -2158,
   -2158, -2158,   772, -2158,  5598,  3245,  3245,  4637, -2158,   353,
   -2158, -2158,  1816, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158,  1899,   152,  1909,  3404, -2158,
     353,   353,   353,  3070, -2158, -2158, -2158,   863, -2158, -2158,
   -2158,  2312,   353, -2158, -2158,  1831,  1919, -2158, -2158,   353,
     353,  3245, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158,   187, -2158, -2158, -2158,  3245, -2158,  3245,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158,   353,   353,  1915,
     830,  1914,  1212, -2158,  1222,  1252,  1233,  1239,  1251,  1260,
    1265,  1290, -2158,  1300, -2158, -2158,  -154,  1301, -2158,  1304,
    1305,  4975,   702,  1934, -2158,  5598,  5598,  1306,  1936, -2158,
   -2158, -2158,  1923,  5006, -2158, -2158, -2158,   110, -2158, -2158,
   -2158, -2158, -2158, -2158,  1191, -2158,   353, -2158, -2158,  1930,
    1920, -2158,  1113,   208,   208,   758, -2158,   758,   121,   121,
     121,   121,   121,  1188,  5037, -2158, -2158, -2158, -2158,  3245,
   -2158, -2158, -2158, -2158,  2415, -2158,   353,  1939,  1514,   353,
   -2158,   353, -2158,  5068, -2158,  3245,  3245, -2158,  5099,  1654,
    3245, -2158, -2158, -2158, -2158,  1307, -2158, -2158,  5598,  5598,
    3245,  1935, -2158,   643, -2158,  3245, -2158,  1937,  1938, -2158,
   -2158,  1940,  1943, -2158, -2158, -2158, -2158, -2158,  1775,  1941,
    1311,  1949,  1950,  1331,  1073,   353, -2158, -2158,  5598,   574,
    1942,    37, -2158, -2158,  1924, -2158, -2158,   317,  5130,  5161,
   -2158, -2158, -2158, -2158, -2158,  1334,  1946,   867,  3245,   353,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158,  1945,  1954, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158,   353, -2158,  1627,  1343, -2158,  1345, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,  3245,  1963,
    1966, -2158, -2158, -2158,  2143,  1955, -2158,  1602, -2158,   758,
   -2158,  1188,  1956,   208,   208, -2158, -2158, -2158, -2158,  5192,
   -2158,  4410, -2158,  1349, -2158, -2158,   858,  1686, -2158,  1564,
    5598, -2158,  1723, -2158,  1967,  5223,   772, -2158,  5598,  2839,
    1969,  1971,  1973,  1974,  1976,  1977,   353,   353,  1979,  1980,
    1982,  5254, -2158, -2158, -2158,  3245,   353,   353, -2158, -2158,
    1988,   353, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
    1997, -2158, -2158, -2158, -2158, -2158,  1350, -2158, -2158, -2158,
   -2158,  1960, -2158, -2158,  2001, -2158,  5598, -2158,   353,   353,
    1252, -2158, -2158, -2158,  2002,  2003, -2158,  5285, -2158,  2009,
   -2158, -2158,  1998,  1351,  1188, -2158,  3245,  2415, -2158, -2158,
    3245,   353,  3245, -2158, -2158, -2158,  5598,  1359, -2158, -2158,
    1969,   353,   353,   353,   353,   353, -2158, -2158,  3245,  3245,
     353,  3245,  1360, -2158, -2158,  2013, -2158,  1367,  2015,  1382,
     353,  3245, -2158, -2158, -2158, -2158,   327,  2020, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158,   353,  2010, -2158, -2158,  5316,
   -2158,  5598, -2158, -2158,  2008,  5347,  2839, -2158,   290, -2158,
    2026,  1391,  1416,  2027,  1426,  2024,  1438,  5378,  5409,  2014,
   -2158,  1445,  5440, -2158,   353,  1962, -2158, -2158, -2158, -2158,
    1686, -2158,  5471,  1714, -2158,  3245,  1690,  1693, -2158,  2033,
   -2158, -2158,  3245,  2438, -2158, -2158,  2038,  2039,   353,   353,
   -2158, -2158,   353, -2158,  1733, -2158, -2158,  3245, -2158,   353,
   -2158,  3245, -2158,  2028,  1446,  1449, -2158,  2032,  5502,   820,
    2035,  2037,   353,  5598,   353,  5598,  1463, -2158, -2158, -2158,
   -2158,  1471, -2158,  2041,  1473,  1483,  1493,  5533, -2158,  5598,
   -2158, -2158, -2158,  3245, -2158, -2158, -2158, -2158, -2158,  2031,
    2438, -2158,   353, -2158,  3245, -2158, -2158,  2036,  3245,  1498,
    1507,  1508,  3245,  2042, -2158, -2158, -2158,  5567,  1515, -2158,
   -2158,  5598,  2045, -2158, -2158, -2158,  3245,  3245,  3245,  2046,
   -2158, -2158, -2158,  5598, -2158, -2158,   -44,   480,  1519, -2158,
    2047,  2053, -2158, -2158, -2158,  2048,  2048,  2048, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158,   486,  2055, -2158,
    1947, -2158,  1524, -2158, -2158, -2158
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -2158, -2158, -2158, -2158, -2158,   -22,  1842,  1183, -2158, -2158,
    -658,   -38, -2158, -2158,  -400, -2158,   799, -2158,   -50,  -591,
   -2158, -2158, -2158,  2153,    89, -2158, -2158, -2158, -2158, -2158,
   -2158,   203,   499,   896, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158,  -156,  -904, -2158, -2158, -2158,   992,   500,  1502,
   -2158,  -158, -1545,   222, -2158, -2158, -2158, -2158, -2158, -2158,
    1500,  -287,  -302, -2158, -2158, -2158,  1499, -2158,  -633, -2158,
   -2158, -2158, -2158,  1381, -2158, -2158,   795, -1258, -1519,  1164,
     484, -1515,  -151,   -12,  1167, -2158,   209,   216, -1795, -2158,
   -1531, -1240, -1524,  -352, -2158,    16, -1587, -1799,  -911, -2158,
   -2158,   616,   950,   386,   -46,   125, -2158,   635, -2158, -2158,
   -2158, -2158, -2158,   -62, -2158, -1448,  -381,  1089, -2158,  1068,
     707,   729,  -362, -2158, -2158,  1031, -2158, -2158, -2158, -2158,
     428,   427,  2050,    80,  -360, -1306,   215,  -431, -1016,   604,
    -420,  -590,  1364,   176,  1670,  -861,  -876, -2158, -2158,  -611,
    -624,  -228, -2158,  -930, -2158,  -576,  -947, -1111, -2158, -2158,
   -2158,   198, -2158, -2158,  1421, -2158, -2158,  1896, -2158,  1897,
   -2158, -2158,   740, -2158,  -384,    13, -2158, -2158,  1898,  1905,
   -2158,   706, -2158,  -737,  -117,  1347, -2158,  1044, -2158, -2158,
     437, -2158,  1110,   512, -2158,  3999,  -392, -1073, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158,  -196, -2158,   510,  -928, -2158,
   -2158, -2158,   324, -1272,  -604,  1154,  -942,  -364,  -283,  -421,
     644,   -23, -2158, -2158, -2158,  1504, -2158, -2158,  1084, -2158,
   -2158,  1054, -2158,  1324, -1944,   989, -2158, -2158, -2158,  1505,
   -2158,  1509, -2158,  1506, -2158,  1503,  -986, -2158, -2158, -2158,
    -128,  -251, -2158, -2158, -2158,  -397, -2158,   607,   760,  -617,
     761, -2158,    47, -2158, -2158, -2158,  -332, -2158, -2158, -2158,
   -2043, -2158, -2158, -2158, -2158, -2158, -1415,  -534,   194, -2158,
    -172, -2158,  1379,  1175, -2158, -2158,  1180, -2158, -2158, -2158,
   -2158,  -295, -2158, -2158,  1111, -2158, -2158,  1148, -2158,   264,
    1177, -2158, -2158,  -849, -2158, -2157, -2158,  -217, -2158, -2158,
     228, -2158,  -754,  -385,  -361, -2158, -2158, -2158, -1649, -2158,
   -2158, -2158, -2158, -2158,  -169, -2158, -2158, -2158,  -310, -2158,
    -335, -2158,  -349, -2158,  -350, -1910, -1156,  -763, -2158,   -92,
    -477, -1020, -2075, -2158, -2158, -2158,  -491, -1781,   467, -2158,
    -720, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
    -485, -1474,   727, -2158,   223, -2158,  1549, -2158,  1720, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -1439,   769,
   -2158,  1456, -2158, -2158, -2158, -2158,  1838, -2158, -2158, -2158,
     284,  1810, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158,   688, -2158, -2158, -2158,   230, -2158, -2158,
   -2158, -2158,   -43, -1893, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158,   556,   430,  -523,
   -1315, -1228, -1216, -1434, -1425, -1408, -2158, -1399, -1395, -1131,
   -2158, -2158, -2158, -2158, -2158,   412, -2158, -2158, -2158, -2158,
   -2158,   457, -1394, -1388, -2158, -2158, -2158,   411, -2158, -2158,
     461, -2158,   291, -2158, -2158, -2158, -2158,   423, -2158, -2158,
   -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -2158, -2158, -2158,   220, -2158,   225,   -83, -2158, -2158, -2158,
   -2158, -2158, -2158, -2158,  1021, -2158,  1375, -2158,  -837, -2158,
     207, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158, -2158,
   -1995,   -97, -2158, -2158, -2158, -2158,   703, -2158, -2158, -2158,
   -2158,    55, -2158,   699, -2158, -2158, -2158, -2158,   697, -2158,
   -2158, -2158, -2158,   692, -2158,    33, -2158,  1385
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1191
static const yytype_int16 yytable[] =
{
     410,   821,   829,   830,   831,   832,   817,   704,  1389,  1701,
     975,  1242,  1735,  1085,  1294,   674,   678,   716,   870,   971,
    1463,  1338,  1626,   693,   422,   422,  1241,   731,   643,   734,
     743,   430,  1902,   643,   737,   738,   739,  1335,  1335,  1244,
    1230,  1606,   740,  1098,  1869,   742,  1345,   744,  1956,  1957,
    1958,   800,  1005,   567,   437,   989,  1365,  2170,  1758,  1611,
     457,  2057,   649,  2059,  1872,   653,   655,  1728,   476,  2147,
    2228,  1874,   479,   747,  1475,   792,   424,   459,  1418,  1001,
    1001,   761,  1183,   812,  1802,  1074,   698,  1888,   963,  1639,
     773,  1182,   375,  1803,   474,  2097,  2103,  2104,   381,  2304,
    1892,   697,     2,     3,   789,  1907,   793,  -541,   388,   794,
    1804,   390,  1464,   802,   393,   805,  1934,  1056,  1554,  1805,
    1750,   399,   501,  1806,  1808,   406,  1596,  1597,   824,   409,
    1809,  2322,   683,   373,   402,  1535,  1894,   405,  2553,  1018,
     839,   421,   842,  1760,  1384,  1586,   402,   428,  1366,   848,
    1225,   432,   433,  -593,   402,   724,  1496,   438,   439,  1241,
    -584,  2229,  1949,   444,   445,  -593,   447,   448,   449,   450,
    1057,   451,   667,  1484,   668,   733,   594,   807,   978,   898,
     460,   405,  1403,  -527,   994,   464,   581,   466,   570,   939,
    2054,   469,  -565,   898,   806,   473,  2230,   475,  1058,  1722,
    1300,  1789,  1602,  1799,   481,  1812,   440,  1818,   485,   899,
     989,  1443,   488,  2110,   490,  1443,   982,  1229,   596,   421,
     984,   498,   500,  2430,  1779,   502,   503,   991,   694,   811,
     853,   509,  -563,   510,  1762,  1000,  1000,   514,  1059,  1836,
    -339,  -384,   695,  -137,   643,  2111,  1138,  -339,  -541,  1060,
     421,  -339,     4,   421,  1444,  -141,  -584,  1751,   421,  1603,
    1465,  -593,  1755,   -61,   541,  2398,   543,  2323,  1536,   974,
    1897,  1537,   908,   548,  2554,  1156,   497,  1061,  1099,   617,
    1504,   421,   920,   921,   922,   923,  1555,  1231,  1790,   421,
    1800,   421,  1813,  -565,  1819,   421,   421,  -527,   421,  2246,
    1791,   924,  1801,   421,  1814,  1556,  -565,  2102,  1034,  2253,
    2254,  1604,  2498,  1270,   421,  1037,   421,   421,  1774,   964,
     589,  2324,     8,  1693,  1445,   421,  1029,  1065,  1445,  1723,
     965,  1044,  2325,  1161,   421,   669,   672,   965,   594,  1195,
    -563,   679,   680,   684,   680,  1339,   688,   690,  2444,  -527,
     696,  1086,   700,   702,   702,   705,  1876,  1878,   708,     5,
    1081,   712,  1587,   708,   990,  1591,  1639,   992,   723,  2058,
    1062,   728,  1019,   708,  2100,   708,  2098,  1802,  1151,   422,
     708,   708,   708,  2147,  -137,  1797,  1803,  1807,   708,  1815,
     741,   708,   989,   708,   989,  1038,  -141,  -584,  2195,   752,
     753,   989,  -593,  1804,   -61,  1632,   764,   766,  1950,   643,
    1685,   772,  1805,   643,  1158,   421,  1806,  1808,   779,  1501,
     643,   783,  1001,  1809,  1645,  1647,  1649,  1139,  1435,  1503,
     736,  1700,   642,   796,  -565,   617,   803,   642,  2456,   825,
    1178,   507,   723,   813,   815,   815, -1115,   819,   796,  -382,
    1602,  1495,  1455,  1456,   827,   827,   827,   827,   827,   587,
     837,  1544,  1180,   841,   594,   843,   779,  1430,   965,   846,
    2457,   944,   849,  1190,  -527,   945,   854,  -594,  1666,  2535,
    2267,  -563,   421,  1789,   720,  1457,   965,  1298,  -527,  -594,
    1212,  2011,  1148,  2542,   872,   694,  1799,  1729,   878,  1452,
     508,  1149,  1812,   960,   563,   871,  1818,  1603,  2328,   695,
    2520,  2521,  1016,  1867,  1868,  1150,  1299,  1152,  2251,   904,
    2496,   594,   892,  1292,  1271,  1017,   910,  1001,  2173,  2049,
     900,   568,   594,   905,   906,   907,   696,  1177,  1295,  -587,
    1179,   913,   914,   895,   896,   919,   696,   696,   696,   696,
     594,  -587,  1293,   927,   928,   911,  1872,  -591,   594,  1604,
    2488,   989,  2177,  1874,  1353,  1354,  1355,  1356,  1357,  -591,
    1790,   643, -1140,  1103,   915,  1885,  2160,  2248,  1363,  2306,
    -383,   571,  1791,  1800,   916,  -594,   594, -1115,   847,  1813,
    2307,  2308,  1545,  1819,  2426,  1801,  2309,   594,  2310,  1328,
    -589,  1814,   985,  1091,  1886,  2329,   986,  2311,  2555,  2312,
     749,  2313,  -589,   594,  2555,  1014,  2330,  2488,  -382,   712,
    1007,   764,   819,   813,   700,  1012,  1369,  1606,   843, -1062,
     963,  1370,  2096,  1025,   988,  1281,   696,  2556,  1282,   787,
   -1151,  1859,   594,  2556,   594,  1611,   795,  -587,   642,  1860,
    2569,  2571,   597,  1549,   708,  1337,   572,  1036, -1167,   696,
     594,  1550,  2354,  2575,   594,  -591, -1170,  1797,   588,   590,
    1837,   974,  1410,   633,   595,   594,   573,   594,  1551,   594,
    1807,  1406,  1053,  1000,  2280,  1241,  1815,  1822,  1823,  1824,
    2397,  1828,  1829,   904, -1104,  1085,  1783,  2557,  1372,  1645,
    1647,  1649,  1624,  1046,   594, -1134,  1345,  1075,  -589,  1080,
    1093,  -527,  1082, -1140,  1750,  -383,  1047,  1760,   574,  1054,
     421, -1114,  1373,  2422,  1127,  1096,  -594,   966,  1098,  1098,
    1001,  1132,  1055,  2314,  1761,  1126,  2315,  1374,  -230,   648,
     617,  2440,  1375,  1376,  1133,  1939,  1134,  2108,  2109,  1377,
   -1139,  1141, -1061,  1144,   575,   576,   696,   643,   643,   643,
     643,   643,   580,   989,   989,   989,   989,   989, -1150,   669,
   -1062,  1174, -1166,  1894,  1895,  1896,  1267,  1739,  2281,  2144,
    1096, -1151,  1340, -1169,  1175, -1103,   696, -1133,  -587,  1268,
    1001,  1001,  1001,    46,  1686,  1341,  1687,  1185,  1678, -1167,
    1680,   696,  1346,  1760,   696,  2406,  -591, -1170,  1762,  1395,
    1186,  1763,  1443,   642,   583,  1347,  1566,   642,  1399,  1427,
    1761,  2316,  1396,   965,   642,  1449,    74,  2387,  -852,   995,
    1233,  1400,  1428,  -387,  2282, -1104,  2096,  1467,  1450,  1245,
    1251,  1257,    50,  1470,  1261,   969, -1134,  1473,  1016,  -589,
    1468,  1751,  2283,  -792,  -792,   621,  1471,  1264,   841,    96,
    1474,  1481, -1114,  1487,  2284,  1573,  2285,  2286,  1739,  2287,
    -385,   577,  2288,  1289,   584,  1266,  1488,  1784,  1674,  2317,
    2318,  2319,   969,  1378,  1304,  1538,  1000,   421,  1625,   109,
    1290, -1139,  1579, -1061,  1762,  1301,   402,  1763,  1539,  2424,
     646,  1785,  1567,   585,   403,   654,   586,  1897,  1546, -1150,
    1585,  1786,   591, -1166,  1379,   592,  1787,  1568,  1569,  1570,
     593,  1547,   598,   656, -1169,  1445, -1103,   985, -1133,  2041,
    2042,   986,  2421,  2088,  2089,  2090,  2091,  2092,   638,   639,
     657,  2352,   118,  1351,  1470,   987,  1644,  1646,  1648,   122,
    1335,  1335,  1335,  1335,  1335,  1552,   599,  1548,  1380,   988,
     675,  1557,   658,  2289,  2084,  2290,   659,  1383,  1553,   660,
    1783,   661,  2062,   662,  1558,   642,  1634,  1241,   669,  1760,
    1635,   663,  1637,  1985,  1387,   666,  1632,  1241,  2495,   402,
     684,   664,   665,  1291,  1592,   676,  1761,  2060,  2061,  1563,
    1582,  1583,  2126,   705,  2069,  2070,  1571,   677,  2131,  1916,
    1666,   985,  1564,  2262,   725,   986,  1850,  1851,  1652,  1572,
    2494,  1239,   638,   639,  1654,  1046,  1495,  1358,  1420,   987,
      25,  1653,  1340,   728,  1656,    29,   904,  1662,  1658,  1681,
     691,  1684,  1664,   988,  1446,  1661,  1668,  1694,  1673,   753,
    2236,  2237,  1226,  1226,  2125,  1665,   775,    47,    48,  1669,
    1695,  1746,   699,   787,   766,  1001,  1001,  1001,  1239,  1241,
    1762,   711,   985,  1763,  1747,  1403,   986,  1760,  1764,  1399,
     714,  1749,   717,   638,   639,   726,  1765,   732,  1469,  1239,
     987,   822,  1932,  1371,  1761,  2202,  1239,  2203,  1703,  1213,
    1214,   834,  1953,   760,   988,  1449,  2204,  1993,  2205,  1959,
    1487,    90,    25,   745,  1997,   750,  1023,    29,  1960,   763,
    1994,    95,  1495,  1995,  1495,  2023,   856,  1998,   858,  2108,
    2109,  1499,  1215,   770,  2250,   859,   860,   861,  2024,    47,
      48,   864,   865,   866,   867,  1754,   868,   869,   771,  1196,
    1197,  1784,  1200,  2043,   881,   882,   883,   884,   886,  2085,
    2105,   642,   642,   642,   642,   642,  2044,   121,  1762,   995,
     995,  1763,  2086,  2106,  1530,  1785,  1764,   774,  -587,  2136,
    1835,  1532,  1532,   777,  1765,  1786,  1540,  1541,  1542,   782,
    1787,  1911,  2137,    90,  2141,  2142,   787,   778,  1090,  2255,
    2256,  2257,   421,    95,  2108,  2109,  1595,  1596,  1597,   784,
    1565,   150,  1532,   938,   940,   941,  1919,   942,  1598,   787,
    2199,  1922,  1646,  1648,   943,  2143,   786,  1599,  1574,   900,
    1532,  2206,  2207,  2200,  2208,  2209,   790,  2012,   822,  2014,
    2015,  2216,   791,  2201,  1877,  1879,  2262,  1746,   897,   121,
    1600,  2019,  1584,   818,  2217,  1601,   826,  1431,   696,  1487,
    2218,  2026,  1301,  2028,  1914,  2210,  1216,  1217,  1746,  1926,
    1218,  1219,  2219,  1470,  2202,   909,  2203,   851,   985,  2382,
    1021,  2220,   986,  1602,   852,  2204,  2221,  2205,  1930,   638,
     639,   605,   606,  1192,  1477,  1031,   987,  1198,  1470,  1952,
    1936,  2116,  1204,   855,  1938,  1208,  1210,  2211,  2223,  2231,
     988,  2222,  1470,  1470,  2239,  2276,  1591,  1359,  1360,  1174,
    2212,  2224,  2232,  1052,   857,  2233,  2234,  2240,  2277,   989,
    1068,  1968,  2300,  1650,   651, -1190,  1072,  1073,  1241,  1487,
    1603,  2360,  1532,   876,   637,   638,   639,  2341,   799,   638,
     639,  2343,  2302,  2343,  1580,  2334,   612,  2357,  2391,  2105,
     612,  1296,  1241,  1297,  2344,   614,  2345,  2416,  1239,   614,
    2358,  2392,  2408,   877,   976,  1487,  1676,  1676,   977,  1676,
    2417,  2433,  1130,   880,   799,   638,   639,   708,  2436,  1075,
    2438,  1075,  1604,   615,  1688,   887,   612,   615,   888,  1449,
    2564,  2565,    25,  2439,   944,   614,   912,    29,   945,   643,
    2206,  2207,  2460,  2208,  2209,   946,   947,  2474,  2475,  1159,
     889,   616,  1206,  1207,  1532,   616,  1711,   890,   985,    47,
      48,  1726,   986,   615,  1487,  1144,   960,  2461,   936,   638,
     639,  1724,   891,   894,  2210,   965,  1746,  2463,   966,   967,
    1752,   696,  1753,  2469,  2216,  1188,  1189,  2216,   640,  2465,
     988,   616,   640,   565,   968,   969,  2470,  2501,  1756,  1559,
    2502,  2510,  1560,  1561,  1562,   972,   993,  1825,   617,  2512,
     807,  1449,   617,    90,  2511,   944,  2211,  2519,  1233,   945,
     983,  1487,  2513,    95,  2515,  1013,   946,   947,   640,  2212,
    1024,  1174,  1245,   949,  2516,   951,  1239,  1251,   953,   954,
     955,   956,   957,  1257,  2517,  2343,  2343,   960,   617,  2532,
    1022,  1261,  1028,  2538,  1923,  1924,  1925,  2558,  2533,  2534,
     618,  1127,  1628,  1865,   618,   978,  2539,   995,  1858,   121,
    2559,  1026,  1035,  1041,  1045,  2574,   405,  1066,  1309,  1310,
    1311,  1312,  1313,  1314,  1315,  1316,  1317,  1318,  1319,  1320,
    1321,  1322,  1323,  1324,  1325,  1326,  1327,  1069,  1887,  2127,
     618,  1071,   566,   822,  2129,   963,  1087,  2384,  1343,  1088,
    1089,  1898,  1094,   150,  1102,  1350,  2116,  1352,   558,   559,
     560,   561,   562,  1104,  1131,  1157,  1168,  1171,  1172,  1181,
    1688,  1184,  1362,  1688,  1688,  1688,  1187,  1201,  1195,  1224,
    1216,  1239,  1260,  1927,  1269,   594,  1308,  2172,   945,  1329,
    1305,  1330,   986,  -342,  1361,  1367,  1368,  1388,   886,  1390,
    1511,  1398,  1434,  1429,  1393,  1412,  1514,  1413,  1414,  1415,
    1517,  1713,  1519,  1517,  1517,   708,  1521,  1416,  1523,  1417,
    1052,  1098,  1426,  1432,  2425,  1495,  -935,  1438,  1447,  1448,
    1453,  1411,  1454,  1458,  1048,  1961,  1049,  1466,  1459,   607,
    1472,  1482,  1419,   608,   609,   610,   611,  1485,  1486,  1492,
    1489,  1490,  1082,  1491,  1759,  1500,   612,  1502,  1509,  1524,
    1512,  1525,   915,   613,  1532,   614,  1526,  2303,  1527,  1528,
    1226,  1226,  1226,  1529,  1226,  1226,  1533,  1576,  1830,  1581,
    1534,  1619,  1627,  1622,  2472,  1628,  2004,  2005,  1633,   373,
    1638,  2244,   787,   615,  1641,  1642,  1643,  1651,   403,  1657,
     978,  2247,  1721,  1683,  1674,  1462,  1691,  1696,  2493,  1098,
     833,  2022,  1697,   607,  1698,  1699,  1444,   608,   609,   610,
     611,   616,  1710,  1476,  1730,  1748,  1833,  1838,  1839,  1832,
     612,  1834,  1880,  1840,  1842,  1483,  1843,   613,  1844,   614,
    1846,  1847,   798,  1848,  1852,  1853,   886,  1497,   834,  1854,
    1855,  1856,   799,   638,   639,  1862,  1864,  1881,   421,  1882,
    2055,  1883,  2493,  1884,   612,  2376,  2377,   615,   748,  1510,
    1890,  1893,  2066,   614,  1906,  1513,  2072,  1908,   617,  1516,
    1931,  1518,  1911,   642,   995,  1520,  1933,  1522,  1912,  1944,
    1945,  1946,  1948,  1962,  1970,   616,  1963,   748,  1969,  -494,
    1987,   615,  1988,  1990,   748,  1991,  1992,  1663,  2002,  1999,
    1272,  1342,   808,  1543,  1751,  2003,   607,  1750,  2010,  2009,
     608,   609,   610,   611,  2013,  2099,  1898,  1898,  1898,   616,
     618,  2016,   421,   612,  2025,  2027,  2029,  2047,  1898,  2038,
     613,  2087,   614,  1226,  1226,  1226,  2039,  2040,  2045,  2046,
    2048,  2050,   617,  1273,  2064,  2065,  2420,  2074,  2423,  1274,
    -381,  2094,  2079,  2095,  2107,   684,   640,  1590,  2112,  2139,
     615,  2115,  2119,  2118,  2359,  2120,   944,  2297,  2274,  1623,
     945,  2121,  1720,  2122,  2123,  1275,   617,   946,   947,  2124,
    2153,  2134,  2140,   948,   949,  2155,   951,  2175,   616,   953,
     954,   955,   956,   957,   618,  2157,  2197,  1630,   960,  1276,
    2198,  1277,  2238,  2241,  2242,  -224,  2249,  2266,  1724,  1636,
    2279,  2296,  1927,   925,  2161,  2295,  -843,  2301,  2293,  2294,
    2338,  2327,  2299,  2321,  2171,   421,  2181,  2335,   618,  2339,
    2348,  2176,  1756,  2349,  1278,  2187,  2351,  2355,  2361,  1279,
    2342,  2394,  2362,  1280,  2369,   617,  2371,  1281,  2372,  2373,
    1282,  2374,  2375,  1825,  2378,  2379,  1348,  2380,  1659,  1031,
    1349,   607,   978,  2386,  2390,   608,   609,   610,   611,  2395,
    2400,  2402,  1283,  1671,   787,  1226,  2405,   748,   612,  2407,
    1679,  2434,  2437,  2453,   787,   613,  1284,   614,  2445,  1682,
     886,  2451,  1285,  2459,  2462,  2468,  1689,   618,  1692,  2464,
    2473,  2482,  2480,  2477,  2481,  2489,  2490,  2503,  2522,  2500,
    2505,   886,  2506,  2540,  2560,   615,  2514,  2530,  1887,  2536,
    2561,  2545,  2573,  2562,   578,  1707,  1575,  1898,  2080,  1898,
    1898,  1898,  1898,  1898,  1898,  2555,  1718,  2158,  1716,  1265,
    1863,  1505,  1442,   616,   917,  1866,  2350,  2272,  2081,  1732,
     926,  1042,   929,  1594,  1738,  2252,  1744,  1302,  2353,  1891,
    1303,  2093,  2101,  2245,  1480,  1737,   787,  1986,  1719,  1078,
    2292,  2156,   787,  2305,  1408,  1391,  1670,  1655,  1773,  1437,
     421,  1778,  1954,  1955,  1798,   436,  1811,  1097,  1817,  2117,
    1821,  2572,  1043,   788,  2130,  1660,   815,  2005,  1690,  1147,
     617,  1129,   600,   601,   602,  1397,  1917,  1918,  1588,  1135,
    1136,   603,  1364,   607,  1142,  1423,  1451,   608,   609,   610,
     611,  1006,  1176,  1498,  1008,  1011,  2340,  1010,  2383,  1009,
     612,  1672,    13,    14,  1675,    15,    16,   613,  2525,   614,
      20,  2066,  1097,  2271,  2159,  2410,  1155,  2507,    23,  1401,
    1169,  1382,   618,    27,  1381,  1871,    30,  2128,  1436,  2455,
    2154,  2419,  2526,  2544,    37,  1392,    38,   615,    40,  2566,
    2568,  1898,  2389,  2000,  1736,  1015,  2166,  1095,  1702,  1191,
     687,  1194,  2132,  1226,   845,   735,  1831,  2167,  2031,  2035,
    2018,    59,  2037,  2364,  2033,   616,  1915,   822,  1343,  2021,
     748,   748,    70,  2186,  2393,  1531,  2399,  2184,   827,   827,
    2194,  1240,  1142,  1232,  2337,  1841,  1845,   944,  2161,  1708,
    1849,   945,   607,  1857,  2346,     0,    85,  1935,   946,   947,
    1937,     0,   421,  1709,   948,   949,   950,   951,  1941,    93,
     953,   954,   955,   956,   957,   958,     0,   959,     0,   960,
     961,     0,   617,     0,     0,     0,     0,   102,     0,     0,
       0,     0,     0,   104,     0,  1226,     0,     0,     0,   978,
    1964,   108,  1967,   110,     0,   112,     0,   114,     0,   833,
       0,     0,   607,  2412,   119,     0,   608,   609,   610,   611,
       0,  1334,  1334,  1096,     0,  1096,  2435,     0,     0,   612,
       0,   130,   131,     0,   618,  2443,   613,     0,   614,     0,
    2446,     0,  2447,     0,     0,     0,     0,   748,   748,   143,
       0,     0,     0,     0,     0,     0,   604,  2448,     0,     0,
       0,     0,     0,     0,     0,   787,   615,  2017,     0,     0,
     155,     0,  2020,   156,  1240,     0,     0,  2055,  2055,     0,
    2169,     0,     0,     0,     0,  2030,     0,     0,     0,   787,
       0,     0,     0,     0,   616,     0,     0,  1402,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  2032,     0,     0,
    2260,     0,  1461,  2034,     0,   607,     0,  2036,     0,   608,
     609,   610,   611,     0,     0,     0,     0,     0,     0,     0,
       0,   421,   612,  1739,  2448,   748,  2508,     0,   607,   613,
       0,   614,   608,   609,   610,   611,     0,     0,     0,  2071,
       0,   617,     0,  2075,  2076,   612,     0,     0,     0,     0,
       0,  2083,   613,   944,   614,     0,     0,   945,  2484,   615,
       0,     0,     0,     0,   946,   947,     0,     0,     0,     0,
    2546,   949,     0, -1191,     0,  2547, -1191, -1191, -1191, -1191,
   -1191,     0,   615,     0,     0,   960,     0,   616,  2547,     0,
    2567,  2570,  2114,   618,  2396,  1226,     0,     0,  1630,     0,
       0,     0,     0,  2570,     0,    13,    14,     0,    15,    16,
     616,     0,     0,    20,     0,  1097,     0,     0,     0,     0,
       0,    23,     0,  2133,   421,     0,    27,     0,  1226,    30,
       0,     0,  2138,     0,     0,     0,     0,    37,     0,    38,
       0,    40,     0,     0,   617,     0,  1226,   421,     0,  2148,
    2149,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    59,     0,     0,   617,     0,     0,
       0,     0,     0,     0,     0,    70,     0,  1732,     0,     0,
       0,     0,     0,     0,     0,   834,  2168,     0,     0,     0,
       0,     0,     0,     0,     0,  2178,   618,     0,     0,    85,
    2179,  2479,     0,     0,     0,  2182,     0,     0,  2183,     0,
       0,  2188,    93,  2189,     0,     0,     0,     0,  2190,   618,
    2191,     0,  2192,  1048,  2193,     0,     0,     0,   607,     0,
     102,     0,   608,   609,   610,   611,   104,     0,     0,     0,
       0,  1593,     0,     0,   108,   612,   110,     0,   112,     0,
     114,     0,   613,     0,   614,  1870,     0,   119,     0,     0,
     607,     0,     0,     0,   608,   609,   610,   611,     0,     0,
       0,     0,     0,     0,   130,   131,     0,   612,     0,     0,
       0,     0,   615,     0,   613,     0,   614,     0,     0,     0,
       0,     0,   143,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  2259,     0,   748,     0,     0,  2261,     0,
     616,     0,     0,   155,   615,     0,   156,     0,     0,  2270,
    1941,  2082,     0,     0,  2275,     0,   607,     0,     0,     0,
     608,   609,   610,   611,  2278,     0,     0,     0,     0,  2291,
       0,     0,   616,   612,     0,     0,     0,   421,     0,     0,
     613,     0,   614,   635,   962,     0,     0,   636,     0,     0,
       0,     0,     0,   637,   638,   639,     0,   617,     0,     0,
       0,  1078,     0,  1078,     0,   612,     0,     0,     0,   421,
     615,     0,  2336,     0,   614,     0,     0,     0,     0,     0,
       0,     0,  1097,  1097,     0,     0,     0,     0,     0,   617,
       0,     0,     0,  1704,     0,     0,     0,     0,   616,   748,
       0,  1714,   615,     0,  1715,     0,     0,     0,     0,   618,
       0,     0,  2347,     0,  2365,     0,   808,     0,  1590,   607,
    1097,  1097,     0,   608,   609,   610,   611,     0,     0,     0,
     616,     0,     0,     0,     0,   421,   612,     0,     0,     0,
       0,   618,   748,   613,     0,   614,     0,     0,     0,  1780,
       0,     0,     0,  2366,     0,   617,     0,     0,   748,   748,
     748,     0,   748,   748,     0,     0,   748,   640,     0,   886,
       0,     0,     0,   615,     0,  1461,     0,     0,   607,     0,
    1240,     0,   608,   609,   610,   611,     0,   617,     0,     0,
       0,     0,     0,     0,  1123,   612,     0,     0,   962,     0,
       0,   616,   613,     0,   614,     0,  1030,   618,     0,   607,
       0,     0,     0,   608,   609,   610,   611,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   612,     0,     0,     0,
    2409,  2261,   615,   613,  2411,   614,  2415,     0,   421,   618,
    1889,     0,     0,     0,     0,   962,     0,     0,     0,     0,
       0,     0,  2427,  2428,     0,  2432,     0,   962,   617,  1461,
     616,     0,   607,   615,     0,  2442,   608,   609,   610,   611,
       0,     0,   748,  1706,     0,     0,   607,     0,     0,   612,
     608,   609,   610,   611,     0,     0,   613,     0,   614,     0,
    2366,   616,     0,   612,     0,     0,     0,   421,     0,     0,
     613,     0,   614,     0,   962,   962,   962,   962,     0,   962,
     618,     0,     0,     0,     0,     0,   615,   617,     0,  2478,
       0,     0,     0,     0,     0,     0,  2483,  2485,   421,     0,
     615,   748,   748,   748,   978,     0,     0,     0,   834,     0,
       0,  2497,     0,     0,   616,  2499,     0,  1731,   617,     0,
     607,  1272,     0,     0,   608,   609,   610,   611,   616,     0,
       0,   962,     0,   962,   962,   962,   962,   612,     0,   618,
       0,     0,     0,     0,   613,     0,   614,   886,     0,     0,
    1097,   421,     0,     0,  2485,     0,     0,     0,  2527,     0,
       0,     0,  2531,     0,  1273,   421,  1744,     0,     0,     0,
     618,   617,     0,     0,   615,     0,     0,     0,     0,     0,
    1744,  2543,  2527,   944,     0,   617,     0,   945,     0,     0,
       0,     0,   962,     0,   946,   947,  1275,     0,     0,     0,
     948,   949,   616,   951,     0,     0,   953,   954,   955,   956,
     957,     0,     0,   959,   962,   960,   961,     0,     0,     0,
    1276,     0,  1277,   618,   962,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   618,     0,   421,
       0,     0,  1240,   748,     0,   962,  1097,  1097,  1097,     0,
    1142,  1142,  1240,     0,     0,  1278,     0,  1142,  1142,   617,
    1279,   962,     0,     0,  1280,   962,   962,     0,  1281,  1966,
       0,  1282,   607,     0,     0,     0,   608,   609,   610,   611,
       0,     0,     0,     0,  1334,  1334,  1334,  1334,  1334,   612,
       0,     0,     0,  1283,     0,   607,   613,     0,   614,   608,
     609,   610,   611,     0,     0,     0,     0,  1284,     0,     0,
       0,   618,   612,  1285,     0,     0,     0,     0,     0,   613,
       0,   614,     0,   962,     0,     0,   615,     0,     0,     0,
       0,   607,     0,     0,  1240,   608,   998,   610,   611,     0,
    1402,     0,     0,     0,     0,     0,     0,     0,   612,   615,
       0,     0,     0,     0,   616,   613,     0,   614,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  2151,     0,     0,     0,   616,     0,     0,
       0,   962,   962,     0,     0,   615,     0,     0,     0,     0,
       0,   421,     0,     0,     0,   808,     0,     0,   944,     0,
       0,     0,   945,   607,     0,     0,  1097,     0,     0,   946,
     947,   617,     0,   616,   421,   948,   949,   950,   951,   952,
       0,   953,   954,   955,   956,   957,   958,  2185,   959,     0,
     960,   961,     0,     0,   617,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  1105,
     421,   748,  2196,  1106,   607,     0,     0,     0,     0,     0,
    1107,  1108,     0,   618,     0,     0,  1109,  1110,  1111,  1112,
     617,     0,  1113,  1114,  1115,  1116,  1117,  1118,  1119,  1120,
       0,  1121,  1122,     0,     0,     0,   618,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1889,     0,   962,   962,   962,   962,   962,   962,   962,   962,
     962,   962,   962,   962,   962,   962,   962,   962,   962,   962,
     962,     0,   618,     0,     0,     0,     0,     0,     0,     0,
    2265,     0,     0,   748,     0,  2268,   962,     0,     0,     0,
       0,     0,   944,   962,  1173,   962,   945,   607,     0,     0,
       0,     0,     0,   946,   947,   962,     0,     0,     0,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,     0,   960,   961,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   962,   944,     0,     0,
    1236,   945,   607,     0,     0,     0,     0,     0,   946,   947,
       0,     0,     0,  1240,   948,   949,   950,   951,     0,     0,
     953,   954,   955,   956,   957,   958,     0,   959,   944,   960,
     961,  1237,   945,   607,     0,     0,     0,  1240,     0,   946,
     947,     0,     0,     0,     0,   948,   949,   950,   951,     0,
       0,   953,   954,   955,   956,   957,   958,     0,   959,     0,
     960,   961,     0,   944,     0,   962,  1238,   945,   607,     0,
       0,     0,     0,     0,   946,   947,     0,     0,     0,   962,
     948,   949,   950,   951,     0,     0,   953,   954,   955,   956,
     957,   958,     0,   959,     0,   960,   961,     0,     0,     0,
     962,   944,     0,     0,     0,   945,   607,     0,     0,     0,
       0,  1097,   946,   947,     0,  1097,     0,  1307,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,     0,   960,   961,     0,     0,     0,   944,     0,
       0,     0,   945,   607,     0,     0,   962,     0,     0,   946,
     947,     0,   748,   748,  1386,   948,   949,   950,   951,     0,
       0,   953,   954,   955,   956,   957,   958,     0,   959,     0,
     960,   961,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,  1097,   748,  1097,  1097,  1097,
       0,     0,     0,   962,  2151,   944,     0,  1394,     0,   945,
     607,     0,     0,     0,   748,     0,   946,   947,     0,     0,
       0,     0,   948,   949,   950,   951,     0,     0,   953,   954,
     955,   956,   957,   958,     0,   959,   962,   960,   961,     0,
       0,     0,     0,   962,     0,     0,     0,     0,     0,   962,
       0,     0,     0,     0,     0,     0,     0,     0,  1097,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   962,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1097,  1097,   962,     0,  1097,     0,     0,     0,
       0,     0,   962,  2151,     0,     0,     0,     0,     0,     0,
       0,     0,   962,     0,     0,   962,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     962,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   962,     0,     0,     0,   944,  1097,  1409,     0,   945,
     607,     0,     0,     0,     0,   962,   946,   947,     0,     0,
       0,   962,   948,   949,   950,   951,     0,   962,   953,   954,
     955,   956,   957,   958,     0,   959,   944,   960,   961,     0,
     945,   607,     0,     0,     0,     0,     0,   946,   947,     0,
       0,     0,  1421,   948,   949,   950,   951,     0,     0,   953,
     954,   955,   956,   957,   958,     0,   959,   944,   960,   961,
       0,   945,   607,     0,     0,     0,     0,     0,   946,   947,
       0,     0,     0,  1424,   948,   949,   950,   951,     0,     0,
     953,   954,   955,   956,   957,   958,     0,   959,   944,   960,
     961,     0,   945,   607,     0,     0,     0,     0,     0,   946,
     947,     0,     0,     0,  1425,   948,   949,   950,   951,     0,
       0,   953,   954,   955,   956,   957,   958,     0,   959,     0,
     960,   961,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   377,   378,   379,   380,     0,   382,     0,
     383,     0,   385,   386,   962,   387,     0,     0,     0,   389,
       0,   391,   392,     0,     0,   394,   395,   396,     0,   398,
       0,   400,   401,     0,     0,   408,     0,     0,     0,     0,
     411,   412,   413,   414,   415,   416,   417,   418,     0,   419,
     420,     0,     0,   425,   426,   427,     0,   429,   962,   431,
       0,     0,   434,   435,     0,     0,     0,     0,     0,     0,
     442,   443,     0,     0,   446,     0,     0,     0,   962,     0,
     962,   452,     0,   454,   962,   455,   456,     0,     0,     0,
     461,   462,   463,     0,     0,   465,     0,     0,   467,   468,
       0,   470,   471,   472,     0,     0,     0,   962,   477,   478,
     962,     0,   480,     0,   482,   483,   484,     0,   486,   487,
       0,     0,   489,     0,   491,   492,   493,   494,   495,   496,
       0,  1123,     0,     0,     0,     0,   504,   505,   506,     0,
       0,     0,     0,   511,   512,   513,     0,   515,   516,   517,
     518,   519,   520,   521,   522,   523,   524,   525,   526,   527,
     528,   529,   530,   531,   532,   533,   534,   535,   536,   537,
     538,   539,   540,     0,   542,     0,   544,     0,   545,     0,
     546,   547,     0,   549,   550,   551,   552,   553,   554,   555,
     556,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   944,     0,     0,     0,   945,   607,
       0,     0,     0,     0,   962,   946,   947,     0,   962,   962,
    1460,   948,   949,   950,   951,     0,   962,   953,   954,   955,
     956,   957,   958,     0,   959,     0,   960,   961,     0,     0,
       0,     0,     0,   634,     0,     0,   647,   944,   650,     0,
       0,   945,   607,     0,     0,     0,     0,   962,   946,   947,
       0,     0,     0,  1507,   948,   949,   950,   951,     0,     0,
     953,   954,   955,   956,   957,   958,   962,   959,   944,   960,
     961,   962,   945,   607,     0,     0,     0,     0,     0,   946,
     947,   962,   962,     0,  1508,   948,   949,   950,   951,     0,
       0,   953,   954,   955,   956,   957,   958,     0,   959,     0,
     960,   961,     0,     0,     0,     0,     0,     0,   944,     0,
       0,   962,   945,   607,     0,     0,     0,     0,     0,   946,
     947,   962,   962,     0,  1640,   948,   949,   950,   951,     0,
       0,   953,   954,   955,   956,   957,   958,     0,   959,   944,
     960,   961,     0,   945,   607,     0,     0,     0,     0,     0,
     946,   947,     0,     0,     0,  1909,   948,   949,   950,   951,
       0,     0,   953,   954,   955,   956,   957,   958,     0,   959,
       0,   960,   961,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   962,     0,   962,   944,     0,  1708,     0,   945,
     607,     0,     0,   962,     0,     0,   946,   947,   962,     0,
       0,   962,   948,   949,   950,   951,     0,     0,   953,   954,
     955,   956,   957,   958,   962,   959,   944,   960,   961,     0,
     945,   607,     0,     0,     0,     0,     0,   946,   947,     0,
       0,     0,  1947,   948,   949,   950,   951,     0,     0,   953,
     954,   955,   956,   957,   958,     0,   959,     0,   960,   961,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   962,
     944,     0,  1965,     0,   945,   607,     0,     0,     0,     0,
     962,   946,   947,     0,     0,     0,     0,   948,   949,   950,
     951,     0,     0,   953,   954,   955,   956,   957,   958,   962,
     959,   944,   960,   961,     0,   945,   607,     0,     0,     0,
       0,     0,   946,   947,     0,     0,     0,  1996,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,     0,   960,   961,     0,     0,     0,     0,     0,
       0,     0,   962,     0,   962,     0,     0,     0,   962,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     962,   962,   944,     0,  2135,   962,   945,   607,     0,     0,
       0,     0,     0,   946,   947,   962,     0,   934,   935,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,     0,   960,   961,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   962,     0,     0,     0,     0,   962,     0,   962,     0,
       0,     0,   944,     0,  2150,     0,   945,   607,     9,     0,
     962,     0,   962,   946,   947,    10,     0,     0,     0,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,     0,   960,   961,     0,     0,     0,     0,
     962,     0,     0,     0,   962,    11,    12,    13,    14,     0,
      15,    16,    17,    18,    19,    20,   962,     0,    21,    22,
       0,     0,     0,    23,    24,    25,     0,    26,    27,    28,
      29,    30,    31,     0,    32,    33,    34,    35,    36,    37,
       0,    38,    39,    40,    41,    42,    43,     0,     0,    44,
      45,    46,    47,    48,     0,     0,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,     0,    71,
       0,    72,    73,     0,    74,    75,    76,     0,     0,    77,
       0,     0,    78,    79,     0,    80,    81,    82,    83,     0,
      84,    85,    86,    87,    88,    89,    90,    91,    92,     0,
       0,     0,     0,     0,    93,    94,    95,    96,     0,     0,
       0,     0,    97,     0,     0,    98,    99,     0,     0,   100,
     101,     0,   102,     0,     0,     0,   103,     0,   104,     0,
     105,     0,     0,     0,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,     0,   116,   117,   118,     0,   119,
       0,   120,   121,     0,   122,     0,   123,   124,   125,   126,
       0,     0,   127,   128,   129,     0,   130,   131,   132,     0,
     133,   134,   135,     0,   136,     0,   137,   138,   139,   140,
     141,     0,   142,     0,   143,   144,     0,     0,   145,   146,
     147,     0,     0,   148,   149,     0,   150,   151,     0,   152,
     153,     0,     0,     0,   154,   155,     0,     0,   156,     0,
       0,   157,     0,     0,     0,   158,   159,     0,     0,   160,
     161,   162,     0,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,     0,   174,     0,     0,   175,     0,
       0,     0,   176,   177,   178,   179,   180,     0,   181,   182,
       0,     0,   183,   184,   185,   186,     0,     0,     0,     0,
     187,   188,   189,   190,   191,   192,     0,     0,     0,     0,
       0,     0,     0,   193,     0,     0,   194,   195,   196,   197,
     198,     0,     0,     0,   199,   200,   201,   202,   203,   204,
     944,   205,   206,     0,   945,   607,   207,     0,     0,     0,
       0,   946,   947,     0,     0,     0,  2235,   948,   949,   950,
     951,     0,     0,   953,   954,   955,   956,   957,   958,     0,
     959,   944,   960,   961,     0,   945,   607,     0,     0,     0,
       0,     0,   946,   947,     0,     0,     0,  2243,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,   944,   960,   961,     0,   945,   607,     0,     0,
       0,     0,     0,   946,   947,     0,     0,     0,  2258,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,   944,   960,   961,  2269,   945,   607,     0,
       0,     0,     0,     0,   946,   947,     0,     0,     0,     0,
     948,   949,   950,   951,     0,     0,   953,   954,   955,   956,
     957,   958,     0,   959,   944,   960,   961,     0,   945,   607,
       0,     0,     0,     0,     0,   946,   947,     0,     0,     0,
    2273,   948,   949,   950,   951,     0,     0,   953,   954,   955,
     956,   957,   958,     0,   959,   944,   960,   961,     0,   945,
     607,     0,     0,     0,     0,     0,   946,   947,     0,     0,
       0,  2332,   948,   949,   950,   951,     0,     0,   953,   954,
     955,   956,   957,   958,     0,   959,   944,   960,   961,     0,
     945,   607,     0,     0,     0,     0,     0,   946,   947,     0,
       0,     0,  2333,   948,   949,   950,   951,     0,     0,   953,
     954,   955,   956,   957,   958,     0,   959,   944,   960,   961,
    2356,   945,   607,     0,     0,     0,     0,     0,   946,   947,
       0,     0,     0,     0,   948,   949,   950,   951,     0,     0,
     953,   954,   955,   956,   957,   958,     0,   959,   944,   960,
     961,     0,   945,   607,     0,     0,     0,     0,     0,   946,
     947,     0,     0,     0,  2363,   948,   949,   950,   951,     0,
       0,   953,   954,   955,   956,   957,   958,     0,   959,   944,
     960,   961,  2381,   945,   607,     0,     0,     0,     0,     0,
     946,   947,     0,     0,     0,     0,   948,   949,   950,   951,
       0,     0,   953,   954,   955,   956,   957,   958,     0,   959,
     944,   960,   961,     0,   945,   607,     0,     0,     0,     0,
       0,   946,   947,     0,     0,     0,  2404,   948,   949,   950,
     951,     0,     0,   953,   954,   955,   956,   957,   958,     0,
     959,   944,   960,   961,  2452,   945,   607,     0,     0,     0,
       0,     0,   946,   947,     0,     0,     0,     0,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,   944,   960,   961,     0,   945,   607,     0,     0,
       0,     0,     0,   946,   947,     0,     0,     0,  2454,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,   944,   960,   961,     0,   945,   607,     0,
       0,     0,     0,     0,   946,   947,     0,     0,     0,  2466,
     948,   949,   950,   951,     0,     0,   953,   954,   955,   956,
     957,   958,     0,   959,   944,   960,   961,  2467,   945,   607,
       0,     0,     0,     0,     0,   946,   947,     0,     0,     0,
       0,   948,   949,   950,   951,     0,     0,   953,   954,   955,
     956,   957,   958,     0,   959,   944,   960,   961,  2471,   945,
     607,     0,     0,     0,     0,     0,   946,   947,     0,     0,
       0,     0,   948,   949,   950,   951,     0,     0,   953,   954,
     955,   956,   957,   958,     0,   959,   944,   960,   961,     0,
     945,   607,     0,     0,     0,     0,     0,   946,   947,     0,
       0,     0,  2476,   948,   949,   950,   951,     0,     0,   953,
     954,   955,   956,   957,   958,     0,   959,   944,   960,   961,
       0,   945,   607,     0,     0,     0,     0,     0,   946,   947,
       0,     0,     0,  2504,   948,   949,   950,   951,     0,     0,
     953,   954,   955,   956,   957,   958,     0,   959,   944,   960,
     961,  2518,   945,   607,     0,     0,     0,     0,     0,   946,
     947,     0,     0,     0,     0,   948,   949,   950,   951,     0,
       0,   953,   954,   955,   956,   957,   958,     0,   959,     0,
     960,   961,   944,     0,  2537,     0,   945,   607,     0,     0,
       0,     0,     0,   946,   947,     0,     0,     0,     0,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,   944,   960,   961,     0,   945,   607,     0,
       0,     0,     0,     0,   946,   947,     0,     0,     0,     0,
     948,   949,   950,   951,     0,     0,   953,   954,   955,   956,
     957,   958,     0,   959,   944,   960,   961,     0,   945,     0,
       0,     0,     0,     0,     0,   946,   947,     0,     0,     0,
       0,   948,   949,   950,   951,     0,     0,   953,   954,   955,
     956,   957,   958,     0,   959,     0,   960,   961
};

static const yytype_int16 yycheck[] =
{
      50,   486,   493,   494,   495,   496,   483,   392,  1024,  1448,
     634,   887,  1486,   750,   918,   375,   378,   401,   541,   630,
    1131,   968,  1328,   387,    62,    63,   887,   411,   256,   413,
     430,    69,  1619,   261,   418,   419,   420,   967,   968,   888,
     877,  1299,   426,   763,  1589,   429,   974,   431,  1697,  1698,
    1699,   472,   656,   209,    76,   645,   998,  2001,  1506,  1299,
      98,  1842,   258,  1844,  1595,   261,   262,  1482,   118,  1962,
    2065,  1595,   122,   433,  1147,   467,    63,    99,  1064,   655,
     656,   442,   845,   480,  1518,   743,   388,  1606,    15,  1361,
     450,   845,    12,  1518,   116,  1890,  1895,  1896,    18,  2174,
    1615,   388,     0,     1,   464,  1620,   468,   116,    28,   469,
    1518,    31,   175,   473,    34,   475,  1661,     5,    41,  1518,
      71,    41,   144,  1518,  1518,    45,    16,    17,   488,    49,
    1518,    94,     8,    15,     7,   182,    15,    15,   182,   673,
     500,   129,   502,   191,  1020,    15,     7,    67,     8,   509,
      21,    71,    72,     8,     7,   406,  1172,    77,    78,  1020,
      20,   315,     8,    83,    84,    20,    86,    87,    88,    89,
      58,    91,     5,  1159,     7,    15,     8,    15,   166,     5,
     100,    15,  1043,    15,    15,   105,   224,   107,   211,   609,
    1839,   111,    15,     5,   477,   115,   350,   117,    86,    47,
      15,  1516,    92,  1518,   124,  1520,    64,  1522,   128,    21,
     800,   169,   132,     5,   134,   169,   637,    21,   241,   129,
     641,   141,   142,  2380,    37,   145,   146,   648,    23,   480,
     513,   151,     8,   153,   282,   655,   656,   157,   126,  1545,
     129,     3,    37,   116,   472,    37,     8,   129,   257,   137,
     129,   129,   150,   129,   208,   116,   116,   208,   129,   149,
     323,   116,   172,   116,   184,  2340,   186,   230,   315,   633,
     149,   318,   574,   193,   318,   809,    15,   165,   763,   149,
    1184,   129,   584,   585,   586,   587,   209,   878,  1516,   129,
    1518,   129,  1520,   116,  1522,   129,   129,   129,   129,  2094,
    1516,   588,  1518,   129,  1520,   228,   129,  1894,   692,  2108,
    2109,   201,  2469,   904,   129,   696,   129,   129,   266,   246,
       8,   284,   116,  1434,   282,   129,   690,   724,   282,   177,
       3,   712,   295,   818,   129,   373,   374,     3,     8,   287,
     116,   379,   380,   381,   382,   969,   384,   385,    21,    37,
     388,   751,   390,   391,   392,   393,  1596,  1597,   396,   257,
      26,   399,  1292,   401,   647,  1293,  1638,   650,   406,  1843,
     258,   409,   674,   411,  1893,   413,  1891,  1811,   799,   417,
     418,   419,   420,  2276,   257,  1516,  1811,  1518,   426,  1520,
     428,   429,   982,   431,   984,   697,   257,   257,  2047,   437,
     438,   991,   257,  1811,   257,  1333,   444,   445,   254,   637,
    1426,   449,  1811,   641,   811,   129,  1811,  1811,   456,  1182,
     648,   459,   998,  1811,  1366,  1367,  1368,   788,  1086,  1183,
     417,  1447,   256,   471,   257,   149,   474,   261,   148,   489,
     840,    15,   480,   481,   482,   483,   116,   485,   486,     3,
      92,  1171,   147,   148,   492,   493,   494,   495,   496,     8,
     498,    15,   843,   501,     8,   503,   504,  1078,     3,   507,
     180,     5,   510,   854,    23,     9,   514,     8,  1406,  2522,
    2129,   257,   129,  1798,   404,   180,     3,     8,    37,    20,
     871,    26,   794,  2536,   544,    23,  1811,  1483,   548,  1103,
       8,    18,  1817,    37,   257,   543,  1821,   149,   191,    37,
    2505,  2506,     8,  1586,  1587,   798,    37,   800,  2105,   569,
    2464,     8,   560,   915,   905,    21,   576,  1103,  2002,  1835,
     568,    15,     8,   571,   572,   573,   574,   839,   919,     8,
     842,   579,   580,   565,   566,   583,   584,   585,   586,   587,
       8,    20,   916,   591,   592,   577,  2087,     8,     8,   201,
    2453,  1151,  2010,  2087,   985,   986,   987,   988,   989,    20,
    1798,   799,   116,   769,     5,     8,  1991,  2096,   998,     5,
       3,     8,  1798,  1811,    15,   116,     8,   257,   508,  1817,
      16,    17,    15,  1821,  2375,  1811,    22,     8,    24,   963,
       8,  1817,     5,   759,    37,   288,     9,    33,   128,    35,
     434,    37,    20,     8,   128,   665,   299,  2510,     3,   657,
     658,   659,   660,   661,   662,   663,  1007,  1885,   666,   116,
      15,  1012,  1890,   683,    37,   191,   674,   157,   194,   463,
     116,    18,     8,   157,     8,  1885,   470,   116,   472,    26,
    2560,  2561,     7,   183,   692,    64,     8,   695,   116,   697,
       8,   191,  2249,  2573,     8,   116,   116,  1798,   231,   232,
    1546,  1035,  1053,    15,   237,     8,     8,     8,   208,     8,
    1811,  1045,   720,  1103,    41,  1546,  1817,  1524,  1525,  1526,
    2339,  1528,  1529,   743,   116,  1432,    84,   217,    43,  1641,
    1642,  1643,  1306,     8,     8,   116,  1634,   745,   116,   747,
     760,    15,   750,   257,    71,     3,    21,   191,     8,     8,
     129,   116,    67,  2372,   774,   763,   257,    15,  1448,  1449,
    1306,   781,    21,   159,   208,   773,   162,    82,     7,    15,
     149,  2390,    87,    88,   782,  1673,   784,    16,    17,    94,
     116,   789,   116,   791,     7,     8,   794,   985,   986,   987,
     988,   989,     8,  1353,  1354,  1355,  1356,  1357,   116,   807,
     257,     8,   116,    15,    16,    17,     8,     5,   135,     7,
     818,   257,     8,   116,    21,   116,   824,   116,   257,    21,
    1366,  1367,  1368,    89,  1427,    21,  1429,   847,  1415,   257,
    1417,   839,     8,   191,   842,  2350,   257,   257,   282,     8,
     848,   285,   169,   637,     8,    21,   244,   641,     8,     8,
     208,   247,    21,     3,   648,     8,   122,  2301,     8,   653,
     880,    21,    21,     3,   191,   257,  2094,     8,    21,   889,
     890,   891,    95,     8,   894,    15,   257,     8,     8,   257,
      21,   208,   209,     7,     8,   251,    21,   895,   896,   155,
      21,    21,   257,     8,   221,  1265,   223,   224,     5,   226,
       3,   124,   229,   911,     8,   897,    21,   265,    15,   305,
     306,   307,    15,   228,   934,     8,  1306,   129,  1308,   185,
     912,   257,  1273,   257,   282,   933,     7,   285,    21,  2373,
     256,   289,   330,     8,    15,   261,     8,   149,     8,   257,
    1291,   299,     8,   257,   259,     8,   304,   345,   346,   347,
       8,    21,   218,     8,   257,   282,   257,     5,   257,   316,
     317,     9,  2371,  1880,  1881,  1882,  1883,  1884,    16,    17,
       8,  2247,   195,    21,     8,    23,  1366,  1367,  1368,   202,
    1880,  1881,  1882,  1883,  1884,     8,   252,    21,   303,    37,
      15,     8,     8,   320,  1868,   322,     8,  1017,    21,     8,
      84,     8,  1848,     8,    21,   799,  1340,  1838,  1016,   191,
    1342,     8,  1346,  1720,  1022,     8,  1914,  1848,  2462,     7,
    1028,     7,     8,   913,  1296,    15,   208,  1846,  1847,     8,
       7,     8,  1930,  1041,  1853,  1854,     8,   149,  1936,  1633,
    1938,     5,    21,  2124,   407,     9,   351,   352,     8,    21,
    2459,     8,    16,    17,  1386,     8,  1746,    21,  1066,    23,
      63,    21,     8,  1071,    21,    68,  1086,  1399,    21,  1420,
      15,  1425,     8,    37,  1094,    21,     8,     8,  1412,  1087,
     348,   349,   876,   877,  1930,    21,   452,    90,    91,    21,
      21,     8,    15,   887,  1102,  1641,  1642,  1643,     8,  1930,
     282,    15,     5,   285,    21,  1936,     9,   191,   290,     8,
     149,    21,     7,    16,    17,    15,   298,    15,  1138,     8,
      23,   487,    21,  1013,   208,    22,     8,    24,  1458,   147,
     148,   497,    21,     8,    37,     8,    33,     8,    35,    21,
       8,   144,    63,    15,     8,    15,   679,    68,    21,    15,
      21,   154,  1842,    21,  1844,     8,   519,    21,   521,    16,
      17,  1181,   180,    15,    21,   528,   529,   530,    21,    90,
      91,   534,   535,   536,   537,  1505,   539,   540,    15,   858,
     859,   265,   861,     8,   550,   551,   552,   553,   554,     8,
       8,   985,   986,   987,   988,   989,    21,   200,   282,   993,
     994,   285,    21,    21,  1224,   289,   290,    15,     8,     8,
    1544,     8,     8,    15,   298,   299,  1236,  1237,  1238,     8,
     304,    21,    21,   144,    21,    21,  1020,    15,   231,  2110,
    2111,  2112,   129,   154,    16,    17,    15,    16,    17,    37,
    1260,   244,     8,   609,   610,   611,  1637,   613,    27,  1043,
       8,  1641,  1642,  1643,   620,    21,    15,    36,  1266,  1267,
       8,   158,   159,    21,   161,   162,    15,  1760,   634,  1762,
    1763,     8,    15,    21,  1596,  1597,  2357,     8,   124,   200,
      59,  1774,  1290,    15,    21,    64,    15,  1081,  1296,     8,
      21,  1784,  1300,  1786,  1628,   192,   314,   315,     8,  1650,
     318,   319,    21,     8,    22,     7,    24,    15,     5,  2295,
     676,    21,     9,    92,    15,    33,    21,    35,  1652,    16,
      17,   247,   248,   856,    21,   691,    23,   860,     8,  1683,
    1664,  1912,   865,    15,  1668,   868,   869,   234,     8,     8,
      37,    21,     8,     8,     8,     8,  2244,   993,   994,     8,
     247,    21,    21,   719,    15,    21,    21,    21,    21,  1919,
     726,  1712,    21,  1371,     5,    15,   732,   733,  2199,     8,
     149,  2269,     8,    15,    15,    16,    17,  2223,    15,    16,
      17,     8,    21,     8,  1274,    21,    27,     8,     8,     8,
      27,   924,  2223,   926,    21,    36,    21,     8,     8,    36,
      21,    21,    21,    15,     5,     8,  1414,  1415,     9,  1417,
      21,    21,   778,    15,    15,    16,    17,  1425,    21,  1427,
       8,  1429,   201,    64,  1432,    15,    27,    64,    15,     8,
    2556,  2557,    63,    21,     5,    36,   124,    68,     9,  1637,
     158,   159,    21,   161,   162,    16,    17,  2437,  2438,   812,
      15,    92,   866,   867,     8,    92,  1464,    15,     5,    90,
      91,  1481,     9,    64,     8,  1473,    37,    21,   239,    16,
      17,  1479,    15,    15,   192,     3,     8,    21,    15,   246,
    1500,  1489,  1502,     8,     8,   851,   852,     8,   129,    21,
      37,    92,   129,   124,   246,    15,    21,    21,  1506,   338,
      21,     8,   341,   342,   343,    21,     8,  1527,   149,     8,
      15,     8,   149,   144,    21,     5,   234,  2503,  1538,     9,
      26,     8,    21,   154,    21,     7,    16,    17,   129,   247,
      15,     8,  1552,    23,    21,    25,     8,  1557,    28,    29,
      30,    31,    32,  1563,    21,     8,     8,    37,   149,    21,
     240,  1571,     8,     8,  1641,  1642,  1643,     8,    21,    21,
     201,  1581,     8,  1583,   201,   166,    21,  1361,  1576,   200,
      21,    15,    15,     8,    15,    21,    15,   175,   944,   945,
     946,   947,   948,   949,   950,   951,   952,   953,   954,   955,
     956,   957,   958,   959,   960,   961,   962,   149,  1606,  1931,
     201,     8,   233,   969,  1934,    15,     8,  2297,   974,     8,
      15,  1619,     8,   244,     8,   981,  2197,   983,   203,   204,
     205,   206,   207,    26,    15,     7,    62,    15,    15,     8,
    1638,    21,   998,  1641,  1642,  1643,     8,    15,   287,     8,
     314,     8,     8,  1651,   213,     8,     8,  2002,     9,    64,
      15,   129,     9,    21,     8,     8,     8,    15,  1024,    67,
    1193,    37,    26,     8,  1030,    15,  1199,    15,    15,    15,
    1203,  1465,  1205,  1206,  1207,  1683,  1209,    15,  1211,    15,
    1046,  2371,    15,     8,  2374,  2375,     7,   129,    15,     7,
      21,  1054,    21,    21,     5,  1703,     7,    37,    18,    10,
      26,     7,  1065,    14,    15,    16,    17,    15,     7,    26,
       8,    15,  1720,    15,  1508,     8,    27,     8,    21,    15,
      21,    15,     5,    34,     8,    36,    15,  2174,    15,    15,
    1524,  1525,  1526,    15,  1528,  1529,    21,    15,  1532,    15,
      21,    15,    21,    62,  2434,     8,  1754,  1755,     7,    15,
       8,  2085,  1546,    64,     8,     8,     8,   255,    15,     7,
     166,  2095,     8,   239,    15,  1131,    21,    16,  2458,  2459,
       7,  1779,    15,    10,    15,    15,   208,    14,    15,    16,
      17,    92,    21,  1149,     7,    15,   315,    15,    15,   330,
      27,   331,   246,    15,    15,  1158,    15,    34,    15,    36,
      15,    15,     5,    15,    15,    15,  1172,  1173,  1174,    15,
      15,    15,    15,    16,    17,    15,     7,   246,   129,   246,
    1840,   246,  2512,   246,    27,  2286,  2287,    64,   434,  1192,
       5,     8,  1852,    36,    15,  1198,  1856,    15,   149,  1202,
       8,  1204,    21,  1637,  1638,  1208,     7,  1210,    21,    21,
      21,    21,     8,    15,     8,    92,    21,   463,   173,    26,
      15,    64,    15,   255,   470,     8,     7,  1400,    15,    21,
      44,     5,   478,  1239,   208,    15,    10,    71,     8,    18,
      14,    15,    16,    17,    15,  1893,  1894,  1895,  1896,    92,
     201,    15,   129,    27,    15,    15,    15,    15,  1906,    21,
      34,     8,    36,  1697,  1698,  1699,    21,    21,    21,    21,
      15,    21,   149,    87,    15,    15,  2371,    21,  2373,    93,
      15,     5,    21,    15,     7,  1933,   129,  1293,     9,  1949,
      64,    20,    26,    21,  2266,    21,     5,   132,   254,  1305,
       9,    21,  1475,    21,    21,   119,   149,    16,    17,    15,
    1970,    21,    15,    22,    23,    26,    25,     8,    92,    28,
      29,    30,    31,    32,   201,    26,    21,  1333,    37,   143,
      26,   145,     8,     7,    21,    15,    26,     8,  1986,  1345,
      15,     8,  1990,   589,  1992,    15,     7,     7,    21,    21,
      15,    37,    21,    21,  2002,   129,  2016,    21,   201,    15,
       7,  2009,  2010,     7,   178,  2025,    21,    21,   255,   183,
     353,    21,    15,   187,    15,   149,    15,   191,    15,    15,
     194,    15,    15,  2043,    15,    15,     5,    15,  1394,  1395,
       9,    10,   166,    15,     7,    14,    15,    16,    17,     8,
       8,     8,   216,  1409,  1838,  1839,     7,   653,    27,    21,
    1416,     8,     7,    15,  1848,    34,   230,    36,     8,  1422,
    1426,    21,   236,     7,     7,    21,  1432,   201,  1434,    15,
      78,     8,   352,   329,   351,     7,     7,    15,    17,    21,
      15,  1447,    15,     8,     7,    64,    15,    21,  2096,    17,
       7,    15,     7,    15,   222,  1461,  1267,  2105,  1865,  2107,
    2108,  2109,  2110,  2111,  2112,   128,  1472,  1988,  1471,   896,
    1581,  1185,  1090,    92,   582,  1585,  2244,  2137,  1866,  1485,
     590,   710,   593,  1298,  1490,  2107,  1492,   933,  2249,  1615,
     933,  1885,  1893,  2087,  1154,  1489,  1930,  1721,  1473,   745,
    2156,  1986,  1936,  2175,  1046,  1026,  1409,  1388,  1511,  1088,
     129,  1514,  1694,  1696,  1517,    75,  1519,   763,  1521,  1914,
    1523,  2562,   711,   463,  1936,  1395,  2174,  2175,  1432,   792,
     149,   777,   246,   246,   246,  1035,  1634,  1637,     5,   785,
     786,   246,   998,    10,   790,  1071,  1102,    14,    15,    16,
      17,   657,   838,  1174,   659,   662,  2216,   661,  2296,   660,
      27,  1411,    45,    46,  1413,    48,    49,    34,  2510,    36,
      53,  2231,   818,  2136,  1990,  2357,   807,  2482,    61,  1041,
     826,  1016,   201,    66,  1014,  1591,    69,  1933,  1087,  2416,
    1972,  2370,  2512,  2538,    77,  1028,    79,    64,    81,  2558,
    2560,  2249,  2304,  1746,  1487,   666,  1993,   761,  1449,   855,
     382,   857,  1938,  2047,   504,   415,  1538,  1997,  1798,  1817,
    1773,   104,  1821,  2276,  1811,    92,  1632,  1633,  1634,  1778,
     876,   877,   115,  2023,  2327,  1224,  2343,  2022,  2286,  2287,
    2043,   887,   888,   878,  2199,  1552,  1557,     5,  2296,     7,
    1563,     9,    10,  1571,  2231,    -1,   139,  1663,    16,    17,
    1666,    -1,   129,    21,    22,    23,    24,    25,  1674,   152,
      28,    29,    30,    31,    32,    33,    -1,    35,    -1,    37,
      38,    -1,   149,    -1,    -1,    -1,    -1,   170,    -1,    -1,
      -1,    -1,    -1,   176,    -1,  2129,    -1,    -1,    -1,   166,
    1706,   184,  1708,   186,    -1,   188,    -1,   190,    -1,     7,
      -1,    -1,    10,  2361,   197,    -1,    14,    15,    16,    17,
      -1,   967,   968,  2371,    -1,  2373,  2386,    -1,    -1,    27,
      -1,   214,   215,    -1,   201,  2395,    34,    -1,    36,    -1,
    2400,    -1,  2402,    -1,    -1,    -1,    -1,   993,   994,   232,
      -1,    -1,    -1,    -1,    -1,    -1,   239,  2405,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  2199,    64,  1770,    -1,    -1,
     253,    -1,  1775,   256,  1020,    -1,    -1,  2437,  2438,    -1,
      78,    -1,    -1,    -1,    -1,  1788,    -1,    -1,    -1,  2223,
      -1,    -1,    -1,    -1,    92,    -1,    -1,  1043,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1810,    -1,    -1,
       5,    -1,     7,  1816,    -1,    10,    -1,  1820,    -1,    14,
      15,    16,    17,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   129,    27,     5,  2482,  1081,  2484,    -1,    10,    34,
      -1,    36,    14,    15,    16,    17,    -1,    -1,    -1,  1855,
      -1,   149,    -1,  1859,  1860,    27,    -1,    -1,    -1,    -1,
      -1,  1867,    34,     5,    36,    -1,    -1,     9,    40,    64,
      -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    -1,
    2540,    23,    -1,    25,    -1,  2545,    28,    29,    30,    31,
      32,    -1,    64,    -1,    -1,    37,    -1,    92,  2558,    -1,
    2560,  2561,  1908,   201,  2338,  2339,    -1,    -1,  1914,    -1,
      -1,    -1,    -1,  2573,    -1,    45,    46,    -1,    48,    49,
      92,    -1,    -1,    53,    -1,  1171,    -1,    -1,    -1,    -1,
      -1,    61,    -1,  1939,   129,    -1,    66,    -1,  2372,    69,
      -1,    -1,  1948,    -1,    -1,    -1,    -1,    77,    -1,    79,
      -1,    81,    -1,    -1,   149,    -1,  2390,   129,    -1,  1965,
    1966,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   104,    -1,    -1,   149,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   115,    -1,  1993,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  2001,  1999,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  2011,   201,    -1,    -1,   139,
    2013,  2445,    -1,    -1,    -1,  2018,    -1,    -1,  2021,    -1,
      -1,  2027,   152,  2029,    -1,    -1,    -1,    -1,  2031,   201,
    2033,    -1,  2035,     5,  2037,    -1,    -1,    -1,    10,    -1,
     170,    -1,    14,    15,    16,    17,   176,    -1,    -1,    -1,
      -1,  1297,    -1,    -1,   184,    27,   186,    -1,   188,    -1,
     190,    -1,    34,    -1,    36,     5,    -1,   197,    -1,    -1,
      10,    -1,    -1,    -1,    14,    15,    16,    17,    -1,    -1,
      -1,    -1,    -1,    -1,   214,   215,    -1,    27,    -1,    -1,
      -1,    -1,    64,    -1,    34,    -1,    36,    -1,    -1,    -1,
      -1,    -1,   232,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  2119,    -1,  1361,    -1,    -1,  2124,    -1,
      92,    -1,    -1,   253,    64,    -1,   256,    -1,    -1,  2135,
    2136,     5,    -1,    -1,  2140,    -1,    10,    -1,    -1,    -1,
      14,    15,    16,    17,  2150,    -1,    -1,    -1,    -1,  2155,
      -1,    -1,    92,    27,    -1,    -1,    -1,   129,    -1,    -1,
      34,    -1,    36,     5,   621,    -1,    -1,     9,    -1,    -1,
      -1,    -1,    -1,    15,    16,    17,    -1,   149,    -1,    -1,
      -1,  1427,    -1,  1429,    -1,    27,    -1,    -1,    -1,   129,
      64,    -1,  2198,    -1,    36,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1448,  1449,    -1,    -1,    -1,    -1,    -1,   149,
      -1,    -1,    -1,  1459,    -1,    -1,    -1,    -1,    92,  1465,
      -1,  1467,    64,    -1,  1470,    -1,    -1,    -1,    -1,   201,
      -1,    -1,  2238,    -1,     5,    -1,  1482,    -1,  2244,    10,
    1486,  1487,    -1,    14,    15,    16,    17,    -1,    -1,    -1,
      92,    -1,    -1,    -1,    -1,   129,    27,    -1,    -1,    -1,
      -1,   201,  1508,    34,    -1,    36,    -1,    -1,    -1,  1515,
      -1,    -1,    -1,  2279,    -1,   149,    -1,    -1,  1524,  1525,
    1526,    -1,  1528,  1529,    -1,    -1,  1532,   129,    -1,  2295,
      -1,    -1,    -1,    64,    -1,     7,    -1,    -1,    10,    -1,
    1546,    -1,    14,    15,    16,    17,    -1,   149,    -1,    -1,
      -1,    -1,    -1,    -1,   771,    27,    -1,    -1,   775,    -1,
      -1,    92,    34,    -1,    36,    -1,     7,   201,    -1,    10,
      -1,    -1,    -1,    14,    15,    16,    17,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    27,    -1,    -1,    -1,
    2356,  2357,    64,    34,  2360,    36,  2362,    -1,   129,   201,
    1606,    -1,    -1,    -1,    -1,   822,    -1,    -1,    -1,    -1,
      -1,    -1,  2378,  2379,    -1,  2381,    -1,   834,   149,     7,
      92,    -1,    10,    64,    -1,  2391,    14,    15,    16,    17,
      -1,    -1,  1638,     7,    -1,    -1,    10,    -1,    -1,    27,
      14,    15,    16,    17,    -1,    -1,    34,    -1,    36,    -1,
    2416,    92,    -1,    27,    -1,    -1,    -1,   129,    -1,    -1,
      34,    -1,    36,    -1,   881,   882,   883,   884,    -1,   886,
     201,    -1,    -1,    -1,    -1,    -1,    64,   149,    -1,  2445,
      -1,    -1,    -1,    -1,    -1,    -1,  2452,  2453,   129,    -1,
      64,  1697,  1698,  1699,   166,    -1,    -1,    -1,  2464,    -1,
      -1,  2467,    -1,    -1,    92,  2471,    -1,     7,   149,    -1,
      10,    44,    -1,    -1,    14,    15,    16,    17,    92,    -1,
      -1,   938,    -1,   940,   941,   942,   943,    27,    -1,   201,
      -1,    -1,    -1,    -1,    34,    -1,    36,  2503,    -1,    -1,
    1746,   129,    -1,    -1,  2510,    -1,    -1,    -1,  2514,    -1,
      -1,    -1,  2518,    -1,    87,   129,  2522,    -1,    -1,    -1,
     201,   149,    -1,    -1,    64,    -1,    -1,    -1,    -1,    -1,
    2536,  2537,  2538,     5,    -1,   149,    -1,     9,    -1,    -1,
      -1,    -1,   999,    -1,    16,    17,   119,    -1,    -1,    -1,
      22,    23,    92,    25,    -1,    -1,    28,    29,    30,    31,
      32,    -1,    -1,    35,  1021,    37,    38,    -1,    -1,    -1,
     143,    -1,   145,   201,  1031,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   201,    -1,   129,
      -1,    -1,  1838,  1839,    -1,  1052,  1842,  1843,  1844,    -1,
    1846,  1847,  1848,    -1,    -1,   178,    -1,  1853,  1854,   149,
     183,  1068,    -1,    -1,   187,  1072,  1073,    -1,   191,     7,
      -1,   194,    10,    -1,    -1,    -1,    14,    15,    16,    17,
      -1,    -1,    -1,    -1,  1880,  1881,  1882,  1883,  1884,    27,
      -1,    -1,    -1,   216,    -1,    10,    34,    -1,    36,    14,
      15,    16,    17,    -1,    -1,    -1,    -1,   230,    -1,    -1,
      -1,   201,    27,   236,    -1,    -1,    -1,    -1,    -1,    34,
      -1,    36,    -1,  1130,    -1,    -1,    64,    -1,    -1,    -1,
      -1,    10,    -1,    -1,  1930,    14,    15,    16,    17,    -1,
    1936,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    27,    64,
      -1,    -1,    -1,    -1,    92,    34,    -1,    36,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1969,    -1,    -1,    -1,    92,    -1,    -1,
      -1,  1188,  1189,    -1,    -1,    64,    -1,    -1,    -1,    -1,
      -1,   129,    -1,    -1,    -1,  1991,    -1,    -1,     5,    -1,
      -1,    -1,     9,    10,    -1,    -1,  2002,    -1,    -1,    16,
      17,   149,    -1,    92,   129,    22,    23,    24,    25,    26,
      -1,    28,    29,    30,    31,    32,    33,  2023,    35,    -1,
      37,    38,    -1,    -1,   149,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     5,
     129,  2047,  2048,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,   201,    -1,    -1,    22,    23,    24,    25,
     149,    -1,    28,    29,    30,    31,    32,    33,    34,    35,
      -1,    37,    38,    -1,    -1,    -1,   201,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    2096,    -1,  1309,  1310,  1311,  1312,  1313,  1314,  1315,  1316,
    1317,  1318,  1319,  1320,  1321,  1322,  1323,  1324,  1325,  1326,
    1327,    -1,   201,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    2126,    -1,    -1,  2129,    -1,  2131,  1343,    -1,    -1,    -1,
      -1,    -1,     5,  1350,     7,  1352,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,  1362,    -1,    -1,    -1,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1393,     5,    -1,    -1,
       8,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,
      -1,    -1,    -1,  2199,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,     5,    37,
      38,     8,     9,    10,    -1,    -1,    -1,  2223,    -1,    16,
      17,    -1,    -1,    -1,    -1,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,    -1,
      37,    38,    -1,     5,    -1,  1462,     8,     9,    10,    -1,
      -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,  1476,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,    -1,    37,    38,    -1,    -1,    -1,
    1497,     5,    -1,    -1,    -1,     9,    10,    -1,    -1,    -1,
      -1,  2297,    16,    17,    -1,  2301,    -1,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,    -1,    37,    38,    -1,    -1,    -1,     5,    -1,
      -1,    -1,     9,    10,    -1,    -1,  1543,    -1,    -1,    16,
      17,    -1,  2338,  2339,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,    -1,
      37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  2371,  2372,  2373,  2374,  2375,
      -1,    -1,    -1,  1590,  2380,     5,    -1,     7,    -1,     9,
      10,    -1,    -1,    -1,  2390,    -1,    16,    17,    -1,    -1,
      -1,    -1,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,  1623,    37,    38,    -1,
      -1,    -1,    -1,  1630,    -1,    -1,    -1,    -1,    -1,  1636,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  2434,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1659,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  2458,  2459,  1671,    -1,  2462,    -1,    -1,    -1,
      -1,    -1,  1679,  2469,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1689,    -1,    -1,  1692,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1707,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1718,    -1,    -1,    -1,     5,  2512,     7,    -1,     9,
      10,    -1,    -1,    -1,    -1,  1732,    16,    17,    -1,    -1,
      -1,  1738,    22,    23,    24,    25,    -1,  1744,    28,    29,
      30,    31,    32,    33,    -1,    35,     5,    37,    38,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,     5,    37,    38,
      -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,
      -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,     5,    37,
      38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,    -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,    -1,
      37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    14,    15,    16,    17,    -1,    19,    -1,
      21,    -1,    23,    24,  1871,    26,    -1,    -1,    -1,    30,
      -1,    32,    33,    -1,    -1,    36,    37,    38,    -1,    40,
      -1,    42,    43,    -1,    -1,    46,    -1,    -1,    -1,    -1,
      51,    52,    53,    54,    55,    56,    57,    58,    -1,    60,
      61,    -1,    -1,    64,    65,    66,    -1,    68,  1915,    70,
      -1,    -1,    73,    74,    -1,    -1,    -1,    -1,    -1,    -1,
      81,    82,    -1,    -1,    85,    -1,    -1,    -1,  1935,    -1,
    1937,    92,    -1,    94,  1941,    96,    97,    -1,    -1,    -1,
     101,   102,   103,    -1,    -1,   106,    -1,    -1,   109,   110,
      -1,   112,   113,   114,    -1,    -1,    -1,  1964,   119,   120,
    1967,    -1,   123,    -1,   125,   126,   127,    -1,   129,   130,
      -1,    -1,   133,    -1,   135,   136,   137,   138,   139,   140,
      -1,  1988,    -1,    -1,    -1,    -1,   147,   148,   149,    -1,
      -1,    -1,    -1,   154,   155,   156,    -1,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
     171,   172,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,    -1,   185,    -1,   187,    -1,   189,    -1,
     191,   192,    -1,   194,   195,   196,   197,   198,   199,   200,
     201,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     5,    -1,    -1,    -1,     9,    10,
      -1,    -1,    -1,    -1,  2071,    16,    17,    -1,  2075,  2076,
      21,    22,    23,    24,    25,    -1,  2083,    28,    29,    30,
      31,    32,    33,    -1,    35,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,   254,    -1,    -1,   257,     5,   259,    -1,
      -1,     9,    10,    -1,    -1,    -1,    -1,  2114,    16,    17,
      -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,  2133,    35,     5,    37,
      38,  2138,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,  2148,  2149,    -1,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,    -1,
      37,    38,    -1,    -1,    -1,    -1,    -1,    -1,     5,    -1,
      -1,  2178,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,  2188,  2189,    -1,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,     5,
      37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
      -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  2259,    -1,  2261,     5,    -1,     7,    -1,     9,
      10,    -1,    -1,  2270,    -1,    -1,    16,    17,  2275,    -1,
      -1,  2278,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,  2291,    35,     5,    37,    38,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,    -1,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  2336,
       5,    -1,     7,    -1,     9,    10,    -1,    -1,    -1,    -1,
    2347,    16,    17,    -1,    -1,    -1,    -1,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,  2366,
      35,     5,    37,    38,    -1,     9,    10,    -1,    -1,    -1,
      -1,    -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  2409,    -1,  2411,    -1,    -1,    -1,  2415,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    2427,  2428,     5,    -1,     7,  2432,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,  2442,    -1,   598,   599,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  2478,    -1,    -1,    -1,    -1,  2483,    -1,  2485,    -1,
      -1,    -1,     5,    -1,     7,    -1,     9,    10,     6,    -1,
    2497,    -1,  2499,    16,    17,    13,    -1,    -1,    -1,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,
    2527,    -1,    -1,    -1,  2531,    43,    44,    45,    46,    -1,
      48,    49,    50,    51,    52,    53,  2543,    -1,    56,    57,
      -1,    -1,    -1,    61,    62,    63,    -1,    65,    66,    67,
      68,    69,    70,    -1,    72,    73,    74,    75,    76,    77,
      -1,    79,    80,    81,    82,    83,    84,    -1,    -1,    87,
      88,    89,    90,    91,    -1,    -1,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,    -1,   117,
      -1,   119,   120,    -1,   122,   123,   124,    -1,    -1,   127,
      -1,    -1,   130,   131,    -1,   133,   134,   135,   136,    -1,
     138,   139,   140,   141,   142,   143,   144,   145,   146,    -1,
      -1,    -1,    -1,    -1,   152,   153,   154,   155,    -1,    -1,
      -1,    -1,   160,    -1,    -1,   163,   164,    -1,    -1,   167,
     168,    -1,   170,    -1,    -1,    -1,   174,    -1,   176,    -1,
     178,    -1,    -1,    -1,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,    -1,   193,   194,   195,    -1,   197,
      -1,   199,   200,    -1,   202,    -1,   204,   205,   206,   207,
      -1,    -1,   210,   211,   212,    -1,   214,   215,   216,    -1,
     218,   219,   220,    -1,   222,    -1,   224,   225,   226,   227,
     228,    -1,   230,    -1,   232,   233,    -1,    -1,   236,   237,
     238,    -1,    -1,   241,   242,    -1,   244,   245,    -1,   247,
     248,    -1,    -1,    -1,   252,   253,    -1,    -1,   256,    -1,
      -1,   259,    -1,    -1,    -1,   263,   264,    -1,    -1,   267,
     268,   269,    -1,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   281,    -1,   283,    -1,    -1,   286,    -1,
      -1,    -1,   290,   291,   292,   293,   294,    -1,   296,   297,
      -1,    -1,   300,   301,   302,   303,    -1,    -1,    -1,    -1,
     308,   309,   310,   311,   312,   313,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   321,    -1,    -1,   324,   325,   326,   327,
     328,    -1,    -1,    -1,   332,   333,   334,   335,   336,   337,
       5,   339,   340,    -1,     9,    10,   344,    -1,    -1,    -1,
      -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,     5,    37,    38,    -1,     9,    10,    -1,    -1,    -1,
      -1,    -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,     5,    37,    38,    -1,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    21,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,     5,    37,    38,     8,     9,    10,    -1,
      -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    -1,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,     5,    37,    38,    -1,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,     5,    37,    38,    -1,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    21,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,     5,    37,    38,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,     5,    37,    38,
       8,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,
      -1,    -1,    -1,    -1,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,     5,    37,
      38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,    -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,     5,
      37,    38,     8,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    -1,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
       5,    37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,
      -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,     5,    37,    38,     8,     9,    10,    -1,    -1,    -1,
      -1,    -1,    16,    17,    -1,    -1,    -1,    -1,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,     5,    37,    38,    -1,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    21,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,     5,    37,    38,    -1,     9,    10,    -1,
      -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,     5,    37,    38,     8,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      -1,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,     5,    37,    38,     8,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    -1,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,     5,    37,    38,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,     5,    37,    38,
      -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,
      -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,     5,    37,
      38,     8,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,    -1,    -1,    -1,    -1,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,    -1,
      37,    38,     5,    -1,     7,    -1,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    -1,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,     5,    37,    38,    -1,     9,    10,    -1,
      -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    -1,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,     5,    37,    38,    -1,     9,    -1,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      -1,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,    -1,    37,    38
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,   357,     0,     1,   150,   257,   358,   359,   116,     6,
      13,    43,    44,    45,    46,    48,    49,    50,    51,    52,
      53,    56,    57,    61,    62,    63,    65,    66,    67,    68,
      69,    70,    72,    73,    74,    75,    76,    77,    79,    80,
      81,    82,    83,    84,    87,    88,    89,    90,    91,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   117,   119,   120,   122,   123,   124,   127,   130,   131,
     133,   134,   135,   136,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   152,   153,   154,   155,   160,   163,   164,
     167,   168,   170,   174,   176,   178,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   193,   194,   195,   197,
     199,   200,   202,   204,   205,   206,   207,   210,   211,   212,
     214,   215,   216,   218,   219,   220,   222,   224,   225,   226,
     227,   228,   230,   232,   233,   236,   237,   238,   241,   242,
     244,   245,   247,   248,   252,   253,   256,   259,   263,   264,
     267,   268,   269,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   281,   283,   286,   290,   291,   292,   293,
     294,   296,   297,   300,   301,   302,   303,   308,   309,   310,
     311,   312,   313,   321,   324,   325,   326,   327,   328,   332,
     333,   334,   335,   336,   337,   339,   340,   344,   360,   362,
     365,   377,   378,   382,   383,   384,   390,   391,   392,   393,
     395,   396,   398,   400,   401,   402,   403,   410,   411,   412,
     413,   414,   415,   419,   420,   421,   425,   426,   464,   466,
     479,   522,   523,   525,   526,   532,   533,   534,   535,   542,
     543,   544,   545,   547,   550,   554,   555,   556,   557,   558,
     559,   565,   566,   567,   578,   579,   580,   582,   585,   588,
     593,   594,   596,   598,   600,   603,   604,   628,   629,   640,
     641,   642,   643,   648,   651,   654,   657,   658,   708,   709,
     710,   711,   712,   713,   714,   715,   721,   723,   725,   727,
     729,   730,   731,   732,   733,   736,   738,   739,   740,   743,
     744,   748,   749,   751,   752,   753,   754,   755,   756,   757,
     760,   765,   770,   772,   773,   774,   775,   777,   778,   779,
     780,   781,   782,   799,   802,   803,   804,   805,   811,   814,
     819,   820,   821,   824,   825,   826,   827,   828,   829,   830,
     831,   832,   833,   834,   835,   836,   837,   838,   843,   844,
     845,   846,   847,   857,   858,   859,   861,   862,   863,   864,
     865,   870,   887,    15,   489,   489,   551,   551,   551,   551,
     551,   489,   551,   551,   361,   551,   551,   551,   489,   551,
     489,   551,   551,   489,   551,   551,   551,   488,   551,   489,
     551,   551,     7,    15,   490,    15,   489,   611,   551,   489,
     374,   551,   551,   551,   551,   551,   551,   551,   551,   551,
     551,   129,   367,   531,   531,   551,   551,   551,   489,   551,
     367,   551,   489,   489,   551,   551,   488,   361,   489,   489,
      64,   373,   551,   551,   489,   489,   551,   489,   489,   489,
     489,   489,   551,   428,   551,   551,   551,   367,   465,   361,
     489,   551,   551,   551,   489,   551,   489,   551,   551,   489,
     551,   551,   551,   489,   361,   489,   374,   551,   551,   374,
     551,   489,   551,   551,   551,   489,   551,   551,   489,   551,
     489,   551,   551,   551,   551,   551,   551,    15,   489,   589,
     489,   361,   489,   489,   551,   551,   551,    15,     8,   489,
     489,   551,   551,   551,   489,   551,   551,   551,   551,   551,
     551,   551,   551,   551,   551,   551,   551,   551,   551,   551,
     551,   551,   551,   551,   551,   551,   551,   551,   551,   551,
     551,   489,   551,   489,   551,   551,   551,   551,   489,   551,
     551,   551,   551,   551,   551,   551,   551,   893,   893,   893,
     893,   893,   893,   257,   577,   124,   233,   398,    15,   370,
     577,     8,     8,     8,     8,     7,     8,   124,   362,   385,
       8,   367,   399,     8,     8,     8,     8,     8,   546,     8,
     546,     8,     8,     8,     8,   546,   577,     7,   218,   252,
     523,   525,   534,   535,   239,   543,   543,    10,    14,    15,
      16,    17,    27,    34,    36,    64,    92,   149,   201,   367,
     379,   495,   496,   498,   499,   500,   501,   507,   508,   509,
     510,   511,   514,    15,   551,     5,     9,    15,    16,    17,
     129,   497,   499,   507,   561,   575,   576,   551,    15,   561,
     551,     5,   560,   561,   576,   561,     8,     8,     8,     8,
       8,     8,     8,     8,     7,     8,     8,     5,     7,   367,
     638,   639,   367,   631,   490,    15,    15,   149,   478,   367,
     367,   741,   742,     8,   367,   655,   656,   742,   367,   369,
     367,    15,   527,   573,    23,    37,   367,   417,   418,    15,
     367,   601,   367,   669,   669,   367,   652,   653,   367,   530,
     427,    15,   367,   581,   149,   747,   530,     7,   473,   474,
     489,   612,   613,   367,   607,   613,    15,   552,   367,   583,
     584,   530,    15,    15,   530,   747,   531,   530,   530,   530,
     530,   367,   530,   370,   530,    15,   422,   490,   498,   499,
      15,   364,   367,   367,   649,   650,   480,   481,   482,   483,
       8,   670,   737,    15,   367,   595,   367,   586,   587,   574,
      15,    15,   367,   490,    15,   495,   750,    15,    15,   367,
     724,   726,     8,   367,    37,   416,    15,   499,   500,   490,
      15,    15,   552,   478,   490,   499,   367,   716,     5,    15,
     575,   576,   490,   367,   368,   490,   574,    15,   498,   632,
     633,   607,   611,   367,   599,   367,   696,   696,    15,   367,
     597,   716,   495,   506,   490,   374,    15,   367,   702,   702,
     702,   702,   702,     7,   495,   590,   591,   367,   592,   490,
     363,   367,   490,   367,   722,   724,   367,   489,   490,   367,
     467,    15,    15,   574,   367,    15,   613,    15,   613,   613,
     613,   613,   785,   841,   613,   613,   613,   613,   613,   613,
     785,   367,   374,   848,   849,   850,    15,    15,   374,   860,
      15,   495,   495,   495,   495,   494,   495,    15,    15,    15,
      15,    15,   367,   885,    15,   361,   361,   124,     5,    21,
     367,   371,   372,   366,   374,   367,   367,   367,   418,     7,
     374,   361,   124,   367,   367,     5,    15,   405,   406,   367,
     418,   418,   418,   418,   417,   498,   416,   367,   367,   422,
     429,   430,   432,   433,   551,   551,   239,   408,   495,   496,
     495,   495,   495,   495,     5,     9,    16,    17,    22,    23,
      24,    25,    26,    28,    29,    30,    31,    32,    33,    35,
      37,    38,   379,    15,   246,     3,    15,   246,   246,    15,
     504,   505,    21,   548,   573,   506,     5,     9,   166,   562,
     563,   564,   575,    26,   575,     5,     9,    23,    37,   497,
     574,   575,   574,     8,    15,   499,   568,   569,    15,   495,
     496,   511,   570,   571,   572,   570,   581,   367,   595,   597,
     599,   601,   367,     7,   374,   722,     8,    21,   633,   418,
     520,   495,   240,   546,    15,   374,    15,   472,     8,   573,
       7,   495,   528,   529,   530,    15,   367,   472,   418,   477,
     478,     8,   429,   520,   472,    15,     8,    21,     5,     7,
     475,   476,   495,   367,     8,    21,     5,    58,    86,   126,
     137,   165,   258,   614,   610,   611,   175,   602,   495,   149,
     541,     8,   495,   495,   366,   367,   423,   424,   498,   503,
     367,    26,   367,   536,   537,   539,   370,     8,     8,    15,
     231,   398,   484,   374,     8,   737,   367,   498,   706,   716,
     734,   735,     8,   561,    26,     5,     9,    16,    17,    22,
      23,    24,    25,    28,    29,    30,    31,    32,    33,    34,
      35,    37,    38,   379,   380,   381,   367,   374,   388,   498,
     495,    15,   374,   367,   367,   498,   498,   521,     8,   670,
     728,   367,   498,   659,   367,   462,   463,   541,   418,    18,
     574,   575,   574,   394,   397,   638,   633,     7,   611,   613,
     706,   716,   717,   718,   417,   418,   456,   457,    62,   498,
     761,    15,    15,     7,     8,    21,   589,   418,   370,   418,
     472,     8,   668,   693,    21,   374,   367,     8,   495,   495,
     472,   498,   546,   806,   498,   287,   818,   818,   546,   815,
     818,    15,   546,   783,   546,   822,   783,   783,   546,   800,
     546,   812,   472,   147,   148,   180,   314,   315,   318,   319,
     375,   851,   852,   853,     8,    21,   499,   674,   854,    21,
     854,   375,   852,   374,   758,   759,     8,     8,     8,     8,
     498,   501,   502,   776,   659,   374,   871,   872,   873,   874,
     875,   374,   878,   879,   880,   881,   882,   374,   883,   884,
       8,   374,   888,   889,   367,   363,   361,     8,    21,   213,
     375,   472,    44,    87,    93,   119,   143,   145,   178,   183,
     187,   191,   194,   216,   230,   236,   386,   387,   389,   367,
     361,   489,   552,   573,   399,   472,   546,   546,     8,    37,
      15,   367,   435,   440,   374,    15,   515,    21,     8,   495,
     495,   495,   495,   495,   495,   495,   495,   495,   495,   495,
     495,   495,   495,   495,   495,   495,   495,   495,   573,    64,
     129,   491,   493,   573,   498,   509,   512,    64,   512,   506,
       8,    21,     5,   495,   549,   564,     8,    21,     5,     9,
     495,    21,   495,   575,   575,   575,   575,   575,    21,   568,
     568,     8,   495,   496,   571,   572,     8,     8,     8,   472,
     472,   489,    43,    67,    82,    87,    88,    94,   228,   259,
     303,   642,   639,   374,   502,   518,    21,   367,    15,   494,
      67,   473,   656,   495,     7,     8,    21,   548,    37,     8,
      21,   653,   498,   501,   517,   519,   573,   745,   475,     7,
     472,   613,    15,    15,    15,    15,    15,    15,   602,   613,
     367,    21,   553,   584,    21,    21,    15,     8,    21,     8,
     505,   499,     8,   538,    26,   366,   650,   481,   129,   485,
     486,   487,   403,   169,   208,   282,   374,    15,     7,     8,
      21,   587,   570,    21,    21,   147,   148,   180,    21,    18,
      21,     7,   495,   513,   175,   323,    37,     8,    21,   374,
       8,    21,    26,     8,    21,   553,   495,    21,   458,   459,
     458,    21,     7,   613,   602,    15,     7,     8,    21,     8,
      15,    15,    26,   703,   704,   706,   494,   495,   591,   374,
       8,   693,     8,   668,   399,   389,   376,    21,    21,    21,
     613,   546,    21,   613,   546,   842,   613,   546,   613,   546,
     613,   546,   613,   546,    15,    15,    15,    15,    15,    15,
     374,   850,     8,    21,    21,   182,   315,   318,     8,    21,
     374,   374,   374,   495,    15,    15,     8,    21,    21,   183,
     191,   208,     8,    21,    41,   209,   228,     8,    21,   338,
     341,   342,   343,     8,    21,   374,   244,   330,   345,   346,
     347,     8,    21,   370,   367,   372,    15,   404,   405,   472,
     489,    15,     7,     8,   367,   472,    15,   509,     5,   407,
     495,   564,   418,   498,   432,    15,    16,    17,    27,    36,
      59,    64,    92,   149,   201,   431,   433,   443,   444,   445,
     446,   447,   448,   449,   450,   435,   440,   441,   442,    15,
     436,   437,    62,   495,   570,   496,   491,    21,     8,   492,
     495,   513,   564,     7,   573,   478,   495,   573,     8,   569,
      21,     8,     8,     8,   496,   572,   496,   572,   496,   572,
     367,   255,     8,    21,   478,   477,    21,     7,    21,   495,
     528,    21,   478,   546,     8,    21,   564,   746,     8,    21,
     476,   495,   614,   573,    15,   616,   367,   615,   615,   495,
     615,   472,   613,   239,   530,   494,   424,   424,   367,   495,
     537,    21,   495,   513,     8,    21,    16,    15,    15,    15,
     494,   734,   735,   490,   498,   766,     7,   495,     7,    21,
      21,   367,   609,   499,   498,   498,   613,   660,   495,   463,
     546,     8,    47,   177,   367,   461,   374,   630,   632,   602,
       7,     7,   495,   719,   720,   717,   718,   457,   495,     5,
     616,   762,   763,   769,   495,   626,     8,    21,    15,    21,
      71,   208,   374,   374,   490,   172,   367,   470,   471,   499,
     191,   208,   282,   285,   290,   298,   786,   787,   788,   795,
     807,   808,   809,   613,   266,   816,   817,   818,   613,    37,
     498,   839,   840,    84,   265,   289,   299,   304,   784,   786,
     787,   788,   789,   790,   791,   793,   794,   795,   613,   786,
     787,   788,   789,   790,   791,   793,   794,   795,   808,   809,
     823,   613,   786,   787,   788,   795,   801,   613,   786,   787,
     813,   613,   854,   854,   854,   374,   855,   856,   854,   854,
     499,   759,   330,   315,   331,   573,   491,   502,    15,    15,
      15,   872,    15,    15,    15,   879,    15,    15,    15,   884,
     351,   352,    15,    15,    15,    15,    15,   889,   367,    18,
      26,   409,    15,   388,     7,   374,   404,   553,   553,   408,
       5,   495,   446,   447,   448,   451,   447,   449,   447,   449,
     246,   246,   246,   246,   246,     8,    37,   367,   434,   498,
       5,   436,   437,     8,    15,    16,    17,   149,   367,   434,
     438,   439,   452,   453,   454,   455,    15,   437,    15,    21,
     516,    21,    21,   505,   573,   495,   506,   549,   563,   575,
     539,   540,   496,   540,   540,   540,   472,   367,   634,   637,
     573,     8,    21,     7,   408,   495,   573,   495,   573,   564,
     627,   495,   617,   618,    21,    21,    21,    21,     8,     8,
     254,   524,   530,    21,   486,   487,   674,   674,   674,    21,
      21,   367,    15,    21,   495,     7,     7,   495,   472,   173,
       8,   664,   665,   666,   667,   668,   670,   671,   672,   675,
     677,   678,   679,   693,   701,   539,   459,    15,    15,   460,
     255,     8,     7,     8,    21,    21,    21,     8,    21,    21,
     704,   705,    15,    15,   367,   367,   468,   469,   471,    18,
       8,    26,   785,    15,   785,   785,    15,   613,   807,   785,
     613,   816,   367,     8,    21,    15,   785,    15,   785,    15,
     613,   784,   613,   823,   613,   801,   613,   813,    21,    21,
      21,   316,   317,     8,    21,    21,    21,    15,    15,   491,
      21,   502,   876,   877,   674,   374,   697,   703,   717,   703,
     659,   659,   502,   886,    15,    15,   374,   890,   891,   659,
     659,   495,   374,   892,    21,   495,   495,   644,   645,    21,
     387,   409,     5,   495,   399,     8,    21,     8,   512,   512,
     512,   512,   512,   443,     5,    15,   433,   444,   437,   367,
     434,   442,   452,   453,   453,     8,    21,     7,    16,    17,
       5,    37,     9,   452,   495,    20,   505,   492,    21,    26,
      21,    21,    21,    21,    15,   502,   564,   478,   655,   490,
     517,   564,   746,   495,    21,     7,     8,    21,   495,   374,
      15,    21,    21,    21,     7,   767,   768,   769,   495,   495,
       7,   498,   661,   374,   666,    26,   461,    26,   380,   634,
     632,   367,   605,   606,   607,   608,   720,   763,   613,    78,
     590,   367,   669,   717,   694,     8,   367,   471,   495,   613,
     796,   374,   613,   613,   841,   498,   839,   374,   495,   495,
     613,   613,   613,   613,   856,   674,   498,    21,    26,     8,
      21,    21,    22,    24,    33,    35,   158,   159,   161,   162,
     192,   234,   247,   698,   699,   700,     8,    21,    21,    21,
      21,    21,    21,     8,    21,   374,   866,   867,   866,   315,
     350,     8,    21,    21,    21,    21,   348,   349,     8,     8,
      21,     7,    21,    21,   573,   451,   444,   573,   434,    26,
      21,   452,   439,   453,   453,   454,   454,   454,    21,   495,
       5,   495,   513,   635,   636,   498,     8,   674,   498,     8,
     495,   618,   374,    21,   254,   495,     8,    21,   495,    15,
      41,   135,   191,   209,   221,   223,   224,   226,   229,   320,
     322,   495,   460,    21,    21,    15,     8,   132,   764,    21,
      21,     7,    21,   696,   698,   469,     5,    16,    17,    22,
      24,    33,    35,    37,   159,   162,   247,   305,   306,   307,
     798,    21,    94,   230,   284,   295,   810,    37,   191,   288,
     299,   792,    21,    21,    21,    21,   495,   877,    15,    15,
     374,   502,   353,     8,    21,    21,   891,   495,     7,     7,
     407,    21,   491,   438,   452,    21,     8,     8,    21,   478,
     564,   255,    15,    21,   768,     5,   495,   662,   663,    15,
     680,    15,    15,    15,    15,    15,   702,   702,    15,    15,
      15,     8,   494,   606,   706,   707,    15,   717,   695,   695,
       7,     8,    21,   842,    21,     8,   499,   674,   698,   867,
       8,   868,     8,   869,    21,     7,   408,    21,    21,   495,
     636,   495,   367,   619,   620,   495,     8,    21,   681,   680,
     716,   734,   674,   716,   717,   706,   703,   495,   495,   673,
     661,   676,   495,    21,     8,   374,    21,     7,     8,    21,
     674,   797,   495,   374,    21,     8,   374,   374,   367,   646,
     647,    21,     8,    15,    21,   663,   148,   180,   682,     7,
      21,    21,     7,    21,    15,    21,    21,     8,    21,     8,
      21,     8,   706,    78,   697,   697,    21,   329,   495,   499,
     352,   351,     8,   495,    40,   495,   621,   622,   769,     7,
       7,   683,   684,   706,   734,   717,   590,   495,   661,   495,
      21,    21,    21,    15,    21,    15,    15,   647,   367,   623,
       8,    21,     8,    21,    15,    21,    21,    21,     8,   494,
     866,   866,    17,   624,   625,   622,   684,   495,   685,   686,
      21,   495,    21,    21,    21,   626,    17,     7,     8,    21,
       8,   771,   626,   495,   686,    15,   374,   374,   687,   688,
     689,   690,   691,   182,   318,   128,   157,   217,     8,    21,
       7,     7,    15,   692,   692,   692,   688,   374,   690,   691,
     374,   691,   493,     7,    21,   691
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 787 "gram1.y"
    { (yyval.bf_node) = BFNULL; ;}
    break;

  case 3:
#line 789 "gram1.y"
    { (yyval.bf_node) = set_stat_list((yyvsp[(1) - (3)].bf_node),(yyvsp[(2) - (3)].bf_node)); ;}
    break;

  case 4:
#line 793 "gram1.y"
    { lastwasbranch = NO;  (yyval.bf_node) = BFNULL; ;}
    break;

  case 5:
#line 795 "gram1.y"
    {
	       if ((yyvsp[(2) - (3)].bf_node) != BFNULL) 
               {	    
	          (yyvsp[(2) - (3)].bf_node)->label = (yyvsp[(1) - (3)].label);
	          (yyval.bf_node) = (yyvsp[(2) - (3)].bf_node);
	 	  if (is_openmp_stmt) {            /*OMP*/
			is_openmp_stmt = 0;
			if((yyvsp[(2) - (3)].bf_node)) {                        /*OMP*/
				if ((yyvsp[(2) - (3)].bf_node)->decl_specs != -BIT_OPENMP) (yyvsp[(2) - (3)].bf_node)->decl_specs = BIT_OPENMP; /*OMP*/
			}                               /*OMP*/
		  }                                       /*OMP*/
               }
	    ;}
    break;

  case 6:
#line 809 "gram1.y"
    { PTR_BFND p;

	     if(lastwasbranch && ! thislabel)
               /*if (warn_all)
		 warn("statement cannot be reached", 36);*/
	     lastwasbranch = thiswasbranch;
	     thiswasbranch = NO;
	     if((yyvsp[(2) - (3)].bf_node)) (yyvsp[(2) - (3)].bf_node)->label = (yyvsp[(1) - (3)].label);
	     if((yyvsp[(1) - (3)].label) && (yyvsp[(2) - (3)].bf_node)) (yyvsp[(1) - (3)].label)->statbody = (yyvsp[(2) - (3)].bf_node); /*8.11.06 podd*/
	     if((yyvsp[(1) - (3)].label)) {
		/*$1->statbody = $2;*/ /*8.11.06 podd*/
		if((yyvsp[(1) - (3)].label)->labtype == LABFORMAT)
		  err("label already that of a format",39);
		else
		  (yyvsp[(1) - (3)].label)->labtype = LABEXEC;
	     }
	     if (is_openmp_stmt) {            /*OMP*/
			is_openmp_stmt = 0;
			if((yyvsp[(2) - (3)].bf_node)) {                        /*OMP*/
				if ((yyvsp[(2) - (3)].bf_node)->decl_specs != -BIT_OPENMP) (yyvsp[(2) - (3)].bf_node)->decl_specs = BIT_OPENMP; /*OMP*/
			}                               /*OMP*/
	     }                                       /*OMP*/
             for (p = pred_bfnd; (yyvsp[(1) - (3)].label) && 
		  ((p->variant == FOR_NODE)||(p->variant == WHILE_NODE)) &&
                  (p->entry.for_node.doend) &&
		  (p->entry.for_node.doend->stateno == (yyvsp[(1) - (3)].label)->stateno);
		  p = p->control_parent)
                ++end_group;
	     (yyval.bf_node) = (yyvsp[(2) - (3)].bf_node);
     ;}
    break;

  case 7:
#line 840 "gram1.y"
    { /* PTR_LLND p; */
			doinclude( (yyvsp[(3) - (3)].charp) );
/*			p = make_llnd(fi, STRING_VAL, LLNULL, LLNULL, SMNULL);
			p->entry.string_val = $3;
			p->type = global_string;
			$$ = get_bfnd(fi, INCLUDE_STAT, SMNULL, p, LLNULL); */
			(yyval.bf_node) = BFNULL;
		;}
    break;

  case 8:
#line 849 "gram1.y"
    {
	      err("Unclassifiable statement", 10);
	      flline();
	      (yyval.bf_node) = BFNULL;
	    ;}
    break;

  case 9:
#line 855 "gram1.y"
    { PTR_CMNT p;
              PTR_BFND bif; 
	    
              if (last_bfnd && last_bfnd->control_parent &&((last_bfnd->control_parent->variant == LOGIF_NODE)
	         ||(last_bfnd->control_parent->variant == FORALL_STAT)))
  	         bif = last_bfnd->control_parent;
              else
                 bif = last_bfnd;
              p=bif->entry.Template.cmnt_ptr;
              if(p)
                 p->string = StringConcatenation(p->string,commentbuf);
              else
              {
                 p = make_comment(fi,commentbuf, FULL);
                 bif->entry.Template.cmnt_ptr = p;
              }
 	      (yyval.bf_node) = BFNULL;         
            ;}
    break;

  case 10:
#line 875 "gram1.y"
    { 
	      flline();	 needkwd = NO;	inioctl = NO;
/*!!!*/
              opt_kwd_ = NO; intonly = NO; opt_kwd_hedr = NO; opt_kwd_r = NO; as_op_kwd_= NO; optcorner = NO;
	      yyerrok; yyclearin;  (yyval.bf_node) = BFNULL;
	    ;}
    break;

  case 11:
#line 884 "gram1.y"
    {
	    if(yystno)
	      {
	      (yyval.label) = thislabel =	make_label_node(fi,yystno);
	      thislabel->scope = cur_scope();
	      if (thislabel->labdefined && (thislabel->scope == cur_scope()))
		 errstr("Label %s already defined",convic(thislabel->stateno),40);
	      else
		 thislabel->labdefined = YES;
	      }
	    else
	      (yyval.label) = thislabel = LBNULL;
	    ;}
    break;

  case 12:
#line 900 "gram1.y"
    { PTR_BFND p;

	        if (pred_bfnd != global_bfnd)
		    err("Misplaced PROGRAM statement", 33);
		p = get_bfnd(fi,PROG_HEDR, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL);
		(yyvsp[(3) - (3)].symbol)->entry.prog_decl.prog_hedr=p;
 		set_blobs(p, global_bfnd, NEW_GROUP1);
	        add_scope_level(p, NO);
	        position = IN_PROC;
	    ;}
    break;

  case 13:
#line 912 "gram1.y"
    {  PTR_BFND q = BFNULL;

	      (yyvsp[(3) - (3)].symbol)->variant = PROCEDURE_NAME;
	      (yyvsp[(3) - (3)].symbol)->decl = YES;   /* variable declaration has been seen. */
	      q = get_bfnd(fi,BLOCK_DATA, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL);
	      set_blobs(q, global_bfnd, NEW_GROUP1);
              add_scope_level(q, NO);
	    ;}
    break;

  case 14:
#line 922 "gram1.y"
    { 
              install_param_list((yyvsp[(3) - (4)].symbol), (yyvsp[(4) - (4)].symbol), LLNULL, PROCEDURE_NAME); 
	      /* if there is only a control end the control parent is not set */
              
	     ;}
    break;

  case 15:
#line 929 "gram1.y"
    { install_param_list((yyvsp[(4) - (5)].symbol), (yyvsp[(5) - (5)].symbol), LLNULL, PROCEDURE_NAME); 
              if((yyvsp[(1) - (5)].ll_node)->variant == RECURSIVE_OP) 
                   (yyvsp[(4) - (5)].symbol)->attr = (yyvsp[(4) - (5)].symbol)->attr | RECURSIVE_BIT;
              pred_bfnd->entry.Template.ll_ptr3 = (yyvsp[(1) - (5)].ll_node);
            ;}
    break;

  case 16:
#line 935 "gram1.y"
    {
              install_param_list((yyvsp[(3) - (5)].symbol), (yyvsp[(4) - (5)].symbol), (yyvsp[(5) - (5)].ll_node), FUNCTION_NAME);  
  	      pred_bfnd->entry.Template.ll_ptr1 = (yyvsp[(5) - (5)].ll_node);
            ;}
    break;

  case 17:
#line 940 "gram1.y"
    {
              install_param_list((yyvsp[(1) - (3)].symbol), (yyvsp[(2) - (3)].symbol), (yyvsp[(3) - (3)].ll_node), FUNCTION_NAME); 
	      pred_bfnd->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
	    ;}
    break;

  case 18:
#line 945 "gram1.y"
    {PTR_BFND p, bif;
	     PTR_SYMB q = SMNULL;
             PTR_LLND l = LLNULL;

	     if(parstate==OUTSIDE || procclass==CLMAIN || procclass==CLBLOCK)
	        err("Misplaced ENTRY statement", 35);

	     bif = cur_scope();
	     if (bif->variant == FUNC_HEDR) {
	        q = make_function((yyvsp[(2) - (4)].hash_entry), bif->entry.Template.symbol->type, YES);
	        l = construct_entry_list(q, (yyvsp[(3) - (4)].symbol), FUNCTION_NAME); 
             }
             else if ((bif->variant == PROC_HEDR) || 
                      (bif->variant == PROS_HEDR) || /* added for FORTRAN M */
                      (bif->variant == PROG_HEDR)) {
	             q = make_procedure((yyvsp[(2) - (4)].hash_entry), YES);
  	             l = construct_entry_list(q, (yyvsp[(3) - (4)].symbol), PROCEDURE_NAME); 
             }
	     p = get_bfnd(fi,ENTRY_STAT, q, l, (yyvsp[(4) - (4)].ll_node), LLNULL);
	     set_blobs(p, pred_bfnd, SAME_GROUP);
             q->decl = YES;   /*4.02.03*/
             q->entry.proc_decl.proc_hedr = p; /*5.02.03*/
	    ;}
    break;

  case 19:
#line 969 "gram1.y"
    { PTR_SYMB s;
	      PTR_BFND p;
/*
	      s = make_global_entity($3, MODULE_NAME, global_default, NO);
	      s->decl = YES;  
	      p = get_bfnd(fi, MODULE_STMT, s, LLNULL, LLNULL, LLNULL);
	      s->entry.Template.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
*/
	      /*position = IN_MODULE;*/


               s = make_module((yyvsp[(3) - (3)].hash_entry));
	       s->decl = YES;   /* variable declaration has been seen. */
	        if (pred_bfnd != global_bfnd)
		    err("Misplaced MODULE statement", 33);
              p = get_bfnd(fi, MODULE_STMT, s, LLNULL, LLNULL, LLNULL);
	      s->entry.Template.func_hedr = p; /* !!!????*/
	      set_blobs(p, global_bfnd, NEW_GROUP1);
	      add_scope_level(p, NO);	
	      position =  IN_MODULE;    /*IN_PROC*/
              privateall = 0;
            ;}
    break;

  case 20:
#line 995 "gram1.y"
    { newprog(); 
	      if (position == IN_OUTSIDE)
	           position = IN_PROC;
              else if (position != IN_INTERNAL_PROC){ 
                if(!is_interface_stat(pred_bfnd))
	           position--;
              }
              else {
                if(!is_interface_stat(pred_bfnd))
                  err("Internal procedures can not contain procedures",304);
              }
	    ;}
    break;

  case 21:
#line 1010 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, RECURSIVE_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 22:
#line 1012 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, PURE_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 23:
#line 1014 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, ELEMENTAL_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 24:
#line 1018 "gram1.y"
    { PTR_BFND p;

	      (yyval.symbol) = make_procedure((yyvsp[(1) - (1)].hash_entry), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
             /* if (pred_bfnd != global_bfnd)
		 {
	         err("Misplaced SUBROUTINE statement", 34);
		 }  
              */
	      p = get_bfnd(fi,PROC_HEDR, (yyval.symbol), LLNULL, LLNULL, LLNULL);
              (yyval.symbol)->entry.proc_decl.proc_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 25:
#line 1035 "gram1.y"
    { PTR_BFND p;

	      (yyval.symbol) = make_function((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
             /* if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement", 34); */
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, LLNULL, LLNULL);
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 26:
#line 1049 "gram1.y"
    { PTR_BFND p;
             PTR_LLND l;

	      (yyval.symbol) = make_function((yyvsp[(4) - (4)].hash_entry), (yyvsp[(1) - (4)].data_type), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
              l->type = (yyvsp[(1) - (4)].data_type);
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, l, LLNULL);
              (yyval.symbol)->entry.func_decl.func_hedr = p;
            /*  if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement", 34);*/
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
/*
	      $$ = make_function($4, $1, LOCAL);
	      $$->decl = YES;
	      p = get_bfnd(fi,FUNC_HEDR, $$, LLNULL, LLNULL, LLNULL);
              if (pred_bfnd != global_bfnd)
	         errstr("cftn.gram: misplaced SUBROUTINE statement.");
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
*/
           ;}
    break;

  case 27:
#line 1073 "gram1.y"
    { PTR_BFND p;
             PTR_LLND l;
	      (yyval.symbol) = make_function((yyvsp[(5) - (5)].hash_entry), (yyvsp[(1) - (5)].data_type), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              if((yyvsp[(2) - (5)].ll_node)->variant == RECURSIVE_OP)
	         (yyval.symbol)->attr = (yyval.symbol)->attr | RECURSIVE_BIT;
              l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
              l->type = (yyvsp[(1) - (5)].data_type);
             /* if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement", 34);*/
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, l, (yyvsp[(2) - (5)].ll_node));
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 28:
#line 1089 "gram1.y"
    { PTR_BFND p;

	      (yyval.symbol) = make_function((yyvsp[(4) - (4)].hash_entry), TYNULL, LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              if((yyvsp[(1) - (4)].ll_node)->variant == RECURSIVE_OP)
	        (yyval.symbol)->attr = (yyval.symbol)->attr | RECURSIVE_BIT;
              /*if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement",34);*/
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, LLNULL, (yyvsp[(1) - (4)].ll_node));
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 29:
#line 1103 "gram1.y"
    { PTR_BFND p;
              PTR_LLND l;
	      (yyval.symbol) = make_function((yyvsp[(5) - (5)].hash_entry), (yyvsp[(2) - (5)].data_type), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              if((yyvsp[(1) - (5)].ll_node)->variant == RECURSIVE_OP)
	        (yyval.symbol)->attr = (yyval.symbol)->attr | RECURSIVE_BIT;
              l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
              l->type = (yyvsp[(2) - (5)].data_type);
             /* if (pred_bfnd != global_bfnd)
	          err("Misplaced FUNCTION statement",34);*/
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, l, (yyvsp[(1) - (5)].ll_node));
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 30:
#line 1121 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 31:
#line 1123 "gram1.y"
    { PTR_SYMB s;
              s = make_scalar((yyvsp[(4) - (5)].hash_entry), TYNULL, LOCAL);
              (yyval.ll_node) = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
            ;}
    break;

  case 32:
#line 1130 "gram1.y"
    { (yyval.hash_entry) = look_up_sym(yytext); ;}
    break;

  case 33:
#line 1133 "gram1.y"
    { (yyval.symbol) = make_program(look_up_sym("_MAIN")); ;}
    break;

  case 34:
#line 1135 "gram1.y"
    {
              (yyval.symbol) = make_program((yyvsp[(1) - (1)].hash_entry));
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
            ;}
    break;

  case 35:
#line 1141 "gram1.y"
    { (yyval.symbol) = make_program(look_up_sym("_BLOCK")); ;}
    break;

  case 36:
#line 1143 "gram1.y"
    {
              (yyval.symbol) = make_program((yyvsp[(1) - (1)].hash_entry)); 
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
	    ;}
    break;

  case 37:
#line 1150 "gram1.y"
    { (yyval.symbol) = SMNULL; ;}
    break;

  case 38:
#line 1152 "gram1.y"
    { (yyval.symbol) = SMNULL; ;}
    break;

  case 39:
#line 1154 "gram1.y"
    { (yyval.symbol) = (yyvsp[(2) - (3)].symbol); ;}
    break;

  case 41:
#line 1159 "gram1.y"
    { (yyval.symbol) = set_id_list((yyvsp[(1) - (3)].symbol), (yyvsp[(3) - (3)].symbol)); ;}
    break;

  case 42:
#line 1163 "gram1.y"
    {
	      (yyval.symbol) = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, IO);
            ;}
    break;

  case 43:
#line 1167 "gram1.y"
    { (yyval.symbol) = make_scalar(look_up_sym("*"), TYNULL, IO); ;}
    break;

  case 44:
#line 1173 "gram1.y"
    { char *s;

	      s = copyn(yyleng+1, yytext);
	      s[yyleng] = '\0';
	      (yyval.charp) = s;
	    ;}
    break;

  case 45:
#line 1182 "gram1.y"
    { needkwd = 1; ;}
    break;

  case 46:
#line 1186 "gram1.y"
    { needkwd = NO; ;}
    break;

  case 47:
#line 1191 "gram1.y"
    { colon_flag = YES; ;}
    break;

  case 61:
#line 1212 "gram1.y"
    {
	      saveall = YES;
	      (yyval.bf_node) = get_bfnd(fi,SAVE_DECL, SMNULL, LLNULL, LLNULL, LLNULL);
	    ;}
    break;

  case 62:
#line 1217 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,SAVE_DECL, SMNULL, (yyvsp[(4) - (4)].ll_node), LLNULL, LLNULL);
            ;}
    break;

  case 63:
#line 1222 "gram1.y"
    { PTR_LLND p;

	      p = make_llnd(fi,STMT_STR, LLNULL, LLNULL, SMNULL);
	      p->entry.string_val = copys(stmtbuf);
	      (yyval.bf_node) = get_bfnd(fi,FORMAT_STAT, SMNULL, p, LLNULL, LLNULL);
             ;}
    break;

  case 64:
#line 1229 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,PARAM_DECL, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 77:
#line 1245 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, INTERFACE_STMT, SMNULL, LLNULL, LLNULL, LLNULL); 
              add_scope_level((yyval.bf_node), NO);     
            ;}
    break;

  case 78:
#line 1249 "gram1.y"
    { PTR_SYMB s;

	      s = make_procedure((yyvsp[(3) - (3)].hash_entry), LOCAL);
	      s->variant = INTERFACE_NAME;
	      (yyval.bf_node) = get_bfnd(fi, INTERFACE_STMT, s, LLNULL, LLNULL, LLNULL);
              add_scope_level((yyval.bf_node), NO);
	    ;}
    break;

  case 79:
#line 1257 "gram1.y"
    { PTR_SYMB s;

	      s = make_function((yyvsp[(4) - (5)].hash_entry), global_default, LOCAL);
	      s->variant = INTERFACE_NAME;
	      (yyval.bf_node) = get_bfnd(fi, INTERFACE_OPERATOR, s, LLNULL, LLNULL, LLNULL);
              add_scope_level((yyval.bf_node), NO);
	    ;}
    break;

  case 80:
#line 1265 "gram1.y"
    { PTR_SYMB s;


	      s = make_procedure(look_up_sym("="), LOCAL);
	      s->variant = INTERFACE_NAME;
	      (yyval.bf_node) = get_bfnd(fi, INTERFACE_ASSIGNMENT, s, LLNULL, LLNULL, LLNULL);
              add_scope_level((yyval.bf_node), NO);
	    ;}
    break;

  case 81:
#line 1274 "gram1.y"
    { parstate = INDCL;
              (yyval.bf_node) = get_bfnd(fi, CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL); 
	      /*process_interface($$);*/ /*podd 01.02.03*/
              delete_beyond_scope_level(pred_bfnd);
	    ;}
    break;

  case 82:
#line 1282 "gram1.y"
    { (yyval.hash_entry) = look_up_sym(yytext); ;}
    break;

  case 83:
#line 1286 "gram1.y"
    { (yyval.hash_entry) = (yyvsp[(1) - (1)].hash_entry); ;}
    break;

  case 84:
#line 1288 "gram1.y"
    { (yyval.hash_entry) = (yyvsp[(1) - (1)].hash_entry); ;}
    break;

  case 85:
#line 1292 "gram1.y"
    { (yyval.hash_entry) = look_up_op(PLUS); ;}
    break;

  case 86:
#line 1294 "gram1.y"
    { (yyval.hash_entry) = look_up_op(MINUS); ;}
    break;

  case 87:
#line 1296 "gram1.y"
    { (yyval.hash_entry) = look_up_op(ASTER); ;}
    break;

  case 88:
#line 1298 "gram1.y"
    { (yyval.hash_entry) = look_up_op(DASTER); ;}
    break;

  case 89:
#line 1300 "gram1.y"
    { (yyval.hash_entry) = look_up_op(SLASH); ;}
    break;

  case 90:
#line 1302 "gram1.y"
    { (yyval.hash_entry) = look_up_op(DSLASH); ;}
    break;

  case 91:
#line 1304 "gram1.y"
    { (yyval.hash_entry) = look_up_op(AND); ;}
    break;

  case 92:
#line 1306 "gram1.y"
    { (yyval.hash_entry) = look_up_op(OR); ;}
    break;

  case 93:
#line 1308 "gram1.y"
    { (yyval.hash_entry) = look_up_op(XOR); ;}
    break;

  case 94:
#line 1310 "gram1.y"
    { (yyval.hash_entry) = look_up_op(NOT); ;}
    break;

  case 95:
#line 1312 "gram1.y"
    { (yyval.hash_entry) = look_up_op(EQ); ;}
    break;

  case 96:
#line 1314 "gram1.y"
    { (yyval.hash_entry) = look_up_op(NE); ;}
    break;

  case 97:
#line 1316 "gram1.y"
    { (yyval.hash_entry) = look_up_op(GT); ;}
    break;

  case 98:
#line 1318 "gram1.y"
    { (yyval.hash_entry) = look_up_op(GE); ;}
    break;

  case 99:
#line 1320 "gram1.y"
    { (yyval.hash_entry) = look_up_op(LT); ;}
    break;

  case 100:
#line 1322 "gram1.y"
    { (yyval.hash_entry) = look_up_op(LE); ;}
    break;

  case 101:
#line 1324 "gram1.y"
    { (yyval.hash_entry) = look_up_op(NEQV); ;}
    break;

  case 102:
#line 1326 "gram1.y"
    { (yyval.hash_entry) = look_up_op(EQV); ;}
    break;

  case 103:
#line 1331 "gram1.y"
    {
             PTR_SYMB s;
         
             type_var = s = make_derived_type((yyvsp[(4) - (4)].hash_entry), TYNULL, LOCAL);	
             (yyval.bf_node) = get_bfnd(fi, STRUCT_DECL, s, LLNULL, LLNULL, LLNULL);
             add_scope_level((yyval.bf_node), NO);
	   ;}
    break;

  case 104:
#line 1340 "gram1.y"
    { PTR_SYMB s;
         
             type_var = s = make_derived_type((yyvsp[(7) - (7)].hash_entry), TYNULL, LOCAL);	
	     s->attr = s->attr | type_opt;
             (yyval.bf_node) = get_bfnd(fi, STRUCT_DECL, s, (yyvsp[(5) - (7)].ll_node), LLNULL, LLNULL);
             add_scope_level((yyval.bf_node), NO);
	   ;}
    break;

  case 105:
#line 1350 "gram1.y"
    {
	     (yyval.bf_node) = get_bfnd(fi, CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL);
	     if (type_var != SMNULL)
               process_type(type_var, (yyval.bf_node));
             type_var = SMNULL;
	     delete_beyond_scope_level(pred_bfnd);
           ;}
    break;

  case 106:
#line 1358 "gram1.y"
    {
             (yyval.bf_node) = get_bfnd(fi, CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL);
	     if (type_var != SMNULL)
               process_type(type_var, (yyval.bf_node));
             type_var = SMNULL;
	     delete_beyond_scope_level(pred_bfnd);	
           ;}
    break;

  case 107:
#line 1368 "gram1.y"
    { 
	      PTR_LLND q, r, l;
	     /* PTR_SYMB s;*/
	      PTR_TYPE t;
	      int type_opts;

	      vartype = (yyvsp[(1) - (7)].data_type);
              if((yyvsp[(6) - (7)].ll_node) && vartype->variant != T_STRING)
                errstr("Non character entity  %s  has length specification",(yyvsp[(3) - (7)].hash_entry)->ident,41);
              t = make_type_node(vartype, (yyvsp[(6) - (7)].ll_node));
	      type_opts = type_options;
	      if ((yyvsp[(5) - (7)].ll_node)) type_opts = type_opts | DIMENSION_BIT;
	      if ((yyvsp[(5) - (7)].ll_node))
		 q = deal_with_options((yyvsp[(3) - (7)].hash_entry), t, type_opts, (yyvsp[(5) - (7)].ll_node), ndim, (yyvsp[(7) - (7)].ll_node), (yyvsp[(5) - (7)].ll_node));
	      else q = deal_with_options((yyvsp[(3) - (7)].hash_entry), t, type_opts, attr_dims, attr_ndim, (yyvsp[(7) - (7)].ll_node), (yyvsp[(5) - (7)].ll_node));
	      r = make_llnd(fi, EXPR_LIST, q, LLNULL, SMNULL);
	      l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
	      l->type = vartype;
	      (yyval.bf_node) = get_bfnd(fi,VAR_DECL, SMNULL, r, l, (yyvsp[(2) - (7)].ll_node));
	    ;}
    break;

  case 108:
#line 1389 "gram1.y"
    { 
	      PTR_LLND q, r;
	    /*  PTR_SYMB s;*/
              PTR_TYPE t;
	      int type_opts;
              if((yyvsp[(5) - (6)].ll_node) && vartype->variant != T_STRING)
                errstr("Non character entity  %s  has length specification",(yyvsp[(3) - (6)].hash_entry)->ident,41);
              t = make_type_node(vartype, (yyvsp[(5) - (6)].ll_node));
	      type_opts = type_options;
	      if ((yyvsp[(4) - (6)].ll_node)) type_opts = type_opts | DIMENSION_BIT;
	      if ((yyvsp[(4) - (6)].ll_node))
		 q = deal_with_options((yyvsp[(3) - (6)].hash_entry), t, type_opts, (yyvsp[(4) - (6)].ll_node), ndim, (yyvsp[(6) - (6)].ll_node), (yyvsp[(4) - (6)].ll_node));
	      else q = deal_with_options((yyvsp[(3) - (6)].hash_entry), t, type_opts, attr_dims, attr_ndim, (yyvsp[(6) - (6)].ll_node), (yyvsp[(4) - (6)].ll_node));
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (6)].bf_node)->entry.Template.ll_ptr1);
       	    ;}
    break;

  case 109:
#line 1408 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 110:
#line 1410 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 111:
#line 1412 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (5)].ll_node); ;}
    break;

  case 112:
#line 1416 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 113:
#line 1418 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); ;}
    break;

  case 114:
#line 1422 "gram1.y"
    { type_options = type_options | PARAMETER_BIT; 
              (yyval.ll_node) = make_llnd(fi, PARAMETER_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 115:
#line 1426 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 116:
#line 1428 "gram1.y"
    { type_options = type_options | ALLOCATABLE_BIT;
              (yyval.ll_node) = make_llnd(fi, ALLOCATABLE_OP, LLNULL, LLNULL, SMNULL);
	    ;}
    break;

  case 117:
#line 1432 "gram1.y"
    { type_options = type_options | DIMENSION_BIT;
	      attr_ndim = ndim;
	      attr_dims = (yyvsp[(2) - (2)].ll_node);
              (yyval.ll_node) = make_llnd(fi, DIMENSION_OP, (yyvsp[(2) - (2)].ll_node), LLNULL, SMNULL);
            ;}
    break;

  case 118:
#line 1438 "gram1.y"
    { type_options = type_options | EXTERNAL_BIT;
              (yyval.ll_node) = make_llnd(fi, EXTERNAL_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 119:
#line 1442 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (4)].ll_node); ;}
    break;

  case 120:
#line 1444 "gram1.y"
    { type_options = type_options | INTRINSIC_BIT;
              (yyval.ll_node) = make_llnd(fi, INTRINSIC_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 121:
#line 1448 "gram1.y"
    { type_options = type_options | OPTIONAL_BIT;
              (yyval.ll_node) = make_llnd(fi, OPTIONAL_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 122:
#line 1452 "gram1.y"
    { type_options = type_options | POINTER_BIT;
              (yyval.ll_node) = make_llnd(fi, POINTER_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 123:
#line 1456 "gram1.y"
    { type_options = type_options | SAVE_BIT; 
              (yyval.ll_node) = make_llnd(fi, SAVE_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 124:
#line 1460 "gram1.y"
    { type_options = type_options | SAVE_BIT; 
              (yyval.ll_node) = make_llnd(fi, STATIC_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 125:
#line 1464 "gram1.y"
    { type_options = type_options | TARGET_BIT; 
              (yyval.ll_node) = make_llnd(fi, TARGET_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 126:
#line 1470 "gram1.y"
    { type_options = type_options | IN_BIT;  type_opt = IN_BIT; 
              (yyval.ll_node) = make_llnd(fi, IN_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 127:
#line 1474 "gram1.y"
    { type_options = type_options | OUT_BIT;  type_opt = OUT_BIT; 
              (yyval.ll_node) = make_llnd(fi, OUT_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 128:
#line 1478 "gram1.y"
    { type_options = type_options | INOUT_BIT;  type_opt = INOUT_BIT;
              (yyval.ll_node) = make_llnd(fi, INOUT_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 129:
#line 1484 "gram1.y"
    { type_options = type_options | PUBLIC_BIT; 
              type_opt = PUBLIC_BIT;
              (yyval.ll_node) = make_llnd(fi, PUBLIC_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 130:
#line 1489 "gram1.y"
    { type_options =  type_options | PRIVATE_BIT;
               type_opt = PRIVATE_BIT;
              (yyval.ll_node) = make_llnd(fi, PRIVATE_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 131:
#line 1496 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(7) - (7)].hash_entry), TYNULL, LOCAL);
	      s->attr = s->attr | type_opt;	
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, INTENT_STMT, SMNULL, r, (yyvsp[(4) - (7)].ll_node), LLNULL);
	    ;}
    break;

  case 132:
#line 1507 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);	
	      s->attr = s->attr | type_opt;
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
  	    ;}
    break;

  case 133:
#line 1520 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(4) - (4)].hash_entry), TYNULL, LOCAL);	
	      s->attr = s->attr | OPTIONAL_BIT;
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, OPTIONAL_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 134:
#line 1531 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);	
	      s->attr = s->attr | OPTIONAL_BIT;
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
  	    ;}
    break;

  case 135:
#line 1544 "gram1.y"
    { 
	      PTR_LLND r;
	      PTR_SYMB s;

              s = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol; 
              s->attr = s->attr | SAVE_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, STATIC_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 136:
#line 1554 "gram1.y"
    { 
	      PTR_LLND r;
	      PTR_SYMB s;

              s = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol;
              s->attr = s->attr | SAVE_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
  	    ;}
    break;

  case 137:
#line 1567 "gram1.y"
    {
	      privateall = 1;
	      (yyval.bf_node) = get_bfnd(fi, PRIVATE_STMT, SMNULL, LLNULL, LLNULL, LLNULL);
	    ;}
    break;

  case 138:
#line 1572 "gram1.y"
    {
	      /*type_options = type_options | PRIVATE_BIT;*/
	      (yyval.bf_node) = get_bfnd(fi, PRIVATE_STMT, SMNULL, (yyvsp[(5) - (5)].ll_node), LLNULL, LLNULL);
            ;}
    break;

  case 139:
#line 1578 "gram1.y"
    {type_opt = PRIVATE_BIT;;}
    break;

  case 140:
#line 1582 "gram1.y"
    { 
	      (yyval.bf_node) = get_bfnd(fi, SEQUENCE_STMT, SMNULL, LLNULL, LLNULL, LLNULL);
            ;}
    break;

  case 141:
#line 1587 "gram1.y"
    {
	      /*saveall = YES;*/ /*14.03.03*/
	      (yyval.bf_node) = get_bfnd(fi, PUBLIC_STMT, SMNULL, LLNULL, LLNULL, LLNULL);
	    ;}
    break;

  case 142:
#line 1592 "gram1.y"
    {
	      /*type_options = type_options | PUBLIC_BIT;*/
	      (yyval.bf_node) = get_bfnd(fi, PUBLIC_STMT, SMNULL, (yyvsp[(5) - (5)].ll_node), LLNULL, LLNULL);
            ;}
    break;

  case 143:
#line 1598 "gram1.y"
    {type_opt = PUBLIC_BIT;;}
    break;

  case 144:
#line 1602 "gram1.y"
    {
	      type_options = 0;
              /* following block added by dbg */
	      ndim = 0;
	      attr_ndim = 0;
	      attr_dims = LLNULL;
	      /* end section added by dbg */
              (yyval.data_type) = make_type_node((yyvsp[(1) - (4)].data_type), (yyvsp[(3) - (4)].ll_node));
            ;}
    break;

  case 145:
#line 1612 "gram1.y"
    { PTR_TYPE t;

	      type_options = 0;
	      ndim = 0;
	      attr_ndim = 0;
	      attr_dims = LLNULL;
              t = lookup_type((yyvsp[(3) - (5)].hash_entry));
	      vartype = t;
	      (yyval.data_type) = make_type_node(t, LLNULL);
            ;}
    break;

  case 146:
#line 1625 "gram1.y"
    {opt_kwd_hedr = YES;;}
    break;

  case 147:
#line 1630 "gram1.y"
    { PTR_TYPE p;
	      PTR_LLND q;
	      PTR_SYMB s;
              s = (yyvsp[(2) - (2)].hash_entry)->id_attr;
	      if (s)
		   s->attr = (yyvsp[(1) - (2)].token);
	      else {
		p = undeftype ? global_unknown : impltype[*(yyvsp[(2) - (2)].hash_entry)->ident - 'a'];
                s = install_entry((yyvsp[(2) - (2)].hash_entry), SOFT);
		s->attr = (yyvsp[(1) - (2)].token);
                set_type(s, p, LOCAL);
	      }
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(2) - (2)].hash_entry)->id_attr);
	      q = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,ATTR_DECL, SMNULL, q, LLNULL, LLNULL);
	    ;}
    break;

  case 148:
#line 1649 "gram1.y"
    { PTR_TYPE p;
	      PTR_LLND q, r;
	      PTR_SYMB s;
	      int att;

	      att = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1->entry.Template.ll_ptr1->
		    entry.Template.symbol->attr;
              s = (yyvsp[(3) - (3)].hash_entry)->id_attr;
	      if (s)
		   s->attr = att;
	      else {
		p = undeftype ? global_unknown : impltype[*(yyvsp[(3) - (3)].hash_entry)->ident - 'a'];
                s = install_entry((yyvsp[(3) - (3)].hash_entry), SOFT);
		s->attr = att;
                set_type(s, p, LOCAL);
	      }
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].hash_entry)->id_attr);
	      for (r = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1;
		   r->entry.list.next;
		   r = r->entry.list.next) ;
	      r->entry.list.next = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);

	    ;}
    break;

  case 149:
#line 1675 "gram1.y"
    { (yyval.token) = ATT_GLOBAL; ;}
    break;

  case 150:
#line 1677 "gram1.y"
    { (yyval.token) = ATT_CLUSTER; ;}
    break;

  case 151:
#line 1689 "gram1.y"
    {
/*		  varleng = ($1<0 || $1==TYLONG ? 0 : typesize[$1]); */
		  vartype = (yyvsp[(1) - (1)].data_type);
		;}
    break;

  case 152:
#line 1696 "gram1.y"
    { (yyval.data_type) = global_int; ;}
    break;

  case 153:
#line 1697 "gram1.y"
    { (yyval.data_type) = global_float; ;}
    break;

  case 154:
#line 1698 "gram1.y"
    { (yyval.data_type) = global_complex; ;}
    break;

  case 155:
#line 1699 "gram1.y"
    { (yyval.data_type) = global_double; ;}
    break;

  case 156:
#line 1700 "gram1.y"
    { (yyval.data_type) = global_dcomplex; ;}
    break;

  case 157:
#line 1701 "gram1.y"
    { (yyval.data_type) = global_bool; ;}
    break;

  case 158:
#line 1702 "gram1.y"
    { (yyval.data_type) = global_string; ;}
    break;

  case 159:
#line 1707 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 160:
#line 1709 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 161:
#line 1713 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, LEN_OP, (yyvsp[(3) - (5)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 162:
#line 1715 "gram1.y"
    { PTR_LLND l;

                 l = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL); 
                 l->entry.string_val = (char *)"*";
                 (yyval.ll_node) = make_llnd(fi, LEN_OP, l,l, SMNULL);
                ;}
    break;

  case 163:
#line 1722 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi, LEN_OP, (yyvsp[(5) - (6)].ll_node), (yyvsp[(5) - (6)].ll_node), SMNULL);;}
    break;

  case 164:
#line 1726 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 165:
#line 1728 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 166:
#line 1730 "gram1.y"
    { /*$$ = make_llnd(fi, PAREN_OP, $2, LLNULL, SMNULL);*/  (yyval.ll_node) = (yyvsp[(3) - (5)].ll_node);  ;}
    break;

  case 167:
#line 1738 "gram1.y"
    { if((yyvsp[(7) - (9)].ll_node)->variant==LENGTH_OP && (yyvsp[(3) - (9)].ll_node)->variant==(yyvsp[(7) - (9)].ll_node)->variant)
                (yyvsp[(7) - (9)].ll_node)->variant=KIND_OP;
                (yyval.ll_node) = make_llnd(fi, CONS, (yyvsp[(3) - (9)].ll_node), (yyvsp[(7) - (9)].ll_node), SMNULL); 
            ;}
    break;

  case 168:
#line 1745 "gram1.y"
    { if(vartype->variant == T_STRING)
                (yyval.ll_node) = make_llnd(fi,LENGTH_OP,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL);
              else
                (yyval.ll_node) = make_llnd(fi,KIND_OP,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL);
            ;}
    break;

  case 169:
#line 1751 "gram1.y"
    { PTR_LLND l;
	      l = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	      l->entry.string_val = (char *)"*";
              (yyval.ll_node) = make_llnd(fi,LENGTH_OP,l,LLNULL,SMNULL);
            ;}
    break;

  case 170:
#line 1757 "gram1.y"
    { /* $$ = make_llnd(fi, SPEC_PAIR, $2, LLNULL, SMNULL); */
	     char *q;
             q = (yyvsp[(1) - (2)].ll_node)->entry.string_val;
  	     if (strcmp(q, "len") == 0)
               (yyval.ll_node) = make_llnd(fi,LENGTH_OP,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
             else
                (yyval.ll_node) = make_llnd(fi,KIND_OP,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);              
            ;}
    break;

  case 171:
#line 1766 "gram1.y"
    { PTR_LLND l;
	      l = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	      l->entry.string_val = (char *)"*";
              (yyval.ll_node) = make_llnd(fi,LENGTH_OP,l,LLNULL,SMNULL);
            ;}
    break;

  case 172:
#line 1774 "gram1.y"
    {endioctl();;}
    break;

  case 173:
#line 1787 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 174:
#line 1789 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node); ;}
    break;

  case 175:
#line 1792 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, POINTST_OP, LLNULL, (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 176:
#line 1796 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! (yyvsp[(5) - (5)].ll_node)) {
		err("No dimensions in DIMENSION statement", 42);
	      }
              if(statement_kind == 1) /*DVM-directive*/
                err("No shape specification", 65);                
	      s = make_array((yyvsp[(4) - (5)].hash_entry), TYNULL, (yyvsp[(5) - (5)].ll_node), ndim, LOCAL);
	      s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(5) - (5)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(5) - (5)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,DIM_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 177:
#line 1811 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! (yyvsp[(4) - (4)].ll_node)) {
		err("No dimensions in DIMENSION statement", 42);
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
	      s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 178:
#line 1827 "gram1.y"
    {/* PTR_SYMB s;*/
	      PTR_LLND r;

	         /*if(!$5) {
		   err("No dimensions in ALLOCATABLE statement",305);		
	           }
	          s = make_array($4, TYNULL, $5, ndim, LOCAL);
	          s->attr = s->attr | ALLOCATABLE_BIT;
	          q = make_llnd(fi,ARRAY_REF, $5, LLNULL, s);
	          s->type->entry.ar_decl.ranges = $5;
                  r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                */
              (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr | ALLOCATABLE_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, ALLOCATABLE_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 179:
#line 1845 "gram1.y"
    {  /*PTR_SYMB s;*/
	      PTR_LLND r;

	        /*  if(! $4) {
		      err("No dimensions in ALLOCATABLE statement",305);
		
	            }
	           s = make_array($3, TYNULL, $4, ndim, LOCAL);
	           s->attr = s->attr | ALLOCATABLE_BIT;
	           q = make_llnd(fi,ARRAY_REF, $4, LLNULL, s);
	           s->type->entry.ar_decl.ranges = $4;
	           r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                */
              (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr | ALLOCATABLE_BIT;
              r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 180:
#line 1865 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND  r;
           
	          /*  if(! $5) {
		      err("No dimensions in POINTER statement",306);	    
	              } 
	             s = make_array($4, TYNULL, $5, ndim, LOCAL);
	             s->attr = s->attr | POINTER_BIT;
	             q = make_llnd(fi,ARRAY_REF, $5, LLNULL, s);
	             s->type->entry.ar_decl.ranges = $5;
                   */

                  /*s = make_pointer( $4->entry.Template.symbol->parent, TYNULL, LOCAL);*/ /*17.02.03*/
                 /*$4->entry.Template.symbol->attr = $4->entry.Template.symbol->attr | POINTER_BIT;*/
              s = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol; /*17.02.03*/
              s->attr = s->attr | POINTER_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, POINTER_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 181:
#line 1885 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND r;

     	        /*  if(! $4) {
	        	err("No dimensions in POINTER statement",306);
	            }
	           s = make_array($3, TYNULL, $4, ndim, LOCAL);
	           s->attr = s->attr | POINTER_BIT;
	           q = make_llnd(fi,ARRAY_REF, $4, LLNULL, s);
	           s->type->entry.ar_decl.ranges = $4;
                */

                /*s = make_pointer( $3->entry.Template.symbol->parent, TYNULL, LOCAL);*/ /*17.02.03*/
                /*$3->entry.Template.symbol->attr = $3->entry.Template.symbol->attr | POINTER_BIT;*/
              s = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol; /*17.02.03*/
              s->attr = s->attr | POINTER_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 182:
#line 1907 "gram1.y"
    {/* PTR_SYMB s;*/
	      PTR_LLND r;


	     /* if(! $5) {
		err("No dimensions in TARGET statement",307);
	      }
	      s = make_array($4, TYNULL, $5, ndim, LOCAL);
	      s->attr = s->attr | TARGET_BIT;
	      q = make_llnd(fi,ARRAY_REF, $5, LLNULL, s);
	      s->type->entry.ar_decl.ranges = $5;
             */
              (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr | TARGET_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, TARGET_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 183:
#line 1924 "gram1.y"
    {  /*PTR_SYMB s;*/
	      PTR_LLND r;

	     /* if(! $4) {
		err("No dimensions in TARGET statement",307);
	      }
	      s = make_array($3, TYNULL, $4, ndim, LOCAL);
	      s->attr = s->attr | TARGET_BIT;
	      q = make_llnd(fi,ARRAY_REF, $4, LLNULL, s);
	      s->type->entry.ar_decl.ranges = $4;
              */
              (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr | TARGET_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 184:
#line 1942 "gram1.y"
    { PTR_LLND p, q;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,COMM_STAT, SMNULL, q, LLNULL, LLNULL);
	    ;}
    break;

  case 185:
#line 1949 "gram1.y"
    { PTR_LLND p, q;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, (yyvsp[(3) - (4)].symbol));
	      (yyval.bf_node) = get_bfnd(fi,COMM_STAT, SMNULL, q, LLNULL, LLNULL);
	    ;}
    break;

  case 186:
#line 1956 "gram1.y"
    { PTR_LLND p, q;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(5) - (5)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, (yyvsp[(3) - (5)].symbol));
	      add_to_lowList(q, (yyvsp[(1) - (5)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 187:
#line 1963 "gram1.y"
    { PTR_LLND p, r;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      /*q = make_llnd(fi,COMM_LIST, p, LLNULL, SMNULL);*/
	      for (r = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1;
		   r->entry.list.next;
		   r = r->entry.list.next);
	      add_to_lowLevelList(p, r->entry.Template.ll_ptr1);
	    ;}
    break;

  case 188:
#line 1976 "gram1.y"
    { PTR_LLND q, r;

              q = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      r = make_llnd(fi,NAMELIST_LIST, q, LLNULL, (yyvsp[(3) - (4)].symbol));
	      (yyval.bf_node) = get_bfnd(fi,NAMELIST_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 189:
#line 1983 "gram1.y"
    { PTR_LLND q, r;

              q = make_llnd(fi,EXPR_LIST, (yyvsp[(5) - (5)].ll_node), LLNULL, SMNULL);
	      r = make_llnd(fi,NAMELIST_LIST, q, LLNULL, (yyvsp[(3) - (5)].symbol));
	      add_to_lowList(r, (yyvsp[(1) - (5)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 190:
#line 1990 "gram1.y"
    { PTR_LLND q, r;

              q = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      for (r = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1;
		   r->entry.list.next;
		   r = r->entry.list.next);
	      add_to_lowLevelList(q, r->entry.Template.ll_ptr1);
	    ;}
    break;

  case 191:
#line 2001 "gram1.y"
    { (yyval.symbol) =  make_local_entity((yyvsp[(2) - (3)].hash_entry), NAMELIST_NAME,global_default,LOCAL); ;}
    break;

  case 192:
#line 2005 "gram1.y"
    { (yyval.symbol) = NULL; /*make_common(look_up_sym("*"));*/ ;}
    break;

  case 193:
#line 2007 "gram1.y"
    { (yyval.symbol) = make_common((yyvsp[(2) - (3)].hash_entry)); ;}
    break;

  case 194:
#line 2012 "gram1.y"
    {  PTR_SYMB s;
	
	      if((yyvsp[(2) - (2)].ll_node)) {
		s = make_array((yyvsp[(1) - (2)].hash_entry), TYNULL, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
                s->attr = s->attr | DIMENSION_BIT;
		s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
		(yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);
	      }
	      else {
		s = make_scalar((yyvsp[(1) - (2)].hash_entry), TYNULL, LOCAL);	
		(yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      }

          ;}
    break;

  case 195:
#line 2030 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_external((yyvsp[(4) - (4)].hash_entry), TYNULL);
	      s->attr = s->attr | EXTERNAL_BIT;
              q = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      p = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,EXTERN_STAT, SMNULL, p, LLNULL, LLNULL);
	    ;}
    break;

  case 196:
#line 2041 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_external((yyvsp[(3) - (3)].hash_entry), TYNULL);
	      s->attr = s->attr | EXTERNAL_BIT;
              p = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      q = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 197:
#line 2053 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_intrinsic((yyvsp[(4) - (4)].hash_entry), TYNULL); /*make_function($3, TYNULL, NO);*/
	      s->attr = s->attr | INTRINSIC_BIT;
              q = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      p = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,INTRIN_STAT, SMNULL, p,
			     LLNULL, LLNULL);
	    ;}
    break;

  case 198:
#line 2065 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_intrinsic((yyvsp[(3) - (3)].hash_entry), TYNULL); /* make_function($3, TYNULL, NO);*/
	      s->attr = s->attr | INTRINSIC_BIT;
              p = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      q = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 199:
#line 2079 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,EQUI_STAT, SMNULL, (yyvsp[(3) - (3)].ll_node),
			     LLNULL, LLNULL);
	    ;}
    break;

  case 200:
#line 2085 "gram1.y"
    { 
	      add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 201:
#line 2092 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,EQUI_LIST, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL);
           ;}
    break;

  case 202:
#line 2098 "gram1.y"
    { PTR_LLND p;
	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(1) - (3)].ll_node), p, SMNULL);
	    ;}
    break;

  case 203:
#line 2104 "gram1.y"
    { PTR_LLND p;

	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(p, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 204:
#line 2112 "gram1.y"
    {  PTR_SYMB s;
           s=make_scalar((yyvsp[(1) - (1)].hash_entry),TYNULL,LOCAL);
           (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
           s->attr = s->attr | EQUIVALENCE_BIT;
            /*$$=$1; $$->entry.Template.symbol->attr = $$->entry.Template.symbol->attr | EQUIVALENCE_BIT; */
        ;}
    break;

  case 205:
#line 2119 "gram1.y"
    {  PTR_SYMB s;
           s=make_array((yyvsp[(1) - (4)].hash_entry),TYNULL,LLNULL,0,LOCAL);
           (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, s);
           s->attr = s->attr | EQUIVALENCE_BIT;
            /*$$->entry.Template.symbol->attr = $$->entry.Template.symbol->attr | EQUIVALENCE_BIT; */
        ;}
    break;

  case 207:
#line 2138 "gram1.y"
    { PTR_LLND p;
              data_stat = NO;
	      p = make_llnd(fi,STMT_STR, LLNULL, LLNULL,
			    SMNULL);
              p->entry.string_val = copys(stmtbuf);
	      (yyval.bf_node) = get_bfnd(fi,DATA_DECL, SMNULL, p, LLNULL, LLNULL);
            ;}
    break;

  case 210:
#line 2152 "gram1.y"
    {data_stat = YES;;}
    break;

  case 211:
#line 2156 "gram1.y"
    {
	      if (parstate == OUTSIDE)
	         { PTR_BFND p;

		   p = get_bfnd(fi,PROG_HEDR,
                                make_program(look_up_sym("_MAIN")),
                                LLNULL, LLNULL, LLNULL);
		   set_blobs(p, global_bfnd, NEW_GROUP1);
	           add_scope_level(p, NO);
		   position = IN_PROC; 
	  	   /*parstate = INDCL;*/
                 }
	      if(parstate < INDCL)
		{
		  /* enddcl();*/
		  parstate = INDCL;
		}
	    ;}
    break;

  case 222:
#line 2201 "gram1.y"
    {;;}
    break;

  case 223:
#line 2205 "gram1.y"
    { (yyval.symbol)= make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);;}
    break;

  case 224:
#line 2209 "gram1.y"
    { (yyval.symbol)= make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL); 
              (yyval.symbol)->attr = (yyval.symbol)->attr | DATA_BIT; 
            ;}
    break;

  case 225:
#line 2215 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_SUBS, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 226:
#line 2219 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_RANGE, (yyvsp[(2) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), SMNULL); ;}
    break;

  case 227:
#line 2223 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 228:
#line 2225 "gram1.y"
    { (yyval.ll_node) = add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].ll_node)); ;}
    break;

  case 229:
#line 2229 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 230:
#line 2231 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 231:
#line 2235 "gram1.y"
    {(yyval.ll_node)= make_llnd(fi, DATA_IMPL_DO, (yyvsp[(2) - (7)].ll_node), (yyvsp[(6) - (7)].ll_node), (yyvsp[(4) - (7)].symbol)); ;}
    break;

  case 232:
#line 2239 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 233:
#line 2241 "gram1.y"
    { (yyval.ll_node) = add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].ll_node)); ;}
    break;

  case 234:
#line 2245 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(2) - (2)].ll_node), LLNULL, (yyvsp[(1) - (2)].symbol)); ;}
    break;

  case 235:
#line 2247 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(2) - (2)].ll_node), LLNULL, (yyvsp[(1) - (2)].symbol)); ;}
    break;

  case 236:
#line 2249 "gram1.y"
    {
              (yyvsp[(2) - (3)].ll_node)->entry.Template.ll_ptr2 = (yyvsp[(3) - (3)].ll_node);
              (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(2) - (3)].ll_node), LLNULL, (yyvsp[(1) - (3)].symbol)); 
            ;}
    break;

  case 237:
#line 2254 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 251:
#line 2278 "gram1.y"
    {if((yyvsp[(2) - (6)].ll_node)->entry.Template.symbol->variant != TYPE_NAME)
               errstr("Undefined type %s",(yyvsp[(2) - (6)].ll_node)->entry.Template.symbol->ident,319); 
           ;}
    break;

  case 268:
#line 2323 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ICON_EXPR, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 269:
#line 2325 "gram1.y"
    {
              PTR_LLND p;

              p = intrinsic_op_node("+", UNARY_ADD_OP, (yyvsp[(2) - (2)].ll_node), LLNULL);
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 270:
#line 2332 "gram1.y"
    {
              PTR_LLND p;
 
              p = intrinsic_op_node("-", MINUS_OP, (yyvsp[(2) - (2)].ll_node), LLNULL);
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 271:
#line 2339 "gram1.y"
    {
              PTR_LLND p;
 
              p = intrinsic_op_node("+", ADD_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node));
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 272:
#line 2346 "gram1.y"
    {
              PTR_LLND p;
 
              p = intrinsic_op_node("-", SUBT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node));
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 273:
#line 2355 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 274:
#line 2357 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("*", MULT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 275:
#line 2359 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("/", DIV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 276:
#line 2363 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 277:
#line 2365 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("**", EXP_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 278:
#line 2369 "gram1.y"
    {
              PTR_LLND p;

              p = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
              p->entry.ival = atoi(yytext);
              p->type = global_int;
              (yyval.ll_node) = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
            ;}
    break;

  case 279:
#line 2378 "gram1.y"
    {
              PTR_LLND p;
 
              p = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol));
              (yyval.ll_node) = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
            ;}
    break;

  case 280:
#line 2385 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL);
            ;}
    break;

  case 281:
#line 2392 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 282:
#line 2394 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 283:
#line 2398 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
             (yyval.ll_node)->entry.Template.symbol->attr = (yyval.ll_node)->entry.Template.symbol->attr | SAVE_BIT;
           ;}
    break;

  case 284:
#line 2402 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,COMM_LIST, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol)); 
            (yyval.ll_node)->entry.Template.symbol->attr = (yyval.ll_node)->entry.Template.symbol->attr | SAVE_BIT;
          ;}
    break;

  case 285:
#line 2408 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(2) - (3)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 286:
#line 2410 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), EXPR_LIST); ;}
    break;

  case 287:
#line 2414 "gram1.y"
    { as_op_kwd_ = YES; ;}
    break;

  case 288:
#line 2418 "gram1.y"
    { as_op_kwd_ = NO; ;}
    break;

  case 289:
#line 2423 "gram1.y"
    { 
             PTR_SYMB s; 
             s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);	
	     s->attr = s->attr | type_opt;
	     (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
            ;}
    break;

  case 290:
#line 2430 "gram1.y"
    { PTR_SYMB s;
	      s = make_function((yyvsp[(3) - (4)].hash_entry), global_default, LOCAL);
	      s->variant = INTERFACE_NAME;
              s->attr = s->attr | type_opt;
              (yyval.ll_node) = make_llnd(fi,OPERATOR_OP, LLNULL, LLNULL, s);
	    ;}
    break;

  case 291:
#line 2437 "gram1.y"
    { PTR_SYMB s;
	      s = make_procedure(look_up_sym("="), LOCAL);
	      s->variant = INTERFACE_NAME;
              s->attr = s->attr | type_opt;
              (yyval.ll_node) = make_llnd(fi,ASSIGNMENT_OP, LLNULL, LLNULL, s);
	    ;}
    break;

  case 292:
#line 2447 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 293:
#line 2449 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 294:
#line 2453 "gram1.y"
    { PTR_SYMB p;

                /* The check if name and expr have compatible types has
                   not been done yet. */ 
		p = make_constant((yyvsp[(1) - (3)].hash_entry), TYNULL);
 	        p->attr = p->attr | PARAMETER_BIT;
                p->entry.const_value = (yyvsp[(3) - (3)].ll_node);
		(yyval.ll_node) = make_llnd(fi,CONST_REF, LLNULL, LLNULL, p);
	    ;}
    break;

  case 295:
#line 2465 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, MODULE_PROC_STMT, SMNULL, (yyvsp[(2) - (2)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 296:
#line 2468 "gram1.y"
    { PTR_SYMB s;
 	      PTR_LLND q;

	      s = make_function((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	      s->variant = ROUTINE_NAME;
              q = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	    ;}
    break;

  case 297:
#line 2477 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_function((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);
	      s->variant = ROUTINE_NAME;
              p = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      q = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 298:
#line 2490 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL);
              /*add_scope_level($3->entry.Template.func_hedr, YES);*/ /*17.06.01*/
              copy_module_scope((yyvsp[(3) - (3)].symbol),LLNULL); /*17.03.03*/
              colon_flag = NO;
            ;}
    break;

  case 299:
#line 2496 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (6)].symbol), (yyvsp[(6) - (6)].ll_node), LLNULL, LLNULL); 
              /*add_scope_level(module_scope, YES); *//* 17.06.01*/
              copy_module_scope((yyvsp[(3) - (6)].symbol),(yyvsp[(6) - (6)].ll_node)); /*17.03.03 */
              colon_flag = NO;
            ;}
    break;

  case 300:
#line 2502 "gram1.y"
    { PTR_LLND l;

	      l = make_llnd(fi, ONLY_NODE, LLNULL, LLNULL, SMNULL);
              (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (6)].symbol), l, LLNULL, LLNULL);
            ;}
    break;

  case 301:
#line 2508 "gram1.y"
    { PTR_LLND l;

	      l = make_llnd(fi, ONLY_NODE, (yyvsp[(7) - (7)].ll_node), LLNULL, SMNULL);
              (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (7)].symbol), l, LLNULL, LLNULL);
            ;}
    break;

  case 302:
#line 2516 "gram1.y"
    {
              if ((yyvsp[(1) - (1)].hash_entry)->id_attr == SMNULL)
	         warn1("Unknown module %s", (yyvsp[(1) - (1)].hash_entry)->ident,308);
              (yyval.symbol) = make_global_entity((yyvsp[(1) - (1)].hash_entry), MODULE_NAME, global_default, NO);
	      module_scope = (yyval.symbol)->entry.Template.func_hedr;
           
            ;}
    break;

  case 303:
#line 2526 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 304:
#line 2528 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 305:
#line 2532 "gram1.y"
    {  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 306:
#line 2534 "gram1.y"
    {  PTR_HASH oldhash,copyhash;
	       PTR_SYMB oldsym, newsym;
	       PTR_LLND m;

	       oldhash = just_look_up_sym_in_scope(module_scope, (yyvsp[(1) - (1)].hash_entry)->ident);
	       if (oldhash == HSNULL) {
                  errstr("Unknown identifier %s.", (yyvsp[(1) - (1)].hash_entry)->ident,309);
	          (yyval.ll_node)= LLNULL;
	       }
	       else {
                 oldsym = oldhash->id_attr;
                 copyhash=just_look_up_sym_in_scope(cur_scope(), (yyvsp[(1) - (1)].hash_entry)->ident);
	         if( copyhash && copyhash->id_attr && copyhash->id_attr->entry.Template.tag==module_scope->id)
                 {
                   newsym = copyhash->id_attr;
                   newsym->entry.Template.tag = 0;
                 }
                 else
                 {
	           newsym = make_local_entity((yyvsp[(1) - (1)].hash_entry), oldsym->variant, oldsym->type,LOCAL);
	           /* copies data in entry.Template structure and attr */
	           copy_sym_data(oldsym, newsym);	         
	             /*newsym->entry.Template.base_name = oldsym;*//*19.03.03*/
                 }
	  	/* l = make_llnd(fi, VAR_REF, LLNULL, LLNULL, oldsym);*/
		 m = make_llnd(fi, VAR_REF, LLNULL, LLNULL, newsym);
		 (yyval.ll_node) = make_llnd(fi, RENAME_NODE, m, LLNULL, oldsym);
 	      }
            ;}
    break;

  case 307:
#line 2567 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 308:
#line 2569 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 309:
#line 2573 "gram1.y"
    {  PTR_HASH oldhash,copyhash;
	       PTR_SYMB oldsym, newsym;
	       PTR_LLND l, m;

	       oldhash = just_look_up_sym_in_scope(module_scope, (yyvsp[(3) - (3)].hash_entry)->ident);
	       if (oldhash == HSNULL) {
                  errstr("Unknown identifier %s", (yyvsp[(3) - (3)].hash_entry)->ident,309);
	          (yyval.ll_node)= LLNULL;
	       }
	       else {
                 oldsym = oldhash->id_attr;
                 copyhash = just_look_up_sym_in_scope(cur_scope(), (yyvsp[(3) - (3)].hash_entry)->ident);
	         if(copyhash && copyhash->id_attr && copyhash->id_attr->entry.Template.tag==module_scope->id)
                 {
                    delete_symbol(copyhash->id_attr);
                    copyhash->id_attr = SMNULL;
                 }
                   newsym = make_local_entity((yyvsp[(1) - (3)].hash_entry), oldsym->variant, oldsym->type, LOCAL);
	           /* copies data in entry.Template structure and attr */
	           copy_sym_data(oldsym, newsym);	
                         
	           /*newsym->entry.Template.base_name = oldsym;*//*19.03.03*/
	  	 l  = make_llnd(fi, VAR_REF, LLNULL, LLNULL, oldsym);
		 m  = make_llnd(fi, VAR_REF, LLNULL, LLNULL, newsym);
		 (yyval.ll_node) = make_llnd(fi, RENAME_NODE, m, l, SMNULL);
 	      }
            ;}
    break;

  case 310:
#line 2611 "gram1.y"
    { ndim = 0;	explicit_shape = 1; (yyval.ll_node) = LLNULL; ;}
    break;

  case 311:
#line 2613 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 312:
#line 2616 "gram1.y"
    { ndim = 0; explicit_shape = 1;;}
    break;

  case 313:
#line 2617 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(2) - (2)].ll_node), LLNULL, SMNULL);
	      (yyval.ll_node)->type = global_default;
	    ;}
    break;

  case 314:
#line 2622 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 315:
#line 2626 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
	      ++ndim;
	    ;}
    break;

  case 316:
#line 2634 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = make_llnd(fi, DDOT, LLNULL, LLNULL, SMNULL);
	      ++ndim;
              explicit_shape = 0;
	    ;}
    break;

  case 317:
#line 2643 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (2)].ll_node), LLNULL, SMNULL);
	      ++ndim;
              explicit_shape = 0;
	    ;}
    break;

  case 318:
#line 2652 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      ++ndim;
	    ;}
    break;

  case 319:
#line 2662 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,STAR_RANGE, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->type = global_default;
              explicit_shape = 0;
	    ;}
    break;

  case 321:
#line 2671 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 322:
#line 2673 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 323:
#line 2677 "gram1.y"
    {PTR_LABEL p;
	     p = make_label_node(fi,convci(yyleng, yytext));
	     p->scope = cur_scope();
	     (yyval.ll_node) = make_llnd_label(fi,LABEL_REF, p);
	  ;}
    break;

  case 324:
#line 2685 "gram1.y"
    { /*PTR_LLND l;*/

          /*   l = make_llnd(fi, EXPR_LIST, $3, LLNULL, SMNULL);*/
             (yyval.bf_node) = get_bfnd(fi,IMPL_DECL, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);
             redefine_func_arg_type();
           ;}
    break;

  case 325:
#line 2700 "gram1.y"
    { /*undeftype = YES;
	    setimpl(TYNULL, (int)'a', (int)'z'); FB COMMENTED---> NOT QUITE RIGHT BUT AVOID PB WITH COMMON*/
	    (yyval.bf_node) = get_bfnd(fi,IMPL_DECL, SMNULL, LLNULL, LLNULL, LLNULL);
	  ;}
    break;

  case 326:
#line 2707 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 327:
#line 2709 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 328:
#line 2713 "gram1.y"
    { 

            (yyval.ll_node) = make_llnd(fi, IMPL_TYPE, (yyvsp[(3) - (4)].ll_node), LLNULL, SMNULL);
            (yyval.ll_node)->type = vartype;
          ;}
    break;

  case 329:
#line 2728 "gram1.y"
    { implkwd = YES; ;}
    break;

  case 330:
#line 2729 "gram1.y"
    { vartype = (yyvsp[(2) - (2)].data_type); ;}
    break;

  case 331:
#line 2733 "gram1.y"
    { (yyval.data_type) = (yyvsp[(2) - (2)].data_type); ;}
    break;

  case 332:
#line 2735 "gram1.y"
    { (yyval.data_type) = (yyvsp[(1) - (1)].data_type);;}
    break;

  case 333:
#line 2747 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 334:
#line 2749 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 335:
#line 2753 "gram1.y"
    {
	      setimpl(vartype, (int)(yyvsp[(1) - (1)].charv), (int)(yyvsp[(1) - (1)].charv));
	      (yyval.ll_node) = make_llnd(fi,CHAR_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.cval = (yyvsp[(1) - (1)].charv);
	    ;}
    break;

  case 336:
#line 2759 "gram1.y"
    { PTR_LLND p,q;
	      
	      setimpl(vartype, (int)(yyvsp[(1) - (3)].charv), (int)(yyvsp[(3) - (3)].charv));
	      p = make_llnd(fi,CHAR_VAL, LLNULL, LLNULL, SMNULL);
	      p->entry.cval = (yyvsp[(1) - (3)].charv);
	      q = make_llnd(fi,CHAR_VAL, LLNULL, LLNULL, SMNULL);
	      q->entry.cval = (yyvsp[(3) - (3)].charv);
	      (yyval.ll_node)= make_llnd(fi,DDOT, p, q, SMNULL);
	    ;}
    break;

  case 337:
#line 2771 "gram1.y"
    {
	      if(yyleng!=1 || yytext[0]<'a' || yytext[0]>'z')
		{
		  err("IMPLICIT item must be single letter", 37);
		  (yyval.charv) = '\0';
		}
	      else (yyval.charv) = yytext[0];
	    ;}
    break;

  case 338:
#line 2782 "gram1.y"
    {
	      if (parstate == OUTSIDE)
	         { PTR_BFND p;

		   p = get_bfnd(fi,PROG_HEDR,
                                make_program(look_up_sym("_MAIN")),
                                LLNULL, LLNULL, LLNULL);
		   set_blobs(p, global_bfnd, NEW_GROUP1);
	           add_scope_level(p, NO);
		   position = IN_PROC; 
	  	   parstate = INSIDE;
                 }
	  
	    ;}
    break;

  case 339:
#line 2799 "gram1.y"
    { switch(parstate)
		{
                case OUTSIDE:  
			{ PTR_BFND p;

			  p = get_bfnd(fi,PROG_HEDR,
                                       make_program(look_up_sym("_MAIN")),
                                       LLNULL, LLNULL, LLNULL);
			  set_blobs(p, global_bfnd, NEW_GROUP1);
			  add_scope_level(p, NO);
			  position = IN_PROC; 
	  		  parstate = INDCL; }
	                  break;
                case INSIDE:    parstate = INDCL;
                case INDCL:     break;

                case INDATA:
                         /*  err(
                     "Statement order error: declaration after DATA or function statement", 
                                 29);*/
                              break;

                default:
                           err("Declaration among executables", 30);
                }
        ;}
    break;

  case 342:
#line 2837 "gram1.y"
    { (yyval.ll_node) = LLNULL; endioctl(); ;}
    break;

  case 343:
#line 2839 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);  endioctl();;}
    break;

  case 344:
#line 2843 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 345:
#line 2845 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 346:
#line 2847 "gram1.y"
    { PTR_LLND l;
	      l = make_llnd(fi, KEYWORD_ARG, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL);
	      l->type = (yyvsp[(2) - (2)].ll_node)->type;
              (yyval.ll_node) = l; 
	    ;}
    break;

  case 347:
#line 2858 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node), LLNULL, EXPR_LIST);
              endioctl(); 
            ;}
    break;

  case 348:
#line 2862 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST);
              endioctl(); 
            ;}
    break;

  case 349:
#line 2868 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 350:
#line 2870 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 351:
#line 2874 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 352:
#line 2876 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 353:
#line 2878 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 354:
#line 2882 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 355:
#line 2884 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 356:
#line 2888 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 357:
#line 2890 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("+", ADD_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 358:
#line 2892 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("-", SUBT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 359:
#line 2894 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("*", MULT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 360:
#line 2896 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("/", DIV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 361:
#line 2898 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("**", EXP_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 362:
#line 2900 "gram1.y"
    { (yyval.ll_node) = defined_op_node((yyvsp[(1) - (2)].hash_entry), (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 363:
#line 2902 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("+", UNARY_ADD_OP, (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 364:
#line 2904 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("-", MINUS_OP, (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 365:
#line 2906 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".eq.", EQ_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 366:
#line 2908 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".gt.", GT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 367:
#line 2910 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".lt.", LT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 368:
#line 2912 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".ge.", GTEQL_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 369:
#line 2914 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".ge.", LTEQL_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 370:
#line 2916 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".ne.", NOTEQL_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 371:
#line 2918 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".eqv.", EQV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 372:
#line 2920 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".neqv.", NEQV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 373:
#line 2922 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".xor.", XOR_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 374:
#line 2924 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".or.", OR_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 375:
#line 2926 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".and.", AND_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 376:
#line 2928 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".not.", NOT_OP, (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 377:
#line 2930 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("//", CONCAT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 378:
#line 2932 "gram1.y"
    { (yyval.ll_node) = defined_op_node((yyvsp[(2) - (3)].hash_entry), (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 379:
#line 2935 "gram1.y"
    { (yyval.token) = ADD_OP; ;}
    break;

  case 380:
#line 2936 "gram1.y"
    { (yyval.token) = SUBT_OP; ;}
    break;

  case 381:
#line 2948 "gram1.y"
    { PTR_SYMB s;
	      PTR_TYPE t;
	     /* PTR_LLND l;*/

       	      if (!(s = (yyvsp[(1) - (1)].hash_entry)->id_attr))
              {
	         s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	     	 s->decl = SOFT;
	      } 
	
	      switch (s->variant)
              {
	      case CONST_NAME:
		   (yyval.ll_node) = make_llnd(fi,CONST_REF,LLNULL,LLNULL, s);
		   t = s->type;
	           if ((t != TYNULL) &&
                       ((t->variant == T_ARRAY) ||  (t->variant == T_STRING) ))
                                 (yyval.ll_node)->variant = ARRAY_REF;

                   (yyval.ll_node)->type = t;
	           break;
	      case DEFAULT:   /* if common region with same name has been
                                 declared. */
		   s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	     	   s->decl = SOFT;

	      case VARIABLE_NAME:
                   (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL, s);
	           t = s->type;
	           if (t != TYNULL) {
                     if ((t->variant == T_ARRAY) ||  (t->variant == T_STRING) ||
                         ((t->variant == T_POINTER) && (t->entry.Template.base_type->variant == T_ARRAY) ) )
                         (yyval.ll_node)->variant = ARRAY_REF;

/*  	              if (t->variant == T_DERIVED_TYPE)
                         $$->variant = RECORD_REF; */
	           }
                   (yyval.ll_node)->type = t;
	           break;
	      case TYPE_NAME:
  	           (yyval.ll_node) = make_llnd(fi,TYPE_REF,LLNULL,LLNULL, s);
	           (yyval.ll_node)->type = s->type;
	           break;
	      case INTERFACE_NAME:
  	           (yyval.ll_node) = make_llnd(fi, INTERFACE_REF,LLNULL,LLNULL, s);
	           (yyval.ll_node)->type = s->type;
	           break;
              case FUNCTION_NAME:
                   if(isResultVar(s)) {
                     (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL, s);
	             t = s->type;
	             if (t != TYNULL) {
                       if ((t->variant == T_ARRAY) ||  (t->variant == T_STRING) ||
                         ((t->variant == T_POINTER) && (t->entry.Template.base_type->variant == T_ARRAY) ) )
                         (yyval.ll_node)->variant = ARRAY_REF;
	             }
                     (yyval.ll_node)->type = t;
	             break;
                   }                                        
	      default:
  	           (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL, s);
	           (yyval.ll_node)->type = s->type;
	           break;
	      }
             /* if ($$->variant == T_POINTER) {
	         l = $$;
	         $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $$->type = l->type->entry.Template.base_type;
	      }
              */ /*11.02.03*/
           ;}
    break;

  case 382:
#line 3022 "gram1.y"
    { PTR_SYMB  s;
	      (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); 
              s= (yyval.ll_node)->entry.Template.symbol;
              if ((((yyvsp[(1) - (1)].ll_node)->variant == VAR_REF) || ((yyvsp[(1) - (1)].ll_node)->variant == ARRAY_REF))  && (s->scope !=cur_scope()))  /*global_bfnd*/
              {
	          if(((s->variant == FUNCTION_NAME) && (!isResultVar(s))) || (s->variant == PROCEDURE_NAME) || (s->variant == ROUTINE_NAME))
                  { s = (yyval.ll_node)->entry.Template.symbol =  make_scalar(s->parent, TYNULL, LOCAL);
		    (yyval.ll_node)->type = s->type;  
		  }
              }
            ;}
    break;

  case 383:
#line 3034 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 384:
#line 3036 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 385:
#line 3040 "gram1.y"
    { int num_triplets;
	      PTR_SYMB s;  /*, sym;*/
	      /* PTR_LLND l; */
	      PTR_TYPE tp;
	      /* l = $1; */
	      s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol;
            
	      /* Handle variable to function conversion. */
	      if (((yyvsp[(1) - (5)].ll_node)->variant == VAR_REF) && 
	          (((s->variant == VARIABLE_NAME) && (s->type) &&
                    (s->type->variant != T_ARRAY)) ||
  	            (s->variant == ROUTINE_NAME))) {
	        s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol =  make_function(s->parent, TYNULL, NO);
	        (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
              }
	      if (((yyvsp[(1) - (5)].ll_node)->variant == VAR_REF) && (s->variant == FUNCTION_NAME)) { 
                if(isResultVar(s))
	          (yyvsp[(1) - (5)].ll_node)->variant = ARRAY_REF;
                else
                  (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
              }
	      if (((yyvsp[(1) - (5)].ll_node)->variant == VAR_REF) && (s->variant == PROGRAM_NAME)) {
                 errstr("The name '%s' is invalid in this context",s->ident,285);
                 (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
              }
              /* l = $1; */
	      num_triplets = is_array_section_ref((yyvsp[(4) - (5)].ll_node));
	      switch ((yyvsp[(1) - (5)].ll_node)->variant)
              {
	      case TYPE_REF:
                   (yyvsp[(1) - (5)].ll_node)->variant = STRUCTURE_CONSTRUCTOR;                  
                   (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
                   (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                   (yyval.ll_node)->type =  lookup_type(s->parent); 
	          /* $$ = make_llnd(fi, STRUCTURE_CONSTRUCTOR, $1, $4, SMNULL);
	           $$->type = $1->type;*//*18.02.03*/
	           break;
	      case INTERFACE_REF:
	       /*  sym = resolve_overloading(s, $4);
	           if (sym != SMNULL)
	  	   {
	              l = make_llnd(fi, FUNC_CALL, $4, LLNULL, sym);
	              l->type = sym->type;
	              $$ = $1; $$->variant = OVERLOADED_CALL;
	              $$->entry.Template.ll_ptr1 = l;
	              $$->type = sym->type;
	           }
	           else {
	             errstr("can't resolve call %s", s->ident,310);
	           }
	           break;
                 */ /*podd 01.02.03*/

                   (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;

	      case FUNC_CALL:
                   (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
                   (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                   if(s->type) 
                     (yyval.ll_node)->type = s->type;
                   else
                     (yyval.ll_node)->type = global_default;
	           late_bind_if_needed((yyval.ll_node));
	           break;
	      case DEREF_OP:
              case ARRAY_REF:
	           /* array element */
	           if (num_triplets == 0) {
                       if ((yyvsp[(4) - (5)].ll_node) == LLNULL) {
                           s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol = make_function(s->parent, TYNULL, NO);
                           s->entry.func_decl.num_output = 1;
                           (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
                           (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                       } else if ((yyvsp[(1) - (5)].ll_node)->type->variant == T_STRING) {
                           PTR_LLND temp = (yyvsp[(4) - (5)].ll_node);
                           int num_input = 0;

                           while (temp) {
                             ++num_input;
                             temp = temp->entry.Template.ll_ptr2;
                           }
                           (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
                           s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol = make_function(s->parent, TYNULL, NO);
                           s->entry.func_decl.num_output = 1;
                           s->entry.func_decl.num_input = num_input;
                           (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
                           (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                       } else {
       	                   (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	                   (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                           (yyval.ll_node)->type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type;
                       }
                   }
                   /* substring */
	           else if ((num_triplets == 1) && 
                            ((yyvsp[(1) - (5)].ll_node)->type->variant == T_STRING)) {
    	           /*
                     $1->entry.Template.ll_ptr1 = $4;
	             $$ = $1; $$->type = global_string;
                   */
	                  (yyval.ll_node) = make_llnd(fi, 
			  ARRAY_OP, LLNULL, LLNULL, SMNULL);
    	                  (yyval.ll_node)->entry.Template.ll_ptr1 = (yyvsp[(1) - (5)].ll_node);
       	                  (yyval.ll_node)->entry.Template.ll_ptr2 = (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1;
	                  (yyval.ll_node)->type = global_string;
                   }           
                   /* array section */
                   else {
    	             (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	             (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node); tp = make_type(fi, T_ARRAY);     /**18.03.17*/
                     tp->entry.ar_decl.base_type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type; /**18.03.17 $1->type */
	             tp->entry.ar_decl.num_dimensions = num_triplets;
	             (yyval.ll_node)->type = tp;
                   }
	           break;
	      default:
                    if((yyvsp[(1) - (5)].ll_node)->entry.Template.symbol)
                      errstr("Can't subscript %s",(yyvsp[(1) - (5)].ll_node)->entry.Template.symbol->ident, 44);
                    else
	              err("Can't subscript",44);
             }
             /*if ($$->variant == T_POINTER) {
	        l = $$;
	        $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	        $$->type = l->type->entry.Template.base_type;
	     }
              */  /*11.02.03*/

	     endioctl(); 
           ;}
    break;

  case 386:
#line 3171 "gram1.y"
    { int num_triplets;
	      PTR_SYMB s;
	      PTR_LLND l;

	      s = (yyvsp[(1) - (6)].ll_node)->entry.Template.symbol;
/*              if ($1->type->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */
	      if (((yyvsp[(1) - (6)].ll_node)->type->variant != T_ARRAY) ||
                  ((yyvsp[(1) - (6)].ll_node)->type->entry.ar_decl.base_type->variant != T_STRING)) {
	         errstr("Can't take substring of %s", s->ident, 45);
              }
              else {
  	        num_triplets = is_array_section_ref((yyvsp[(4) - (6)].ll_node));
	           /* array element */
                if (num_triplets == 0) {
                   (yyvsp[(1) - (6)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (6)].ll_node);
                  /* $1->entry.Template.ll_ptr2 = $6;*/
	          /* $$ = $1;*/
                   l=(yyvsp[(1) - (6)].ll_node);
                   /*$$->type = $1->type->entry.ar_decl.base_type;*/
                   l->type = global_string;  /**18.03.17* $1->type->entry.ar_decl.base_type;*/
                }
                /* array section */
                else {
    	           (yyvsp[(1) - (6)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (6)].ll_node);
    	           /*$1->entry.Template.ll_ptr2 = $6;
	           $$ = $1; $$->type = make_type(fi, T_ARRAY);
                   $$->type->entry.ar_decl.base_type = $1->type;
	           $$->type->entry.ar_decl.num_dimensions = num_triplets;
                  */
                   l = (yyvsp[(1) - (6)].ll_node); l->type = make_type(fi, T_ARRAY);
                   l->type->entry.ar_decl.base_type = global_string;   /**18.03.17* $1->type*/
	           l->type->entry.ar_decl.num_dimensions = num_triplets;
               }
                (yyval.ll_node) = make_llnd(fi, ARRAY_OP, l, (yyvsp[(6) - (6)].ll_node), SMNULL);
	        (yyval.ll_node)->type = l->type;
              
              /* if ($$->variant == T_POINTER) {
	          l = $$;
	          $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	          $$->type = l->type->entry.Template.base_type;
	       }
               */  /*11.02.03*/
             }
             endioctl();
          ;}
    break;

  case 387:
#line 3221 "gram1.y"
    {  int num_triplets;
	      PTR_LLND l,l1,l2;
              PTR_TYPE tp;

         /*   if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

              num_triplets = is_array_section_ref((yyvsp[(3) - (4)].ll_node));
              (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
              l2 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr2;  
              l1 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1;                
              if(l2 && l2->type->variant == T_STRING)/*substring*/
                if(num_triplets == 1){
	           l = make_llnd(fi, ARRAY_OP, LLNULL, LLNULL, SMNULL);
    	           l->entry.Template.ll_ptr1 = l2;
       	           l->entry.Template.ll_ptr2 = (yyvsp[(3) - (4)].ll_node)->entry.Template.ll_ptr1;
	           l->type = global_string; 
                   (yyval.ll_node)->entry.Template.ll_ptr2 = l;                                          
                } else
                   err("Can't subscript",44);
              else if (l2 && l2->type->variant == T_ARRAY) {
                 if(num_triplets > 0) { /*array section*/
                   tp = make_type(fi,T_ARRAY);
                   tp->entry.ar_decl.base_type = (yyvsp[(1) - (4)].ll_node)->type->entry.ar_decl.base_type;
                   tp->entry.ar_decl.num_dimensions = num_triplets;
                   (yyval.ll_node)->type = tp;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                   l2->type = (yyval.ll_node)->type;   
                  }                 
                 else {  /*array element*/
                   l2->type = l2->type->entry.ar_decl.base_type;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);   
                   if(l1->type->variant != T_ARRAY)  
                     (yyval.ll_node)->type = l2->type;
                 }
              } else 
                   {err("Can't subscript",44); /*fprintf(stderr,"%d  %d",$1->variant,l2);*/}
                   /*errstr("Can't subscript %s",l2->entry.Template.symbol->ident,441);*/
         ;}
    break;

  case 388:
#line 3265 "gram1.y"
    { int num_triplets;
	      PTR_LLND l,q;

          /*     if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

              (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
	      if (((yyvsp[(1) - (5)].ll_node)->type->variant != T_ARRAY) &&
                  ((yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type->variant != T_STRING)) {
	         err("Can't take substring",45);
              }
              else {
  	        num_triplets = is_array_section_ref((yyvsp[(3) - (5)].ll_node));
                l = (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr2;
                if(l) {
                /* array element */
	        if (num_triplets == 0) {
                   l->entry.Template.ll_ptr1 = (yyvsp[(3) - (5)].ll_node);       	           
                   l->type = global_string;
                }
                /* array section */
                else {	
    	             l->entry.Template.ll_ptr1 = (yyvsp[(3) - (5)].ll_node);
	             l->type = make_type(fi, T_ARRAY);
                     l->type->entry.ar_decl.base_type = global_string;
	             l->type->entry.ar_decl.num_dimensions = num_triplets;
                }
	        q = make_llnd(fi, ARRAY_OP, l, (yyvsp[(5) - (5)].ll_node), SMNULL);
	        q->type = l->type;
                (yyval.ll_node)->entry.Template.ll_ptr2 = q;
                if((yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1->type->variant != T_ARRAY)  
                     (yyval.ll_node)->type = q->type;
               }
             }
          ;}
    break;

  case 389:
#line 3307 "gram1.y"
    { PTR_TYPE t;
	      PTR_SYMB  field;
	    /*  PTR_BFND at_scope;*/
              PTR_LLND l;


/*              if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

	      t = (yyvsp[(1) - (3)].ll_node)->type; 
	      
	      if (( ( ((yyvsp[(1) - (3)].ll_node)->variant == VAR_REF) 
	          ||  ((yyvsp[(1) - (3)].ll_node)->variant == CONST_REF) 
                  ||  ((yyvsp[(1) - (3)].ll_node)->variant == ARRAY_REF)
                  ||  ((yyvsp[(1) - (3)].ll_node)->variant == RECORD_REF)) && (t->variant == T_DERIVED_TYPE)) 
	          ||((((yyvsp[(1) - (3)].ll_node)->variant == ARRAY_REF) || ((yyvsp[(1) - (3)].ll_node)->variant == RECORD_REF)) && (t->variant == T_ARRAY) &&
                      (t = t->entry.ar_decl.base_type) && (t->variant == T_DERIVED_TYPE))) 
                {
                 t->name = lookup_type_symbol(t->name);
	         if ((field = component(t->name, yytext))) {                   
	            l =  make_llnd(fi, VAR_REF, LLNULL, LLNULL, field);
                    l->type = field->type;
                    if(field->type->variant == T_ARRAY || field->type->variant == T_STRING)
                      l->variant = ARRAY_REF; 
                    (yyval.ll_node) = make_llnd(fi, RECORD_REF, (yyvsp[(1) - (3)].ll_node), l, SMNULL);
                    if((yyvsp[(1) - (3)].ll_node)->type->variant != T_ARRAY)
                       (yyval.ll_node)->type = field->type;
                    else {
                       (yyval.ll_node)->type = make_type(fi,T_ARRAY);
                       if(field->type->variant != T_ARRAY) 
	                 (yyval.ll_node)->type->entry.ar_decl.base_type = field->type;
                       else
                         (yyval.ll_node)->type->entry.ar_decl.base_type = field->type->entry.ar_decl.base_type;
	               (yyval.ll_node)->type->entry.ar_decl.num_dimensions = t->entry.ar_decl.num_dimensions;
                       }
                 }
                  else  
                    errstr("Illegal component  %s", yytext,311);
              }                     
               else 
                    errstr("Can't take component  %s", yytext,311);
             ;}
    break;

  case 390:
#line 3365 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 391:
#line 3367 "gram1.y"
    {(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 392:
#line 3369 "gram1.y"
    {  int num_triplets;
               PTR_TYPE tp;
              /* PTR_LLND l;*/
	      if ((yyvsp[(1) - (5)].ll_node)->type->variant == T_ARRAY)
              {
  	         num_triplets = is_array_section_ref((yyvsp[(4) - (5)].ll_node));
	         /* array element */
	         if (num_triplets == 0) {
       	            (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
       	            (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                    (yyval.ll_node)->type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type;
                 }
                 /* substring */
	       /*  else if ((num_triplets == 1) && 
                          ($1->type->variant == T_STRING)) {
    	                  $1->entry.Template.ll_ptr1 = $4;
	                  $$ = $1; $$->type = global_string;
                 }   */ /*podd*/        
                 /* array section */
                 else {
    	             (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	             (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node); tp = make_type(fi, T_ARRAY);
                     tp->entry.ar_decl.base_type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type;  /**18.03.17* $1->type */
	             tp->entry.ar_decl.num_dimensions = num_triplets;
                     (yyval.ll_node)->type = tp;
                 }
             } 
             else err("can't subscript",44);

            /* if ($$->variant == T_POINTER) {
	        l = $$;
	        $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	        $$->type = l->type->entry.Template.base_type;
	     }
             */  /*11.02.03*/

            endioctl();
           ;}
    break;

  case 393:
#line 3409 "gram1.y"
    {  int num_triplets;
	      PTR_LLND l,l1,l2;

         /*   if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

              num_triplets = is_array_section_ref((yyvsp[(3) - (4)].ll_node));
              (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
              l2 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr2;  
              l1 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1;                
              if(l2 && l2->type->variant == T_STRING)/*substring*/
                if(num_triplets == 1){
	           l = make_llnd(fi, ARRAY_OP, LLNULL, LLNULL, SMNULL);
    	           l->entry.Template.ll_ptr1 = l2;
       	           l->entry.Template.ll_ptr2 = (yyvsp[(3) - (4)].ll_node)->entry.Template.ll_ptr1;
	           l->type = global_string; 
                   (yyval.ll_node)->entry.Template.ll_ptr2 = l;                                          
                } else
                   err("Can't subscript",44);
              else if (l2 && l2->type->variant == T_ARRAY) {
                 if(num_triplets > 0) { /*array section*/
                   (yyval.ll_node)->type = make_type(fi,T_ARRAY);
                   (yyval.ll_node)->type->entry.ar_decl.base_type = l2->type->entry.ar_decl.base_type;
                   (yyval.ll_node)->type->entry.ar_decl.num_dimensions = num_triplets;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                   l2->type = (yyval.ll_node)->type;   
                  }                 
                 else {  /*array element*/
                   l2->type = l2->type->entry.ar_decl.base_type;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);   
                   if(l1->type->variant != T_ARRAY)  
                     (yyval.ll_node)->type = l2->type;
                 }
              } else 
                   err("Can't subscript",44);
         ;}
    break;

  case 394:
#line 3451 "gram1.y"
    { 
	      if ((yyvsp[(1) - (2)].ll_node)->type->variant == T_STRING) {
                 (yyvsp[(1) - (2)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (2)].ll_node);
                 (yyval.ll_node) = (yyvsp[(1) - (2)].ll_node); (yyval.ll_node)->type = global_string;
              }
              else errstr("can't subscript of %s", (yyvsp[(1) - (2)].ll_node)->entry.Template.symbol->ident,44);
            ;}
    break;

  case 395:
#line 3461 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 396:
#line 3463 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 397:
#line 3467 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, (yyvsp[(2) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), SMNULL); ;}
    break;

  case 398:
#line 3471 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 399:
#line 3473 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 400:
#line 3477 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 401:
#line 3479 "gram1.y"
    { PTR_TYPE t;
               t = make_type_node((yyvsp[(1) - (3)].ll_node)->type, (yyvsp[(3) - (3)].ll_node));
               (yyval.ll_node) = (yyvsp[(1) - (3)].ll_node);
               (yyval.ll_node)->type = t;
             ;}
    break;

  case 402:
#line 3485 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 403:
#line 3487 "gram1.y"
    { PTR_TYPE t;
               t = make_type_node((yyvsp[(1) - (3)].ll_node)->type, (yyvsp[(3) - (3)].ll_node));
               (yyval.ll_node) = (yyvsp[(1) - (3)].ll_node);
               (yyval.ll_node)->type = t;
             ;}
    break;

  case 404:
#line 3493 "gram1.y"
    {
              if ((yyvsp[(2) - (2)].ll_node) != LLNULL)
              {
		 (yyval.ll_node) = make_llnd(fi, ARRAY_OP, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL); 
                 (yyval.ll_node)->type = global_string;
              }
	      else 
                 (yyval.ll_node) = (yyvsp[(1) - (2)].ll_node);
             ;}
    break;

  case 405:
#line 3506 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,BOOL_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.bval = 1;
	      (yyval.ll_node)->type = global_bool;
	    ;}
    break;

  case 406:
#line 3512 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,BOOL_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.bval = 0;
	      (yyval.ll_node)->type = global_bool;
	    ;}
    break;

  case 407:
#line 3519 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,FLOAT_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
	      (yyval.ll_node)->type = global_float;
	    ;}
    break;

  case 408:
#line 3525 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,DOUBLE_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
	      (yyval.ll_node)->type = global_double;
	    ;}
    break;

  case 409:
#line 3533 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.ival = atoi(yytext);
	      (yyval.ll_node)->type = global_int;
	    ;}
    break;

  case 410:
#line 3541 "gram1.y"
    { PTR_TYPE t;
	      PTR_LLND p,q;
	      (yyval.ll_node) = make_llnd(fi,STRING_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
              if(yyquote=='\"') 
	        t = global_string_2;
              else
	        t = global_string;

	      p = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
	      p->entry.ival = yyleng;
	      p->type = global_int;
              q = make_llnd(fi, LEN_OP, p, LLNULL, SMNULL); 
              (yyval.ll_node)->type = make_type_node(t, q);
	    ;}
    break;

  case 411:
#line 3557 "gram1.y"
    { PTR_TYPE t;
	      (yyval.ll_node) = make_llnd(fi,STRING_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
              if(yyquote=='\"') 
	        t = global_string_2;
              else
	        t = global_string;
	      (yyval.ll_node)->type = make_type_node(t, (yyvsp[(1) - (3)].ll_node));
            ;}
    break;

  case 412:
#line 3567 "gram1.y"
    { PTR_TYPE t;
	      (yyval.ll_node) = make_llnd(fi,STRING_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
              if(yyquote=='\"') 
	        t = global_string_2;
              else
	        t = global_string;
	      (yyval.ll_node)->type = make_type_node(t, (yyvsp[(1) - (3)].ll_node));
            ;}
    break;

  case 413:
#line 3580 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,COMPLEX_VAL, (yyvsp[(2) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), SMNULL);
	      (yyval.ll_node)->type = global_complex;
	    ;}
    break;

  case 414:
#line 3587 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 415:
#line 3589 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 416:
#line 3612 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),SMNULL); ;}
    break;

  case 417:
#line 3614 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (2)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 418:
#line 3616 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,make_llnd(fi,DDOT,(yyvsp[(1) - (5)].ll_node),(yyvsp[(3) - (5)].ll_node),SMNULL),(yyvsp[(5) - (5)].ll_node),SMNULL); ;}
    break;

  case 419:
#line 3618 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,make_llnd(fi,DDOT,(yyvsp[(1) - (4)].ll_node),LLNULL,SMNULL),(yyvsp[(4) - (4)].ll_node),SMNULL); ;}
    break;

  case 420:
#line 3620 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, make_llnd(fi,DDOT,LLNULL,(yyvsp[(2) - (4)].ll_node),SMNULL),(yyvsp[(4) - (4)].ll_node),SMNULL); ;}
    break;

  case 421:
#line 3622 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL),(yyvsp[(3) - (3)].ll_node),SMNULL); ;}
    break;

  case 422:
#line 3624 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,(yyvsp[(2) - (2)].ll_node),SMNULL); ;}
    break;

  case 423:
#line 3626 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL); ;}
    break;

  case 424:
#line 3629 "gram1.y"
    {in_vec=YES;;}
    break;

  case 425:
#line 3629 "gram1.y"
    {in_vec=NO;;}
    break;

  case 426:
#line 3630 "gram1.y"
    { PTR_TYPE array_type;
             (yyval.ll_node) = make_llnd (fi,CONSTRUCTOR_REF,(yyvsp[(4) - (6)].ll_node),LLNULL,SMNULL); 
             /*$$->type = $2->type;*/ /*28.02.03*/
             array_type = make_type(fi, T_ARRAY);
	     array_type->entry.ar_decl.num_dimensions = 1;
             if((yyvsp[(4) - (6)].ll_node)->type->variant == T_ARRAY)
	       array_type->entry.ar_decl.base_type = (yyvsp[(4) - (6)].ll_node)->type->entry.ar_decl.base_type;
             else
               array_type->entry.ar_decl.base_type = (yyvsp[(4) - (6)].ll_node)->type;
             (yyval.ll_node)->type = array_type;
           ;}
    break;

  case 427:
#line 3644 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 428:
#line 3646 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 429:
#line 3669 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 430:
#line 3671 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); endioctl(); ;}
    break;

  case 431:
#line 3673 "gram1.y"
    { stat_alloc = make_llnd(fi, SPEC_PAIR, (yyvsp[(4) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL);
                  endioctl();
                ;}
    break;

  case 432:
#line 3689 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 433:
#line 3691 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); endioctl(); ;}
    break;

  case 434:
#line 3693 "gram1.y"
    { stat_alloc = make_llnd(fi, SPEC_PAIR, (yyvsp[(4) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL);
             endioctl();
           ;}
    break;

  case 435:
#line 3706 "gram1.y"
    {stat_alloc = LLNULL;;}
    break;

  case 436:
#line 3710 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 437:
#line 3712 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 438:
#line 3720 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 439:
#line 3722 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 440:
#line 3724 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 441:
#line 3726 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(2) - (2)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (2)].ll_node);
            ;}
    break;

  case 442:
#line 3780 "gram1.y"
    { PTR_BFND biff;

	      (yyval.bf_node) = get_bfnd(fi,CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL); 
	      bind(); 
	      biff = cur_scope();
	      if ((biff->variant == FUNC_HEDR) || (biff->variant == PROC_HEDR)
		  || (biff->variant == PROS_HEDR) 
	          || (biff->variant == PROG_HEDR)
                  || (biff->variant == BLOCK_DATA)) {
                if(biff->control_parent == global_bfnd) position = IN_OUTSIDE;
		else if(!is_interface_stat(biff->control_parent)) position++;
              } else if  (biff->variant == MODULE_STMT)
                position = IN_OUTSIDE;
	      else err("Unexpected END statement read", 52);
             /* FB ADDED set the control parent so the empty function unparse right*/
              if ((yyval.bf_node))
                (yyval.bf_node)->control_parent = biff;
              delete_beyond_scope_level(pred_bfnd);
            ;}
    break;

  case 443:
#line 3802 "gram1.y"
    {
              make_extend((yyvsp[(3) - (3)].symbol));
              (yyval.bf_node) = BFNULL; 
              /* delete_beyond_scope_level(pred_bfnd); */
             ;}
    break;

  case 444:
#line 3815 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL); 
	    bind(); 
	    delete_beyond_scope_level(pred_bfnd);
	    position = IN_OUTSIDE;
          ;}
    break;

  case 445:
#line 3824 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 446:
#line 3827 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(2) - (2)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (2)].ll_node);
            ;}
    break;

  case 447:
#line 3877 "gram1.y"
    { thiswasbranch = NO;
              (yyvsp[(1) - (2)].bf_node)->variant = LOGIF_NODE;
              (yyval.bf_node) = make_logif((yyvsp[(1) - (2)].bf_node), (yyvsp[(2) - (2)].bf_node));
	      set_blobs((yyvsp[(1) - (2)].bf_node), pred_bfnd, SAME_GROUP);
	    ;}
    break;

  case 448:
#line 3883 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node);
	      set_blobs((yyval.bf_node), pred_bfnd, NEW_GROUP1); 
            ;}
    break;

  case 449:
#line 3888 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(2) - (3)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (3)].ll_node);
	      set_blobs((yyval.bf_node), pred_bfnd, NEW_GROUP1); 
            ;}
    break;

  case 450:
#line 3906 "gram1.y"
    { make_elseif((yyvsp[(4) - (7)].ll_node),(yyvsp[(7) - (7)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL;;}
    break;

  case 451:
#line 3908 "gram1.y"
    { make_else((yyvsp[(3) - (3)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL; ;}
    break;

  case 452:
#line 3910 "gram1.y"
    { make_endif((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 453:
#line 3912 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 454:
#line 3914 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, CONTAINS_STMT, SMNULL, LLNULL, LLNULL, LLNULL); ;}
    break;

  case 455:
#line 3917 "gram1.y"
    { thiswasbranch = NO;
              (yyvsp[(1) - (2)].bf_node)->variant = FORALL_STAT;
              (yyval.bf_node) = make_logif((yyvsp[(1) - (2)].bf_node), (yyvsp[(2) - (2)].bf_node));
	      set_blobs((yyvsp[(1) - (2)].bf_node), pred_bfnd, SAME_GROUP);
	    ;}
    break;

  case 456:
#line 3923 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 457:
#line 3925 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(2) - (2)].bf_node); (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (2)].ll_node);;}
    break;

  case 458:
#line 3927 "gram1.y"
    { make_endforall((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 459:
#line 3930 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 460:
#line 3932 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 461:
#line 3934 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 462:
#line 3961 "gram1.y"
    { 	     
	     /*  if($5 && $5->labdefined)
		 execerr("no backward DO loops", (char *)NULL); */
	       (yyval.bf_node) = make_do(WHILE_NODE, LBNULL, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL);
	       /*$$->entry.Template.ll_ptr3 = $1;*/	     
           ;}
    break;

  case 463:
#line 3970 "gram1.y"
    {
               if( (yyvsp[(4) - (7)].label) && (yyvsp[(4) - (7)].label)->labdefined)
		  err("No backward DO loops", 46);
	        (yyval.bf_node) = make_do(WHILE_NODE, (yyvsp[(4) - (7)].label), SMNULL, (yyvsp[(7) - (7)].ll_node), LLNULL, LLNULL);            
	    ;}
    break;

  case 464:
#line 3978 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 465:
#line 3980 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(5) - (6)].ll_node);;}
    break;

  case 466:
#line 3982 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (4)].ll_node);;}
    break;

  case 467:
#line 3987 "gram1.y"
    {  
               if( (yyvsp[(4) - (11)].label) && (yyvsp[(4) - (11)].label)->labdefined)
		  err("No backward DO loops", 46);
	        (yyval.bf_node) = make_do(FOR_NODE, (yyvsp[(4) - (11)].label), (yyvsp[(7) - (11)].symbol), (yyvsp[(9) - (11)].ll_node), (yyvsp[(11) - (11)].ll_node), LLNULL);            
	    ;}
    break;

  case 468:
#line 3994 "gram1.y"
    {
               if( (yyvsp[(4) - (13)].label) && (yyvsp[(4) - (13)].label)->labdefined)
		  err("No backward DO loops", 46);
	        (yyval.bf_node) = make_do(FOR_NODE, (yyvsp[(4) - (13)].label), (yyvsp[(7) - (13)].symbol), (yyvsp[(9) - (13)].ll_node), (yyvsp[(11) - (13)].ll_node), (yyvsp[(13) - (13)].ll_node));            
	    ;}
    break;

  case 469:
#line 4002 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, CASE_NODE, (yyvsp[(4) - (4)].symbol), (yyvsp[(3) - (4)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 470:
#line 4004 "gram1.y"
    { /*PTR_LLND p;*/
	     /* p = make_llnd(fi, DEFAULT, LLNULL, LLNULL, SMNULL); */
	      (yyval.bf_node) = get_bfnd(fi, DEFAULT_NODE, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL); ;}
    break;

  case 471:
#line 4008 "gram1.y"
    { make_endselect((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 472:
#line 4011 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, SWITCH_NODE, SMNULL, (yyvsp[(6) - (7)].ll_node), LLNULL, LLNULL) ; ;}
    break;

  case 473:
#line 4013 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, SWITCH_NODE, SMNULL, (yyvsp[(7) - (8)].ll_node), LLNULL, (yyvsp[(1) - (8)].ll_node)) ; ;}
    break;

  case 474:
#line 4017 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 475:
#line 4023 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 476:
#line 4025 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, (yyvsp[(1) - (2)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 477:
#line 4027 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, LLNULL, (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 478:
#line 4029 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL); ;}
    break;

  case 479:
#line 4033 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, EXPR_LIST, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 480:
#line 4035 "gram1.y"
    { PTR_LLND p;
	      
	      p = make_llnd(fi, EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(p, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 481:
#line 4043 "gram1.y"
    { (yyval.symbol) = SMNULL; ;}
    break;

  case 482:
#line 4045 "gram1.y"
    { (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), CONSTRUCT_NAME, global_default,
                                     LOCAL); ;}
    break;

  case 483:
#line 4051 "gram1.y"
    {(yyval.hash_entry) = HSNULL;;}
    break;

  case 484:
#line 4053 "gram1.y"
    { (yyval.hash_entry) = (yyvsp[(1) - (1)].hash_entry);;}
    break;

  case 485:
#line 4057 "gram1.y"
    {(yyval.hash_entry) = look_up_sym(yytext);;}
    break;

  case 486:
#line 4061 "gram1.y"
    { PTR_SYMB s;
	             s = make_local_entity( (yyvsp[(1) - (2)].hash_entry), CONSTRUCT_NAME, global_default, LOCAL);             
                    (yyval.ll_node) = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
                   ;}
    break;

  case 487:
#line 4082 "gram1.y"
    { (yyval.bf_node) = make_if((yyvsp[(4) - (5)].ll_node)); ;}
    break;

  case 488:
#line 4085 "gram1.y"
    { (yyval.bf_node) = make_forall((yyvsp[(4) - (6)].ll_node),(yyvsp[(5) - (6)].ll_node)); ;}
    break;

  case 489:
#line 4089 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, EXPR_LIST, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 490:
#line 4091 "gram1.y"
    { PTR_LLND p;	      
	      p = make_llnd(fi, EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(p, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 491:
#line 4098 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi, FORALL_OP, (yyvsp[(3) - (3)].ll_node), LLNULL, (yyvsp[(1) - (3)].symbol)); ;}
    break;

  case 492:
#line 4102 "gram1.y"
    { (yyval.ll_node)=LLNULL;;}
    break;

  case 493:
#line 4104 "gram1.y"
    { (yyval.ll_node)=(yyvsp[(2) - (2)].ll_node);;}
    break;

  case 494:
#line 4115 "gram1.y"
    { PTR_SYMB  s;
              s = (yyvsp[(1) - (1)].hash_entry)->id_attr;
      	      if (!s || s->variant == DEFAULT)
              {
	         s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	     	 s->decl = SOFT;
	      }
              (yyval.symbol) = s; 
	 ;}
    break;

  case 495:
#line 4128 "gram1.y"
    { PTR_SYMB s;
              PTR_LLND l;
              int vrnt;

            /*  s = make_scalar($1, TYNULL, LOCAL);*/ /*16.02.03*/
              s = (yyvsp[(1) - (5)].symbol);
	      if (s->variant != CONST_NAME) {
                if(in_vec) 
                   vrnt=SEQ;
                else
                   vrnt=DDOT;     
                l = make_llnd(fi, SEQ, make_llnd(fi, vrnt, (yyvsp[(3) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL),
                              LLNULL, SMNULL);
		(yyval.ll_node) = make_llnd(fi,IOACCESS, LLNULL, l, s);
		do_name_err = NO;
	      }
	      else {
		err("Symbolic constant not allowed as DO variable", 47);
		do_name_err = YES;
	      }
	    ;}
    break;

  case 496:
#line 4151 "gram1.y"
    { PTR_SYMB s;
              PTR_LLND l;
              int vrnt;
              /*s = make_scalar($1, TYNULL, LOCAL);*/ /*16.02.03*/
              s = (yyvsp[(1) - (7)].symbol);
	      if( s->variant != CONST_NAME ) {
                if(in_vec) 
                   vrnt=SEQ;
                else
                   vrnt=DDOT;     
                l = make_llnd(fi, SEQ, make_llnd(fi, vrnt, (yyvsp[(3) - (7)].ll_node), (yyvsp[(5) - (7)].ll_node), SMNULL), (yyvsp[(7) - (7)].ll_node),
                              SMNULL);
		(yyval.ll_node) = make_llnd(fi,IOACCESS, LLNULL, l, s);
		do_name_err = NO;
	      }
	      else {
		err("Symbolic constant not allowed as DO variable", 47);
		do_name_err = YES;
	      }
	    ;}
    break;

  case 497:
#line 4174 "gram1.y"
    { (yyval.label) = LBNULL; ;}
    break;

  case 498:
#line 4176 "gram1.y"
    {
	       (yyval.label)  = make_label_node(fi,convci(yyleng, yytext));
	       (yyval.label)->scope = cur_scope();
	    ;}
    break;

  case 499:
#line 4183 "gram1.y"
    { make_endwhere((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 500:
#line 4185 "gram1.y"
    { make_elsewhere((yyvsp[(3) - (3)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL; ;}
    break;

  case 501:
#line 4187 "gram1.y"
    { make_elsewhere_mask((yyvsp[(4) - (6)].ll_node),(yyvsp[(6) - (6)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL; ;}
    break;

  case 502:
#line 4189 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, WHERE_BLOCK_STMT, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 503:
#line 4191 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, WHERE_BLOCK_STMT, SMNULL, (yyvsp[(5) - (6)].ll_node), LLNULL, (yyvsp[(1) - (6)].ll_node)); ;}
    break;

  case 504:
#line 4196 "gram1.y"
    { PTR_LLND p, r;
             PTR_SYMB s1, s2 = SMNULL, s3, arg_list;
	     PTR_HASH hash_entry;

	   /*  if (just_look_up_sym("=") != HSNULL) {
	        p = intrinsic_op_node("=", EQUAL, $2, $4);
   	        $$ = get_bfnd(fi, OVERLOADED_ASSIGN_STAT, SMNULL, p, $2, $4);
             }	      
             else */ if ((yyvsp[(2) - (4)].ll_node)->variant == FUNC_CALL) {
                if(parstate==INEXEC){
                  	  err("Declaration among executables", 30);
                 /*   $$=BFNULL;*/
 	         (yyval.bf_node) = get_bfnd(fi,STMTFN_STAT, SMNULL, (yyvsp[(2) - (4)].ll_node), LLNULL, LLNULL);
                } 
                else {	         
  	         (yyvsp[(2) - (4)].ll_node)->variant = STMTFN_DECL;
		 /* $2->entry.Template.ll_ptr2 = $4; */
                 if( (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1) {
		   r = (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1->entry.Template.ll_ptr1;
                   if(r->variant != VAR_REF && r->variant != ARRAY_REF){
                     err("A dummy argument of a statement function must be a scalar identifier", 333);
                     s1 = SMNULL;
                   }
                   else                       
		     s1 = r ->entry.Template.symbol;
                 } else
                   s1 = SMNULL;
		 if (s1)
	            s1->scope = cur_scope();
 	         (yyval.bf_node) = get_bfnd(fi,STMTFN_STAT, SMNULL, (yyvsp[(2) - (4)].ll_node), LLNULL, LLNULL);
	         add_scope_level((yyval.bf_node), NO);
                 arg_list = SMNULL;
		 if (s1) 
                 {
	            /*arg_list = SMNULL;*/
                    p = (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1;
                    while (p != LLNULL)
                    {
		    /*   if (p->entry.Template.ll_ptr1->variant != VAR_REF) {
			  errstr("cftn.gram:1: illegal statement function %s.", $2->entry.Template.symbol->ident);
			  break;
		       } 
                    */
                       r = p->entry.Template.ll_ptr1;
                       if(r->variant != VAR_REF && r->variant != ARRAY_REF){
                         err("A dummy argument of a statement function must be a scalar identifier", 333);
                         break;
                       }
	               hash_entry = look_up_sym(r->entry.Template.symbol->parent->ident);
	               s3 = make_scalar(hash_entry, s1->type, IO);
                       replace_symbol_in_expr(s3,(yyvsp[(4) - (4)].ll_node));
	               if (arg_list == SMNULL) 
                          s2 = arg_list = s3;
             	       else 
                       {
                          s2->id_list = s3;
                          s2 = s3;
                       }
                       p = p->entry.Template.ll_ptr2;
                    }
                 }
  		    (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
		    install_param_list((yyvsp[(2) - (4)].ll_node)->entry.Template.symbol,
				       arg_list, LLNULL, FUNCTION_NAME);
	            delete_beyond_scope_level((yyval.bf_node));
		 
		/* else
		    errstr("cftn.gram: Illegal statement function declaration %s.", $2->entry.Template.symbol->ident); */
               }
	     }
	     else {
		(yyval.bf_node) = get_bfnd(fi,ASSIGN_STAT,SMNULL, (yyvsp[(2) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), LLNULL);
                 parstate = INEXEC;
             }
	  ;}
    break;

  case 505:
#line 4272 "gram1.y"
    { /*PTR_SYMB s;*/
	
	      /*s = make_scalar($2, TYNULL, LOCAL);*/
  	      (yyval.bf_node) = get_bfnd(fi, POINTER_ASSIGN_STAT, SMNULL, (yyvsp[(3) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), LLNULL);
	    ;}
    break;

  case 506:
#line 4284 "gram1.y"
    { PTR_SYMB p;

	      p = make_scalar((yyvsp[(5) - (5)].hash_entry), TYNULL, LOCAL);
	      p->variant = LABEL_VAR;
  	      (yyval.bf_node) = get_bfnd(fi,ASSLAB_STAT, p, (yyvsp[(3) - (5)].ll_node),LLNULL,LLNULL);
            ;}
    break;

  case 507:
#line 4291 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,CONT_STAT,SMNULL,LLNULL,LLNULL,LLNULL); ;}
    break;

  case 509:
#line 4294 "gram1.y"
    { inioctl = NO; ;}
    break;

  case 510:
#line 4296 "gram1.y"
    { PTR_LLND	p;

	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(10) - (10)].ll_node), LLNULL, SMNULL);
	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(8) - (10)].ll_node), p, SMNULL);
	      (yyval.bf_node)= get_bfnd(fi,ARITHIF_NODE, SMNULL, (yyvsp[(4) - (10)].ll_node),
			    make_llnd(fi,EXPR_LIST, (yyvsp[(6) - (10)].ll_node), p, SMNULL), LLNULL);
	      thiswasbranch = YES;
            ;}
    break;

  case 511:
#line 4305 "gram1.y"
    {
	      (yyval.bf_node) = subroutine_call((yyvsp[(1) - (1)].symbol), LLNULL, LLNULL, PLAIN);
/*	      match_parameters($1, LLNULL);
	      $$= get_bfnd(fi,PROC_STAT, $1, LLNULL, LLNULL, LLNULL);
*/	      endioctl(); 
            ;}
    break;

  case 512:
#line 4312 "gram1.y"
    {
	      (yyval.bf_node) = subroutine_call((yyvsp[(1) - (3)].symbol), LLNULL, LLNULL, PLAIN);
/*	      match_parameters($1, LLNULL);
	      $$= get_bfnd(fi,PROC_STAT,$1,LLNULL,LLNULL,LLNULL);
*/	      endioctl(); 
	    ;}
    break;

  case 513:
#line 4319 "gram1.y"
    {
	      (yyval.bf_node) = subroutine_call((yyvsp[(1) - (4)].symbol), (yyvsp[(3) - (4)].ll_node), LLNULL, PLAIN);
/*	      match_parameters($1, $3);
	      $$= get_bfnd(fi,PROC_STAT,$1,$3,LLNULL,LLNULL);
*/	      endioctl(); 
	    ;}
    break;

  case 514:
#line 4327 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,RETURN_STAT,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);
	      thiswasbranch = YES;
	    ;}
    break;

  case 515:
#line 4332 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,(yyvsp[(1) - (3)].token),SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);
	      thiswasbranch = ((yyvsp[(1) - (3)].token) == STOP_STAT);
	    ;}
    break;

  case 516:
#line 4337 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, CYCLE_STMT, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL); ;}
    break;

  case 517:
#line 4340 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, EXIT_STMT, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL); ;}
    break;

  case 518:
#line 4343 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, ALLOCATE_STMT,  SMNULL, (yyvsp[(5) - (6)].ll_node), stat_alloc, LLNULL); ;}
    break;

  case 519:
#line 4346 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, DEALLOCATE_STMT, SMNULL, (yyvsp[(5) - (6)].ll_node), stat_alloc , LLNULL); ;}
    break;

  case 520:
#line 4349 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, NULLIFY_STMT, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 521:
#line 4352 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, WHERE_NODE, SMNULL, (yyvsp[(4) - (8)].ll_node), (yyvsp[(6) - (8)].ll_node), (yyvsp[(8) - (8)].ll_node)); ;}
    break;

  case 522:
#line 4370 "gram1.y"
    {(yyval.ll_node) = LLNULL;;}
    break;

  case 523:
#line 4374 "gram1.y"
    {
	      (yyval.bf_node)=get_bfnd(fi,GOTO_NODE,SMNULL,LLNULL,LLNULL,(PTR_LLND)(yyvsp[(3) - (3)].ll_node));
	      thiswasbranch = YES;
	    ;}
    break;

  case 524:
#line 4379 "gram1.y"
    { PTR_SYMB p;

	      if((yyvsp[(3) - (3)].hash_entry)->id_attr)
		p = (yyvsp[(3) - (3)].hash_entry)->id_attr;
	      else {
	        p = make_scalar((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);
		p->variant = LABEL_VAR;
	      }

	      if(p->variant == LABEL_VAR) {
		  (yyval.bf_node) = get_bfnd(fi,ASSGOTO_NODE,p,LLNULL,LLNULL,LLNULL);
		  thiswasbranch = YES;
	      }
	      else {
		  err("Must go to assigned variable", 48);
		  (yyval.bf_node) = BFNULL;
	      }
	    ;}
    break;

  case 525:
#line 4398 "gram1.y"
    { PTR_SYMB p;

	      if((yyvsp[(3) - (7)].hash_entry)->id_attr)
		p = (yyvsp[(3) - (7)].hash_entry)->id_attr;
	      else {
	        p = make_scalar((yyvsp[(3) - (7)].hash_entry), TYNULL, LOCAL);
		p->variant = LABEL_VAR;
	      }

	      if (p->variant == LABEL_VAR) {
		 (yyval.bf_node) = get_bfnd(fi,ASSGOTO_NODE,p,(yyvsp[(6) - (7)].ll_node),LLNULL,LLNULL);
		 thiswasbranch = YES;
	      }
	      else {
		err("Must go to assigned variable",48);
		(yyval.bf_node) = BFNULL;
	      }
	    ;}
    break;

  case 526:
#line 4417 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,COMGOTO_NODE, SMNULL, (yyvsp[(4) - (7)].ll_node), (yyvsp[(7) - (7)].ll_node), LLNULL); ;}
    break;

  case 529:
#line 4425 "gram1.y"
    { (yyval.symbol) = make_procedure((yyvsp[(3) - (4)].hash_entry), YES); ;}
    break;

  case 530:
#line 4429 "gram1.y"
    { 
              (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node), LLNULL, EXPR_LIST);
              endioctl();
            ;}
    break;

  case 531:
#line 4434 "gram1.y"
    { 
               (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST);
               endioctl();
            ;}
    break;

  case 532:
#line 4441 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 533:
#line 4443 "gram1.y"
    { (yyval.ll_node)  = make_llnd(fi, KEYWORD_ARG, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 534:
#line 4445 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,LABEL_ARG,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 535:
#line 4448 "gram1.y"
    { (yyval.token) = PAUSE_NODE; ;}
    break;

  case 536:
#line 4449 "gram1.y"
    { (yyval.token) = STOP_STAT; ;}
    break;

  case 537:
#line 4460 "gram1.y"
    { if(parstate == OUTSIDE)
		{ PTR_BFND p;

		  p = get_bfnd(fi,PROG_HEDR, make_program(look_up_sym("_MAIN")), LLNULL, LLNULL, LLNULL);
		  set_blobs(p, global_bfnd, NEW_GROUP1);
		  add_scope_level(p, NO);
		  position = IN_PROC; 
		}
		if(parstate < INDATA) enddcl();
		parstate = INEXEC;
		yystno = 0;
	      ;}
    break;

  case 538:
#line 4475 "gram1.y"
    { intonly = YES; ;}
    break;

  case 539:
#line 4479 "gram1.y"
    { intonly = NO; ;}
    break;

  case 540:
#line 4487 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 541:
#line 4490 "gram1.y"
    { PTR_LLND p, q = LLNULL;

		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"unit";
		  q->type = global_string;
		  p = make_llnd(fi, SPEC_PAIR, q, (yyvsp[(2) - (2)].ll_node), SMNULL);
		  (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = p;
		  endioctl();
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 542:
#line 4500 "gram1.y"
    { PTR_LLND p, q, r;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"unit";
		  q->type = global_string;
		  r = make_llnd(fi, SPEC_PAIR, p, q, SMNULL);
		  (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = r;
		  endioctl();
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 543:
#line 4513 "gram1.y"
    { PTR_LLND p, q, r;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"**";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"unit";
		  q->type = global_string;
		  r = make_llnd(fi, SPEC_PAIR, p, q, SMNULL);
		  (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = r;
		  endioctl();
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 544:
#line 4526 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 545:
#line 4529 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 546:
#line 4531 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 547:
#line 4534 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 548:
#line 4537 "gram1.y"
    { (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (3)].ll_node);
		  (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 549:
#line 4541 "gram1.y"
    { (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (4)].ll_node);
		  (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (4)].bf_node); ;}
    break;

  case 550:
#line 4550 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 551:
#line 4553 "gram1.y"
    { (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (3)].ll_node);
		  (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 552:
#line 4557 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 553:
#line 4559 "gram1.y"
    { (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 554:
#line 4565 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 555:
#line 4569 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, BACKSPACE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 556:
#line 4571 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, REWIND_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 557:
#line 4573 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, ENDFILE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 558:
#line 4580 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 559:
#line 4584 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, OPEN_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 560:
#line 4586 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, CLOSE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 561:
#line 4590 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi, INQUIRE_STAT, SMNULL, LLNULL, (yyvsp[(4) - (4)].ll_node), LLNULL);;}
    break;

  case 562:
#line 4592 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi, INQUIRE_STAT, SMNULL, (yyvsp[(5) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), LLNULL);;}
    break;

  case 563:
#line 4596 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q = LLNULL;

		  if ((yyvsp[(1) - (1)].ll_node)->variant == INT_VAL)
 	          {
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(1) - (1)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		  }
		  else p = (yyvsp[(1) - (1)].ll_node); 
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		  endioctl();
		;}
    break;

  case 564:
#line 4615 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		  endioctl();
		;}
    break;

  case 565:
#line 4631 "gram1.y"
    { PTR_LLND p;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"unit";
		  p->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, p, (yyvsp[(2) - (3)].ll_node), SMNULL);
		  endioctl();
		;}
    break;

  case 566:
#line 4642 "gram1.y"
    { 
		  (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);
		  endioctl();
		 ;}
    break;

  case 567:
#line 4649 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); endioctl();;}
    break;

  case 568:
#line 4651 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); endioctl();;}
    break;

  case 569:
#line 4655 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;
 
		  nioctl++;
		  if ((nioctl == 2) && ((yyvsp[(1) - (1)].ll_node)->variant == INT_VAL))
 	          {
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(1) - (1)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		  }
		  else p = (yyvsp[(1) - (1)].ll_node); 
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  if (nioctl == 1)
		        q->entry.string_val = (char *)"unit"; 
		  else {
                     if(((yyvsp[(1) - (1)].ll_node)->variant == VAR_REF) && (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol->variant == NAMELIST_NAME)
                       q->entry.string_val = (char *)"nml";
                     else
                       q->entry.string_val = (char *)"fmt";
                  }
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		;}
    break;

  case 570:
#line 4681 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;

		  nioctl++;
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  if (nioctl == 1)
		        q->entry.string_val = (char *)"unit"; 
		  else  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		;}
    break;

  case 571:
#line 4696 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;

		  nioctl++;
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"**";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  if (nioctl == 1)
		        q->entry.string_val = (char *)"unit"; 
		  else  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		;}
    break;

  case 572:
#line 4711 "gram1.y"
    { 
		  PTR_LLND p;
		  char *q;

		  q = (yyvsp[(1) - (2)].ll_node)->entry.string_val;
  		  if ((strcmp(q, "end") == 0) || (strcmp(q, "err") == 0) || (strcmp(q, "eor") == 0) || ((strcmp(q,"fmt") == 0) && ((yyvsp[(2) - (2)].ll_node)->variant == INT_VAL)))
 	          {
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(2) - (2)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		  }
		  else p = (yyvsp[(2) - (2)].ll_node);

		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), p, SMNULL); ;}
    break;

  case 573:
#line 4728 "gram1.y"
    { PTR_LLND p;
                  
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), p, SMNULL);
		;}
    break;

  case 574:
#line 4736 "gram1.y"
    { PTR_LLND p;
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), p, SMNULL);
		;}
    break;

  case 575:
#line 4745 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  (yyval.ll_node)->entry.string_val = copys(yytext);
		  (yyval.ll_node)->type = global_string;
	        ;}
    break;

  case 576:
#line 4753 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, READ_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 577:
#line 4758 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, WRITE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 578:
#line 4763 "gram1.y"
    {
	    PTR_LLND p, q, l;

	    if ((yyvsp[(3) - (4)].ll_node)->variant == INT_VAL)
		{
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(3) - (4)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		}
	    else p = (yyvsp[(3) - (4)].ll_node);
	    
            q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	    q->entry.string_val = (char *)"fmt";
            q->type = global_string;
            l = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);

            (yyval.bf_node) = get_bfnd(fi, PRINT_STAT, SMNULL, LLNULL, l, LLNULL);
	    endioctl();
	   ;}
    break;

  case 579:
#line 4785 "gram1.y"
    { PTR_LLND p, q, r;
		
	     p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	     p->entry.string_val = (char *)"*";
	     p->type = global_string;
	     q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	     q->entry.string_val = (char *)"fmt";
             q->type = global_string;
             r = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
	     (yyval.bf_node) = get_bfnd(fi, PRINT_STAT, SMNULL, LLNULL, r, LLNULL);
	     endioctl();
           ;}
    break;

  case 580:
#line 4801 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST);;}
    break;

  case 581:
#line 4803 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST);;}
    break;

  case 582:
#line 4807 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 583:
#line 4809 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) = (yyvsp[(4) - (5)].ll_node);
		;}
    break;

  case 584:
#line 4816 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST);  (yyval.ll_node)->type = (yyvsp[(1) - (1)].ll_node)->type;;}
    break;

  case 585:
#line 4818 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 586:
#line 4820 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 587:
#line 4824 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 588:
#line 4826 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 589:
#line 4828 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 590:
#line 4830 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 591:
#line 4832 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 592:
#line 4834 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 593:
#line 4838 "gram1.y"
    { (yyval.ll_node) =  set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST);
	          (yyval.ll_node)->type = global_complex; ;}
    break;

  case 594:
#line 4841 "gram1.y"
    { (yyval.ll_node) =  set_ll_list((yyvsp[(2) - (3)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (3)].ll_node)->type; ;}
    break;

  case 595:
#line 4844 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) =  set_ll_list((yyvsp[(4) - (5)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (5)].ll_node)->type; 
		;}
    break;

  case 596:
#line 4850 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) =  set_ll_list((yyvsp[(4) - (5)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (5)].ll_node)->type; 
		;}
    break;

  case 597:
#line 4856 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) =  set_ll_list((yyvsp[(4) - (5)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (5)].ll_node)->type; 
		;}
    break;

  case 598:
#line 4864 "gram1.y"
    { inioctl = YES; ;}
    break;

  case 599:
#line 4868 "gram1.y"
    { startioctl();;}
    break;

  case 600:
#line 4876 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 601:
#line 4878 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 602:
#line 4882 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 603:
#line 4884 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 604:
#line 4886 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,(yyvsp[(2) - (3)].token), (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 605:
#line 4891 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,MULT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 606:
#line 4896 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,DIV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 607:
#line 4901 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,EXP_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 608:
#line 4906 "gram1.y"
    {
	      if((yyvsp[(1) - (2)].token) == SUBT_OP)
		{
		  (yyval.ll_node) = make_llnd(fi,SUBT_OP, (yyvsp[(2) - (2)].ll_node), LLNULL, SMNULL);
		  set_expr_type((yyval.ll_node));
		}
	      else	(yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);
	    ;}
    break;

  case 609:
#line 4915 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,CONCAT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 610:
#line 4920 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 611:
#line 4925 "gram1.y"
    { comments = cur_comment = CMNULL; ;}
    break;

  case 612:
#line 4927 "gram1.y"
    { PTR_CMNT p;
	    p = make_comment(fi,*commentbuf, HALF);
	    if (cur_comment)
               cur_comment->next = p;
            else {
	       if ((pred_bfnd->control_parent->variant == LOGIF_NODE) ||(pred_bfnd->control_parent->variant == FORALL_STAT))

	           pred_bfnd->control_parent->entry.Template.cmnt_ptr = p;

	       else last_bfnd->entry.Template.cmnt_ptr = p;
            }
	    comments = cur_comment = CMNULL;
          ;}
    break;

  case 676:
#line 5010 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_TEMPLATE_STAT, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 677:
#line 5012 "gram1.y"
    { PTR_SYMB s;
                if((yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr2)
                {
                  s = (yyvsp[(3) - (3)].ll_node)->entry.Template.ll_ptr1->entry.Template.symbol;
                  s->attr = s->attr | COMMON_BIT;
                }
	        add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	      ;}
    break;

  case 678:
#line 5023 "gram1.y"
    {PTR_SYMB s;
	      PTR_LLND q;
	    /* 27.06.18
	      if(! explicit_shape)   
                err("Explicit shape specification is required", 50);
	    */  
	      s = make_array((yyvsp[(1) - (2)].hash_entry), TYNULL, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
              if(s->attr & TEMPLATE_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT))
                errstr( "Inconsistent declaration of identifier  %s ", s->ident, 16);
              else
	        s->attr = s->attr | TEMPLATE_BIT;
              if((yyvsp[(2) - (2)].ll_node)) s->attr = s->attr | DIMENSION_BIT;  
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	     ;}
    break;

  case 679:
#line 5044 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_DYNAMIC_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 680:
#line 5048 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 681:
#line 5050 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 682:
#line 5054 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if(s->attr &  DYNAMIC_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & HEAP_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
              else
                s->attr = s->attr | DYNAMIC_BIT;        
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 683:
#line 5067 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_INHERIT_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 684:
#line 5071 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 685:
#line 5073 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 686:
#line 5077 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & ALIGN_BIT) || (s->attr & DISTRIBUTE_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
              else
                s->attr = s->attr | INHERIT_BIT;        
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 687:
#line 5088 "gram1.y"
    { PTR_LLND q;
             q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
              /* (void)fprintf(stderr,"hpf.gram: shadow\n");*/ 
	     (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_DIR,SMNULL,q,(yyvsp[(4) - (4)].ll_node),LLNULL);
            ;}
    break;

  case 688:
#line 5099 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);;}
    break;

  case 689:
#line 5103 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 690:
#line 5105 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 691:
#line 5109 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 692:
#line 5111 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);;}
    break;

  case 693:
#line 5113 "gram1.y"
    {
            if(parstate!=INEXEC) 
               err("Illegal shadow width specification", 56);  
            (yyval.ll_node) = make_llnd(fi,SHADOW_NAMES_OP, (yyvsp[(3) - (4)].ll_node), LLNULL, SMNULL);
          ;}
    break;

  case 694:
#line 5128 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if(s->attr & SHADOW_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & HEAP_BIT)) 
                      errstr( "Inconsistent declaration of identifier %s", s->ident, 16); 
              else
        	      s->attr = s->attr | SHADOW_BIT;  
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 695:
#line 5140 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
              err("Explicit shape specification is required", 50);
		/* $$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr &  PROCESSORS_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT) || (s->attr & INHERIT_BIT))
                errstr("Inconsistent declaration of identifier %s", s->ident, 16);
              else
	        s->attr = s->attr | PROCESSORS_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,HPF_PROCESSORS_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 696:
#line 5160 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
              err("Explicit shape specification is required", 50);
		/* $$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr &  PROCESSORS_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT) || (s->attr & INHERIT_BIT))
                errstr("Inconsistent declaration of identifier %s", s->ident, 16);
              else
	        s->attr = s->attr | PROCESSORS_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,HPF_PROCESSORS_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 697:
#line 5180 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
		err("Explicit shape specification is required", 50);
		/*$$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr &  PROCESSORS_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & TASK_BIT) || (s->attr &  DVM_POINTER_BIT) || (s->attr & INHERIT_BIT) )
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16);
              else
	        s->attr = s->attr | PROCESSORS_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 698:
#line 5202 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_INDIRECT_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 699:
#line 5208 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	           ;
                ;}
    break;

  case 700:
#line 5217 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), REF_GROUP_NAME,global_default,LOCAL);
          if((yyval.symbol)->attr &  INDIRECT_BIT)
                errstr( "Multiple declaration of identifier  %s ", (yyval.symbol)->ident, 73);
           (yyval.symbol)->attr = (yyval.symbol)->attr | INDIRECT_BIT;
          ;}
    break;

  case 701:
#line 5225 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_REMOTE_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 702:
#line 5231 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
                ;}
    break;

  case 703:
#line 5239 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), REF_GROUP_NAME,global_default,LOCAL);
           if((yyval.symbol)->attr &  INDIRECT_BIT)
                errstr( "Inconsistent declaration of identifier  %s ", (yyval.symbol)->ident, 16);
          ;}
    break;

  case 704:
#line 5246 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_REDUCTION_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 705:
#line 5252 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	           ;
                ;}
    break;

  case 706:
#line 5261 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), REDUCTION_GROUP_NAME,global_default,LOCAL);;}
    break;

  case 707:
#line 5265 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 708:
#line 5271 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);	           
                ;}
    break;

  case 709:
#line 5279 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), CONSISTENT_GROUP_NAME,global_default,LOCAL);;}
    break;

  case 710:
#line 5293 "gram1.y"
    { PTR_SYMB s;
            if(parstate == INEXEC){
              if (!(s = (yyvsp[(2) - (3)].hash_entry)->id_attr))
              {
	         s = make_array((yyvsp[(2) - (3)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
	     	 s->decl = SOFT;
	      } 
            } else
              s = make_array((yyvsp[(2) - (3)].hash_entry), TYNULL, LLNULL, 0, LOCAL);

              (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (3)].ll_node), LLNULL, s);
            ;}
    break;

  case 711:
#line 5306 "gram1.y"
    { (yyval.ll_node) = LLNULL; opt_kwd_ = NO;;}
    break;

  case 712:
#line 5312 "gram1.y"
    { PTR_LLND q;
             if(!(yyvsp[(4) - (5)].ll_node))
               err("Distribution format list is omitted", 51);
            /* if($6)
               err("NEW_VALUE specification in DISTRIBUTE directive");*/
             q = set_ll_list((yyvsp[(3) - (5)].ll_node),LLNULL,EXPR_LIST);
	     (yyval.bf_node) = get_bfnd(fi,DVM_DISTRIBUTE_DIR,SMNULL,q,(yyvsp[(4) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node));
            ;}
    break;

  case 713:
#line 5328 "gram1.y"
    { PTR_LLND q;
                /*  if(!$4)
                  {err("Distribution format is omitted", 51); errcnt--;}
                 */
              q = set_ll_list((yyvsp[(3) - (6)].ll_node),LLNULL,EXPR_LIST);
                 /* r = LLNULL;
                   if($6){
                     r = set_ll_list($6,LLNULL,EXPR_LIST);
                     if($7) r = set_ll_list(r,$7,EXPR_LIST);
                   } else
                     if($7) r = set_ll_list(r,$7,EXPR_LIST);
                 */
	      (yyval.bf_node) = get_bfnd(fi,DVM_REDISTRIBUTE_DIR,SMNULL,q,(yyvsp[(4) - (6)].ll_node),(yyvsp[(6) - (6)].ll_node));;}
    break;

  case 714:
#line 5343 "gram1.y"
    {
                 /* r = LLNULL;
                    if($5){
                      r = set_ll_list($5,LLNULL,EXPR_LIST);
                      if($6) r = set_ll_list(r,$6,EXPR_LIST);
                    } else
                      if($6) r = set_ll_list(r,$6,EXPR_LIST);
                  */
	      (yyval.bf_node) = get_bfnd(fi,DVM_REDISTRIBUTE_DIR,SMNULL,(yyvsp[(8) - (8)].ll_node) ,(yyvsp[(3) - (8)].ll_node),(yyvsp[(5) - (8)].ll_node) );
             ;}
    break;

  case 715:
#line 5371 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 716:
#line 5373 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 717:
#line 5377 "gram1.y"
    {(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 718:
#line 5379 "gram1.y"
    {(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 719:
#line 5383 "gram1.y"
    {  PTR_SYMB s;
 
          if(parstate == INEXEC){
            if (!(s = (yyvsp[(1) - (1)].hash_entry)->id_attr))
              {
	         s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
	     	 s->decl = SOFT;
	      } 
            if(s->attr & PROCESSORS_BIT)
              errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);
            if(s->attr & TASK_BIT)
              errstr( "Illegal use of task array name %s ", s->ident, 71);

          } else {
            s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
            if(s->attr & DISTRIBUTE_BIT)
              errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
            else if( (s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & INHERIT_BIT))
              errstr("Inconsistent declaration of identifier  %s",s->ident, 16);
            else
              s->attr = s->attr | DISTRIBUTE_BIT;
          } 
         if(s->attr & ALIGN_BIT)
               errstr("A distributee may not have the ALIGN attribute:%s",s->ident, 54);
          (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);               	  
	;}
    break;

  case 720:
#line 5412 "gram1.y"
    {  PTR_SYMB s;
          s = make_array((yyvsp[(1) - (4)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
        
          if(parstate != INEXEC) 
               errstr( "Illegal distributee:%s", s->ident, 312);
          else {
            if(s->attr & PROCESSORS_BIT)
               errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);  
            if(s->attr & TASK_BIT)
               errstr( "Illegal use of task array name %s ", s->ident, 71);        
            if(s->attr & ALIGN_BIT)
               errstr("A distributee may not have the ALIGN attribute:%s",s->ident, 54);
            if(!(s->attr & DVM_POINTER_BIT))
               errstr("Illegal distributee:%s", s->ident, 312);
          /*s->attr = s->attr | DISTRIBUTE_BIT;*/
	  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, s); 
          }
        
	;}
    break;

  case 721:
#line 5435 "gram1.y"
    {  PTR_SYMB s;
          if((s=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL)
            s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
          if((parstate == INEXEC) && !(s->attr & PROCESSORS_BIT))
               errstr( "'%s' is not processor array ", s->ident, 67);
	  (yyval.symbol) = s;
	;}
    break;

  case 722:
#line 5455 "gram1.y"
    { (yyval.ll_node) = LLNULL;  ;}
    break;

  case 723:
#line 5457 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (2)].ll_node);;}
    break;

  case 724:
#line 5461 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);;}
    break;

  case 725:
#line 5482 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 726:
#line 5484 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),EXPR_LIST); ;}
    break;

  case 727:
#line 5487 "gram1.y"
    { opt_kwd_ = YES; ;}
    break;

  case 728:
#line 5496 "gram1.y"
    {  
               (yyval.ll_node) = make_llnd(fi,BLOCK_OP, LLNULL, LLNULL, SMNULL);
        ;}
    break;

  case 729:
#line 5500 "gram1.y"
    {  err("Distribution format BLOCK(n) is not permitted in FDVM", 55);
          (yyval.ll_node) = make_llnd(fi,BLOCK_OP, (yyvsp[(4) - (5)].ll_node), LLNULL, SMNULL);
          endioctl();
        ;}
    break;

  case 730:
#line 5505 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,BLOCK_OP, LLNULL, LLNULL, (yyvsp[(3) - (4)].symbol)); ;}
    break;

  case 731:
#line 5507 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,BLOCK_OP,  (yyvsp[(5) - (6)].ll_node),  LLNULL,  (yyvsp[(3) - (6)].symbol)); ;}
    break;

  case 732:
#line 5509 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,BLOCK_OP,  LLNULL, (yyvsp[(3) - (4)].ll_node),  SMNULL); ;}
    break;

  case 733:
#line 5511 "gram1.y"
    { 
          (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
          (yyval.ll_node)->entry.string_val = (char *) "*";
          (yyval.ll_node)->type = global_string;
        ;}
    break;

  case 734:
#line 5517 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,INDIRECT_OP, LLNULL, LLNULL, (yyvsp[(3) - (4)].symbol)); ;}
    break;

  case 735:
#line 5519 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,INDIRECT_OP, (yyvsp[(3) - (4)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 736:
#line 5523 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
       
	      (yyval.symbol) = s;
	   ;}
    break;

  case 737:
#line 5533 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DERIVED_OP, (yyvsp[(2) - (6)].ll_node), (yyvsp[(6) - (6)].ll_node), SMNULL); ;}
    break;

  case 738:
#line 5537 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 739:
#line 5539 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 740:
#line 5544 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 741:
#line 5546 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);;}
    break;

  case 742:
#line 5550 "gram1.y"
    { 
              (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol));
	    ;}
    break;

  case 743:
#line 5554 "gram1.y"
    { 
              (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, (yyvsp[(1) - (4)].symbol));
	    ;}
    break;

  case 744:
#line 5560 "gram1.y"
    { 
              if (!((yyval.symbol) = (yyvsp[(1) - (1)].hash_entry)->id_attr))
              {
	         (yyval.symbol) = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL,0,LOCAL);
	         (yyval.symbol)->decl = SOFT;
	      } 
            ;}
    break;

  case 745:
#line 5570 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 746:
#line 5572 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 747:
#line 5576 "gram1.y"
    {  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 748:
#line 5578 "gram1.y"
    {  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 749:
#line 5580 "gram1.y"
    {
                      (yyvsp[(2) - (3)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node); 
                      (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);   
                   ;}
    break;

  case 750:
#line 5587 "gram1.y"
    { PTR_SYMB s;
            s = make_scalar((yyvsp[(1) - (1)].hash_entry),TYNULL,LOCAL);
	    (yyval.ll_node) = make_llnd(fi,DUMMY_REF, LLNULL, LLNULL, s);
            /*$$->type = global_int;*/
          ;}
    break;

  case 751:
#line 5604 "gram1.y"
    {  (yyval.ll_node) = LLNULL; ;}
    break;

  case 752:
#line 5606 "gram1.y"
    {  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 753:
#line 5610 "gram1.y"
    {  (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 754:
#line 5612 "gram1.y"
    {  (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 755:
#line 5616 "gram1.y"
    {  if((yyvsp[(1) - (1)].ll_node)->type->variant != T_STRING)
                 errstr( "Illegal type of shadow_name", 627);
               (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); 
            ;}
    break;

  case 756:
#line 5623 "gram1.y"
    { char *q;
          nioctl = 1;
          q = (yyvsp[(1) - (2)].ll_node)->entry.string_val;
          if((!strcmp(q,"shadow")) && ((yyvsp[(2) - (2)].ll_node)->variant == INT_VAL))                          (yyval.ll_node) = make_llnd(fi,SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL);
          else
          {  err("Illegal shadow width specification", 56);
             (yyval.ll_node) = LLNULL;
          }
        ;}
    break;

  case 757:
#line 5633 "gram1.y"
    { char *ql, *qh;
          PTR_LLND p1, p2;
          nioctl = 2;
          ql = (yyvsp[(1) - (5)].ll_node)->entry.string_val;
          qh = (yyvsp[(4) - (5)].ll_node)->entry.string_val;
          if((!strcmp(ql,"low_shadow")) && ((yyvsp[(2) - (5)].ll_node)->variant == INT_VAL) && (!strcmp(qh,"high_shadow")) && ((yyvsp[(5) - (5)].ll_node)->variant == INT_VAL)) 
              {
                 p1 = make_llnd(fi,SPEC_PAIR, (yyvsp[(1) - (5)].ll_node), (yyvsp[(2) - (5)].ll_node), SMNULL);
                 p2 = make_llnd(fi,SPEC_PAIR, (yyvsp[(4) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL);
                 (yyval.ll_node) = make_llnd(fi,CONS, p1, p2, SMNULL);
              } 
          else
          {  err("Illegal shadow width specification", 56);
             (yyval.ll_node) = LLNULL;
          }
        ;}
    break;

  case 758:
#line 5662 "gram1.y"
    { PTR_LLND q;
              q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
              (yyval.bf_node) = (yyvsp[(4) - (4)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr1 = q;
            ;}
    break;

  case 759:
#line 5677 "gram1.y"
    { PTR_LLND q;
              q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
              (yyval.bf_node) = (yyvsp[(4) - (4)].bf_node);
              (yyval.bf_node)->variant = DVM_REALIGN_DIR; 
              (yyval.bf_node)->entry.Template.ll_ptr1 = q;
            ;}
    break;

  case 760:
#line 5684 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(3) - (6)].bf_node);
              (yyval.bf_node)->variant = DVM_REALIGN_DIR; 
              (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(6) - (6)].ll_node);
            ;}
    break;

  case 761:
#line 5702 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 762:
#line 5704 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 763:
#line 5708 "gram1.y"
    {  PTR_SYMB s;
          s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
          if((s->attr & ALIGN_BIT)) 
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
          if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & INHERIT_BIT)) 
                errstr( "Inconsistent declaration of identifier  %s", s->ident, 16); 
          else  if(s->attr & DISTRIBUTE_BIT)
               errstr( "An alignee may not have the DISTRIBUTE attribute:'%s'", s->ident,57);             else
                s->attr = s->attr | ALIGN_BIT;     
	  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	;}
    break;

  case 764:
#line 5722 "gram1.y"
    {PTR_SYMB s;
        s = (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol;
        if(s->attr & PROCESSORS_BIT)
               errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);
        else  if(s->attr & TASK_BIT)
              errstr( "Illegal use of task array name %s ", s->ident, 71);
        else if( !(s->attr & DIMENSION_BIT) && !(s->attr & DVM_POINTER_BIT))
            errstr("The alignee %s isn't an array", s->ident, 58);
        else {
            /*  if(!(s->attr & DYNAMIC_BIT))
                 errstr("'%s' hasn't the DYNAMIC attribute", s->ident, 59);
             */
              if(!(s->attr & ALIGN_BIT) && !(s->attr & INHERIT_BIT))
                 errstr("'%s' hasn't the ALIGN attribute", s->ident, 60);
              if(s->attr & DISTRIBUTE_BIT)
                 errstr("An alignee may not have the DISTRIBUTE attribute: %s", s->ident, 57);

/*               if(s->entry.var_decl.local == IO)
 *                 errstr("An alignee may not be the dummy argument");
*/
          }
	  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	;}
    break;

  case 765:
#line 5748 "gram1.y"
    { /* PTR_LLND r;
              if($7) {
                r = set_ll_list($6,LLNULL,EXPR_LIST);
                r = set_ll_list(r,$7,EXPR_LIST);
              }
              else
                r = $6;
              */
            (yyval.bf_node) = get_bfnd(fi,DVM_ALIGN_DIR,SMNULL,LLNULL,(yyvsp[(2) - (6)].ll_node),(yyvsp[(6) - (6)].ll_node));
           ;}
    break;

  case 766:
#line 5761 "gram1.y"
    {
           (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, (yyvsp[(1) - (4)].symbol));        
          ;}
    break;

  case 767:
#line 5777 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 768:
#line 5779 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 769:
#line 5782 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 770:
#line 5784 "gram1.y"
    {
                  (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
                  (yyval.ll_node)->entry.string_val = (char *) "*";
                  (yyval.ll_node)->type = global_string;
                 ;}
    break;

  case 771:
#line 5790 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 772:
#line 5794 "gram1.y"
    { 
         /* if(parstate == INEXEC){ *for REALIGN directive*
              if (!($$ = $1->id_attr))
              {
	         $$ = make_array($1, TYNULL, LLNULL,0,LOCAL);
	     	 $$->decl = SOFT;
	      } 
          } else
             $$ = make_array($1, TYNULL, LLNULL, 0, LOCAL);
          */
          if (!((yyval.symbol) = (yyvsp[(1) - (1)].hash_entry)->id_attr))
          {
	       (yyval.symbol) = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL,0,LOCAL);
	       (yyval.symbol)->decl = SOFT;
	  } 
          (yyval.symbol)->attr = (yyval.symbol)->attr | ALIGN_BASE_BIT;
          if((yyval.symbol)->attr & PROCESSORS_BIT)
               errstr( "Illegal use of PROCESSORS name %s ", (yyval.symbol)->ident, 53);
          else  if((yyval.symbol)->attr & TASK_BIT)
               errstr( "Illegal use of task array name %s ", (yyval.symbol)->ident, 71);
          else
          if((parstate == INEXEC) /* for  REALIGN directive */
             &&   !((yyval.symbol)->attr & DIMENSION_BIT) && !((yyval.symbol)->attr & DVM_POINTER_BIT))
            errstr("The align-target %s isn't declared as array", (yyval.symbol)->ident, 61); 
         ;}
    break;

  case 773:
#line 5822 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 774:
#line 5824 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 775:
#line 5828 "gram1.y"
    { PTR_SYMB s;
            s = make_scalar((yyvsp[(1) - (1)].hash_entry),TYNULL,LOCAL);
            if(s->type->variant != T_INT || s->attr & PARAMETER_BIT)             
              errstr("The align-dummy %s isn't a scalar integer variable", s->ident, 62); 
	   (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
           (yyval.ll_node)->type = global_int;
         ;}
    break;

  case 776:
#line 5836 "gram1.y"
    {  
          (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
          (yyval.ll_node)->entry.string_val = (char *) "*";
          (yyval.ll_node)->type = global_string;
        ;}
    break;

  case 777:
#line 5842 "gram1.y"
    {   (yyval.ll_node) = make_llnd(fi,DDOT, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 778:
#line 5845 "gram1.y"
    { PTR_SYMB s;
	             PTR_LLND q, r, p;
                     int numdim;
                     if(type_options & PROCESSORS_BIT) {    /* 27.06.18 || (type_options & TEMPLATE_BIT)){ */
                       if(! explicit_shape) {
                         err("Explicit shape specification is required", 50);
		         /*$$ = BFNULL;*/
	               }
                     } 

                    /*  else {
                       if($6)
                         err("Shape specification is not permitted", 263);
                     } */

                     if(type_options & DIMENSION_BIT)
                       { p = attr_dims; numdim = attr_ndim;}
                     else
                       { p = LLNULL; numdim = 0; }
                     if((yyvsp[(6) - (6)].ll_node))          /*dimension information after the object name*/
                     { p = (yyvsp[(6) - (6)].ll_node); numdim = ndim;} /*overrides the DIMENSION attribute */
	             s = make_array((yyvsp[(5) - (6)].hash_entry), TYNULL, p, numdim, LOCAL);

                     if((type_options & COMMON_BIT) && !(type_options & TEMPLATE_BIT))
                     {
                        err("Illegal combination of attributes", 63);
                        type_options = type_options & (~COMMON_BIT);
                     }
                     if((type_options & PROCESSORS_BIT) &&((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT) ))
                        err("Illegal combination of attributes", 63);
                     else  if((type_options & PROCESSORS_BIT) && ((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT)) )
                     {  errstr("Inconsistent declaration of  %s", s->ident, 16);
                        type_options = type_options & (~PROCESSORS_BIT);
                     }
                     else if ((s->attr & PROCESSORS_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT))) 
                        errstr("Inconsistent declaration of  %s", s->ident, 16);
                     else if ((s->attr & INHERIT_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT)))
                        errstr("Inconsistent declaration of  %s", s->ident, 16);
                     if(( s->attr & DISTRIBUTE_BIT) &&  (type_options & DISTRIBUTE_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & ALIGN_BIT) &&  (type_options & ALIGN_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & SHADOW_BIT) &&  (type_options & SHADOW_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & TEMPLATE_BIT) &&  (type_options & TEMPLATE_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & PROCESSORS_BIT) &&  (type_options & PROCESSORS_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
	             s->attr = s->attr | type_options;
                     if((yyvsp[(6) - (6)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
                     if((s->attr & DISTRIBUTE_BIT) && (s->attr & ALIGN_BIT))
                       errstr("%s has the DISTRIBUTE and ALIGN attribute",s->ident, 64);
	             q = make_llnd(fi,ARRAY_REF, (yyvsp[(6) - (6)].ll_node), LLNULL, s);
	             if(p) s->type->entry.ar_decl.ranges = p;
	             r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	             (yyval.bf_node) = get_bfnd(fi,DVM_VAR_DECL, SMNULL, r, LLNULL,(yyvsp[(1) - (6)].ll_node));
	            ;}
    break;

  case 779:
#line 5903 "gram1.y"
    { PTR_SYMB s;
	             PTR_LLND q, r, p;
                     int numdim;
                    if(type_options & PROCESSORS_BIT) { /*23.10.18  || (type_options & TEMPLATE_BIT)){ */
                       if(! explicit_shape) {
                         err("Explicit shape specification is required", 50);
		         /*$$ = BFNULL;*/
	               }
                     } 
                    /* else {
                       if($4)
                         err("Shape specification is not permitted", 263);
                     } */
                     if(type_options & DIMENSION_BIT)
                       { p = attr_dims; numdim = attr_ndim;}
                     else
                       { p = LLNULL; numdim = 0; }
                     if((yyvsp[(4) - (4)].ll_node))                   /*dimension information after the object name*/
                     { p = (yyvsp[(4) - (4)].ll_node); numdim = ndim;}/*overrides the DIMENSION attribute */
	             s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, p, numdim, LOCAL);

                     if((type_options & COMMON_BIT) && !(type_options & TEMPLATE_BIT))
                     {
                        err("Illegal combination of attributes", 63);
                        type_options = type_options & (~COMMON_BIT);
                     }
                     if((type_options & PROCESSORS_BIT) &&((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT) ))
                       err("Illegal combination of attributes", 63);
                     else  if((type_options & PROCESSORS_BIT) && ((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT)) )
                     {  errstr("Inconsistent declaration of identifier %s", s->ident, 16);
                        type_options = type_options & (~PROCESSORS_BIT);
                     }
                     else if ((s->attr & PROCESSORS_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT))) 
                          errstr("Inconsistent declaration of identifier  %s", s->ident,16);
                     else if ((s->attr & INHERIT_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT)))
                          errstr("Inconsistent declaration of identifier %s", s->ident, 16);
                     if(( s->attr & DISTRIBUTE_BIT) &&  (type_options & DISTRIBUTE_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & ALIGN_BIT) &&  (type_options & ALIGN_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & SHADOW_BIT) &&  (type_options & SHADOW_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & TEMPLATE_BIT) &&  (type_options & TEMPLATE_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & PROCESSORS_BIT) &&  (type_options & PROCESSORS_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);   
	             s->attr = s->attr | type_options;
                     if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
                     if((s->attr & DISTRIBUTE_BIT) && (s->attr & ALIGN_BIT))
                           errstr("%s has the DISTRIBUTE and ALIGN attribute",s->ident, 64);
	             q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	             if(p) s->type->entry.ar_decl.ranges = p;
	             r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	             add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	            ;}
    break;

  case 780:
#line 5967 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); type_options = type_opt; ;}
    break;

  case 781:
#line 5969 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),EXPR_LIST); type_options = type_options | type_opt;;}
    break;

  case 782:
#line 5972 "gram1.y"
    { type_opt = TEMPLATE_BIT;
               (yyval.ll_node) = make_llnd(fi,TEMPLATE_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 783:
#line 5976 "gram1.y"
    { type_opt = PROCESSORS_BIT;
                (yyval.ll_node) = make_llnd(fi,PROCESSORS_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 784:
#line 5980 "gram1.y"
    { type_opt = PROCESSORS_BIT;
                (yyval.ll_node) = make_llnd(fi,PROCESSORS_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 785:
#line 5984 "gram1.y"
    { type_opt = DYNAMIC_BIT;
                (yyval.ll_node) = make_llnd(fi,DYNAMIC_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 786:
#line 6001 "gram1.y"
    {
                if(! explicit_shape) {
                  err("Explicit shape specification is required", 50);
                }
                if(! (yyvsp[(3) - (4)].ll_node)) {
                  err("No shape specification", 65);
	        }
                type_opt = DIMENSION_BIT;
                attr_ndim = ndim; attr_dims = (yyvsp[(3) - (4)].ll_node);
                (yyval.ll_node) = make_llnd(fi,DIMENSION_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);
	       ;}
    break;

  case 787:
#line 6013 "gram1.y"
    { type_opt = SHADOW_BIT;
                  (yyval.ll_node) = make_llnd(fi,SHADOW_OP,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
                 ;}
    break;

  case 788:
#line 6017 "gram1.y"
    { type_opt = ALIGN_BIT;
                  (yyval.ll_node) = make_llnd(fi,ALIGN_OP,(yyvsp[(3) - (7)].ll_node),(yyvsp[(7) - (7)].ll_node),SMNULL);
                 ;}
    break;

  case 789:
#line 6021 "gram1.y"
    { type_opt = ALIGN_BIT;
                  (yyval.ll_node) = make_llnd(fi,ALIGN_OP,LLNULL,SMNULL,SMNULL);
                ;}
    break;

  case 790:
#line 6031 "gram1.y"
    { 
                 type_opt = DISTRIBUTE_BIT;
                 (yyval.ll_node) = make_llnd(fi,DISTRIBUTE_OP,(yyvsp[(2) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),SMNULL);
                ;}
    break;

  case 791:
#line 6036 "gram1.y"
    { 
                 type_opt = DISTRIBUTE_BIT;
                 (yyval.ll_node) = make_llnd(fi,DISTRIBUTE_OP,LLNULL,LLNULL,SMNULL);
                ;}
    break;

  case 792:
#line 6041 "gram1.y"
    {
                 type_opt = COMMON_BIT;
                 (yyval.ll_node) = make_llnd(fi,COMMON_OP, LLNULL, LLNULL, SMNULL);
                ;}
    break;

  case 793:
#line 6048 "gram1.y"
    { 
	      PTR_LLND  l;
	      l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
	      l->type = (yyvsp[(1) - (11)].data_type);
	      (yyval.bf_node) = get_bfnd(fi,DVM_POINTER_DIR, SMNULL, (yyvsp[(11) - (11)].ll_node),(yyvsp[(7) - (11)].ll_node), l);
	    ;}
    break;

  case 794:
#line 6056 "gram1.y"
    {ndim = 0;;}
    break;

  case 795:
#line 6057 "gram1.y"
    { PTR_LLND  q;
             if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		q = make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL);
	      ++ndim;
              (yyval.ll_node) = set_ll_list(q, LLNULL, EXPR_LIST);
	       /*$$ = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);*/
	       /*$$->type = global_default;*/
	    ;}
    break;

  case 796:
#line 6068 "gram1.y"
    { PTR_LLND  q;
             if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		q = make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL);
	      ++ndim;
              (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), q, EXPR_LIST);
            ;}
    break;

  case 797:
#line 6079 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 798:
#line 6081 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 799:
#line 6085 "gram1.y"
    {PTR_SYMB s;
           /* s = make_scalar($1,TYNULL,LOCAL);*/
            s = make_array((yyvsp[(1) - (1)].hash_entry),TYNULL,LLNULL,0,LOCAL);
            s->attr = s->attr | DVM_POINTER_BIT;
            if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & INHERIT_BIT))
               errstr( "Inconsistent declaration of identifier %s", s->ident, 16);     
            (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL,s);
            ;}
    break;

  case 800:
#line 6096 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_HEAP_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 801:
#line 6100 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 802:
#line 6102 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 803:
#line 6106 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              s->attr = s->attr | HEAP_BIT;
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & ALIGN_BIT) || (s->attr & DISTRIBUTE_BIT) || (s->attr & INHERIT_BIT) || (s->attr & DYNAMIC_BIT) || (s->attr & SHADOW_BIT) || (s->attr & DVM_POINTER_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
      
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 804:
#line 6117 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 805:
#line 6121 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 806:
#line 6123 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 807:
#line 6127 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              s->attr = s->attr | CONSISTENT_BIT;
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & ALIGN_BIT) || (s->attr & DISTRIBUTE_BIT) || (s->attr & INHERIT_BIT) || (s->attr & DYNAMIC_BIT) || (s->attr & SHADOW_BIT) || (s->attr & DVM_POINTER_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
      
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 808:
#line 6139 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCID_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 809:
#line 6141 "gram1.y"
    { PTR_LLND p;
              p = make_llnd(fi,COMM_LIST, LLNULL, LLNULL, SMNULL);              
              (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCID_DIR, SMNULL, (yyvsp[(8) - (8)].ll_node), p, LLNULL);
            ;}
    break;

  case 810:
#line 6148 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 811:
#line 6150 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 812:
#line 6154 "gram1.y"
    {  PTR_SYMB s;
              if((yyvsp[(2) - (2)].ll_node)){
                  s = make_array((yyvsp[(1) - (2)].hash_entry), global_default, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
		  s->variant = ASYNC_ID;
                  s->attr = s->attr | DIMENSION_BIT;
                  s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
                  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);
              } else {
              s = make_local_entity((yyvsp[(1) - (2)].hash_entry), ASYNC_ID, global_default, LOCAL);
	      (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
              }
	   ;}
    break;

  case 813:
#line 6170 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_NEW_VALUE_DIR,SMNULL, LLNULL, LLNULL,LLNULL);;}
    break;

  case 814:
#line 6180 "gram1.y"
    {  if((yyvsp[(6) - (7)].ll_node) &&  (yyvsp[(6) - (7)].ll_node)->entry.Template.symbol->attr & TASK_BIT)
                        (yyval.bf_node) = get_bfnd(fi,DVM_PARALLEL_TASK_DIR,SMNULL,(yyvsp[(6) - (7)].ll_node),(yyvsp[(7) - (7)].ll_node),(yyvsp[(4) - (7)].ll_node));
                    else
                        (yyval.bf_node) = get_bfnd(fi,DVM_PARALLEL_ON_DIR,SMNULL,(yyvsp[(6) - (7)].ll_node),(yyvsp[(7) - (7)].ll_node),(yyvsp[(4) - (7)].ll_node));
                 ;}
    break;

  case 815:
#line 6189 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 816:
#line 6191 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 817:
#line 6195 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 818:
#line 6198 "gram1.y"
    { (yyval.ll_node) = LLNULL; opt_kwd_ = NO;;}
    break;

  case 819:
#line 6203 "gram1.y"
    {
          if((yyvsp[(1) - (4)].ll_node)->type->variant != T_ARRAY) 
             errstr("'%s' isn't array", (yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->ident, 66);
          (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
          (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
          (yyval.ll_node)->type = (yyvsp[(1) - (4)].ll_node)->type->entry.ar_decl.base_type;
        ;}
    break;

  case 820:
#line 6213 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 821:
#line 6215 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 822:
#line 6219 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 823:
#line 6221 "gram1.y"
    {
             (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
             (yyval.ll_node)->entry.string_val = (char *) "*";
             (yyval.ll_node)->type = global_string;
            ;}
    break;

  case 824:
#line 6229 "gram1.y"
    {  (yyval.ll_node) = LLNULL;;}
    break;

  case 825:
#line 6231 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 826:
#line 6235 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 827:
#line 6237 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (2)].ll_node),(yyvsp[(2) - (2)].ll_node),EXPR_LIST); ;}
    break;

  case 839:
#line 6255 "gram1.y"
    { if((yyvsp[(5) - (8)].symbol)->attr & INDIRECT_BIT)
                            errstr("'%s' is not remote group name", (yyvsp[(5) - (8)].symbol)->ident, 68);
                          (yyval.ll_node) = make_llnd(fi,REMOTE_ACCESS_OP,(yyvsp[(7) - (8)].ll_node),LLNULL,(yyvsp[(5) - (8)].symbol));
                        ;}
    break;

  case 840:
#line 6260 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,REMOTE_ACCESS_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 841:
#line 6264 "gram1.y"
    {
                          (yyval.ll_node) = make_llnd(fi,CONSISTENT_OP,(yyvsp[(7) - (8)].ll_node),LLNULL,(yyvsp[(5) - (8)].symbol));
                        ;}
    break;

  case 842:
#line 6268 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,CONSISTENT_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 843:
#line 6272 "gram1.y"
    {  
            if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL){
                errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
                (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),CONSISTENT_GROUP_NAME,global_default,LOCAL);
            } else {
                if((yyval.symbol)->variant != CONSISTENT_GROUP_NAME)
                   errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
            }
          ;}
    break;

  case 844:
#line 6285 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,NEW_SPEC_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 845:
#line 6289 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,NEW_SPEC_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 846:
#line 6293 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_PRIVATE_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 847:
#line 6297 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_CUDA_BLOCK_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 848:
#line 6300 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 849:
#line 6302 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 850:
#line 6304 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(3) - (5)].ll_node),EXPR_LIST); (yyval.ll_node) = set_ll_list((yyval.ll_node),(yyvsp[(5) - (5)].ll_node),EXPR_LIST);;}
    break;

  case 851:
#line 6308 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 852:
#line 6310 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 853:
#line 6314 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_TIE_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 854:
#line 6318 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 855:
#line 6320 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 856:
#line 6324 "gram1.y"
    { if(!((yyvsp[(5) - (8)].symbol)->attr & INDIRECT_BIT))
                         errstr("'%s' is not indirect group name", (yyvsp[(5) - (8)].symbol)->ident, 313);
                      (yyval.ll_node) = make_llnd(fi,INDIRECT_ACCESS_OP,(yyvsp[(7) - (8)].ll_node),LLNULL,(yyvsp[(5) - (8)].symbol));
                    ;}
    break;

  case 857:
#line 6329 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,INDIRECT_ACCESS_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 858:
#line 6333 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,STAGE_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 859:
#line 6337 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,ACROSS_OP,(yyvsp[(4) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 860:
#line 6339 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,ACROSS_OP,(yyvsp[(4) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),SMNULL);;}
    break;

  case 861:
#line 6343 "gram1.y"
    {  if((yyvsp[(3) - (5)].ll_node))
                     (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(3) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),SMNULL);
                   else
                     (yyval.ll_node) = (yyvsp[(4) - (5)].ll_node);
                ;}
    break;

  case 862:
#line 6351 "gram1.y"
    { opt_in_out = YES; ;}
    break;

  case 863:
#line 6355 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "in";
              (yyval.ll_node)->type = global_string;
            ;}
    break;

  case 864:
#line 6361 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "out";
              (yyval.ll_node)->type = global_string;
            ;}
    break;

  case 865:
#line 6367 "gram1.y"
    {  (yyval.ll_node) = LLNULL; opt_in_out = NO;;}
    break;

  case 866:
#line 6371 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 867:
#line 6373 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 868:
#line 6377 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 869:
#line 6379 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
                    (yyval.ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);  
                  ;}
    break;

  case 870:
#line 6383 "gram1.y"
    { /*  PTR_LLND p;
                       p = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
                       p->entry.string_val = (char *) "corner";
                       p->type = global_string;
                   */
                   (yyvsp[(1) - (7)].ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(3) - (7)].ll_node);  
                   (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),SMNULL);
                 ;}
    break;

  case 871:
#line 6395 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 872:
#line 6397 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 873:
#line 6401 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);;}
    break;

  case 874:
#line 6405 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 875:
#line 6407 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 876:
#line 6411 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (5)].ll_node),make_llnd(fi,DDOT,(yyvsp[(3) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 877:
#line 6413 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),make_llnd(fi,DDOT,(yyvsp[(3) - (3)].ll_node),LLNULL,SMNULL),SMNULL); ;}
    break;

  case 878:
#line 6415 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),make_llnd(fi,DDOT,LLNULL,(yyvsp[(3) - (3)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 879:
#line 6417 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 880:
#line 6419 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 881:
#line 6421 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,make_llnd(fi,DDOT,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL),SMNULL); ;}
    break;

  case 882:
#line 6423 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,make_llnd(fi,DDOT,LLNULL,(yyvsp[(1) - (1)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 883:
#line 6427 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 884:
#line 6431 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 885:
#line 6435 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 886:
#line 6439 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);;}
    break;

  case 887:
#line 6443 "gram1.y"
    {PTR_LLND q;
                /* q = set_ll_list($9,$6,EXPR_LIST); */
                 q = set_ll_list((yyvsp[(6) - (10)].ll_node),LLNULL,EXPR_LIST); /*podd 11.10.01*/
                 q = add_to_lowLevelList((yyvsp[(9) - (10)].ll_node),q);        /*podd 11.10.01*/
                 (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,q,LLNULL,SMNULL);
                ;}
    break;

  case 888:
#line 6450 "gram1.y"
    {PTR_LLND q;
                 q = set_ll_list((yyvsp[(6) - (8)].ll_node),LLNULL,EXPR_LIST);
                 (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,q,LLNULL,SMNULL);
                ;}
    break;

  case 889:
#line 6456 "gram1.y"
    {  (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,(yyvsp[(9) - (10)].ll_node),LLNULL,(yyvsp[(6) - (10)].symbol)); ;}
    break;

  case 890:
#line 6460 "gram1.y"
    { opt_kwd_r = YES; ;}
    break;

  case 891:
#line 6463 "gram1.y"
    { opt_kwd_r = NO; ;}
    break;

  case 892:
#line 6467 "gram1.y"
    { 
                  if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL) {
                      errstr("'%s' is not declared as reduction group", (yyvsp[(1) - (1)].hash_entry)->ident, 69);
                      (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),REDUCTION_GROUP_NAME,global_default,LOCAL);
                  } else {
                    if((yyval.symbol)->variant != REDUCTION_GROUP_NAME)
                      errstr("'%s' is not declared as reduction group", (yyvsp[(1) - (1)].hash_entry)->ident, 69);
                  }
                ;}
    break;

  case 893:
#line 6480 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 894:
#line 6482 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),EXPR_LIST);;}
    break;

  case 895:
#line 6486 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (4)].ll_node),(yyvsp[(3) - (4)].ll_node),SMNULL);;}
    break;

  case 896:
#line 6488 "gram1.y"
    {(yyvsp[(3) - (6)].ll_node) = set_ll_list((yyvsp[(3) - (6)].ll_node),(yyvsp[(5) - (6)].ll_node),EXPR_LIST);
            (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (6)].ll_node),(yyvsp[(3) - (6)].ll_node),SMNULL);;}
    break;

  case 897:
#line 6493 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "sum";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 898:
#line 6499 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "product";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 899:
#line 6505 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "min";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 900:
#line 6511 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "max";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 901:
#line 6517 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "or";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 902:
#line 6523 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "and";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 903:
#line 6529 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "eqv";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 904:
#line 6535 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "neqv";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 905:
#line 6541 "gram1.y"
    { err("Illegal reduction operation name", 70);
               errcnt--;
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "unknown";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 906:
#line 6550 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "maxloc";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 907:
#line 6556 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "minloc";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 908:
#line 6573 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_RENEW_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 909:
#line 6581 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_START_OP,LLNULL,LLNULL,(yyvsp[(4) - (4)].symbol));;}
    break;

  case 910:
#line 6589 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_WAIT_OP,LLNULL,LLNULL,(yyvsp[(4) - (4)].symbol));;}
    break;

  case 911:
#line 6591 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_COMP_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 912:
#line 6593 "gram1.y"
    {  (yyvsp[(5) - (9)].ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(7) - (9)].ll_node); (yyval.ll_node) = make_llnd(fi,SHADOW_COMP_OP,(yyvsp[(5) - (9)].ll_node),LLNULL,SMNULL);;}
    break;

  case 913:
#line 6597 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), SHADOW_GROUP_NAME,global_default,LOCAL);;}
    break;

  case 914:
#line 6601 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 915:
#line 6603 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 916:
#line 6607 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 917:
#line 6609 "gram1.y"
    { PTR_LLND p;
          p = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
          p->entry.string_val = (char *) "corner";
          p->type = global_string;
          (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (5)].ll_node),p,SMNULL);
         ;}
    break;

  case 918:
#line 6617 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
          (yyval.ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);  
        ;}
    break;

  case 919:
#line 6621 "gram1.y"
    { PTR_LLND p;
          p = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
          p->entry.string_val = (char *) "corner";
          p->type = global_string;
          (yyvsp[(1) - (9)].ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(4) - (9)].ll_node);  
          (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (9)].ll_node),p,SMNULL);
       ;}
    break;

  case 920:
#line 6632 "gram1.y"
    { optcorner = YES; ;}
    break;

  case 921:
#line 6636 "gram1.y"
    { PTR_SYMB s;
         s = (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol;
         if(s->attr & PROCESSORS_BIT)
             errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);
         else if(s->attr & TASK_BIT)
             errstr( "Illegal use of task array name %s ", s->ident, 71);
         else
           if(s->type->variant != T_ARRAY) 
             errstr("'%s' isn't array", s->ident, 66);
           else 
              if((!(s->attr & DISTRIBUTE_BIT)) && (!(s->attr & ALIGN_BIT)))
               ; /*errstr("hpf.gram: %s is not distributed array", s->ident);*/
                
         (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
        ;}
    break;

  case 922:
#line 6654 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 923:
#line 6656 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 924:
#line 6660 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_START_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 925:
#line 6662 "gram1.y"
    {errstr("Missing DVM directive prefix", 49);;}
    break;

  case 926:
#line 6666 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_WAIT_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 927:
#line 6668 "gram1.y"
    {errstr("Missing DVM directive prefix", 49);;}
    break;

  case 928:
#line 6672 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_GROUP_DIR,(yyvsp[(3) - (6)].symbol),(yyvsp[(5) - (6)].ll_node),LLNULL,LLNULL);;}
    break;

  case 929:
#line 6676 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_REDUCTION_START_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 930:
#line 6680 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_REDUCTION_WAIT_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 931:
#line 6689 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_START_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 932:
#line 6693 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_WAIT_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 933:
#line 6697 "gram1.y"
    { if(((yyvsp[(4) - (7)].symbol)->attr & INDIRECT_BIT))
                errstr("'%s' is not remote group name", (yyvsp[(4) - (7)].symbol)->ident, 68);
           (yyval.bf_node) = get_bfnd(fi,DVM_REMOTE_ACCESS_DIR,(yyvsp[(4) - (7)].symbol),(yyvsp[(6) - (7)].ll_node),LLNULL,LLNULL);
         ;}
    break;

  case 934:
#line 6702 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_REMOTE_ACCESS_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 935:
#line 6706 "gram1.y"
    {  
            if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL){
                errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
                (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),REF_GROUP_NAME,global_default,LOCAL);
            } else {
              if((yyval.symbol)->variant != REF_GROUP_NAME)
                errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
            }
          ;}
    break;

  case 936:
#line 6718 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 937:
#line 6720 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 938:
#line 6724 "gram1.y"
    {
              (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
              (yyval.ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
            ;}
    break;

  case 939:
#line 6729 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 940:
#line 6733 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 941:
#line 6735 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 942:
#line 6739 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 943:
#line 6741 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, LLNULL, LLNULL, SMNULL);;}
    break;

  case 944:
#line 6745 "gram1.y"
    {  PTR_LLND q;
             q = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
             (yyval.bf_node) = get_bfnd(fi,DVM_TASK_DIR,SMNULL,q,LLNULL,LLNULL);
          ;}
    break;

  case 945:
#line 6750 "gram1.y"
    {   PTR_LLND q;
              q = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
          ;}
    break;

  case 946:
#line 6757 "gram1.y"
    { 
             PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (2)].hash_entry), global_int, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
              if((yyvsp[(2) - (2)].ll_node)){
                  s->attr = s->attr | DIMENSION_BIT;
                  s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
              }
              else
                  err("No dimensions in TASK directive", 75);
              if(ndim > 1)
                  errstr("Illegal rank of '%s'", s->ident, 76);
              if(s->attr & TASK_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & PROCESSORS_BIT)  || (s->attr & DVM_POINTER_BIT) || (s->attr & INHERIT_BIT))
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16);
              else
	        s->attr = s->attr | TASK_BIT;
    
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);	  
	    ;}
    break;

  case 947:
#line 6780 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 948:
#line 6782 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (4)].symbol),(yyvsp[(4) - (4)].ll_node),LLNULL,LLNULL);;}
    break;

  case 949:
#line 6784 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (4)].symbol),LLNULL,(yyvsp[(4) - (4)].ll_node),LLNULL);;}
    break;

  case 950:
#line 6786 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (5)].symbol),(yyvsp[(4) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),LLNULL);;}
    break;

  case 951:
#line 6788 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (5)].symbol),(yyvsp[(5) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),LLNULL);;}
    break;

  case 952:
#line 6792 "gram1.y"
    { PTR_SYMB s;
              if((s=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL)
                s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              
              if(!(s->attr & TASK_BIT))
                 errstr("'%s' is not task array", s->ident, 77);
              (yyval.symbol) = s;
              ;}
    break;

  case 953:
#line 6803 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_END_TASK_REGION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 954:
#line 6807 "gram1.y"
    {  PTR_SYMB s;
              PTR_LLND q;
             /*
              s = make_array($1, TYNULL, LLNULL, 0, LOCAL);                           
	      if((parstate == INEXEC) && !(s->attr & TASK_BIT))
                 errstr("'%s' is not task array", s->ident, 77);  
              q =  set_ll_list($3,LLNULL,EXPR_LIST);
	      $$ = make_llnd(fi,ARRAY_REF, q, LLNULL, s);
              */

              s = (yyvsp[(1) - (4)].symbol);
              q =  set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, q, LLNULL, s);
	   ;}
    break;

  case 955:
#line 6822 "gram1.y"
    {  PTR_LLND q; 
              q =  set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, q, LLNULL, (yyvsp[(1) - (4)].symbol));
	   ;}
    break;

  case 956:
#line 6829 "gram1.y"
    {              
         (yyval.bf_node) = get_bfnd(fi,DVM_ON_DIR,SMNULL,(yyvsp[(3) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),LLNULL);
    ;}
    break;

  case 957:
#line 6835 "gram1.y"
    {(yyval.ll_node) = LLNULL;;}
    break;

  case 958:
#line 6837 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 959:
#line 6841 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_END_ON_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 960:
#line 6845 "gram1.y"
    { PTR_LLND q;
        /* if(!($6->attr & PROCESSORS_BIT))
           errstr("'%s' is not processor array", $6->ident, 67);
         */
        q = make_llnd(fi,ARRAY_REF, (yyvsp[(7) - (7)].ll_node), LLNULL, (yyvsp[(6) - (7)].symbol));
        (yyval.bf_node) = get_bfnd(fi,DVM_MAP_DIR,SMNULL,(yyvsp[(3) - (7)].ll_node),q,LLNULL);
      ;}
    break;

  case 961:
#line 6853 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_MAP_DIR,SMNULL,(yyvsp[(3) - (6)].ll_node),LLNULL,(yyvsp[(6) - (6)].ll_node)); ;}
    break;

  case 962:
#line 6857 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_PREFETCH_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 963:
#line 6861 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_RESET_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 964:
#line 6869 "gram1.y"
    { if(!((yyvsp[(4) - (7)].symbol)->attr & INDIRECT_BIT))
                         errstr("'%s' is not indirect group name", (yyvsp[(4) - (7)].symbol)->ident, 313);
                      (yyval.bf_node) = get_bfnd(fi,DVM_INDIRECT_ACCESS_DIR,(yyvsp[(4) - (7)].symbol),(yyvsp[(6) - (7)].ll_node),LLNULL,LLNULL);
                    ;}
    break;

  case 965:
#line 6874 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_INDIRECT_ACCESS_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 966:
#line 6888 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 967:
#line 6890 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 968:
#line 6894 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 969:
#line 6896 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node); (yyval.ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);;}
    break;

  case 970:
#line 6905 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 971:
#line 6907 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 972:
#line 6909 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL, LLNULL, (yyvsp[(3) - (3)].ll_node), LLNULL);;}
    break;

  case 973:
#line 6911 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL, (yyvsp[(3) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node),LLNULL);;}
    break;

  case 974:
#line 6947 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,REDUCTION_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 975:
#line 6951 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCHRONOUS_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 976:
#line 6955 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ENDASYNCHRONOUS_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 977:
#line 6959 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCWAIT_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 978:
#line 6963 "gram1.y"
    {  
            if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL) {
                errstr("'%s' is not declared as ASYNCID", (yyvsp[(1) - (1)].hash_entry)->ident, 115);
                (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),ASYNC_ID,global_default,LOCAL);
            } else {
              if((yyval.symbol)->variant != ASYNC_ID)
                errstr("'%s' is not declared as ASYNCID", (yyvsp[(1) - (1)].hash_entry)->ident, 115);
            }
     ;}
    break;

  case 979:
#line 6975 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol));;}
    break;

  case 980:
#line 6977 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, (yyvsp[(1) - (4)].symbol));;}
    break;

  case 981:
#line 6981 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_F90_DIR,SMNULL,(yyvsp[(3) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),LLNULL);;}
    break;

  case 982:
#line 6984 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_DEBUG_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 983:
#line 6986 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_DEBUG_DIR,SMNULL,(yyvsp[(3) - (6)].ll_node),(yyvsp[(5) - (6)].ll_node),LLNULL);;}
    break;

  case 984:
#line 6990 "gram1.y"
    { 
              (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node), LLNULL, EXPR_LIST);
              endioctl();
            ;}
    break;

  case 985:
#line 6995 "gram1.y"
    { 
              (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST);
              endioctl();
            ;}
    break;

  case 986:
#line 7002 "gram1.y"
    { (yyval.ll_node)  = make_llnd(fi, KEYWORD_ARG, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 987:
#line 7005 "gram1.y"
    {
	         (yyval.ll_node) = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
	         (yyval.ll_node)->entry.ival = atoi(yytext);
	         (yyval.ll_node)->type = global_int;
	        ;}
    break;

  case 988:
#line 7013 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ENDDEBUG_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 989:
#line 7017 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_INTERVAL_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 990:
#line 7021 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 991:
#line 7024 "gram1.y"
    { if((yyvsp[(1) - (1)].ll_node)->type->variant != T_INT)             
                    err("Illegal interval number", 78);
                  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
                 ;}
    break;

  case 992:
#line 7032 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_EXIT_INTERVAL_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 993:
#line 7036 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ENDINTERVAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 994:
#line 7040 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TRACEON_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 995:
#line 7044 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TRACEOFF_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 996:
#line 7048 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_BARRIER_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 997:
#line 7052 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CHECK_DIR,SMNULL,(yyvsp[(9) - (9)].ll_node),(yyvsp[(5) - (9)].ll_node),LLNULL); ;}
    break;

  case 998:
#line 7056 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_IO_MODE_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 999:
#line 7059 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1000:
#line 7061 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1001:
#line 7065 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_ASYNC_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1002:
#line 7067 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_LOCAL_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1003:
#line 7069 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,PARALLEL_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1004:
#line 7073 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_ADD_DIR,SMNULL,(yyvsp[(4) - (9)].ll_node),(yyvsp[(6) - (9)].ll_node),(yyvsp[(9) - (9)].ll_node)); ;}
    break;

  case 1005:
#line 7077 "gram1.y"
    {
                 if((yyvsp[(1) - (4)].ll_node)->type->variant != T_ARRAY) 
                    errstr("'%s' isn't array", (yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->ident, 66);
                 if(!((yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->attr & TEMPLATE_BIT))
                    errstr("'%s' isn't TEMPLATE", (yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->ident, 628);
                 (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                 (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
                 /*$$->type = $1->type->entry.ar_decl.base_type;*/
               ;}
    break;

  case 1006:
#line 7089 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1007:
#line 7091 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1008:
#line 7095 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 1009:
#line 7097 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 1010:
#line 7101 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1011:
#line 7103 "gram1.y"
    { (yyval.ll_node) = LLNULL; opt_kwd_ = NO;;}
    break;

  case 1012:
#line 7107 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_LOCALIZE_DIR,SMNULL,(yyvsp[(4) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),LLNULL); ;}
    break;

  case 1013:
#line 7111 "gram1.y"
    {
                 if((yyvsp[(1) - (1)].ll_node)->type->variant != T_ARRAY) 
                    errstr("'%s' isn't array", (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol->ident, 66); 
                 (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
                ;}
    break;

  case 1014:
#line 7117 "gram1.y"
    {
                 if((yyvsp[(1) - (4)].ll_node)->type->variant != T_ARRAY) 
                    errstr("'%s' isn't array", (yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->ident, 66); 
                                 
                 (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                 (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
                 (yyval.ll_node)->type = (yyvsp[(1) - (4)].ll_node)->type->entry.ar_decl.base_type;
                ;}
    break;

  case 1015:
#line 7129 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1016:
#line 7131 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1017:
#line 7135 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 1018:
#line 7137 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, LLNULL, LLNULL, SMNULL);;}
    break;

  case 1019:
#line 7141 "gram1.y"
    { 
            (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
            (yyval.ll_node)->entry.string_val = (char *) "*";
            (yyval.ll_node)->type = global_string;
          ;}
    break;

  case 1020:
#line 7149 "gram1.y"
    { 
                PTR_LLND q;
                if((yyvsp[(16) - (16)].ll_node))
                  q = make_llnd(fi,ARRAY_OP, (yyvsp[(14) - (16)].ll_node), (yyvsp[(16) - (16)].ll_node), SMNULL);
                else
                  q = (yyvsp[(14) - (16)].ll_node);                  
                (yyval.bf_node) = get_bfnd(fi,DVM_CP_CREATE_DIR,SMNULL,(yyvsp[(3) - (16)].ll_node),(yyvsp[(8) - (16)].ll_node),q); 
              ;}
    break;

  case 1021:
#line 7160 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 1022:
#line 7162 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, PARALLEL_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 1023:
#line 7164 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_LOCAL_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 1024:
#line 7168 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CP_LOAD_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL); ;}
    break;

  case 1025:
#line 7172 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CP_SAVE_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL); ;}
    break;

  case 1026:
#line 7174 "gram1.y"
    {
                PTR_LLND q;
                q = make_llnd(fi,ACC_ASYNC_OP,LLNULL,LLNULL,SMNULL);
                (yyval.bf_node) = get_bfnd(fi,DVM_CP_SAVE_DIR,SMNULL,(yyvsp[(3) - (6)].ll_node),q,LLNULL);
              ;}
    break;

  case 1027:
#line 7182 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CP_WAIT_DIR,SMNULL,(yyvsp[(3) - (9)].ll_node),(yyvsp[(8) - (9)].ll_node),LLNULL); ;}
    break;

  case 1028:
#line 7186 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TEMPLATE_CREATE_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL); ;}
    break;

  case 1029:
#line 7189 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 1030:
#line 7191 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 1031:
#line 7195 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TEMPLATE_DELETE_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL); ;}
    break;

  case 1059:
#line 7229 "gram1.y"
    {
          (yyval.bf_node) = get_bfnd(fi,OMP_ONETHREAD_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1060:
#line 7235 "gram1.y"
    {
  	   (yyval.bf_node) = make_endparallel();
	;}
    break;

  case 1061:
#line 7241 "gram1.y"
    {
  	   (yyval.bf_node) = make_parallel();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1062:
#line 7247 "gram1.y"
    {
  	   (yyval.bf_node) = make_parallel();
	   opt_kwd_ = NO;
	;}
    break;

  case 1063:
#line 7253 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1064:
#line 7257 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);	
	;}
    break;

  case 1074:
#line 7274 "gram1.y"
    {
		(yyval.ll_node) = (yyvsp[(4) - (5)].ll_node);
        ;}
    break;

  case 1075:
#line 7279 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_PRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1076:
#line 7284 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_FIRSTPRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1077:
#line 7290 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_LASTPRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1078:
#line 7296 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_COPYIN,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1079:
#line 7302 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_SHARED,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1080:
#line 7307 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_DEFAULT,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1081:
#line 7313 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "private";
		(yyval.ll_node)->type = global_string;
	;}
    break;

  case 1082:
#line 7319 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "shared";
		(yyval.ll_node)->type = global_string;
	;}
    break;

  case 1083:
#line 7325 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "none";
		(yyval.ll_node)->type = global_string;
	;}
    break;

  case 1084:
#line 7332 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_IF,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1085:
#line 7338 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_NUM_THREADS,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1086:
#line 7344 "gram1.y"
    {
		PTR_LLND q;
		q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
		(yyval.ll_node) = make_llnd(fi,OMP_REDUCTION,q,LLNULL,SMNULL);
	;}
    break;

  case 1087:
#line 7351 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(2) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),SMNULL);;}
    break;

  case 1089:
#line 7357 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "+";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1090:
#line 7363 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "-";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1091:
#line 7370 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "*";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1092:
#line 7376 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "/";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1093:
#line 7382 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "min";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1094:
#line 7388 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "max";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1095:
#line 7394 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".or.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1096:
#line 7400 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".and.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1097:
#line 7406 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".eqv.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1098:
#line 7412 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".neqv.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1099:
#line 7418 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "iand";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1100:
#line 7424 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "ieor";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1101:
#line 7430 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "ior";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1102:
#line 7436 "gram1.y"
    { err("Illegal reduction operation name", 70);
               errcnt--;
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "unknown";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1103:
#line 7446 "gram1.y"
    {
  	   (yyval.bf_node) = make_sections((yyvsp[(4) - (4)].ll_node));
	   opt_kwd_ = NO;
	;}
    break;

  case 1104:
#line 7451 "gram1.y"
    {
  	   (yyval.bf_node) = make_sections(LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1105:
#line 7457 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1106:
#line 7461 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1111:
#line 7473 "gram1.y"
    {
		PTR_LLND q;
   	        (yyval.bf_node) = make_endsections();
		q = set_ll_list((yyvsp[(4) - (4)].ll_node),LLNULL,EXPR_LIST);
                (yyval.bf_node)->entry.Template.ll_ptr1 = q;
                opt_kwd_ = NO;
	;}
    break;

  case 1112:
#line 7481 "gram1.y"
    {
   	        (yyval.bf_node) = make_endsections();
	        opt_kwd_ = NO; 
	;}
    break;

  case 1113:
#line 7487 "gram1.y"
    {
           (yyval.bf_node) = make_ompsection();
	;}
    break;

  case 1114:
#line 7493 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_DO_DIR,SMNULL,(yyvsp[(4) - (4)].ll_node),LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1115:
#line 7498 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1116:
#line 7504 "gram1.y"
    {
		PTR_LLND q;
		q = set_ll_list((yyvsp[(4) - (4)].ll_node),LLNULL,EXPR_LIST);
	        (yyval.bf_node) = get_bfnd(fi,OMP_END_DO_DIR,SMNULL,q,LLNULL,LLNULL);
      	        opt_kwd_ = NO;
	;}
    break;

  case 1117:
#line 7511 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_END_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1118:
#line 7517 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1119:
#line 7521 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1126:
#line 7535 "gram1.y"
    {
		/*$$ = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		$$->entry.string_val = (char *) "ORDERED";
		$$->type = global_string;*/
                (yyval.ll_node) = make_llnd(fi,OMP_ORDERED,LLNULL,LLNULL,SMNULL);
	;}
    break;

  case 1127:
#line 7544 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_SCHEDULE,(yyvsp[(4) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),SMNULL);
	;}
    break;

  case 1128:
#line 7548 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_SCHEDULE,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1129:
#line 7554 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "STATIC";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1130:
#line 7561 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "DYNAMIC";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1131:
#line 7568 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "GUIDED";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1132:
#line 7575 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "RUNTIME";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1133:
#line 7584 "gram1.y"
    {
  	   (yyval.bf_node) = make_single();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1134:
#line 7590 "gram1.y"
    {
  	   (yyval.bf_node) = make_single();
	   opt_kwd_ = NO;
	;}
    break;

  case 1135:
#line 7596 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1136:
#line 7600 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1139:
#line 7610 "gram1.y"
    {
  	   (yyval.bf_node) = make_endsingle();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1140:
#line 7616 "gram1.y"
    {
  	   (yyval.bf_node) = make_endsingle();
	   opt_kwd_ = NO;
	;}
    break;

  case 1141:
#line 7622 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1142:
#line 7626 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1145:
#line 7637 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_COPYPRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1146:
#line 7643 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_NOWAIT,LLNULL,LLNULL,SMNULL);
	;}
    break;

  case 1147:
#line 7649 "gram1.y"
    {
           (yyval.bf_node) = make_workshare();
	;}
    break;

  case 1148:
#line 7654 "gram1.y"
    {
		PTR_LLND q;
   	        (yyval.bf_node) = make_endworkshare();
		q = set_ll_list((yyvsp[(4) - (4)].ll_node),LLNULL,EXPR_LIST);
                (yyval.bf_node)->entry.Template.ll_ptr1 = q;
  	        opt_kwd_ = NO;
	;}
    break;

  case 1149:
#line 7662 "gram1.y"
    {
   	        (yyval.bf_node) = make_endworkshare();
                opt_kwd_ = NO;
	;}
    break;

  case 1150:
#line 7668 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_PARALLEL_DO_DIR,SMNULL,(yyvsp[(4) - (4)].ll_node),LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1151:
#line 7673 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_PARALLEL_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1152:
#line 7680 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1153:
#line 7684 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1165:
#line 7704 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_END_PARALLEL_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1166:
#line 7709 "gram1.y"
    {
           (yyval.bf_node) = make_parallelsections((yyvsp[(4) - (4)].ll_node));
	   opt_kwd_ = NO;
	;}
    break;

  case 1167:
#line 7714 "gram1.y"
    {
           (yyval.bf_node) = make_parallelsections(LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1168:
#line 7721 "gram1.y"
    {
           (yyval.bf_node) = make_endparallelsections();
	;}
    break;

  case 1169:
#line 7726 "gram1.y"
    {
           (yyval.bf_node) = make_parallelworkshare();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1170:
#line 7732 "gram1.y"
    {
           (yyval.bf_node) = make_parallelworkshare();
	   opt_kwd_ = NO;
	;}
    break;

  case 1171:
#line 7738 "gram1.y"
    {
           (yyval.bf_node) = make_endparallelworkshare();
	;}
    break;

  case 1172:
#line 7743 "gram1.y"
    { 
	   (yyval.bf_node) = get_bfnd(fi,OMP_THREADPRIVATE_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);
	;}
    break;

  case 1173:
#line 7748 "gram1.y"
    {
  	   (yyval.bf_node) = make_master();
	;}
    break;

  case 1174:
#line 7753 "gram1.y"
    {
  	   (yyval.bf_node) = make_endmaster();
	;}
    break;

  case 1175:
#line 7757 "gram1.y"
    {
  	   (yyval.bf_node) = make_ordered();
	;}
    break;

  case 1176:
#line 7762 "gram1.y"
    {
  	   (yyval.bf_node) = make_endordered();
	;}
    break;

  case 1177:
#line 7767 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_BARRIER_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1178:
#line 7771 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_ATOMIC_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1179:
#line 7776 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_FLUSH_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);
	;}
    break;

  case 1180:
#line 7780 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_FLUSH_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1181:
#line 7786 "gram1.y"
    {
  	   (yyval.bf_node) = make_critical();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	;}
    break;

  case 1182:
#line 7791 "gram1.y"
    {
  	   (yyval.bf_node) = make_critical();
	;}
    break;

  case 1183:
#line 7797 "gram1.y"
    {
  	   (yyval.bf_node) = make_endcritical();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	;}
    break;

  case 1184:
#line 7802 "gram1.y"
    {
  	   (yyval.bf_node) = make_endcritical();
	;}
    break;

  case 1185:
#line 7808 "gram1.y"
    { 
		PTR_SYMB s;
		PTR_LLND l;
		s = make_common((yyvsp[(2) - (5)].hash_entry)); 
		l = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
		(yyval.ll_node) = make_llnd(fi,OMP_THREADPRIVATE, l, LLNULL, SMNULL);
	;}
    break;

  case 1186:
#line 7818 "gram1.y"
    {
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1187:
#line 7822 "gram1.y"
    {	
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1188:
#line 7826 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);
	;}
    break;

  case 1189:
#line 7830 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);
	;}
    break;

  case 1190:
#line 7835 "gram1.y"
    {
		operator_slash = 1;
	;}
    break;

  case 1191:
#line 7838 "gram1.y"
    {
		operator_slash = 0;
	;}
    break;

  case 1199:
#line 7852 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_REGION_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1200:
#line 7856 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_CHECKSECTION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1201:
#line 7860 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_GET_ACTUAL_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1202:
#line 7862 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_GET_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1203:
#line 7864 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_GET_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1204:
#line 7868 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ACTUAL_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1205:
#line 7870 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1206:
#line 7872 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1207:
#line 7876 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 1208:
#line 7878 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 1209:
#line 7882 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1210:
#line 7884 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1211:
#line 7888 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1212:
#line 7891 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1213:
#line 7894 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1214:
#line 7899 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_INOUT_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1215:
#line 7901 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_IN_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1216:
#line 7903 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_OUT_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1217:
#line 7905 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_LOCAL_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1218:
#line 7907 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_INLOCAL_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1219:
#line 7911 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_TARGETS_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1220:
#line 7915 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_ASYNC_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1221:
#line 7920 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 1222:
#line 7924 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1223:
#line 7926 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1224:
#line 7930 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_HOST_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1225:
#line 7932 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_CUDA_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1226:
#line 7936 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_END_REGION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1227:
#line 7940 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_END_CHECKSECTION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1228:
#line 7944 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ROUTINE_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1229:
#line 7948 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 1230:
#line 7950 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1237:
#line 7962 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,SPF_ANALYSIS_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1238:
#line 7966 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,SPF_PARALLEL_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1239:
#line 7970 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,SPF_TRANSFORM_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1240:
#line 7974 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,SPF_PARALLEL_REG_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 1241:
#line 7976 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,SPF_PARALLEL_REG_DIR,(yyvsp[(3) - (10)].symbol),(yyvsp[(8) - (10)].ll_node),(yyvsp[(10) - (10)].ll_node),LLNULL);;}
    break;

  case 1242:
#line 7978 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,SPF_PARALLEL_REG_DIR,(yyvsp[(3) - (10)].symbol),(yyvsp[(10) - (10)].ll_node),(yyvsp[(8) - (10)].ll_node),LLNULL);;}
    break;

  case 1243:
#line 7982 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1244:
#line 7984 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1245:
#line 7988 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_CODE_COVERAGE_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1246:
#line 7992 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 1247:
#line 7994 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(5) - (6)].ll_node);;}
    break;

  case 1248:
#line 7998 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 1249:
#line 8000 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(5) - (6)].ll_node);;}
    break;

  case 1250:
#line 8004 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,SPF_END_PARALLEL_REG_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1251:
#line 8008 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1252:
#line 8010 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1256:
#line 8019 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 1257:
#line 8023 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_PRIVATE_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1258:
#line 8027 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_PARAMETER_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1259:
#line 8030 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 1260:
#line 8032 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 1261:
#line 8036 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, ASSGN_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL); ;}
    break;

  case 1262:
#line 8040 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1263:
#line 8042 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1267:
#line 8051 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1268:
#line 8055 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACROSS_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1269:
#line 8059 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,REMOTE_ACCESS_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1270:
#line 8063 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1271:
#line 8065 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1272:
#line 8069 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_NOINLINE_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1273:
#line 8071 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_FISSION_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1274:
#line 8073 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_EXPAND_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1275:
#line 8075 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_EXPAND_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1276:
#line 8078 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_SHRINK_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1277:
#line 8082 "gram1.y"
    { (yyval.symbol) = make_parallel_region((yyvsp[(1) - (1)].hash_entry));;}
    break;

  case 1278:
#line 8086 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 1279:
#line 8088 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 1280:
#line 8092 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,SPF_CHECKPOINT_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1281:
#line 8096 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1282:
#line 8098 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1283:
#line 8102 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_TYPE_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1284:
#line 8104 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_VARLIST_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1285:
#line 8106 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_EXCEPT_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1286:
#line 8108 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_FILES_COUNT_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1287:
#line 8110 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_INTERVAL_OP,(yyvsp[(4) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),SMNULL);;}
    break;

  case 1288:
#line 8114 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1289:
#line 8116 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1290:
#line 8120 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_ASYNC_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1291:
#line 8122 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_FLEXIBLE_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1292:
#line 8126 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_TIME_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1293:
#line 8128 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_ITER_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1294:
#line 8132 "gram1.y"
    { if(position==IN_OUTSIDE)
                 err("Misplaced SPF-directive",103);
             ;}
    break;


/* Line 1267 of yacc.c.  */
#line 14068 "gram1.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



