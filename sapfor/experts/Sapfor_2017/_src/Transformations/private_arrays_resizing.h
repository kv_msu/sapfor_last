#include "dvm.h"
#include "../GraphLoop/graph_loops.h"
#include <string>
#include <vector>
#include <set>


int privateArraysResizing(SgFile *file, std::vector<LoopGraph*> &loopGraphs, const std::set<SgSymbol*> &doForThisPrivates, std::vector<Messages> &messages, const std::map<std::pair<std::string, int>, std::set<SgStatement*>>& usersDirectives);
